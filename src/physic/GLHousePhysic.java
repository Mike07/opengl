package physic;

import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.buildings.GLHouse;
import falk_guenther_meier.model.math.GLLayer;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLRectangle;

/**
 * Haus mit GLStaticPlanePhysic-Objekten zur Begrenzung des Innenraums f�r "physikalische Experimente".
 * 
 * (Mindest-)Anforderungen:
 * - Darf sp�ter nicht mehr skaliert, rotiert oder verschoben werden!!
 * 
 * @author Johannes
 */
public class GLHousePhysic extends GLHouse {
	public GLHousePhysic(List<PhysicAnimation> ani, GLVertex center, float width, float height, float depth,
			float roofHeight) {
		super(center, width, height, depth, roofHeight);
		GLRectangle rect;
		float restitution = 1.0f;
		float friction = 0.1f;
		float variance = 0.01f;

		List<GLStaticPlanePhysic> list = new ArrayList<GLStaticPlanePhysic>(8);
		// unten
		rect = this.getFloor();
		float distance = - GLLayer.getDistanceRectanglePoint(rect, new GLVertex()) + variance;
		list.add(new GLStaticPlanePhysic(rect.getNormalOfPlane(), distance, 0.0f, restitution, friction));
		// links
		rect = this.getLeft().getRight().getRectRear();
		distance = - GLLayer.getDistanceRectanglePoint(rect, new GLVertex()) + variance;
		list.add(new GLStaticPlanePhysic(rect.getNormalOfPlane(), distance, 0.0f, restitution, friction));
		// rechts
		rect = this.getRight().getRight().getRectRear();
		distance = - GLLayer.getDistanceRectanglePoint(rect, new GLVertex()) + variance;
		list.add(new GLStaticPlanePhysic(rect.getNormalOfPlane(), distance, 0.0f, restitution, friction));
		// vorne
		rect = this.getFront().getRight().getRectRear();
		distance = - GLLayer.getDistanceRectanglePoint(rect, new GLVertex()) + variance;
		list.add(new GLStaticPlanePhysic(rect.getNormalOfPlane(), distance, 0.0f, restitution, friction));
		// hinten
		rect = this.getBack().getRight().getRectRear();
		distance = - GLLayer.getDistanceRectanglePoint(rect, new GLVertex()) + variance;
		list.add(new GLStaticPlanePhysic(rect.getNormalOfPlane(), distance, 0.0f, restitution, friction));
		// Dach links
		rect = this.getRoofLeft().getRectRear();
		distance = - GLLayer.getDistanceRectanglePoint(rect, new GLVertex()) + variance;
		list.add(new GLStaticPlanePhysic(rect.getNormalOfPlane(), distance, 0.0f, restitution, friction));
		// Dach rechts
		rect = this.getRoofRight().getRectRear();
		distance = - GLLayer.getDistanceRectanglePoint(rect, new GLVertex()) + variance;
		list.add(new GLStaticPlanePhysic(rect.getNormalOfPlane(), distance, 0.0f, restitution, friction));

		for (PhysicAnimation a : ani) {
			for (GLStaticPlanePhysic p : list) {
				a.addPhysicable(p);
			}
		}
	}
}
