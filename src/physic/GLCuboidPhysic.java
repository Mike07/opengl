package physic;

import javax.media.opengl.GL2;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;

import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.observers.AnimationStatusObservable;
import falk_guenther_meier.model.observers.AnimationStatusObserver;

/**
 * Quader mit Physik-Anbindung.
 * Anforderungen:
 * - darf nicht verschoben, rotiert oder scaliert werden!
 * 
 * @author Johannes
 */
public class GLCuboidPhysic extends GLCuboid implements PhysicableObject, AnimationStatusObserver {
	private float mass;
	private float restitution;
	private float friction;
	private RigidBody body;
	private GLVertex initialPosition;
	private GLVertex initialLinear;
	private GLVertex initialAngle;

	public GLCuboidPhysic(GLVertex center, float width, float height, float depth,
			float mass, float restitution, float friction) {
		super(center, width, height, depth);

		this.initialPosition = center.clone();

		this.mass = mass;
		this.restitution = restitution;
		this.friction = friction;

		Transform startTransform = new Transform();
		startTransform.setIdentity();
		Vector3f pos = new Vector3f(center.getX(), center.getY(), center.getZ());
		startTransform.origin.set(pos);

		CollisionShape shape = new BoxShape(new Vector3f(width / 2.0f, height / 2.0f,
				depth / 2.0f));

		Vector3f localInertia = new Vector3f(0f, 0f, 0f);
		if (this.isDynamic()) {
			shape.calculateLocalInertia(this.mass, localInertia);
		}

		// using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects

		DefaultMotionState myMotionState = new DefaultMotionState(startTransform);
		RigidBodyConstructionInfo cInfo = new RigidBodyConstructionInfo(this.mass, myMotionState,
				shape, localInertia);

		cInfo.restitution = restitution; // Sto�zahl, Restitutionskoeffizient:  0: vollkommen plastischer Sto� (Bewegungsenergie nimmt ab, z.B. wegen W�rme), 1: vollkommen elastischer Sto� (Bewegungsenergie bleibt konstant)
		cInfo.friction = friction; // Reibung

		this.body = new RigidBody(cInfo);

		
		Transform worldTrans = body.getWorldTransform(new Transform());
		worldTrans.origin.set(pos);
		worldTrans.setRotation(new Quat4f(0f, 0f, 0f, 1f));
		body.setWorldTransform(worldTrans);

//		body.setCcdMotionThreshold(1f);
//		body.setCcdSweptSphereRadius(0.2f);

//		this.setInitialVelocity(new GLVertex(), new GLVertex());

		this.setPosition(new GLVertex()); // wichtig!!
		this.setReferencePoint(this.position);
	}

	public GLCuboidPhysic(GLVertex center, float width, float height, float depth, float mass) {
		this(center, width, height, depth, mass, 0.0f, 0.5f);
	}

	public GLCuboidPhysic(GLVertex center, float width, float height, float depth) {
		this(center, width, height, depth, 1.0f);
	}

	@Override
	public void beginGL(GL2 gl) {
		Transform trans = new Transform();
		body.getMotionState().getWorldTransform(trans);

		Matrix4f matrix = new Matrix4f();
		trans.getMatrix(matrix);
		GLMatrix mat = PhysicUtility.convertMatrix(trans.basis);

		this.glTransformNow(mat);
		this.glTranslateNow(trans.origin.x, trans.origin.y, trans.origin.z);

		super.beginGL(gl);
	}

	@Override
	public final GLVertex getLinearVelocity() {
		Vector3f v = new Vector3f();
		v = this.body.getLinearVelocity(v);
		return new GLVertex(v.x, v.y, v.z);
	}

	@Override
	public final void setLinearVelocity(GLVertex linearVelocity) {
		if (this.initialLinear == null) {
			this.initialLinear = linearVelocity;
		}
		this.body.setLinearVelocity(new Vector3f(linearVelocity.getX(), linearVelocity.getY(),
				linearVelocity.getZ()));
	}

	@Override
	public final GLVertex getAngularVelocity() {
		Vector3f v = new Vector3f();
		v = this.body.getAngularVelocity(v);
		return new GLVertex(v.x, v.y, v.z);
	}

	@Override
	public final void setAngularVelocity(GLVertex angularVelocity) {
		if (this.initialAngle == null) {
			this.initialAngle = angularVelocity;
		}
		this.body.setAngularVelocity(new Vector3f(angularVelocity.getX(), angularVelocity.getY(),
				angularVelocity.getZ()));
	}

	@Override
	public final float getMass() {
		return mass;
	}

	@Override
	public final boolean isDynamic() {
		return this.mass != 0.0f;
	}

	@Override
	public final float getRestitution() {
		return restitution;
	}

	@Override
	public final float getFriction() {
		return friction;
	}

	@Override
	public final RigidBody getRigidBody() {
		return body;
	}

	@Override
	public void updateStartOfAnimation(AnimationStatusObservable changed) {
		this.setPhysicPosition(this.initialPosition);
		if (this.initialLinear == null) {
			this.initialLinear = new GLVertex();
		}
		this.setLinearVelocity(this.initialLinear);
		if (this.initialAngle == null) {
			this.initialAngle = new GLVertex();
		}
		this.setAngularVelocity(this.initialAngle);
	}

	@Override
	public void updateEndOfAnimation(AnimationStatusObservable changed) {
		// bleibt leer
	}

	@Override
	public void updatePause(AnimationStatusObservable changed) {
		// bleibt leer
	}

	@Override
	public void updateResume(AnimationStatusObservable changed) {
		// bleibt leer
	}

	/**
	 * Setzt die Position des physikalischen K�rpers in der Physik-Welt.
	 * @param position
	 */
	public void setPhysicPosition(GLVertex position) {
		Transform startTransform = new Transform();
		startTransform.setIdentity();
		Vector3f pos = new Vector3f(position.getX(), position.getY(), position.getZ());
		startTransform.origin.set(pos);

		DefaultMotionState myMotionState = new DefaultMotionState(startTransform);

		this.body.setMotionState(myMotionState);		
	}

	/**
	 * Setzt die initialen Geschwindigkeiten / Richtungen von Position und Drehung.
	 * @param linear
	 * @param angular
	 */
	public void setInitialVelocity(GLVertex linear, GLVertex angular) {
		this.initialLinear = linear;
		this.initialAngle = angular;
		this.setLinearVelocity(linear);
		this.setAngularVelocity(angular);
	}
}
