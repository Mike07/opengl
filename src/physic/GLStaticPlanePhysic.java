package physic;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.StaticPlaneShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.observers.AnimationStatusObservable;

/**
 * Unendliche gro�e Ebene (nicht zum Zeichnen!). mass != 0 macht eigentlich keinen Sinn.
 * Kann zum Beispiel zur Begrenzung eines physikalischen Raumes benutzt werden.
 * @author Michael
 */
public class GLStaticPlanePhysic implements PhysicableObject {
	private float mass;
	private float restitution;
	private float friction;
	private RigidBody body;
	
	/**
	 * Erzeugt eine Ebene mit phys. Verhalten. planeConstant verschiebt die Ebene abh�ngig von der Normalen.
	 * @param normal Normale der Ebene
	 * @param planeConstant Verschiebung vom Ursprung aus
	 * @param mass
	 * @param restitution
	 * @param friction
	 */
	public GLStaticPlanePhysic(GLVertex normal, float planeConstant, float mass, float restitution, float friction) {
		this.mass = mass;
		this.restitution = restitution;
		this.friction = friction;

		Transform startTransform = new Transform();
		startTransform.setIdentity();
		Vector3f pos = new Vector3f(0, 0, 0);
		startTransform.origin.set(pos);

		CollisionShape shape = new StaticPlaneShape(new Vector3f(normal.getValues()), planeConstant);

		Vector3f localInertia = new Vector3f(0f, 0f, 0f);
		if (this.isDynamic()) {
			shape.calculateLocalInertia(this.mass, localInertia);
		}

		// using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects

		DefaultMotionState myMotionState = new DefaultMotionState(startTransform);		
		RigidBodyConstructionInfo cInfo = new RigidBodyConstructionInfo(this.mass, myMotionState,
				shape, localInertia);

		cInfo.restitution = restitution; // Sto�zahl, Restitutionskoeffizient:  0: vollkommen plastischer Sto� (Bewegungsenergie nimmt ab, z.B. wegen W�rme), 1: vollkommen elastischer Sto� (Bewegungsenergie bleibt konstant)
		cInfo.friction = friction; // Reibung

		this.body = new RigidBody(cInfo);

		
		Transform worldTrans = body.getWorldTransform(new Transform());
		worldTrans.origin.set(pos);
		worldTrans.setRotation(new Quat4f(0f, 0f, 0f, 1f));
		body.setWorldTransform(worldTrans);

//		body.setCcdMotionThreshold(1f);
//		body.setCcdSweptSphereRadius(0.2f);		
	}

	public GLStaticPlanePhysic(GLVertex normal, float planeConstant, float mass) {
		this(normal, planeConstant, mass, 1f, 0.6f);
	}

	public GLStaticPlanePhysic(GLVertex normal, float planeConstant) {
		this(normal, planeConstant, 0.0f);
	}

	@Override
	public final GLVertex getLinearVelocity() {
		Vector3f v = new Vector3f();
		v = this.body.getLinearVelocity(v);
		return new GLVertex(v.x, v.y, v.z);
	}

	@Override
	public final void setLinearVelocity(GLVertex linearVelocity) {
		this.body.setLinearVelocity(new Vector3f(linearVelocity.getX(), linearVelocity.getY(),
				linearVelocity.getZ()));
	}

	@Override
	public final GLVertex getAngularVelocity() {
		Vector3f v = new Vector3f();
		v = this.body.getAngularVelocity(v);
		return new GLVertex(v.x, v.y, v.z);
	}

	@Override
	public final void setAngularVelocity(GLVertex angularVelocity) {
		this.body.setAngularVelocity(new Vector3f(angularVelocity.getX(), angularVelocity.getY(),
				angularVelocity.getZ()));
	}

	@Override
	public final float getMass() {
		return mass;
	}

	@Override
	public final boolean isDynamic() {
		return this.mass != 0.0f;
	}

	@Override
	public final float getRestitution() {
		return restitution;
	}

	@Override
	public final float getFriction() {
		return friction;
	}

	@Override
	public final RigidBody getRigidBody() {
		return body;
	}

	@Override
	public void updateStartOfAnimation(AnimationStatusObservable changed) {
		// bleibt leer
	}

	@Override
	public void updateEndOfAnimation(AnimationStatusObservable changed) {
		// bleibt leer
	}

	@Override
	public void updatePause(AnimationStatusObservable changed) {
		// bleibt leer
	}

	@Override
	public void updateResume(AnimationStatusObservable changed) {
		// bleibt leer
	}
}
