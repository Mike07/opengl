package physic;

import com.bulletphysics.dynamics.RigidBody;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.observers.AnimationStatusObserver;

/**
 * Schnittstelle f�r alle Physik-Objekte mit den relevanten Eigenschaften.
 * @author Johannes
 */
public interface PhysicableObject extends AnimationStatusObserver {
	public float getMass();
	public boolean isDynamic();
	public float getRestitution();
	public float getFriction();
	public GLVertex getLinearVelocity();
	public void setLinearVelocity(GLVertex linearVelocity);
	public GLVertex getAngularVelocity();
	public void setAngularVelocity(GLVertex angularVelocity);
	public RigidBody getRigidBody();
}
