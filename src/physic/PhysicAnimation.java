package physic;

import javax.vecmath.Vector3f;

import com.bulletphysics.collision.broadphase.AxisSweep3;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;

import falk_guenther_meier.model.animation.AbstractAnimation;
import falk_guenther_meier.model.time.TimeComponent;

/**
 * Enth�lt die Simulation einer physikalischen Welt mit JBullet.
 * @author Johannes
 */
public class PhysicAnimation extends AbstractAnimation {
	private DiscreteDynamicsWorld dynamicsWorld;

	public PhysicAnimation(TimeComponent time) {
		super(time);
		this.init();
	}

	private void init() {
		CollisionConfiguration collisionConfiguration = new DefaultCollisionConfiguration();
		CollisionDispatcher dispatcher = new CollisionDispatcher(collisionConfiguration);

		// the maximum size of the collision world. Make sure objects stay
		// within these boundaries
		// Don't make the world AABB size too large, it will harm simulation
		// quality and performance
		Vector3f worldAabbMin = new Vector3f(-10000, -10000, -10000);
		Vector3f worldAabbMax = new Vector3f(10000, 10000, 10000);
		int maxProxies = 1024;
		AxisSweep3 overlappingPairCache = new AxisSweep3(worldAabbMin, worldAabbMax, maxProxies);

		SequentialImpulseConstraintSolver solver = new SequentialImpulseConstraintSolver();

		this.dynamicsWorld = new DiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver,
				collisionConfiguration);
		this.dynamicsWorld.setGravity(new Vector3f(0, -9.81f, 0));
	}

	@Override
	public void doAnimation() {
		// simple dynamics world doesn't handle fixed-time-stepping
		// step the simulation
		if (dynamicsWorld != null) {
			dynamicsWorld.stepSimulation(this.getTime()); // Angabe in Sekunden
			// optional but useful: debug drawing
			dynamicsWorld.debugDrawWorld();
		}
	}

	public final void addPhysicable(PhysicableObject obj) {
		this.dynamicsWorld.addRigidBody(obj.getRigidBody());
		this.addAnimationStatusObserver(obj);
	}
}
