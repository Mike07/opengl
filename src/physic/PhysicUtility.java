package physic;

import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;

import falk_guenther_meier.model.math.GLMatrix;

/**
 * Enth�lt hilfreiche Methoden f�r die Physik-Engine-Anbindung.
 */
public abstract class PhysicUtility {
	public static GLMatrix convertMatrix(Matrix4f mat) {
			GLMatrix result = new GLMatrix();
			result.set(mat.m00, mat.m10, mat.m20, mat.m30, mat.m01, mat.m11, mat.m21, mat.m31, mat.m02, mat.m12, mat.m22, mat.m32, mat.m03, mat.m13, mat.m23, mat.m33);
	//		result.set(mat.m00, mat.m01, mat.m02, mat.m03, mat.m10, mat.m11, mat.m12, mat.m13, mat.m20, mat.m21, mat.m22, mat.m23, mat.m30, mat.m31, mat.m32, mat.m33);
			return result;
		}

	public static GLMatrix convertMatrix(Matrix3f mat) {
			GLMatrix result = new GLMatrix();
			result.set(mat.m00, mat.m10, mat.m20, 0f, mat.m01, mat.m11, mat.m21, 0f, mat.m02, mat.m12, mat.m22, 0f, 0f, 0f, 0f, 1f);
	//		result.set(mat.m00, mat.m01, mat.m02, 0f, mat.m10, mat.m11, mat.m12, 0f, mat.m20, mat.m21, mat.m22, 0f, 0f, 0f, 0f, 1f);
			return result;
	}
}
