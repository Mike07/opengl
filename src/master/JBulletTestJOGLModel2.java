package master;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import physic.GLCuboidPhysic;
import physic.PhysicAnimation;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.Utility;

public class JBulletTestJOGLModel2 extends JoglApp {	
	private GLCuboidPhysic ground;
	private PhysicAnimation ani;
	private List<GLCuboidPhysic> objects = new LinkedList<GLCuboidPhysic>();
	private ShaderProgram shader;
	private GLPointLight pointLight;
	
	public JBulletTestJOGLModel2(String name_value, int width, int height) {
		super(name_value, width, height);
	}

	public static void main(String[] args) {
		new JBulletTestJOGLModel2("JBullet", 800, 600);		
	}
	
	private void initPhysics() {
		this.ani = new PhysicAnimation(this.getKonfiguration().getTime());
		this.addAnimation(ani);

		this.ground = new GLCuboidPhysic(new GLVertex(0.0f, -10.0f, 0.0f), 30f, 2f, 30f, 0.0f);
		ground.setColor(new GLVertex(0.4f, 0.3f, 0.2f));
		this.ani.addPhysicable(this.ground);
		
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				GLCuboidPhysic box = new GLCuboidPhysic(new GLVertex(2.0f, 10.0f, 0.0f), 1f, 1f, 1f, 1.0f, 1.0f, 0.6f);
				box.setColor(Utility.getRandomColor());
				ani.addPhysicable(box);
				synchronized(objects) {
					objects.add(box);
				}
			}
		}, new Date(1), 400);
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		this.getKonfiguration().getCamera().setPosition(new GLVertex(17, 0, 17));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(-15, -3, -15));
		
		pointLight = new GLPointLight(GL2.GL_LIGHT7, new GLVertex(5f, 7f, 0f)); // new GLVertex(-2f, 5f, 3f)
		pointLight.setUseColorMaterial(true);
		pointLight.setAmbient(new GLVertex(0.7f, 0.7f, 0.7f, 1.0f));
		pointLight.setDiffuse(new GLVertex(0.8f, 0.8f, 0.8f, 1.0f));
		pointLight.setSpecular(new GLVertex(0.8f, 0.8f, 0.8f, 1.0f));
		pointLight.setAttenuation(1.0f, 0.00f, 0.001f);
		
		GLCuboid lightCube = new GLCuboid(new GLVertex(0f, 0f, 0f), 0.5f);
		lightCube.setColor(new GLVertex(1f, 1f, 1f));
		pointLight.setLightObject(lightCube);
		
		this.addLight(pointLight);
		
		shader = new ShaderProgram("vertexPhong.glsl", "fragmentPhong.glsl");
		shader.compileShaders(this.getClass(), drawable.getGL().getGL2());

		this.initPhysics();
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		
		shader.useProgram(gl);
		ground.draw(gl);
		synchronized(objects) {
			for (GLCuboidPhysic obj : objects) {
				obj.draw(gl);
			}
		}
		ShaderProgram.clearShaderProgram(gl);
	}
}
