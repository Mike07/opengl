package hanno;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLTriangle;
import falk_guenther_meier.model.objects.GLVertexBufferObject;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.time.TimeComponent;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.ObjLoader;

public class Plane extends GLComplexObject {
	
	private TimeComponent timeComponent;
	private GLTriangle currentOrientation;
	GLVertexBufferObject propellerVBO;
	
	public Plane(TimeComponent timeComponent) {
		super();
		this.timeComponent = timeComponent;
		this.position = new GLVertex();
		
		initOrientation();

		
		/*GLCuboid body = new GLCuboid(new GLVertex(), 5.5f, 2f, 2f);
		objects.add(body);
		GLCuboid wings = new GLCuboid(new GLVertex(), 3f, 0.3f, 9f);
		objects.add(wings);
		
		propeller = new GLCustomComplexObject();
		propeller.add(new GLCuboid(new GLVertex(2.85f, 0f, 0f), 0.2f, 4f, 0.5f));
		propeller.add(new GLCuboid(new GLVertex(2.85f, 0f, 0f), 0.2f, 0.5f, 4f));
		objects.add(propeller);
		
		this.setColor(new GLVertex(0.4f,0.4f,0.4f));
		propeller.setColor(new GLVertex(0.7f,0.5f,0.4f));
		//this.setMaterial(GLMaterial.EMERALD);*/
		
		
		// load plane  // http://thefree3dmodels.com/stuff/aircraft/russian_ww_2_p_39_airacobra/15-1-0-4021
		GLCustomComplexObject complexObj = ObjLoader.objFile2ComplexObj(FileLoader.getFileFromFilesFolder("obj/airacobra/p392.obj", true));
		
		// extract propeller
		GLCustomComplexObject propeller = new GLCustomComplexObject();
		for(GLObject subObject: complexObj.getObjects()) {
			GLComplexObject subComplexObj = (GLComplexObject)subObject;
			for (GLObject object: subComplexObj.getObjects()) {
				GLVertex[] objNodes = object.getNodes();
				float centerZ = (objNodes[0].getZ() + objNodes[1].getZ() + objNodes[2].getZ()) / 3f;
				if (object instanceof GLTriangle && centerZ < -2.65f) {
					propeller.add(object);
				}
			}
			subComplexObj.getObjects().removeAll(propeller.getObjects());
			propeller.setTexture(subObject.getTextureBindings().get(0).getTexture());
		}
		
		GLComplexObject bodyVBOs = GLVertexBufferObject.convert2VBOs(complexObj);
		propellerVBO = new GLVertexBufferObject(GL2.GL_TRIANGLES, propeller);
		
		this.add(bodyVBOs);
		this.add(propellerVBO);

	}
	
	private void initOrientation() {
		GLVertex currentPosition = new GLVertex();
		GLVertex currentDirection = new GLVertex(0f,0f,-1f);
		GLVertex currentUp = new GLVertex(0f, 1f, 0f);
		this.currentOrientation = new GLTriangle(currentPosition, GLVertex.add(currentPosition, currentDirection), GLVertex.add(currentPosition, currentUp), true);	
	}
	
	public void beginGL(GL2 gl) {
		this.glScaleNow(0.5f, 0.5f, 0.5f);
		propellerVBO.glRotateNow(0f, 0f, timeComponent.getTime()*500f);
		
		initOrientation();
		
		this.glRotateNow(0f, 0f, -5f); // Flugzeug schr�g stellen
		currentOrientation.rotate(0f, 0f, -5f);
		currentOrientation.rotate(0f, -30f, 0f); // etwas mehr zur Insel schauen
		
		this.glTranslateNow(-10f, 5f, 0f);
		currentOrientation.multMatrix(GLMatrix.createTranslateMatrix(-13f, 6f, 4f)); // Beobachter-Perspektive
		
		this.glRotateNow(0f, -timeComponent.getTime()*5f, 0f);
		currentOrientation.rotate(0f, -timeComponent.getTime()*5f, 0f);
		
		//currentOrientation.draw(gl);
		super.beginGL(gl);
	}

	public GLVertex getCurrentPosition() {
		return currentOrientation.getFirst();
	}

	public GLVertex getCurrentDirection() {
		return GLVertex.sub(currentOrientation.getSecond(), currentOrientation.getFirst());
	}
	
	public GLVertex getCurrentUp() {
		return GLVertex.sub(currentOrientation.getThird(), currentOrientation.getFirst());
	}

}
