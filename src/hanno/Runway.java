package hanno;

import java.io.File;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.util.FileLoader;

public class Runway extends GLRectangle {
	
	private ShaderProgram runwayShader;
	
	public Runway() {
		super(new GLVertex(), 11, 1);
		
		File texFileRunway = FileLoader.getFileFromFilesFolder("texture/bumpmapping/texture_4.png", true);
		File texFileRunwayNormal = FileLoader.getFileFromFilesFolder("texture/bumpmapping/normal_4.png", true);
		
		
		this.rotate(-90, 0, 0);
		this.setPosition(new GLVertex(-2.7f, 1.34f, -8f));
		this.setColor(new GLVertex(1,1,1));
		
		GLTexture2D texRunway = new GLTexture2D(texFileRunway);
		texRunway.setGLTextureWrap(GL2.GL_REPEAT);
		this.setTexture(texRunway, 0);
		
		GLTexture2D texRunwayNormal = new GLTexture2D(texFileRunwayNormal);
		texRunwayNormal.setGLTextureWrap(GL2.GL_REPEAT);		
		this.setTexture(texRunwayNormal, 1);
		
		
		
	}
	
	
	public void init(GL2 gl) {
		runwayShader = new ShaderProgram("runwayBumpVertex.glsl", "runwayBumpFragment.glsl");
		runwayShader.compileShaders(this.getClass(), gl);
		
		this.setShader(runwayShader);
	}
	
	
	public void beginGL(GL2 gl) {
		runwayShader.useProgram(gl);
		runwayShader.setTextureUnit(gl, "texRender", 0);
		runwayShader.setTextureUnit(gl, "texNormal", 1);
		super.beginGL(gl);
	}

}
