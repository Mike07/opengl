package hanno;

import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.time.TimeComponent;

public class PlaneCamera {
	
	private boolean isFollowing;
	private Plane plane;
	private GLCamera camera;
	private TimeComponent timeComponent;
	
	private float startTime, endTime;
	private GLVertex startPos;
	private GLVertex startDir;
	

	public PlaneCamera(Plane plane, GLCamera camera, TimeComponent timeComponent) {
		this.isFollowing = false;
		this.plane = plane;
		this.camera = camera;
		this.timeComponent = timeComponent;
		
		// TODO Auto-generated constructor stub
	}
	
	public void switchFollowMode() {
		if (!isFollowing) {
			isFollowing = true;
			startTime = timeComponent.getTime();
			endTime = startTime + 10f; // Dauer in Sekunden
			startPos = camera.getPosition();
			startDir = camera.getDirection();
		} else {
			isFollowing = false;
		}
	}
	
	public void update() {
		if (isFollowing) {
			GLVertex pos = World_Utility.getSinusInterpolation(startPos, plane.getCurrentPosition(), startTime, endTime, timeComponent.getTime());
			GLVertex dir = World_Utility.getSinusInterpolation(startDir, plane.getCurrentDirection(), startTime, endTime, timeComponent.getTime());
			
			if (!dir.equals(new GLVertex(0f,0f,0f))) {
				camera.setPosition(pos);
				camera.setDirection(dir);
			}	
			
		}
	}
	

}
