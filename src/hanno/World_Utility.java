package hanno;

import java.awt.Font;
import java.io.File;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLSkyBox;
import falk_guenther_meier.model.textures.GLTextureCube;
import falk_guenther_meier.model.world.Konfiguration;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.SkyBoxLoader;

public class World_Utility {
	
	public static Konfiguration getWorldKonfiguration() {
		GLCamera camera = new GLCamera();
		
		camera.setPosition(new GLVertex(10,10,10));
		camera.setDirection(new GLVertex(-1,-0.5f,-1));
		//camera.setSpeed(4);
		
		/*GLVertex position = new GLVertex(0.0f, 1.0f, 1.0f);
		GLVertex ambient = new GLVertex(0.5f, 0.5f, 0.5f);
		GLVertex diffuse = new GLVertex(0.5f, 0.5f, 0.5f);
		GLVertex specular = new GLVertex(0.5f, 0.5f, 0.5f);*/
		
		GLVertex position = new GLVertex(0f, 20f, 100f, 1.0f);
		GLVertex ambient = new GLVertex(0.45f, 0.45f, 0.45f, 1.0f);
		GLVertex diffuse = new GLVertex(0.5f, 0.5f, 0.5f, 1.0f);
		GLVertex specular = new GLVertex(0.2f, 0.2f, 0.2f, 1.0f);
		GLPointLight light = new GLPointLight(GL2.GL_LIGHT7, position, ambient, diffuse, specular, true);
		
		GLSkyBox skybox = SkyBoxLoader.getLostAtSeaSkyBox(camera, 1000);
		
		Font textFont = new Font("SansSerif", Font.BOLD, 20);
		
		return new Konfiguration(camera, light, skybox, textFont, false, true, true);
	}
	
	
	public static GLTextureCube getLostAtSeaTextureCube() {
		File texFile_right = FileLoader.getFileFromFilesFolder("cubemap/lostatseaday/lostatseaday_right.jpg", true);
		File texFile_left = FileLoader.getFileFromFilesFolder("cubemap/lostatseaday/lostatseaday_left.jpg", true);
		File texFile_top = FileLoader.getFileFromFilesFolder("cubemap/lostatseaday/lostatseaday_top.jpg", true);
		File texFile_down = FileLoader.getFileFromFilesFolder("cubemap/lostatseaday/lostatseaday_down.jpg", true);
		File texFile_front = FileLoader.getFileFromFilesFolder("cubemap/lostatseaday/lostatseaday_front.jpg", true);
		File texFile_back = FileLoader.getFileFromFilesFolder("cubemap/lostatseaday/lostatseaday_back.jpg", true);
		
		return new GLTextureCube(new File[]{texFile_right, texFile_left, texFile_top, texFile_down, texFile_front, texFile_back});
	}
	
	/**
	 * Analog zu smoothstep im Shader
	 * @param start
	 */
	public static float smoothstep(float start, float end, float current) {
		if (current < start)
			return 0f;
		if (current > end)
			return 1f;
		return (current-start)/(end-start);
	}
	
	/**
	 * Analog zu mix im Shader
	 * @param pV2 percentVertex2 =  1-percentVertex1
	 */
	public static GLVertex mix(GLVertex vertex1, GLVertex vertex2, float pV2) {
		return GLVertex.add(GLVertex.mult(vertex2, pV2),GLVertex.mult(vertex1, 1-pV2));
	}
	
	/**
	 * f�r ruhige Animationen
	 * wirkt zun�chst beschleunigend, gegen ende abbremsend auf das Ergebnis
	 */
	public static GLVertex getSinusInterpolation(GLVertex startPos, GLVertex endPos, float startTime, float endTime, float time) {
		float smoothValue = smoothstep(startTime, endTime, time);
		smoothValue = ((float)Math.sin((double)smoothValue*Math.PI - Math.PI / 2.0) + 1f) /2f;
		return mix(startPos, endPos, smoothValue);
	}


}
