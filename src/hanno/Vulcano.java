package hanno;

import java.io.File;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.objects.GLTriangleStrip;
import falk_guenther_meier.model.objects.GLVertexBufferObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.util.FileLoader;

public class Vulcano {
	
	private GLVertexBufferObject heightMapVBO;
	private ShaderProgram heightMapShader;
	private GLTriangleStrip heightMap;
	
	public Vulcano() {
		GLRectangle basisRect = new GLRectangle(new GLVertex(-1, 0, 1), new GLVertex(1, 0, 1), new GLVertex(1, 0, -1), new GLVertex(-1, 0, -1), true);
		heightMap = new GLTriangleStrip(basisRect,50);
		
		File texFileMountain = FileLoader.getFileFromFilesFolder("texture/Dirt_rocky.jpg", true);
		File texFileLand = FileLoader.getFileFromFilesFolder("texture/gras_small.jpg", true);
		File texFileLowLand = FileLoader.getFileFromFilesFolder("texture/wand_mit_riss_small.JPG", true);
		
		heightMap.setColor(new GLVertex(1,1,1));
		
		GLTexture2D texHeightMap = new GLTexture2D(getHeightMapFile());
		heightMap.setTexture(texHeightMap);
			
		GLTexture2D texMountain = new GLTexture2D(texFileMountain);
		texMountain.setGLTextureWrap(GL2.GL_REPEAT);
		heightMap.setTexture(texMountain, 2);
		
		GLTexture2D texLand = new GLTexture2D(texFileLand);
		texLand.setGLTextureWrap(GL2.GL_REPEAT);
		heightMap.setTexture(texLand, 3);
		
		GLTexture2D texLowLand = new GLTexture2D(texFileLowLand);
		texLowLand.setGLTextureWrap(GL2.GL_REPEAT);
		heightMap.setTexture(texLowLand, 4);
		
		heightMapVBO = new GLVertexBufferObject(heightMap);
	}
	
	public static File getHeightMapFile(){
		return FileLoader.getFileFromFilesFolder("heightmap/Vulkan03.png", true);
	}
	
	public void init(GL2 gl) {
		heightMapShader = new ShaderProgram("heightMapVertex.glsl", "heightMapFragment.glsl");
		heightMapShader.compileShaders(this.getClass(), gl);
	}
	
	
	
	public void draw(GL2 gl) {
		heightMapShader.useProgram(gl);
		heightMapShader.setTextureUnit(gl, "texHeightMap", 0);
		heightMapShader.setTextureUnit(gl, "texMountain", 2);
		heightMapShader.setTextureUnit(gl, "texLand", 3);
		heightMapShader.setTextureUnit(gl, "texLowLand", 4);
		
		heightMapVBO.glScaleNow(15, 5, 15);
		heightMapVBO.draw(gl);
		
	}
	
	
	

}
