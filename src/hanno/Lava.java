package hanno;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.particle.Particle;
import falk_guenther_meier.model.textures.GLTexGrid;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.time.TimeComponent;
import falk_guenther_meier.util.FileLoader;

public class Lava {
	
	private List<Particle> particles;
	private GLTexture2D explosionTexture;
	private TimeComponent timeComponent;
	
	public Lava(TimeComponent timeComponent) {
		this.timeComponent = timeComponent;
		int count = 5;
		this.particles = new ArrayList<Particle>(count);
		for (int i = 0; i < count; i++) {
			this.particles.add(this.createParticle());
		}
		File explosionTextureFile = FileLoader.getFileFromFilesFolder("texture/explosion2.png");
		explosionTexture = new GLTexture2D(explosionTextureFile);
		explosionTexture.setTexGrid(new GLTexGrid(4,4));
		
	}

	private Particle createParticle() {
		return new Particle(1.0f, 0.01f,
				new GLVertex(0.6f, 2.0f, 2.3f),
				new GLVertex(0.001f - (float) Math.random() / 50.0f,
						0.1f - (float) Math.random() / 10.0f, 0.001f - (float) Math.random() / 50.0f));
	}

	public void draw(GL2 gl) {
		
		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glDepthMask(false);
		
		for (int i = 0; i < particles.size(); i++) {
			// alte Partikel durch neue ersetzen
			if (this.particles.get(i).getPosition().getY() < 0.0f) {
				this.particles.set(i, this.createParticle());
			}
			// Partikel fortbewegen
			Particle p = this.particles.get(i);
			p.setSpeed(new GLVertex(p.getSpeed().getX(), p.getSpeed().getY() - 0.001f, p.getSpeed().getZ()));
			p.evolve();
			// Partikel zeichnen
			
			float picsPerSecond = 4;
			float time = timeComponent.getTime();
			int texCols = explosionTexture.getTexGrid().getColCount();
			int texRows = explosionTexture.getTexGrid().getRowCount();
			int texNr = Math.round(time * picsPerSecond + i) % (texCols*texRows);
			int texCol = texNr % texCols;
			int texRow = texNr / texRows;
			GLRectangle rect = new GLRectangle(p.getPosition(), 1f, 1f);
			rect.rotate(0f, time*20f, 0f);
			rect.setTexture(explosionTexture);
			rect.setTexRect(texCol, texRow);
			rect.draw(gl);
		}
		
	
		gl.glDepthMask(true);
		gl.glDisable(GL2.GL_BLEND);
	}

}
