varying vec3 position;
varying mat3 mTBN;
attribute vec3 tangente;

void main() {	
	vec4 position4 = gl_ModelViewMatrix * gl_Vertex;
	position = position4.xyz / position4.w;

	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

	gl_TexCoord[0] = gl_MultiTexCoord0;

	vec3 normal = gl_Normal;
	vec3 tang = normalize(tangente);
	vec3 binorm = normalize(cross(tang,normal));

	mTBN[0] = tang;
	mTBN[1] = binorm;
	mTBN[2] = normal;
	mTBN = gl_NormalMatrix * mTBN;
}