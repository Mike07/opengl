uniform sampler2D texHeightMap;
uniform sampler2D texMountain;
uniform sampler2D texLand;
uniform sampler2D texLowLand;

varying vec3 position;

vec4 ambientColor(int nr, vec4 front) // ambient
{
	return front * gl_LightSource[nr].ambient + front * gl_LightModel.ambient;
}

vec4 diffuseColor(int nr, vec3 direction, float diffuseIntensity, vec4 front) // diffuse
{
	return front * diffuseIntensity * gl_LightSource[nr].diffuse;
}

vec4 specularColor(int nr, vec3 direction, float diffuseIntensity, vec3 normal, vec4 front, float shininess) // specular
{
	// "reflect"-Operator: Phong-Modell (ohne Blinn (Blinn-Phong==Phnong vereinfacht)!!)
	vec3 reflection = normalize(reflect( - normalize(direction), normal));
	float specAngle = max(0.0, dot(normal, reflection));

	vec4 sp = vec4(0.0);
	if (diffuseIntensity != 0.0) {
		float specularValue = pow(specAngle, shininess); // / (direction * direction)
		sp += gl_LightSource[nr].specular * front * specularValue; // specular
	}
	return sp;
}

vec4 phong(int nr, vec3 direction, vec3 normal) // Hilfsmethode: Licht und Material
{
	float inten = max(0.0, dot(normal, normalize(direction)));
	vec4 color = diffuseColor(nr, direction, inten, gl_FrontMaterial.diffuse);
	color += ambientColor(nr, gl_FrontMaterial.ambient);
	color += specularColor(nr, direction, inten, normal, gl_FrontMaterial.specular, gl_FrontMaterial.shininess);
	return color;
}

vec4 phongTexture(int nr, vec3 direction, vec3 normal, vec4 textureColor) // Hilfsmethode: Licht und Textur
{
	float inten = max(0.0, dot(normal, normalize(direction)));
	vec4 color = diffuseColor(nr, direction, inten, textureColor);
	color += ambientColor(nr, textureColor);
	color += specularColor(nr, direction, inten, normal, textureColor, 0.6); // TODO: shininess ?! vec4(1.0) ??
	return color;
}


vec4 directionalLight(int nr, vec3 normal) // ambient, diffuse, specular of a directional light
{
	return phong(nr, - gl_LightSource[nr].position.xyz, normal);
}

vec4 directionalLightTexture(int nr, vec3 normal, vec4 textureColor)
{
	return phongTexture(nr, - gl_LightSource[nr].position.xyz, normal, textureColor);
}

vec4 pointLight(int nr, vec3 normal) // ambient, diffuse, specular of a point light
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// color
	vec4 color = phong(nr, direction, normal);
	// attenuation
	float dist = length(direction); // Abstand zwischen Lichtquelle und Vertex
	color *= 1.0 / (gl_LightSource[nr].constantAttenuation + gl_LightSource[nr].linearAttenuation * dist
		+ gl_LightSource[nr].quadraticAttenuation * dist * dist); // Abschwächung einrechnen
	return color;
}

vec4 pointLightTexture(int nr, vec3 normal, vec4 textureColor)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// color
	vec4 color = phongTexture(nr, direction, normal, textureColor);
	// attenuation
	float dist = length(direction); // Abstand zwischen Lichtquelle und Vertex
	color *= 1.0 / (gl_LightSource[nr].constantAttenuation + gl_LightSource[nr].linearAttenuation * dist
		+ gl_LightSource[nr].quadraticAttenuation * dist * dist); // Abschwächung einrechnen
	return color;
}

vec4 spotLight(int nr, vec3 normal)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// spot calculation
	vec3 actDir = normalize(direction).xyz;
	float attenuation = 0.0;
	float spotEffect = dot(normalize(gl_LightSource[nr].spotDirection), - actDir);
	if (spotEffect > gl_LightSource[nr].spotCosCutoff) {
		// TODO: zwei verschiedene Varianten ?!
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent); // Intensität berechnen
		//attenuation = pow(spotEffect, gl_LightSource[nr].spotExponent); // Intensität berechnen
	}
	// color
	return attenuation * pointLight(nr, normal); // Abschwächung ist hier bereits eingerechnet!
}

vec4 spotLightTexture(int nr, vec3 normal, vec4 textureColor)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// spot calculation
	vec3 actDir = normalize(direction).xyz;
	float attenuation = 0.0;
	float spotEffect = dot(normalize(gl_LightSource[nr].spotDirection), - actDir);
	if (spotEffect > gl_LightSource[nr].spotCosCutoff) {
		// TODO: zwei verschiedene Varianten ?!
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent); // Intensität berechnen
		//attenuation = pow(spotEffect, gl_LightSource[nr].spotExponent); // Intensität berechnen
	}
	// color
	return attenuation * pointLightTexture(nr, normal, textureColor); // Abschwächung ist hier bereits eingerechnet!
}

vec4 spotLightDX(int nr, vec3 normal)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	vec3 actDir = normalize(direction).xyz;
	// falloff factor calculation
		// aktueller Winkel zwischen Lichtrichtung und Vektor zwischen Objekt und Lichtquelle
	float cos_cur_angle = dot(normalize(gl_LightSource[nr].spotDirection), - actDir);
		// halber Strahlwinkel des Spotlights
	float cos_inner_cone_angle = gl_LightSource[nr].spotCosCutoff;
		// halber Winkel des inneren nicht abgeschwächten Bereichs (Kosinus-Wert) 
	float cos_outer_cone_angle = sqrt((1.0 + cos_inner_cone_angle) / 2.0); // entspricht dem halben Spot-Winkel
		// Abschwächungs-Faktor
	float fallOff = 1.0 - clamp((cos_cur_angle - cos_outer_cone_angle) / (cos_inner_cone_angle - cos_outer_cone_angle), 0.0, 1.0);
	// spot calculation
	float attenuation = 0.0;
	if (cos_cur_angle > cos_inner_cone_angle) { // liegt das Objekt innerhalb des Spot-Kegels?
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent);
	}
	// color calculation
	return attenuation * pointLight(nr, normal) * fallOff;
}

vec4 spotLightDXTexture(int nr, vec3 normal, vec4 textureColor)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	vec3 actDir = normalize(direction).xyz;
	// falloff factor calculation
		// aktueller Winkel zwischen Lichtrichtung und Vektor zwischen Objekt und Lichtquelle
	float cos_cur_angle = dot(normalize(gl_LightSource[nr].spotDirection), - actDir);
		// halber Strahlwinkel des Spotlights
	float cos_inner_cone_angle = gl_LightSource[nr].spotCosCutoff;
		// halber Winkel des inneren nicht abgeschwächten Bereichs (Kosinus-Wert) 
	float cos_outer_cone_angle = sqrt((1.0 + cos_inner_cone_angle) / 2.0); // entspricht dem halben Spot-Winkel
		// Abschwächungs-Faktor
	float fallOff = 1.0 - clamp((cos_cur_angle - cos_outer_cone_angle) / (cos_inner_cone_angle - cos_outer_cone_angle), 0.0, 1.0);
	// spot calculation
	float attenuation = 0.0;
	if (cos_cur_angle > cos_inner_cone_angle) { // liegt das Objekt innerhalb des Spot-Kegels?
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent);
	}
	// color calculation
	return attenuation * pointLightTexture(nr, normal, textureColor) * fallOff;
}

//---------------------------------------------------------------------

vec3 getNormal()
{
	vec2 texCoord = gl_TexCoord[0].st;
	float offset = 0.01;
    
    float texelLeft = texture2D(texHeightMap, texCoord + vec2(-offset, 0)).r;
    float texelRight = texture2D(texHeightMap, texCoord + vec2(offset, 0)).r;
    float texelUp = texture2D(texHeightMap, texCoord + vec2(0, -offset)).r;
    float texelBottom = texture2D(texHeightMap, texCoord + vec2(0, offset)).r;
    vec3 vRight = vec3(2.0*offset, texelRight - texelLeft, 0);
    vec3 vUp = vec3(0, texelUp - texelBottom, -2.0*offset);
    
    return normalize(cross(vRight,vUp));
}

float getHeight()
{
	return texture2D(texHeightMap, gl_TexCoord[0].st).r;
}

float getDiffuseIntensity() {
	vec3 normal = getNormal();
	vec3 lightPos = vec3(1.0, 1.0, 0.0);
	
	return max(0.3, dot(normal, normalize(lightPos))); // und min. 0.3 ambientes Licht
}

vec4 fog(vec4 color) {
	float near = 10.0;
	float far = 50.0;
	
	vec4 fogColor = vec4(0.7, 0.7, 0.7, 0.6);
	float fogTaker = smoothstep(near, far, length(position));
	vec4 fog = mix(color, fogColor, fogTaker);
	return fog;
}

void main()
{
	vec2 texCoord = gl_TexCoord[0].st;

	// Texel-Farbe für die verschiedenen Texturen ermitteln
	vec4 texMountainColor = texture2D(texMountain, texCoord) * vec4(0.6,0.6,0.6,1.0);
	vec4 texLandColor = texture2D(texLand, 2.0 * texCoord);
	vec4 texLowLandColor = texture2D(texLowLand, texCoord);
	vec4 beachColor = vec4(1.0, 0.90, 0.65, 1.0); 
	vec4 waterColor = vec4(0.3, 0.5, 0.7,0.0);
	
	float texHeight = getHeight();
	
	// Texturen auf Ebenenen entsprechend der texHeight auswählen und mischen
	
	vec4 texColor = mix(waterColor, beachColor, smoothstep(0.0,0.1, texHeight));
	texColor = mix(texColor, texLowLandColor, smoothstep(0.1,0.2, texHeight)); // Übergang Beach -> LowLand
	texColor = mix(texColor, texLandColor, smoothstep(0.3,0.35, texHeight));        // Übergang LowLand -> Land
	texColor = mix(texColor, texMountainColor, smoothstep(0.5,0.8, texHeight));     // Übergang Land -> Mountain
	
	//gl_FragColor = texColor;
	//gl_FragColor = vec4(getDiffuseIntensity() * texColor.rgb, texColor.a); // mit einem direktionalen Licht
	//gl_FragColor = vec4(getNormal()*0.5 + 0.5, 1.0) + 0.01 * texColor; // zeigt die NormalMap
	//gl_FragColor = gl_Color;
	
	vec3 normal = getNormal();
	normal = normalize(gl_NormalMatrix * normal);
	vec4 color = pointLightTexture(7, normal, texColor);
	color+= spotLightDXTexture(2, normal, texColor);
	gl_FragColor =  mix(color, fog(color), smoothstep(0.05,0.07, texHeight)); // Nebel kann nur über dem Wasser liegen
}
