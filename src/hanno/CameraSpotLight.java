package hanno;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.light.GLSpotLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.observers.RotateObservable;
import falk_guenther_meier.model.observers.TranslateObservable;

public class CameraSpotLight extends GLSpotLight {
	
	GLCamera camera;
	
	public CameraSpotLight(GLCamera camera) {
		super(GL2.GL_LIGHT2, camera.getPosition(), camera.getDirection());
		this.camera = camera;
		
		setAmbient(new GLVertex(0.7f, 0.7f, 0.7f));
		setDiffuse(new GLVertex(0.7f, 0.7f, 0.7f));
		setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		setSpotCutoff(40f);
		setSpotExponent(0.9f);
		setAttenuation(1.0f, 0.01f, 0.00f);
		
		camera.addRotateObserver(this);
		camera.addTranslateObserver(this);
	}
	
	@Override
	public void updateRotate(RotateObservable changed, float x, float y, float z) {
		this.setDirection(camera.getDirection());
	}
	
	@Override
	public void updateTranslate(TranslateObservable changed, GLVertex v) {
		this.setPosition(camera.getPosition());
	}
	
	
	@Override
	public void updateAbsolutePosition(TranslateObservable changed, GLVertex v) {
		this.setPosition(camera.getPosition());
	}

	
	
}
