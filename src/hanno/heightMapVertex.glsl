uniform sampler2D texHeightMap;
uniform sampler2D texMountain;
uniform sampler2D texLand;
uniform sampler2D texWater;

varying vec3 position;

void main()
{	
	vec4 position4 = gl_ModelViewMatrix * gl_Vertex;
	position = position4.xyz / position4.w;

	// Berechnung im Shader Vertex -> TexCoord
	gl_TexCoord[0] = vec4((gl_Vertex.x + 1.0)/2.0, (gl_Vertex.z + 1.0)/2.0, 0.0, 1.0);
	//gl_TexCoord[0] = gl_MultiTexCoord0;
	
	// manuelles Clamp, sonst gibts falsche H�hen durch Rundungsfehler am Rand
	vec2 hmTexCoord = vec2( clamp(gl_TexCoord[0].s, 0.0000001, 0.9999999), clamp(gl_TexCoord[0].t, 0.0000001, 0.9999999) );
	
	// H�he aus der HeightMap auslesen
	float texHeight = texture2D(texHeightMap, hmTexCoord).r;
	
	// Erh�hten Vertex setzen
	vec4 vertex = gl_Vertex;
	vertex.y = texHeight;
	gl_Position = gl_ModelViewProjectionMatrix * vertex;

	gl_FrontColor = gl_Color;
}