package hanno;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.media.opengl.GL2;

import falk_guenther_meier.model.BillBoardDrawManager;
import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLBillBoard;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.util.FileLoader;

public class Trees extends BillBoardDrawManager {
	
	private ShaderProgram simpleShader;
	
	public Trees(GLCamera camera) {
		super();
		
		BufferedImage bufferedImage = null;
		try {
			bufferedImage = ImageIO.read(Vulcano.getHeightMapFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		List<GLTexture2D> treeTextures = new ArrayList<GLTexture2D>();
		for(int i = 0; i<6; i++) {
			File texFile = FileLoader.getFileFromFilesFolder("billboard/baum0"+(i+1)+".png", false);
			treeTextures.add(new GLTexture2D(texFile));
		}
			
		
		for(int i = 1; i<=100; i++) {
			
			GLVertex position = findTreePosition(bufferedImage);
			position.addValues(0f, 0.35f, 0f);
			
			GLBillBoard billboard = new GLBillBoard(camera, treeTextures.get(i%6), position, 0.3f, 0.7f);
			this.addBillBoard(billboard);
		}
		
	}
	
	
	public void init(GL2 gl) {
		simpleShader = new ShaderProgram("simpleVertex.glsl", "simpleFragment.glsl");
		simpleShader.compileShaders(this.getClass(), gl);
	}
	
	public void drawBillBoards(GL2 gl) {
		simpleShader.useProgram(gl);
		simpleShader.setTextureUnit(gl, "texture", 0);
		super.drawBillBoards(gl);
	}
	
	
	
	static GLVertex findTreePosition(BufferedImage bufferedImage) {
		float posX, posY, posZ = 0f;
		
		int imgWidth = bufferedImage.getWidth();
		int imgHeight = bufferedImage.getHeight();
		
		float yFaktor = 5f;
		float xzFaktor = 15f;
		
		
		int x;
		int y;
		int steps = 0;
		boolean found = false;
		do {
			x = (int) Math.floor(Math.random()*imgWidth);
			y = (int) Math.floor(Math.random()*imgHeight);
			
			int color = bufferedImage.getRGB(x, y);
			
			int red = (color & 0x00ff0000) >> 16;
			posY = (float)red/255f;
			
			found = (posY > 0.35f) && (posY < 0.7f);
			steps++;
		} while (!found && (steps < 100000)); // green area in heightMap
		
		posY*=yFaktor;
		
		posX = (float)x/(float)imgWidth * 2f - 1f;
		posX*= xzFaktor;

		posZ = (float)y/(float)imgWidth * 2f - 1f;
		posZ*= xzFaktor;
		
		return new GLVertex(posX, posY, posZ);
	}

}
