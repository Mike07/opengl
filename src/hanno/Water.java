package hanno;

import java.io.File;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.textures.GLTextureCube;
import falk_guenther_meier.model.time.TimeComponent;
import falk_guenther_meier.util.FileLoader;

public class Water extends GLRectangle {
	
	private ShaderProgram waterReflectShader;
	private GLTextureCube skyCube;
	private TimeComponent timeComponent;
	
	
	public Water(TimeComponent timeComponent) {
		super(new GLVertex(), 1500, 1500);
		this.timeComponent = timeComponent;
		
		this.rotate(-90, 0, 0);
		this.setColor(new GLVertex(0.5f, 0.75f, 0.9f, 0.9f));
		this.translate(0, 0.6f, 0);
		
		File texFileNoise = FileLoader.getFileFromFilesFolder("noise/water_noise.jpg", true);
		GLTexture2D texNoise = new GLTexture2D(texFileNoise);
		texNoise.setGLTextureWrap(GL2.GL_REPEAT);
		this.setTexture(texNoise, 3);
		
		skyCube = World_Utility.getLostAtSeaTextureCube();
	}
	
	public void init(GL2 gl) {
		waterReflectShader = new ShaderProgram("waterReflectVertex.glsl", "waterReflectFragment.glsl");
		waterReflectShader.compileShaders(this.getClass(), gl);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		waterReflectShader.useProgram(gl);
		waterReflectShader.setTextureUnit(gl, "skyCube", 0);
		waterReflectShader.setTextureUnit(gl, "texNoise", 3);
		waterReflectShader.setUniformFloat(gl, "time", timeComponent.getTime());
		
		gl.glEnable(GL2.GL_TEXTURE_CUBE_MAP);
		skyCube.bind(gl);	
		super.beginGL(gl);
	}
	
	@Override
	public void endGL(GL2 gl) {
		super.endGL(gl);
		gl.glDisable(GL2.GL_TEXTURE_CUBE_MAP);	
	}

}
