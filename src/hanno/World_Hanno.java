package hanno;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.world.JoglApp;

public class World_Hanno extends JoglApp {

	private Vulcano vulcano;
	private Lava lava;
	private Water water;
	private Runway runway;
	private Trees trees;
	
	private Plane plane;
	private PlaneCamera planeCamera;
	
	
	
	public World_Hanno(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false, World_Utility.getWorldKonfiguration());
		setAngle(80);
		setDistance(1000);
		
		CameraSpotLight spotLight = new CameraSpotLight(this.getKonfiguration().getCamera());
		this.addLight(spotLight);
		
		vulcano = new Vulcano();
		lava = new Lava(this.getKonfiguration().getTime());
		water = new Water(this.getKonfiguration().getTime());
		runway = new Runway();
		trees = new Trees(this.getKonfiguration().getCamera());
		plane = new Plane(this.getKonfiguration().getTime());
		planeCamera = new PlaneCamera(plane, this.getKonfiguration().getCamera(), this.getKonfiguration().getTime());
		
		start();
		
	}

	public static void main(String[] args) {
		new World_Hanno("Einzelprojekt Hanno", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();
		
		
		vulcano.init(gl);
		water.init(gl);
		runway.init(gl);
		trees.init(gl);
	
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		
		plane.draw(gl);
		planeCamera.update();
		
		runway.draw(gl);
		
		
		drawTransparents(gl);

	}
	
	
	private void drawTransparents(GL2 gl) {
		
		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);

		vulcano.draw(gl);
		water.draw(gl);
		trees.drawBillBoards(gl);

		ShaderProgram.clearShaderProgram(gl);
		
		gl.glDisable(GL2.GL_BLEND);
		
		lava.draw(gl);
	}
	
	@Override
	public void keyP() {
		planeCamera.switchFollowMode();
	}
	
	@Override
	public void printMyKeys() {
		super.printMyKeys();
		System.out.println("Flugzeug verfolgen / Verfolgung abbrechen: P");	
	}
	
	
}
