uniform samplerCube skyCube;
uniform sampler2D texNoise;
uniform float time;
varying vec3 position;
varying vec4 vertex;

void main()
{	
	vec4 position4 = gl_ModelViewMatrix * gl_Vertex;
	position = position4.xyz / position4.w;

	gl_Position = ftransform(); 
	gl_TexCoord[3] = gl_MultiTexCoord3;
	
	vertex = gl_Vertex;
	gl_FrontColor = gl_Color;
}