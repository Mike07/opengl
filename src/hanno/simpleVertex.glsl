uniform sampler2D texture;
varying vec3 position;

void main()
{	
	vec4 position4 = gl_ModelViewMatrix * gl_Vertex;
	position = position4.xyz / position4.w;

	gl_Position = ftransform(); 
	gl_TexCoord[0] = gl_MultiTexCoord0;
	
	gl_FrontColor = gl_Color;
}