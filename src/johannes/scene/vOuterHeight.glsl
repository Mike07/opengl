varying vec3 position;
varying vec3 normal;

uniform sampler2D texHeightMap;
uniform sampler2D texMountain;
uniform sampler2D texLand;
uniform sampler2D texWater;

void main() {	
	// H�he aus der HeightMap auslesen
	gl_TexCoord[0] = gl_MultiTexCoord0;
	vec2 hmTexCoord = vec2( clamp(gl_MultiTexCoord0.s, 0.0000001, 0.9999999), clamp(gl_MultiTexCoord0.t, 0.0000001, 0.9999999) ); // manuelles Clamp, funktioniert sonst nicht
	float texHeight = texture2D(texHeightMap, hmTexCoord).r;

	// Erh�hten Vertex setzen
	vec4 vertex = gl_Vertex;
	vertex.y = texHeight;
	gl_Position = gl_ModelViewProjectionMatrix * vertex;

	vec4 position2 = gl_ModelViewMatrix * vertex;
	position = position2.xyz / position2.w;

	normal = gl_NormalMatrix * gl_Normal;
}
