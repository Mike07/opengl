uniform samplerCube textureCubeMap;
varying vec3 reflectDir;

void main (void) {
  gl_FragColor = textureCube(textureCubeMap, reflectDir);
}
