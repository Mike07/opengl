package johannes.scene;

import java.awt.Color;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import johannes.J_Utility;
import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.animation.PositionAnimation;
import falk_guenther_meier.model.buildings.GLBoxPointLight;
import falk_guenther_meier.model.buildings.GLHouse;
import falk_guenther_meier.model.buildings.GLWallWithHole;
import falk_guenther_meier.model.interpolation.LinearInterpolation;
import falk_guenther_meier.model.interpolation.SinusInterpolation;
import falk_guenther_meier.model.light.GLDirectionLight;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.light.GLSpotLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglAppShadowMapping;
import falk_guenther_meier.util.FileLoader;

public class ShadowSceneReal extends JoglAppShadowMapping {
	private GLTexture2D shadowOuterTexture;
	private GLTexture2D shadowOuterRoofTexture;
	private GLTexture2D shadowNormalFloorTexture;
	private GLTexture2D shadowColorFloorTexture;
	private GLTexture2D shadowNormalWallTexture;
	private GLTexture2D shadowColorWallTexture;
	private GLTexture2D shadowNormalObjTexture;
	private GLTexture2D shadowColorObjTexture;

	private ShaderProgram shadowShaderShadow;
	private ShaderProgram shadowShaderShadowBump;
	private ShaderProgram allShader;
	private ShaderProgram allShaderTexture;

	private List<GLObject> shadowInShadow;
	private List<GLObject> shadowOutBump;
	private List<GLObject> shadowInShadowBump;
	private List<GLObject> shadowOutObj;
	private GLPointLight shadowPointLight; // 3
	private GLSpotLight shadowSpotLight; // 4
	private GLDirectionLight shadowOuterDirectLight; // 7

	public ShadowSceneReal(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
	}

	@Override
	protected void initConstructor() {
		super.initConstructor();

		File textureFile = FileLoader.getFileFromFilesFolder("texture/Mauertextur_001.jpg");
		shadowOuterTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Dachtextur_2.jpg");
		shadowOuterRoofTexture = new GLTexture2D(textureFile);

		textureFile = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-c.jpg"); // texture_4.png
		shadowColorWallTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-n.jpg"); // normal_4.png
		shadowNormalWallTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-c.jpg"); // brickwork-c.jpg
		shadowColorObjTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-n.jpg"); // brickwork-n.jpg
		shadowNormalObjTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-c.jpg"); // masonry-wall-c.jpg
		shadowColorFloorTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-n.jpg"); // masonry-wall-n.jpg
		shadowNormalFloorTexture = new GLTexture2D(textureFile);
	}

	public static void main(String[] args) {
		new ShadowSceneReal("Shadow Scene Johannes", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();

		this.getKonfiguration().setCoordinates(false);
		this.getKonfiguration().setShowFrameRate(false);
		this.getKonfiguration().getCamera().setPosition(new GLVertex(-2.0f, 1.5f, 20.0f));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(0.0f, 0.0f, -1.0f));

		shadowInShadow = new LinkedList<GLObject>();
		shadowOutObj = new LinkedList<GLObject>();

		GLHouse house = new GLHouse(new GLVertex(0.0f, 0.0f, 0.0f), 14, 8, 10, 6);
		house.setColor(Color.WHITE);
		house.setTextureOuterWall(shadowOuterTexture, 0);
		house.setTextureOuterRoof(shadowOuterRoofTexture, 0);

		house.setTextureFloor(shadowColorFloorTexture, 0);		
		house.setTextureFloor(shadowNormalFloorTexture, 1);		

		house.setTextureInnerWall(shadowColorWallTexture, 0);
		house.setTextureInnerWall(shadowNormalWallTexture, 1);
		house.setTextureInnerRoof(shadowColorWallTexture, 0);
		house.setTextureInnerRoof(shadowNormalWallTexture, 1);

		shadowInShadowBump = house.getInnerPlanes();
		shadowOutBump = house.getOuterPlanesWithTexture();
		shadowOutObj.addAll(house.getOuterPlanesWithOutTexture());

		shadowInShadowBump.add(J_Utility.createCube(new GLVertex(-4.0f, 0.5f, 1.0f), 90.0f, shadowColorObjTexture, shadowNormalObjTexture));
		shadowInShadowBump.add(J_Utility.createCube(new GLVertex(-4.0f, 1.5f, 0.0f), 90.0f, shadowColorObjTexture, shadowNormalObjTexture));
		shadowInShadowBump.add(J_Utility.createCube(new GLVertex(-4.0f, 0.5f, -1.0f), 90.0f, shadowColorObjTexture, shadowNormalObjTexture));
		shadowInShadowBump.add(J_Utility.createCube(new GLVertex(-4.0f, 1.5f, -2.0f), 90.0f, shadowColorObjTexture, shadowNormalObjTexture));
		shadowInShadowBump.add(J_Utility.createCube(new GLVertex(-4.0f, 0.5f, -3.0f), 90.0f, shadowColorObjTexture, shadowNormalObjTexture));
		shadowInShadowBump.add(J_Utility.createCube(new GLVertex(-4.0f, 2.5f, -1.0f), 90.0f, shadowColorObjTexture, shadowNormalObjTexture));

		GLWallWithHole wall = new GLWallWithHole(new GLVertex(2.0f, 3.0f, 2.0f),
				3.0f, 1.0f, 1.0f, 3.0f, 1.0f, 1.0f, 1.0f);
		wall.rotate(0.0f, 45.0f, 0.0f);
		wall.setColor(Color.WHITE);
		wall.setMaterial(GLMaterial.createTextureMaterial());
		wall.setTexture(shadowColorObjTexture, 0);
		wall.setTexture(shadowNormalObjTexture, 1);
		shadowInShadowBump.add(wall);

		PositionAnimation ani = new PositionAnimation(this.getKonfiguration().getTime(), wall);
		ani.addInterpolation(new LinearInterpolation(wall.getPosition().clone(), new GLVertex(3.0f, 0.0f, 0.0f),
				false), 4.0f);
		ani.addInterpolation(new LinearInterpolation(ani.getActuallEndPoint(), new GLVertex(0.0f, 0.0f, -5.0f),
				false), 4.0f);
		ani.addInterpolation(new SinusInterpolation(ani.getActuallEndPoint(), new GLVertex(-3.0f, 0.0f, 5.0f), false,
				new GLVertex(0.0f, 1.0f, 0.0f), 3), 4.0f);
		ani.setLoop(true);
		this.addAnimation(ani);

		for (GLObject o : shadowInShadowBump) {
			o.setShader(shadowShaderShadowBump);
		}

		shadowShaderShadow = new ShaderProgram("vShadowStandard.glsl", "fShadowStandard.glsl");
		shadowShaderShadow.compileShaders(this.getClass(), gl);

		shadowShaderShadowBump = new ShaderProgram("vShadowBump.glsl", "fShadowBump.glsl");
		shadowShaderShadowBump.compileShaders(this.getClass(), gl);

		allShader = new ShaderProgram("vAllHouseStandard.glsl", "fAllHouseStandard.glsl");
		allShader.compileShaders(this.getClass(), gl);

		allShaderTexture = new ShaderProgram("vAllHouseTexture.glsl", "fAllHouseTexture.glsl");
		allShaderTexture.compileShaders(this.getClass(), gl);

		shadowPointLight = new GLPointLight(GL2.GL_LIGHT3, new GLVertex());
		shadowPointLight.setUseColorMaterial(true);
		shadowPointLight.setAmbient(new GLVertex(0.1f, 0.1f, 0.1f));
		shadowPointLight.setDiffuse(new GLVertex(0.3f, 0.3f, 0.3f));
		shadowPointLight.setSpecular(new GLVertex(0.4f, 0.4f, 0.4f));
		shadowPointLight.setAttenuation(0.5f, 0.1f, 0.01f);

		GLBoxPointLight box = new GLBoxPointLight(shadowPointLight, house.getPosition().clone(), 0.2f, 1.5f);
		shadowInShadow.add(box);

		shadowSpotLight = new GLSpotLight(GL2.GL_LIGHT4,
				new GLVertex(6.0f, 2.0f, 0.0f), new GLVertex(-6.0f, 1.0f, 0.0f));
		shadowSpotLight.setConstantAttenuation(0.2f);
		shadowSpotLight.setLinearAttenuation(0.03f);
		shadowSpotLight.setQuadraticAttenuation(0.003f);
		shadowSpotLight.setAmbient(new GLVertex(0.1f, 0.1f, 0.1f));
		shadowSpotLight.setDiffuse(new GLVertex(0.2f, 0.2f, 0.2f));
		shadowSpotLight.setSpecular(new GLVertex(0.2f, 0.2f, 0.2f));
		shadowSpotLight.setSpotCutoff(25.0f);
		this.addLight(shadowSpotLight);
		this.setLightForShadowMapping(shadowSpotLight);

		shadowOuterDirectLight = new GLDirectionLight(GL2.GL_LIGHT7, new GLVertex(2.0f, -1.0f, 2.0f));
		shadowOuterDirectLight.setAmbient(new GLVertex(0.2f, 0.2f, 0.2f));
		shadowOuterDirectLight.setDiffuse(new GLVertex(0.4f, 0.4f, 0.4f));
		shadowOuterDirectLight.setSpecular(new GLVertex(0.2f, 0.2f, 0.2f));
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		shadowSpotLight.beginGL(gl);
		shadowPointLight.beginGL(gl);
		shadowOuterDirectLight.beginGL(gl);

		shadowShaderShadowBump.useProgram(gl);
		shadowShaderShadowBump.setTextureUnit(gl, "renderTexture", 0);
		shadowShaderShadowBump.setTextureUnit(gl, "normalTexture", 1);
		shadowShaderShadowBump.setTextureUnit(gl, "shadowMap", 2);
		for (GLObject o : shadowInShadowBump) {
			o.draw(gl);
		}
		allShaderTexture.useProgram(gl);
		allShaderTexture.setTextureUnit(gl, "texUnit", 0);
		for (GLObject o : shadowOutBump) {
			o.draw(gl);
		}

		shadowShaderShadow.useProgram(gl);
		shadowShaderShadow.setTextureUnit(gl, "shadowMap", 2);
		for (GLObject o : shadowInShadow) {
			o.draw(gl);
		}

		allShader.useProgram(gl);
		for (GLObject o : shadowOutObj) {
			o.draw(gl);
		}

		ShaderProgram.clearShaderProgram(gl);
	}

	@Override
	protected void drawShadowRendering(GL2 gl) {
		super.drawShadowRendering(gl);

		for (GLObject o : shadowInShadowBump) {
			o.draw(gl);
		}
		for (GLObject o : shadowInShadow) {
			o.draw(gl);
		}
	}
}
