package johannes.scene;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import physic.GLCuboidPhysic;
import physic.GLHousePhysic;
import physic.PhysicAnimation;
import falk_guenther_meier.model.buildings.GLBoxPointLight;
import falk_guenther_meier.model.light.GLDirectionLight;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.observers.TimeIntervallObservable;
import falk_guenther_meier.model.observers.TimeIntervallObserver;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.time.TimeIntervallSender;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.Utility;

public class PhysicScene extends JoglApp implements TimeIntervallObserver {
	private GLTexture2D outerTexture;
	private GLTexture2D innerTexture;
	private GLTexture2D outerRoofTexture;
	private GLTexture2D innerRoofTexture;
	private GLTexture2D floorTexture;
	private ShaderProgram shader;
	private ShaderProgram shaderTexture;
	private ShaderProgram allShader;
	private ShaderProgram allShaderTexture;

	private List<GLObject> inObj;
	private List<GLObject> inTexture;
	private List<GLObject> outObj;
	private List<GLObject> outTexture;

	private PhysicAnimation physicAni;
	private GLPointLight physicLight; // 2
	private GLDirectionLight physicOuterDirectLight; // 7

	public PhysicScene(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
		File textureFile = FileLoader.getFileFromFilesFolder("texture/Mauertextur_001.jpg");
		outerTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Holztextur_1.jpg");
		innerTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Dachtextur_2.jpg");
		outerRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Holztextur_3.jpg");
		innerRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/img1348666317.jpg"); // Granit17.jpg
		floorTexture = new GLTexture2D(textureFile);
		start();
	}
	
	public static void main(String[] args) {
		new PhysicScene("Physic Scene Johannes", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();

		this.getKonfiguration().setCoordinates(false);
		this.getKonfiguration().setShowFrameRate(false);
		this.getKonfiguration().getCamera().setPosition(new GLVertex(-2.0f, 1.5f, 20.0f));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(0.0f, 0.0f, -1.0f));

		inObj = new LinkedList<GLObject>();
		inTexture = new LinkedList<GLObject>();
		outObj = new LinkedList<GLObject>();
		outTexture = new LinkedList<GLObject>();

		List<PhysicAnimation> list = new ArrayList<PhysicAnimation>();

		physicAni = new PhysicAnimation(this.getKonfiguration().getTime());
		this.addAnimation(physicAni);
		list.add(physicAni);

		GLHousePhysic house = new GLHousePhysic(list, new GLVertex(0.0f, 0.0f, 0.0f), 14, 8, 10, 6);
		house.setColor(Color.WHITE);
		house.getRoofLeft().setColor(Color.WHITE);
		house.getRoofRight().setColor(Color.WHITE);
		house.getRoofRidge().setColor(Color.WHITE);
		house.setTextureOuterWall(outerTexture, 0);
		house.setTextureInnerWall(innerTexture, 0);
		house.setTextureOuterRoof(outerRoofTexture, 0);
		house.setTextureInnerRoof(innerRoofTexture, 0);
		house.setTextureFloor(floorTexture, 0);		

		for (int i = 0; i < 6; i ++) {
			GLCuboidPhysic cube = new GLCuboidPhysic(GLVertex.addValues(house.getPosition().clone(),
					i - 3.0f, 2.0f, 0.0f),
					0.5f, 0.5f, 0.5f, 1.0f, 1.0f, 0.1f);
			cube.setColor(Color.RED);
			cube.setInitialVelocity(new GLVertex(i, 20.0f, 100.0f), new GLVertex(0.2f, 0.2f, 0.2f));
			physicAni.addPhysicable(cube);
			inObj.add(cube);
		}

		inTexture.addAll(house.getInnerPlanes());
		outTexture.addAll(house.getOuterPlanesWithTexture());
		outObj.addAll(house.getOuterPlanesWithOutTexture());

		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++) {
				for (int z = 0; z < 3; z++) {
					GLCuboidPhysic c = new GLCuboidPhysic(GLVertex.addValues(house.getPosition().clone(),
							3.0f + x * 0.5f, 2.0f + y * 0.5f, 3.0f + z * 0.5f), 0.4f, 0.4f, 0.4f, 2.0f, 1.0f, 0.65f);
					Utility.setRandomColor(c);
					physicAni.addPhysicable(c);
					inObj.add(c);
				}				
			}			
		}

		shader = new ShaderProgram("vPhysicStandard.glsl", "fPhysicStandard.glsl");
		shader.compileShaders(this.getClass(), gl);

		shaderTexture = new ShaderProgram("vPhysicTexture.glsl", "fPhysicTexture.glsl");
		shaderTexture.compileShaders(this.getClass(), gl);

		allShader = new ShaderProgram("vAllHouseStandard.glsl", "fAllHouseStandard.glsl");
		allShader.compileShaders(this.getClass(), gl);

		allShaderTexture = new ShaderProgram("vAllHouseTexture.glsl", "fAllHouseTexture.glsl");
		allShaderTexture.compileShaders(this.getClass(), gl);

		physicLight = new GLPointLight(GL2.GL_LIGHT2, new GLVertex());
		physicLight.setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		physicLight.setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		physicLight.setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		physicLight.setAttenuation(0.4f, 0.1f, 0.01f);

		physicOuterDirectLight = new GLDirectionLight(GL2.GL_LIGHT7, new GLVertex(2.0f, -1.0f, 2.0f));
		physicOuterDirectLight.setAmbient(new GLVertex(0.2f, 0.2f, 0.2f));
		physicOuterDirectLight.setDiffuse(new GLVertex(0.4f, 0.4f, 0.4f));
		physicOuterDirectLight.setSpecular(new GLVertex(0.2f, 0.2f, 0.2f));

		GLBoxPointLight box = new GLBoxPointLight(physicLight, house.getPosition().clone(), 0.2f, 2.5f);
		inObj.add(box);

		TimeIntervallSender sender = new TimeIntervallSender(this.getKonfiguration().getTime(), 30.0f);
		sender.addTimeIntervallObserver(this);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		physicLight.beginGL(gl);
		physicOuterDirectLight.beginGL(gl);

		shaderTexture.useProgram(gl);
		shaderTexture.setTextureUnit(gl, "texUnit", 0);
		for (GLObject o : inTexture) {
			o.draw(gl);
		}
		allShaderTexture.useProgram(gl);
		allShaderTexture.setTextureUnit(gl, "texUnit", 0);
		for (GLObject o : outTexture) {
			o.draw(gl);
		}

		shader.useProgram(gl);
		for (GLObject o : inObj) {
			o.draw(gl);
		}
		allShader.useProgram(gl);
		for (GLObject o : outObj) {
			o.draw(gl);
		}
		ShaderProgram.clearShaderProgram(gl);
	}

	@Override
	public void updateTimeIntervall(TimeIntervallObservable changed) {
		physicAni.stopAnimation();
		physicAni.startAnimation();
	}
}
