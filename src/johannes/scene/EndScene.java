package johannes.scene;

import java.awt.Color;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import johannes.J_Utility;

import com.jogamp.opengl.util.gl2.GLUT;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.animation.PositionAnimation;
import falk_guenther_meier.model.buildings.GLBoxPointLight;
import falk_guenther_meier.model.buildings.GLChair;
import falk_guenther_meier.model.buildings.GLHouse;
import falk_guenther_meier.model.buildings.GLPicture;
import falk_guenther_meier.model.buildings.GLTable;
import falk_guenther_meier.model.interpolation.BezierInterpolation;
import falk_guenther_meier.model.interpolation.ConstantInterpolation;
import falk_guenther_meier.model.interpolation.LinearInterpolation;
import falk_guenther_meier.model.light.GLDirectionLight;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.light.GLSpotLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.observers.TimeIntervallObservable;
import falk_guenther_meier.model.observers.TimeIntervallObserver;
import falk_guenther_meier.model.observers.TranslateObservable;
import falk_guenther_meier.model.particle.WaterParticleSystem;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.textures.GLTextureCube;
import falk_guenther_meier.model.time.TimeIntervallSender;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;

public class EndScene extends JoglApp implements TimeIntervallObserver {
	private GLTexture2D endOuterTexture;
	private GLTexture2D endInnerTexture;
	private GLTexture2D endOuterRoofTexture;
	private GLTexture2D endInnerRoofTexture;
	private GLTexture2D endFloorTexture;
	private GLTexture2D endPictureTexture;

	private ShaderProgram endShader;
	private ShaderProgram endShaderTexture;
	private ShaderProgram endShaderReflection;
	private ShaderProgram endShaderToon;
	private ShaderProgram endShaderTextureToon;
	private ShaderProgram endShaderReflectionToon;
	private ShaderProgram allShader;
	private ShaderProgram allShaderTexture;

	private boolean endToon;

	private List<GLObject> endInObj;
	private List<GLObject> endInTexture;
	private List<GLObject> endOutObj;
	private List<GLObject> endOutTexture;

	private GLTable endTable;

	private File endTexSkyMap_right;
	private File endTexSkyMap_left;
	private File endTexSkyMap_top;
	private File endTexSkyMap_down;
	private File endTexSkyMap_front;
	private File endTexSkyMap_back;
	private GLTexture endTextureCube;

	private GLPointLight endPointLight; // 5
	private GLSpotLight endSpotLight; // 6
	private GLDirectionLight endOuterDirectLight; // 7

	private WaterParticleSystem endP;

	public EndScene(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
		File textureFile = FileLoader.getFileFromFilesFolder("texture/Mauertextur_001.jpg");
		endOuterTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Holztextur_1.jpg");
		endInnerTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Dachtextur_2.jpg");
		endOuterRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Holztextur_3.jpg");
		endInnerRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/carpet2.jpg");
		endFloorTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("dome256.jpg");
		endPictureTexture = new GLTexture2D(textureFile);
		endToon = false;

		endTexSkyMap_right = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_right.png", true);
		endTexSkyMap_left = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_left.png", true);
		endTexSkyMap_top = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_top.png", true);
		endTexSkyMap_down = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_down.png", true);
		endTexSkyMap_front = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_front.png", true);
		endTexSkyMap_back = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_back.png", true);

		endTextureCube = new GLTextureCube(new File[] {
				endTexSkyMap_right, endTexSkyMap_left, endTexSkyMap_top, endTexSkyMap_down, endTexSkyMap_front, endTexSkyMap_back});

		start();
	}
	
	public static void main(String[] args) {
		new EndScene("End Scene Johannes", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();

		this.getKonfiguration().setCoordinates(false);
		this.getKonfiguration().setShowFrameRate(true);
		this.getKonfiguration().getCamera().setPosition(new GLVertex(-2.0f, 1.5f, 20.0f));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(0.0f, 0.0f, -1.0f));

		endPointLight = new GLPointLight(GL2.GL_LIGHT5, new GLVertex());
		endPointLight.setPosition(new GLVertex(0.0f, 0.2f, 0.2f));
		endPointLight.setAmbient(new GLVertex(0.2f, 0.1f, 0.2f));
		endPointLight.setDiffuse(new GLVertex(0.1f, 0.0f, 0.1f));
		endPointLight.setSpecular(new GLVertex(0.1f, 0.0f, 0.1f));
		endPointLight.setAttenuation(0.2f, 0.1f, 0.01f); // c, l, q

		endSpotLight = new GLSpotLight(GL2.GL_LIGHT6, new GLVertex(3.0f, 1.0f, -3.0f),
				new GLVertex(-3.0f, 0.2f, 3.0f));
		endSpotLight.setSpotCutoff(20.0f);
		endSpotLight.setAmbient(new GLVertex(0.1f, 0.1f, 0.1f));
		endSpotLight.setDiffuse(new GLVertex(0.3f, 0.3f, 0.3f));
		endSpotLight.setSpecular(new GLVertex(0.2f, 0.2f, 0.2f));
		endSpotLight.setAttenuation(0.4f, 0.01f, 0.001f);

		endOuterDirectLight = new GLDirectionLight(GL2.GL_LIGHT7, new GLVertex(2.0f, -1.0f, 2.0f));
		endOuterDirectLight.setAmbient(new GLVertex(0.2f, 0.2f, 0.2f));
		endOuterDirectLight.setDiffuse(new GLVertex(0.4f, 0.4f, 0.4f));
		endOuterDirectLight.setSpecular(new GLVertex(0.2f, 0.2f, 0.2f));

		endInObj = new LinkedList<GLObject>();
		endInTexture = new LinkedList<GLObject>();
		endOutObj = new LinkedList<GLObject>();
		endOutTexture = new LinkedList<GLObject>();

		GLBoxPointLight box = new GLBoxPointLight(this.endPointLight,
				GLVertex.addValues(new GLVertex(), 3.0f, 0.0f, 3.0f),
				0.2f, 1.0f);
		endInObj.add(box);

		GLHouse house = new GLHouse(new GLVertex(0.0f, 0.0f, 0.0f), 12, 6, 12, 6);
		house.setColor(Color.WHITE);
		house.setTextureOuterWall(endOuterTexture, 0);
		house.setTextureInnerWall(endInnerTexture, 0);
		house.setTextureOuterRoof(endOuterRoofTexture, 0);
		house.setTextureInnerRoof(endInnerRoofTexture, 0);
		house.setTextureFloor(endFloorTexture, 0);

		endInTexture.addAll(house.getInnerPlanes());
		endOutTexture.addAll(house.getOuterPlanesWithTexture());
		endOutObj.addAll(house.getOuterPlanesWithOutTexture());

		GLVertex color = new GLVertex(0.4f, 0.6f, 0.3f);
		endTable = new GLTable(new GLVertex(), 2.0f, 0.9f, 0.7f, 0.2f);
		endTable.setColor(color.clone());
		endInObj.add(endTable);
		GLChair chair = new GLChair(new GLVertex(1.5f, 0, 0), 0.4f, 0.4f, 0.5f, 0.65f);
		chair.setColor(color.clone());
		chair.rotate(0.0f, 270.0f, 0.0f);
		endInObj.add(chair);
		chair = new GLChair(new GLVertex(-0.5f, 0, -0.95f), 0.4f, 0.4f, 0.5f, 0.65f);
		chair.setColor(color.clone());
		chair.rotate(0.0f, 0.0f, 0.0f);
		endInObj.add(chair);

		GLPicture pict = new GLPicture(house.getBack().getLeft().getRectRear().getAbsoluteCenter(),
				1.0f, 2.0f, 0.1f, endPictureTexture, 0);
		pict.translate(0.0f, 0.0f, 0.01f);
		endInObj.addAll(pict.getObjectsWithOutTexture());
		endInTexture.add(pict.getPicturePlane());

		endShader = new ShaderProgram("vEndStandard.glsl", "fEndStandard.glsl");
		endShader.compileShaders(this.getClass(), gl);

		endShaderTexture = new ShaderProgram("vEndTexture.glsl", "fEndTexture.glsl");
		endShaderTexture.compileShaders(this.getClass(), gl);

		endShaderReflection = new ShaderProgram("vEndReflection.glsl", "fEndReflection.glsl");
		endShaderReflection.compileShaders(this.getClass(), gl);

		endShaderToon = new ShaderProgram("vEndToon.glsl", "fEndToon.glsl");
		endShaderToon.compileShaders(this.getClass(), gl);

		endShaderTextureToon = new ShaderProgram("vEndTextureToon.glsl", "fEndTextureToon.glsl");
		endShaderTextureToon.compileShaders(this.getClass(), gl);

		endShaderReflectionToon = new ShaderProgram("vEndReflectionToon.glsl", "fEndReflectionToon.glsl");
		endShaderReflectionToon.compileShaders(this.getClass(), gl);

		allShader = new ShaderProgram("vAllHouseStandard.glsl", "fAllHouseStandard.glsl");
		allShader.compileShaders(this.getClass(), gl);

		allShaderTexture = new ShaderProgram("vAllHouseTexture.glsl", "fAllHouseTexture.glsl");
		allShaderTexture.compileShaders(this.getClass(), gl);

		// Partikel-System
		GLCuboid help = new GLCuboid(new GLVertex(2.0f, 0.1f, 2.0f), 0.2f);
		help.setColor(Color.YELLOW);
		endInObj.add(help);

		endP = new WaterParticleSystem(200);

		// Relatives, Bezier-Animation, ...
		GLCuboid c = new GLCuboid(new GLVertex(-3.0f, 1.5f, 3.5f), 0.5f);
		c.setColor(Color.RED);
		endInObj.add(c);
		PositionAnimation ani = new PositionAnimation(this.getKonfiguration().getTime(), c);
		ani.addInterpolation(new BezierInterpolation(c.getPosition().clone(), new GLVertex(0.0f, 0.0f, -4.0f), false,
				new GLVertex(-4.0f, 4.0f, 0.5f), new GLVertex(-3.0f, -3.0f, 4.5f), new GLVertex(-2.0f, 3.0f, 1.0f)), 10.0f);
		ani.addInterpolation(new ConstantInterpolation(ani.getActuallEndPoint()), 1.0f);
		ani.addInterpolation(new LinearInterpolation(ani.getActuallEndPoint(), new GLVertex(0.0f, 0.0f, 4.0f), false),
				3.0f);
		ani.addInterpolation(new ConstantInterpolation(ani.getActuallEndPoint()), 0.5f);
		ani.setLoop(true);
		this.addAnimation(ani);

		GLCuboid d = new GLCuboid(new GLVertex(-3.0f, 1.5f, 3.5f), 0.5f) {
			@Override
			public void updateAbsolutePosition(TranslateObservable changed, GLVertex position) {
				super.updateAbsolutePosition(changed, position);
				this.setPosition(new GLVertex(position.getX(), position.getY() * 2.0f, position.getZ()));
			}
		};
		c.addTranslateObserver(d);
		d.setColor(Color.BLUE);
		endInObj.add(d);

		// Intervall einstellen zum Toon-Shader an/aus-stellen
		TimeIntervallSender sender = new TimeIntervallSender(this.getKonfiguration().getTime(), 10.0f);
		sender.addTimeIntervallObserver(this);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		endPointLight.beginGL(gl);
		endSpotLight.beginGL(gl);
		endOuterDirectLight.beginGL(gl);

		if (endToon) {
			endShaderTextureToon.useProgram(gl);
			endShaderTextureToon.setTextureUnit(gl, "texUnit", 0);			
		} else {
			endShaderTexture.useProgram(gl);
			endShaderTexture.setTextureUnit(gl, "texUnit", 0);
		}
		for (GLObject o : endInTexture) {
			o.draw(gl);
		}
		allShaderTexture.useProgram(gl);
		allShaderTexture.setTextureUnit(gl, "texUnit", 0);
		for (GLObject o : endOutTexture) {
			o.draw(gl);
		}
		ShaderProgram.clearShaderProgram(gl);


		if (endToon) {
			endShaderToon.useProgram(gl);
		} else {
			endShader.useProgram(gl);
		}
		for (GLObject o : endInObj) {
			o.draw(gl);
		}
		allShader.useProgram(gl);
		for (GLObject o : endOutObj) {
			o.draw(gl);
		}

		if (endToon) {
			endShaderToon.useProgram(gl);
		} else {
			endShader.useProgram(gl);
		}
		gl.glColor3f(1.0f, 0.5f, 0.5f);
		GLMaterial.createColor(new GLVertex(1.0f, 0.5f, 0.5f)).toGL(gl);
		J_Utility.renderTeaPot(gl, endTable.getTableDesk().getRectTop().getAbsoluteCenter(), 270.0f, 1.0f);

		gl.glPushMatrix();
			gl.glTranslatef(2.0f, 0.0f, 2.0f);
			gl.glScalef(20.0f, 20.0f, 20.0f);
			endP.beginGL(gl);
		gl.glPopMatrix();

		// Reflection Mapping Start
		if (endToon) {
			endShaderReflectionToon.useProgram(gl);
			endShaderReflectionToon.setTextureUnit(gl, "textureCubeMap", 0);			
		} else {
			endShaderReflection.useProgram(gl);
			endShaderReflection.setTextureUnit(gl, "textureCubeMap", 0);
		}

		gl.glEnable(GL2.GL_TEXTURE_CUBE_MAP);
		endTextureCube.bind(gl);

		GLUT glut = new GLUT();
		gl.glPushMatrix();
			gl.glTranslatef(0.0f, 3.0f, 0.0f);
			glut.glutSolidSphere(1, 100, 100);
		gl.glPopMatrix();		

		gl.glDisable(GL2.GL_TEXTURE_CUBE_MAP);
		// Reflection Mapping Ende

		ShaderProgram.clearShaderProgram(gl);
	}

	@Override
	public void updateTimeIntervall(TimeIntervallObservable changed) {
		endToon = ! endToon;
	}
}
