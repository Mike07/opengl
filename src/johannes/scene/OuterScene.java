package johannes.scene;

import java.io.File;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.animation.PositionDirectionAnimation;
import falk_guenther_meier.model.interpolation.BezierInterpolation;
import falk_guenther_meier.model.interpolation.ConstantInterpolation;
import falk_guenther_meier.model.interpolation.LinearInterpolation;
import falk_guenther_meier.model.interpolation.SinusInterpolation;
import falk_guenther_meier.model.light.GLDirectionLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLHeightMapTriangleStrip;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;

public class OuterScene extends JoglApp {
	private ShaderProgram outerShader;
	private GLHeightMapTriangleStrip outerHeightMap;
	private File outerTexFileHeightMap;
	private File outerTexFileMountain;
	private File outerTexFileLand;
	private File outerTexFileWater;

	private GLDirectionLight outerDirectLight; // 7

	public OuterScene(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);

		/*
		 * initial: H�he (0..1), x-z-Ebene (-1,-1)..(1,1) -> ver�ndern durch scaleNow
		 */
		outerTexFileHeightMap = FileLoader.getFileFromFilesFolder("heightmap/151009heightmapsr_bed.png", true);
		outerTexFileMountain = FileLoader.getFileFromFilesFolder("texture/textures-large-2.jpg", true);
		outerTexFileLand = FileLoader.getFileFromFilesFolder("texture/img1346756638.jpg", true);
		outerTexFileWater = FileLoader.getFileFromFilesFolder("texture/zTexturise-water-texture2.jpg", true);

		start();
	}

	public static void main(String[] args) {
		new OuterScene("Outer Scene Johannes", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();

		this.getKonfiguration().setCoordinates(false);
		this.getKonfiguration().setShowFrameRate(false);
		this.getKonfiguration().getCamera().setPosition(new GLVertex(5.0f, 2.5f, 10.0f));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(-1.0f, 0.0f, -2.0f));

		outerDirectLight = new GLDirectionLight(GL2.GL_LIGHT7, new GLVertex(2.0f, -1.0f, 2.0f));
		outerDirectLight.setAmbient(new GLVertex(0.2f, 0.2f, 0.2f));
		outerDirectLight.setDiffuse(new GLVertex(0.4f, 0.4f, 0.4f));
		outerDirectLight.setSpecular(new GLVertex(0.2f, 0.2f, 0.2f));

		outerShader = new ShaderProgram("vOuterHeight.glsl", "fOuterHeight.glsl");
		outerShader.compileShaders(this.getClass(), gl);

		outerHeightMap = new GLHeightMapTriangleStrip(50);
		outerHeightMap.setColor(new GLVertex(1,1,1));

		GLTexture2D texHeightMap = new GLTexture2D(outerTexFileHeightMap);
		outerHeightMap.setTexture(texHeightMap);

		GLTexture2D texMountain = new GLTexture2D(outerTexFileMountain);
		outerHeightMap.setTexture(texMountain, 2);

		GLTexture2D texLand = new GLTexture2D(outerTexFileLand);
		outerHeightMap.setTexture(texLand, 3);

		GLTexture2D texWater = new GLTexture2D(outerTexFileWater);
		outerHeightMap.setTexture(texWater, 4);

		// Animation der Kamera einbauen
		PositionDirectionAnimation ani = new PositionDirectionAnimation(this.getKonfiguration().getTime(),
				this.getKonfiguration().getCamera());

		// vorw�rts gehen
		ani.addInterpolationAlong(new LinearInterpolation(this.getKonfiguration().getCamera().getPosition().clone(),
				new GLVertex(-0.8f, 0.0f, -0.8f), false), 2.0f);
		// drehen
		ani.addInterpolationVariableDirection(new LinearInterpolation(ani.getActuallEndDirection(),
				new GLVertex(0.0f, 0.2f, -1.0f), true), 1.0f);
		// vorw�rts gehen
		ani.addInterpolationAlong(new LinearInterpolation(ani.getActuallEndPoint(),
				new GLVertex(0.0f, 0.2f, -1.0f), false), 2.0f);
		// drehen
		ani.addInterpolationVariableDirection(new LinearInterpolation(ani.getActuallEndDirection(),
				new GLVertex(2.0f, 0.4f, -3.0f), true), 1.0f);
		// vorw�rts gehen
		ani.addInterpolationAlong(new LinearInterpolation(ani.getActuallEndPoint(),
				new GLVertex(2.0f, 0.4f, -3.0f), false), 2.0f);
		// drehen
		ani.addInterpolationVariableDirection(new LinearInterpolation(ani.getActuallEndDirection(),
				new GLVertex(0.8f, -1.0f, -0.8f), true), 1.0f);
		// vorw�rts gehen
		ani.addInterpolationBothVariable(new SinusInterpolation(ani.getActuallEndPoint(),
				new GLVertex(2.0f, 0.5f, -3.0f), false, new GLVertex(0.0f, 0.3f, 0.0f), 3),
				new ConstantInterpolation(new GLVertex(0.8f, -1.0f, -0.8f), new GLVertex(0.8f, -1.0f, -0.8f)),
				4.0f);
//		ani.addInterpolationVariablePosition(new LinearInterpolation(ani.getActuallEndPoint(),
//				new GLVertex(-2.0f, 2.0f, -1.0f), false),
//				3.0f);
		// vorw�rts gehen
		GLVertex n = new GLVertex(-10.0f, 4.0f, -2.0f);
		ani.addInterpolationBothVariable(new BezierInterpolation(ani.getActuallEndPoint(), n.clone(), false,
				GLVertex.addValues(ani.getActuallEndPoint(), -2.0f, 10.0f, -0.4f),
				GLVertex.addValues(ani.getActuallEndPoint(), -4.0f, -4.0f, -0.9f),
				GLVertex.addValues(ani.getActuallEndPoint(), -7.0f, 5.0f, -1.4f)),
				new LinearInterpolation(new GLVertex(0.8f, -1.0f, -0.8f), new GLVertex(0.1f, -10.0f, 0.1f), true),
				5.0f);
//		// Pause vor Schluss-Animation
		ani.addInterpolationPause(1.0f);
		// vorw�rts gehen
		ani.addInterpolationBothVariable(new LinearInterpolation(ani.getActuallEndPoint(),
				new GLVertex(-0.22f, 22.0f, -0.22f), false),
				new ConstantInterpolation(new GLVertex(0.1f, -10.0f, 0.1f)), 8.0f);
//		ani.addInterpolationVariablePosition(new LinearInterpolation(ani.getActuallEndPoint(),
//				new GLVertex(-0.22f, 22.0f, -0.22f), false),
//				8.0f);

		// Pause am Ende
		ani.addInterpolationPause(2.0f);

		// finale Einstellungen
		ani.setLoop(true);
		this.addAnimation(ani);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		outerDirectLight.beginGL(gl);

		outerShader.useProgram(gl);
		outerShader.setTextureUnit(gl, "texHeightMap", 0);
		outerShader.setTextureUnit(gl, "texMountain", 2);
		outerShader.setTextureUnit(gl, "texLand", 3);
		outerShader.setTextureUnit(gl, "texWater", 4);

		outerHeightMap.glScaleNow(10, 4, 10); // 30, 10, 30
		outerHeightMap.draw(gl);

		ShaderProgram.clearShaderProgram(gl);
	}
}
