package johannes.scene;

import java.awt.Color;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import johannes.J_Utility;
import falk_guenther_meier.model.BillBoardDrawManager;
import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.buildings.GLBoxPointLight;
import falk_guenther_meier.model.buildings.GLChair;
import falk_guenther_meier.model.buildings.GLHouse;
import falk_guenther_meier.model.buildings.GLPicture;
import falk_guenther_meier.model.buildings.GLTable;
import falk_guenther_meier.model.light.GLDirectionLight;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.light.GLSpotLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLBillBoard;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexGrid;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.ObjLoader;

public class HouseScene extends JoglApp {
	private GLTexture2D houseOuterTexture;
	private GLTexture2D houseInnerTexture;
	private GLTexture2D houseOuterRoofTexture;
	private GLTexture2D houseInnerRoofTexture;
	private GLTexture2D houseFloorTexture;
	private GLTexture2D housePictureTexture;
	private ShaderProgram houseShader;
	private ShaderProgram houseShaderTexture;
	private ShaderProgram allShader;
	private ShaderProgram allShaderTexture;

	private GLVertex housePos;
	private float houseM;
	private GLTable houseTable;
	private List<GLObject> houseInObj;
	private List<GLObject> houseInTexture;
	private List<GLObject> houseOutObj;
	private List<GLObject> houseOutTexture;

	private BillBoardDrawManager houseBbManager;
	private GLComplexObject hOBJ;

	private GLRectangle houseRectExplosion;
	private GLTexture2D houseExplosionTexture;
	private GLPointLight housePointLight; // 0
	private GLSpotLight houseSpot; // 1
	private GLDirectionLight houseOuterDirectLight; // 7

	public HouseScene(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
		File textureFile = FileLoader.getFileFromFilesFolder("texture/Mauertextur_001.jpg");
		houseOuterTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/holz-vertafelung.jpg"); // Holztextur_1.jpg
		houseInnerTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Dachtextur_2.jpg");
		houseOuterRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Beadboard.jpg"); // Holztextur_3.jpg
		houseInnerRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Carpet.jpg");
		houseFloorTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("dome256.jpg");
		housePictureTexture = new GLTexture2D(textureFile);

		File explosionTextureFile = FileLoader.getFileFromFilesFolder("texture/explosion2.png");
		houseExplosionTexture = new GLTexture2D(explosionTextureFile);
		houseExplosionTexture.setTexGrid(new GLTexGrid(4,4));

		housePos = new GLVertex();
		houseM = 1.0f;

//		File f = new File("D:/Studium/Inhalt, Lehre/Semester-7-1213/OpenGL/opengl/src/falk_guenther_meier/files/");
		File f = FileLoader.getFileFromFilesFolder("obj/bunny.obj", true);
//		System.out.println(f.toString());
		hOBJ = ObjLoader.objFile2ComplexObj(f);

		start();
	}
	
	public static void main(String[] args) {
		new HouseScene("House Scene Johannes", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();

		this.getKonfiguration().setCoordinates(false);
		this.getKonfiguration().setShowFrameRate(false);
		this.getKonfiguration().getCamera().setPosition(new GLVertex(-2.0f, 1.5f, 20.0f));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(0.0f, 0.0f, -1.0f));

		houseOuterDirectLight = new GLDirectionLight(GL2.GL_LIGHT7, new GLVertex(2.0f, -1.0f, 2.0f));
		houseOuterDirectLight.setAmbient(new GLVertex(0.2f, 0.2f, 0.2f));
		houseOuterDirectLight.setDiffuse(new GLVertex(0.4f, 0.4f, 0.4f));
		houseOuterDirectLight.setSpecular(new GLVertex(0.2f, 0.2f, 0.2f));

		housePointLight = new GLPointLight(GL2.GL_LIGHT0, new GLVertex(), new GLVertex(0.25f, 0.25f, 0.2f),
				new GLVertex(0.2f, 0.2f, 0.1f), new GLVertex(0.2f, 0.2f, 0.1f));
		housePointLight.setAttenuation(0.35f, 0.02f, 0.002f);

		houseSpot = new GLSpotLight(GL2.GL_LIGHT1, GLVertex.add(housePos, new GLVertex(2.0f, 2.0f, 2.0f)),
				new GLVertex(-2.0f, 1.0f, 0.0f));
		houseSpot.setSpotCutoff(20.0f);
		houseSpot.setAmbient(new GLVertex(0.3f, 0.3f, 0.3f));
		houseSpot.setDiffuse(new GLVertex(0.3f, 0.3f, 0.3f));
		houseSpot.setSpecular(new GLVertex(0.3f, 0.3f, 0.3f));

		houseInObj = new LinkedList<GLObject>();
		houseInTexture = new LinkedList<GLObject>();
		houseOutObj = new LinkedList<GLObject>();
		houseOutTexture = new LinkedList<GLObject>();

		GLHouse house = new GLHouse(housePos.clone(), 12 * houseM, 6 * houseM, 12 * houseM, 6 * houseM);
		house.setColor(Color.WHITE);
		house.setTextureOuterWall(houseOuterTexture, 0);
		house.setTextureInnerWall(houseInnerTexture, 0);
		house.setTextureOuterRoof(houseOuterRoofTexture, 0);
		house.setTextureInnerRoof(houseInnerRoofTexture, 0);
		house.setTextureFloor(houseFloorTexture, 0);		

		GLBoxPointLight box = new GLBoxPointLight(this.housePointLight,
				GLVertex.addValues(house.getPosition().clone(), 3.0f, 0.0f, -3.0f),
				0.2f * houseM, 1.5f * houseM);
		houseInObj.add(box);
		System.out.println(housePointLight.getPosition());
		System.out.println(house.getPosition());
		
		houseInTexture.addAll(house.getInnerPlanes());
		houseOutTexture.addAll(house.getOuterPlanesWithTexture());
		houseOutObj.addAll(house.getOuterPlanesWithOutTexture());

		GLVertex color = new GLVertex(0.4f, 0.6f, 0.3f);
		houseTable = new GLTable(housePos.clone(), 2.0f * houseM, 0.9f * houseM, 0.7f * houseM, 0.2f * houseM);
		houseTable.setColor(color.clone());
		houseInObj.add(houseTable);
		GLChair chair = new GLChair(GLVertex.add(housePos, new GLVertex(1.5f * houseM, 0, 0)),
				0.4f * houseM, 0.4f * houseM, 0.5f * houseM, 0.65f * houseM);
		chair.rotate(0.0f, 270.0f, 0.0f);
		chair.setColor(color.clone());
		houseInObj.add(chair);
		chair = new GLChair(GLVertex.add(housePos, new GLVertex(-0.5f * houseM, 0, -0.95f * houseM)),
				0.4f * houseM, 0.4f * houseM, 0.5f * houseM, 0.65f * houseM);
		chair.rotate(0.0f, 0.0f, 0.0f);
		chair.setColor(color.clone());
		houseInObj.add(chair);

		GLPicture pict = new GLPicture(house.getBack().getLeft().getRectRear().getAbsoluteCenter(),
				2.0f * houseM, 1.0f * houseM, 0.1f * houseM, housePictureTexture, 0);
		pict.translate(0.0f, 0.0f, 0.01f);
		houseInObj.addAll(pict.getObjectsWithOutTexture());
		houseInTexture.add(pict.getPicturePlane());

		houseShader = new ShaderProgram("vHouseStandard.glsl", "fHouseStandard.glsl");
		houseShader.compileShaders(this.getClass(), gl);

		houseShaderTexture = new ShaderProgram("vHouseTexture.glsl", "fHouseTexture.glsl");
		houseShaderTexture.compileShaders(this.getClass(), gl);

		allShader = new ShaderProgram("vAllHouseStandard.glsl", "fAllHouseStandard.glsl");
		allShader.compileShaders(this.getClass(), gl);

		allShaderTexture = new ShaderProgram("vAllHouseTexture.glsl", "fAllHouseTexture.glsl");
		allShaderTexture.compileShaders(this.getClass(), gl);

		// BillBoards
		this.houseBbManager = new BillBoardDrawManager();
		GLBillBoard bb;
		bb = new GLBillBoard(this.getKonfiguration().getCamera(), "billboard/AC-Ezio_Render.png",
				GLVertex.addValues(housePos.clone(), 1.0f, 1.0f, 1.0f), 1.0f, 2.0f);
		houseBbManager.addBillBoard(bb);
		bb = new GLBillBoard(this.getKonfiguration().getCamera(), "billboard/AC-Ezio_Render.png",
				GLVertex.addValues(housePos.clone(), 3.0f, 1.0f, 3.0f), 1.0f, 2.0f);
		houseBbManager.addBillBoard(bb);

		// Explosion
		houseRectExplosion = new GLRectangle(GLVertex.add(housePos, new GLVertex(-4.5f, 2.0f, -5.5f)), 2f);
		houseRectExplosion.setColor(new GLVertex(1.0f, 1.0f, 1.0f, 1.0f));
		houseRectExplosion.setTexture(houseExplosionTexture);

		// Objekte
//		GLSphere sphere = new GLSphere(1.0f, 2);
//		sphere.translate(0.0f, 2.0f, -3.0f);
//		sphere.setColor(Color.RED);
//		houseInObj.add(sphere);
//		GLCylinder cy = new GLCylinder(1.0f, 3.0f, 6);
//		cy.setColor(Color.BLUE);
//		houseInObj.add(cy);
		hOBJ.scale(5.0f, 5.0f, 5.0f);
		hOBJ.translate(-3.0f, 1.0f, 3.0f);
		hOBJ.setColor(Color.RED);
		houseInObj.add(hOBJ); // TODO: wieder einbauen !!
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		housePointLight.beginGL(gl);
		houseSpot.beginGL(gl);
		houseOuterDirectLight.beginGL(gl);

		houseShaderTexture.useProgram(gl);
		houseShaderTexture.setTextureUnit(gl, "texUnit", 0);
		for (GLObject o : houseInTexture) {
			o.draw(gl);
		}
		allShaderTexture.useProgram(gl);
		allShaderTexture.setTextureUnit(gl, "texUnit", 0);
		for (GLObject o : houseOutTexture) {
			o.draw(gl);
		}

		houseShader.useProgram(gl);
		for (GLObject o : houseInObj) {
			o.draw(gl);
		}
		allShader.useProgram(gl);
		for (GLObject o : houseOutObj) {
			o.draw(gl);
		}
		houseShader.useProgram(gl);
		gl.glColor3f(1.0f, 0.5f, 0.5f);
		GLMaterial.createColor(new GLVertex(1.0f, 0.5f, 0.5f));
		J_Utility.renderTeaPot(gl, houseTable.getTableDesk().getRectTop().getAbsoluteCenter(), 270.0f, houseM);		

		ShaderProgram.clearShaderProgram(gl);

		// Explosion
		float picsPerSecond = 20;
		float time = this.getKonfiguration().getTime().getTime();
		int texCols = houseExplosionTexture.getTexGrid().getColCount();
		int texRows = houseExplosionTexture.getTexGrid().getRowCount();
		int texNr = Math.round(time * picsPerSecond) % (texCols*texRows);
		int texCol = texNr % texCols;
		int texRow = texNr / texRows;
		houseRectExplosion.setTexRect(texCol, texRow);

		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glDepthMask(false);
		
		houseRectExplosion.draw(gl);
		
		gl.glDepthMask(true);
		gl.glDisable(GL2.GL_BLEND);
	}

	@Override
	public void endGL(GL2 gl) {
		houseShaderTexture.useProgram(gl);
		houseShaderTexture.setTextureUnit(gl, "texUnit", 0);
		houseBbManager.drawBillBoards(gl);
		ShaderProgram.clearShaderProgram(gl);
		super.endGL(gl);
	}
}
