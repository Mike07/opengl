package johannes;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import physic.GLCuboidPhysic;
import physic.GLHousePhysic;
import physic.PhysicAnimation;

import com.jogamp.opengl.util.gl2.GLUT;

import falk_guenther_meier.model.BillBoardDrawManager;
import falk_guenther_meier.model.buildings.GLBoxPointLight;
import falk_guenther_meier.model.buildings.GLChair;
import falk_guenther_meier.model.buildings.GLHouse;
import falk_guenther_meier.model.buildings.GLPicture;
import falk_guenther_meier.model.buildings.GLTable;
import falk_guenther_meier.model.buildings.GLWallWithHole;
import falk_guenther_meier.model.light.GLDirectionLight;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.light.GLSpotLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLBillBoard;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLHeightMapTriangleStrip;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.observers.TimeIntervallObservable;
import falk_guenther_meier.model.observers.TimeIntervallObserver;
import falk_guenther_meier.model.particle.WaterParticleSystem;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexGrid;
import falk_guenther_meier.model.textures.GLTexture;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.textures.GLTextureCube;
import falk_guenther_meier.model.time.TimeIntervallSender;
import falk_guenther_meier.model.world.JoglAppShadowMapping;
import falk_guenther_meier.util.FileLoader;

public class WorldScene extends JoglAppShadowMapping implements TimeIntervallObserver {
	/**
	 * 0: Outer
	 * 1: House
	 * 2: Physic
	 * 3: Shadow
	 * 4: End
	 */
	private int scene;

	// Outer
	private ShaderProgram outerShader;
	private GLHeightMapTriangleStrip outerHeightMap;
	private File outerTexFileHeightMap;
	private File outerTexFileMountain;
	private File outerTexFileLand;
	private File outerTexFileWater;

	private GLDirectionLight outerDirectLight; // 7

	// House
	private GLTexture2D houseOuterTexture;
	private GLTexture2D houseInnerTexture;
	private GLTexture2D houseOuterRoofTexture;
	private GLTexture2D houseInnerRoofTexture;
	private GLTexture2D houseFloorTexture;
	private GLTexture2D housePictureTexture;
	private ShaderProgram houseShader;
	private ShaderProgram houseShaderTexture;

	private GLVertex housePos; // H�he des Bodens der gesamten Szene!
	private float houseM; // Gr��e eines Meters!
	private GLTable houseTable;
	private List<GLObject> houseInObj;
	private List<GLObject> houseInTexture;
	private List<GLObject> houseOutObj;
	private List<GLObject> houseOutTexture;

	private BillBoardDrawManager houseBbManager;

	private GLRectangle houseRectExplosion;
	private GLTexture2D houseExplosionTexture;
	private GLPointLight housePointLight; // 0
	private GLSpotLight houseSpot; // 1

	// Physic
	private GLTexture2D outerTexture;
	private GLTexture2D innerTexture;
	private GLTexture2D outerRoofTexture;
	private GLTexture2D innerRoofTexture;
	private GLTexture2D floorTexture;
	private ShaderProgram shader;
	private ShaderProgram shaderTexture;

	private List<GLObject> inObj;
	private List<GLObject> inTexture;
	private List<GLObject> outObj;
	private List<GLObject> outTexture;

	private PhysicAnimation physicAni;
	GLPointLight physicLight; // 2

	// Shadow
	private GLTexture2D shadowOuterTexture;
	private GLTexture2D shadowOuterRoofTexture;
	private GLTexture2D shadowNormalFloorTexture;
	private GLTexture2D shadowColorFloorTexture;
	private GLTexture2D shadowNormalWallTexture;
	private GLTexture2D shadowColorWallTexture;
	private GLTexture2D shadowNormalObjTexture;
	private GLTexture2D shadowColorObjTexture;

	private ShaderProgram shadowShaderShadow;
	private ShaderProgram shadowShaderShadowBump;

	private List<GLObject> shadowInShadow;
	private List<GLObject> shadowOutBump;
	private List<GLObject> shadowInShadowBump;
	private List<GLObject> shadowOutObj;
	private GLPointLight shadowPointLight; // 3
	private GLSpotLight shadowSpotLight; // 4

	
	// End
	private GLTexture2D endOuterTexture;
	private GLTexture2D endInnerTexture;
	private GLTexture2D endOuterRoofTexture;
	private GLTexture2D endInnerRoofTexture;
	private GLTexture2D endFloorTexture;
	private GLTexture2D endPictureTexture;

	private ShaderProgram endShader;
	private ShaderProgram endShaderTexture;
	private ShaderProgram endShaderReflection;
	private ShaderProgram endShaderReflectionToon;
	private ShaderProgram endShaderToon;

	private boolean endToon;

	private List<GLObject> endInObj;
	private List<GLObject> endInTexture;
	private List<GLObject> endOutObj;
	private List<GLObject> endOutTexture;

	private GLTable endTable;

	private File endTexSkyMap_right;
	private File endTexSkyMap_left;
	private File endTexSkyMap_top;
	private File endTexSkyMap_down;
	private File endTexSkyMap_front;
	private File endTexSkyMap_back;
	private GLTexture endTextureCube;

	private GLPointLight endPointLight; // 5
	private GLSpotLight endSpotLight; // 6
	private WaterParticleSystem endP;

	
	public WorldScene(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
		this.lights.clear();

		scene = 0;
		this.setDrawShadow(false);
		this.setLightForShadowMapping(this.getKonfiguration().getCamera());

		// House
		File textureFile = FileLoader.getFileFromFilesFolder("texture/Mauertextur_001.jpg");
		houseOuterTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/holz-vertafelung.jpg"); // Holztextur_1.jpg
		houseInnerTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Dachtextur_2.jpg");
		houseOuterRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Beadboard.jpg"); // Holztextur_3.jpg
		houseInnerRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Carpet.jpg");
		houseFloorTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("dome256.jpg");
		housePictureTexture = new GLTexture2D(textureFile);

		File explosionTextureFile = FileLoader.getFileFromFilesFolder("texture/explosion2.png");
		houseExplosionTexture = new GLTexture2D(explosionTextureFile);
		houseExplosionTexture.setTexGrid(new GLTexGrid(4,4));

		housePos = new GLVertex();
		houseM = 1.0f;

		// Physic
		textureFile = FileLoader.getFileFromFilesFolder("texture/Mauertextur_001.jpg");
		outerTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Holztextur_1.jpg");
		innerTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Dachtextur_2.jpg");
		outerRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Holztextur_3.jpg");
		innerRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Granit17.jpg");
		floorTexture = new GLTexture2D(textureFile);

		
		// Shadow
		textureFile = FileLoader.getFileFromFilesFolder("texture/Mauertextur_001.jpg");
		shadowOuterTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Dachtextur_2.jpg");
		shadowOuterRoofTexture = new GLTexture2D(textureFile);

		textureFile = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-c.jpg");
		shadowColorWallTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-n.jpg");
		shadowNormalWallTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/bumpmapping/brickwork-c.jpg");
		shadowColorObjTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/bumpmapping/brickwork-n.jpg");
		shadowNormalObjTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Floral_Carpet.jpg");
		shadowColorFloorTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/bumpmapping/nlinsample.jpg");
		shadowNormalFloorTexture = new GLTexture2D(textureFile);

		// End
		textureFile = FileLoader.getFileFromFilesFolder("texture/Mauertextur_001.jpg");
		endOuterTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Holztextur_1.jpg");
		endInnerTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Dachtextur_2.jpg");
		endOuterRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Holztextur_3.jpg");
		endInnerRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/carpet2.jpg");
		endFloorTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("dome256.jpg");
		endPictureTexture = new GLTexture2D(textureFile);
		endToon = false;

		endTexSkyMap_right = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_right.png", true);
		endTexSkyMap_left = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_left.png", true);
		endTexSkyMap_top = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_top.png", true);
		endTexSkyMap_down = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_down.png", true);
		endTexSkyMap_front = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_front.png", true);
		endTexSkyMap_back = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_back.png", true);

		endTextureCube = new GLTextureCube(new File[] {
				endTexSkyMap_right, endTexSkyMap_left, endTexSkyMap_top, endTexSkyMap_down, endTexSkyMap_front, endTexSkyMap_back});

		
		// Outer
		/*
		 * initial: H�he (0..1), x-z-Ebene (-1,-1)..(1,1) -> ver�ndern durch scaleNow
		 */
		outerTexFileHeightMap = FileLoader.getFileFromFilesFolder("heightmap/151009heightmapsr_bed.png", true);
		outerTexFileMountain = FileLoader.getFileFromFilesFolder("texture/textures-large-2.jpg", true);
		outerTexFileLand = FileLoader.getFileFromFilesFolder("texture/img1346756638.jpg", true);
		outerTexFileWater = FileLoader.getFileFromFilesFolder("texture/zTexturise-water-texture2.jpg", true);

		
		start();
	}

	public static void main(String[] args) {
		new WorldScene("World Scene", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();

		// House
		housePointLight = new GLPointLight(GL2.GL_LIGHT0, new GLVertex(), new GLVertex(0.3f, 0.3f, 0.2f),
				new GLVertex(0.2f, 0.2f, 0.1f), new GLVertex(0.1f, 0.1f, 0.1f));
		housePointLight.setAttenuation(0.1f, 0.07f, 0.01f);

		houseSpot = new GLSpotLight(GL2.GL_LIGHT1, GLVertex.add(housePos, new GLVertex(2.0f, 2.0f, 2.0f)),
				new GLVertex(-2.0f, 1.0f, 0.0f));
		houseSpot.setSpotCutoff(20.0f);
		houseSpot.setAmbient(new GLVertex(0.3f, 0.3f, 0.3f));
		houseSpot.setDiffuse(new GLVertex(0.3f, 0.3f, 0.3f));
		houseSpot.setSpecular(new GLVertex(0.3f, 0.3f, 0.3f));

		houseInObj = new LinkedList<GLObject>();
		houseInTexture = new LinkedList<GLObject>();
		houseOutObj = new LinkedList<GLObject>();
		houseOutTexture = new LinkedList<GLObject>();

		GLHouse house = new GLHouse(housePos.clone(), 12 * houseM, 6 * houseM, 12 * houseM, 6 * houseM);
		J_Utility.initObj(house);
		house.getRoofLeft().setColor(Color.WHITE);
		house.getRoofRight().setColor(Color.WHITE);
		house.getRoofRidge().setColor(Color.WHITE);
		house.setTextureOuterWall(houseOuterTexture, 0);
		house.setTextureInnerWall(houseInnerTexture, 0);
		house.setTextureOuterRoof(houseOuterRoofTexture, 0);
		house.setTextureInnerRoof(houseInnerRoofTexture, 0);
		house.setTextureFloor(houseFloorTexture, 0);		

		GLBoxPointLight box = new GLBoxPointLight(this.housePointLight,
				GLVertex.addValues(house.getPosition().clone(), 3.0f, 0.0f, -3.0f),
				0.2f * houseM, 1.5f * houseM);
		houseInObj.add(box);
		
		houseInTexture.addAll(house.getInnerPlanes());
		houseOutTexture.addAll(house.getOuterPlanesWithTexture());
		houseOutObj.addAll(house.getOuterPlanesWithOutTexture());

		GLVertex color = new GLVertex(0.4f, 0.6f, 0.3f);
		houseTable = new GLTable(housePos.clone(), 2.0f * houseM, 0.9f * houseM, 0.7f * houseM, 0.2f * houseM);
		J_Utility.initObj(houseTable);
		houseTable.setColor(color.clone());
		houseInObj.add(houseTable);
		GLChair chair = new GLChair(GLVertex.add(housePos, new GLVertex(1.5f * houseM, 0, 0)),
				0.4f * houseM, 0.4f * houseM, 0.5f * houseM, 0.65f * houseM);
		J_Utility.initObj(chair);
		chair.rotate(0.0f, 270.0f, 0.0f);
		chair.setColor(color.clone());
		houseInObj.add(chair);
		chair = new GLChair(GLVertex.add(housePos, new GLVertex(-0.5f * houseM, 0, -0.95f * houseM)),
				0.4f * houseM, 0.4f * houseM, 0.5f * houseM, 0.65f * houseM);
		J_Utility.initObj(chair);
		chair.rotate(0.0f, 0.0f, 0.0f);
		chair.setColor(color.clone());
		houseInObj.add(chair);

		GLPicture pict = new GLPicture(house.getBack().getLeft().getRectRear().getAbsoluteCenter(),
				1.0f * houseM, 0.5f * houseM, 0.04f * houseM, housePictureTexture, 0);
		pict.translate(0.0f, 0.0f, 0.01f);
		houseInObj.addAll(pict.getObjectsWithOutTexture());
		houseInTexture.add(pict.getPicturePlane());

		houseShader = new ShaderProgram("vHouseStandard.glsl", "fHouseStandard.glsl");
		houseShader.compileShaders(this.getClass(), gl);

		houseShaderTexture = new ShaderProgram("vHouseTexture.glsl", "fHouseTexture.glsl");
		houseShaderTexture.compileShaders(this.getClass(), gl);

		// BillBoards
		this.houseBbManager = new BillBoardDrawManager();
		GLBillBoard bb;
		bb = new GLBillBoard(this.getKonfiguration().getCamera(), "billboard/AC-Ezio_Render.png",
				GLVertex.addValues(housePos.clone(), 1.0f, 1.0f, 1.0f), 1.0f, 2.0f);
		houseBbManager.addBillBoard(bb);
		bb = new GLBillBoard(this.getKonfiguration().getCamera(), "billboard/AC-Ezio_Render.png",
				GLVertex.addValues(housePos.clone(), 3.0f, 1.0f, 3.0f), 1.0f, 2.0f);
		houseBbManager.addBillBoard(bb);

		// Explosion
		houseRectExplosion = new GLRectangle(GLVertex.add(housePos, new GLVertex(-4.5f, 2.0f, -5.5f)), 2f);
		houseRectExplosion.setColor(new GLVertex(1.0f, 1.0f, 1.0f, 1.0f));
		houseRectExplosion.setTexture(houseExplosionTexture);

		
		// Physic
		inObj = new LinkedList<GLObject>();
		inTexture = new LinkedList<GLObject>();
		outObj = new LinkedList<GLObject>();
		outTexture = new LinkedList<GLObject>();

		List<PhysicAnimation> list = new ArrayList<PhysicAnimation>();

		physicAni = new PhysicAnimation(this.getKonfiguration().getTime());
		this.addAnimation(physicAni);
		list.add(physicAni);

		GLHousePhysic houseP = new GLHousePhysic(list, new GLVertex(0.0f, 0.0f, 0.0f), 14, 8, 10, 6);
		J_Utility.initObj(houseP);
		houseP.getRoofLeft().setColor(Color.WHITE);
		houseP.getRoofRight().setColor(Color.WHITE);
		houseP.getRoofRidge().setColor(Color.WHITE);
		houseP.setTextureOuterWall(outerTexture, 0);
		houseP.setTextureInnerWall(innerTexture, 0);
		houseP.setTextureOuterRoof(outerRoofTexture, 0);
		houseP.setTextureInnerRoof(innerRoofTexture, 0);
		houseP.setTextureFloor(floorTexture, 0);		

		GLCuboidPhysic cube = new GLCuboidPhysic(GLVertex.addValues(houseP.getPosition().clone(), 0.0f, 2.0f, 0.0f),
				0.5f, 0.5f, 0.5f, 1.0f, 1.0f, 0.1f);
		cube.setInitialVelocity(new GLVertex(0.0f, 20.0f, 100.0f), new GLVertex(0.2f, 0.2f, 0.2f));
		physicAni.addPhysicable(cube);

		inTexture.addAll(houseP.getInnerPlanes());
		outTexture.addAll(houseP.getOuterPlanesWithTexture());
		outObj.addAll(houseP.getOuterPlanesWithOutTexture());

		for (int x = 0; x < 2; x++) {
			for (int y = 0; y < 2; y++) {
				for (int z = 0; z < 2; z++) {
					GLCuboidPhysic c = new GLCuboidPhysic(GLVertex.addValues(houseP.getPosition().clone(),
							3.0f + x * 0.5f, 2.0f + y * 0.5f, 3.0f + z * 0.5f), 0.4f, 0.4f, 0.4f, 2.0f, 1.0f, 0.65f);
					physicAni.addPhysicable(c);
					inObj.add(c);
				}				
			}			
		}

		shader = new ShaderProgram("vPhysicStandard.glsl", "fPhysicStandard.glsl");
		shader.compileShaders(this.getClass(), gl);

		shaderTexture = new ShaderProgram("vPhysicTexture.glsl", "fPhysicTexture.glsl");
		shaderTexture.compileShaders(this.getClass(), gl);

		physicLight = new GLPointLight(GL2.GL_LIGHT2, new GLVertex());
		physicLight.setUseColorMaterial(true);
		physicLight.setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		physicLight.setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		physicLight.setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		physicLight.setAttenuation(0.3f, 0.1f, 0.01f);

		box = new GLBoxPointLight(physicLight, houseP.getPosition().clone(), 0.2f, 2.5f);
		inObj.add(box);

		
		// Shadow
		shadowInShadow = new LinkedList<GLObject>();
		shadowOutObj = new LinkedList<GLObject>();

		house = new GLHouse(new GLVertex(0.0f, 0.0f, 0.0f), 14, 8, 10, 6);
		J_Utility.initObj(house);
		house.getRoofLeft().setColor(Color.WHITE);
		house.getRoofRight().setColor(Color.WHITE);
		house.getRoofRidge().setColor(Color.WHITE);
		house.setTextureOuterWall(shadowOuterTexture, 0);
		house.setTextureOuterRoof(shadowOuterRoofTexture, 0);

		house.setTextureFloor(shadowColorFloorTexture, 0);		
		house.setTextureFloor(shadowNormalFloorTexture, 1);		

		house.setTextureInnerWall(shadowColorWallTexture, 0);
		house.setTextureInnerWall(shadowNormalWallTexture, 1);
		house.setTextureInnerRoof(shadowColorWallTexture, 0);
		house.setTextureInnerRoof(shadowNormalWallTexture, 1);

		shadowInShadowBump = house.getInnerPlanes();
		shadowOutBump = house.getOuterPlanesWithTexture();
		shadowOutObj.addAll(house.getOuterPlanesWithOutTexture());

		shadowInShadowBump.add(J_Utility.createCube(new GLVertex(-4.0f, 0.5f, 1.0f), 90.0f, shadowColorObjTexture, shadowNormalObjTexture));
		shadowInShadowBump.add(J_Utility.createCube(new GLVertex(-4.0f, 1.5f, 0.0f), 90.0f, shadowColorObjTexture, shadowNormalObjTexture));
		shadowInShadowBump.add(J_Utility.createCube(new GLVertex(-4.0f, 0.5f, -1.0f), 90.0f, shadowColorObjTexture, shadowNormalObjTexture));
		shadowInShadowBump.add(J_Utility.createCube(new GLVertex(-4.0f, 1.5f, -2.0f), 90.0f, shadowColorObjTexture, shadowNormalObjTexture));
		shadowInShadowBump.add(J_Utility.createCube(new GLVertex(-4.0f, 0.5f, -3.0f), 90.0f, shadowColorObjTexture, shadowNormalObjTexture));
		shadowInShadowBump.add(J_Utility.createCube(new GLVertex(-4.0f, 2.5f, -1.0f), 90.0f, shadowColorObjTexture, shadowNormalObjTexture));

		GLWallWithHole wall = new GLWallWithHole(new GLVertex(2.0f, 3.0f, 2.0f),
				3.0f, 1.0f, 1.0f, 3.0f, 1.0f, 1.0f, 1.0f);
		wall.rotate(0.0f, 45.0f, 0.0f);
		J_Utility.initObj(wall);
		wall.setTexture(shadowColorObjTexture, 0); // TODO: Repeat-Modus ?!
		wall.setTexture(shadowNormalObjTexture, 1);
		shadowInShadowBump.add(wall);

//		PositionAnimation ani = new PositionAnimation(this.getKonfiguration().getTime(), wall);
//		ani.addInterpolation(new LinearInterpolation(wall.getPosition().clone(), new GLVertex(3.0f, 0.0f, 0.0f)), 4.0f);
//		ani.addInterpolation(new LinearInterpolation(ani.getActualEndPoint(), new GLVertex(0.0f, 0.0f, -5.0f)), 4.0f);
//		ani.addInterpolation(new SinusInterpolation(ani.getActualEndPoint(), new GLVertex(-3.0f, 0.0f, 5.0f),
//				new GLVertex(0.0f, 1.0f, 0.0f), 3), 4.0f);
//		ani.setLoop(true);
//		this.addAnimation(ani);

		for (GLObject o : shadowInShadowBump) {
			o.setShader(shadowShaderShadowBump);
		}

		shadowShaderShadow = new ShaderProgram("vShadowStandard.glsl", "fShadowStandard.glsl");
		shadowShaderShadow.compileShaders(this.getClass(), gl);

		shadowShaderShadowBump = new ShaderProgram("vShadowBump.glsl", "fShadowBump.glsl");
		shadowShaderShadowBump.compileShaders(this.getClass(), gl);

		shadowPointLight = new GLPointLight(GL2.GL_LIGHT3, new GLVertex());
		shadowPointLight.setUseColorMaterial(true);
		shadowPointLight.setAmbient(new GLVertex(0.2f, 0.2f, 0.2f));
		shadowPointLight.setDiffuse(new GLVertex(0.2f, 0.2f, 0.2f));
		shadowPointLight.setSpecular(new GLVertex(0.4f, 0.4f, 0.4f));
		shadowPointLight.setAttenuation(0.5f, 0.06f, 0.006f);
		
		this.setLightForShadowMapping(shadowSpotLight);

		box = new GLBoxPointLight(shadowPointLight, house.getPosition().clone(), 0.2f, 1.5f);
		shadowInShadow.add(box);

		shadowSpotLight = new GLSpotLight(GL2.GL_LIGHT4,
				new GLVertex(6.0f, 2.0f, 0.0f), new GLVertex(-6.0f, 1.0f, 0.0f));
		shadowSpotLight.setConstantAttenuation(0.2f);
		shadowSpotLight.setLinearAttenuation(0.03f);
		shadowSpotLight.setQuadraticAttenuation(0.003f);
		shadowSpotLight.setAmbient(new GLVertex(0.1f, 0.1f, 0.1f));
		shadowSpotLight.setDiffuse(new GLVertex(0.2f, 0.2f, 0.2f));
		shadowSpotLight.setSpecular(new GLVertex(0.2f, 0.2f, 0.2f));
		shadowSpotLight.setSpotCutoff(25.0f);
		this.addLight(shadowSpotLight);
		this.setLightForShadowMapping(shadowSpotLight);

		
		// End
		endPointLight = new GLPointLight(GL2.GL_LIGHT5, new GLVertex());
		endPointLight.setPosition(new GLVertex(0.0f, 2.0f, 2.0f));
		endPointLight.setAmbient(new GLVertex(0.2f, 0.2f, 0.2f));
		endPointLight.setDiffuse(new GLVertex(0.1f, 0.1f, 0.1f));
		endPointLight.setSpecular(new GLVertex(0.1f, 0.1f, 0.1f));
		endPointLight.setAttenuation(0.3f, 0.1f, 0.01f); // c, l, q

		endSpotLight = new GLSpotLight(GL2.GL_LIGHT3, new GLVertex(2.0f, 2.0f, 2.0f),
				new GLVertex(-1.0f, -0.1f, 0.0f));
		endSpotLight.setSpotCutoff(20.0f);
		endSpotLight.setAmbient(new GLVertex(0.1f, 0.1f, 0.1f));
		endSpotLight.setDiffuse(new GLVertex(0.3f, 0.3f, 0.3f));
		endSpotLight.setSpecular(new GLVertex(0.2f, 0.2f, 0.2f));

		endInObj = new LinkedList<GLObject>();
		endInTexture = new LinkedList<GLObject>();
		endOutObj = new LinkedList<GLObject>();
		endOutTexture = new LinkedList<GLObject>();

		house = new GLHouse(new GLVertex(0.0f, 0.0f, 0.0f), 12, 6, 12, 6);
		J_Utility.initObj(house);
		house.getRoofLeft().setColor(Color.WHITE);
		house.getRoofRight().setColor(Color.WHITE);
		house.getRoofRidge().setColor(Color.WHITE);
		house.setTextureOuterWall(endOuterTexture, 0);
		house.setTextureInnerWall(endInnerTexture, 0);
		house.setTextureOuterRoof(endOuterRoofTexture, 0);
		house.setTextureInnerRoof(endInnerRoofTexture, 0);
		house.setTextureFloor(endFloorTexture, 0);

		endInTexture.addAll(house.getInnerPlanes());
		endOutTexture.addAll(house.getOuterPlanesWithTexture());
		endOutObj.addAll(house.getOuterPlanesWithOutTexture());

		color = new GLVertex(0.4f, 0.6f, 0.3f);
		endTable = new GLTable(new GLVertex(), 2.0f, 0.9f, 0.7f, 0.2f);
		endTable.setColor(color.clone());
		endInObj.add(endTable);
		chair = new GLChair(new GLVertex(1.5f, 0, 0), 0.4f, 0.4f, 0.5f, 0.65f);
		chair.setColor(color.clone());
		chair.rotate(0.0f, 270.0f, 0.0f);
		endInObj.add(chair);
		chair = new GLChair(new GLVertex(-0.5f, 0, -0.95f), 0.4f, 0.4f, 0.5f, 0.65f);
		chair.setColor(color.clone());
		chair.rotate(0.0f, 0.0f, 0.0f);
		endInObj.add(chair);

		pict = new GLPicture(house.getBack().getLeft().getRectRear().getAbsoluteCenter(),
				1.0f, 0.5f, 0.04f, endPictureTexture, 0);
		pict.translate(0.0f, 0.0f, 0.01f);
		endInObj.addAll(pict.getObjectsWithOutTexture());
		endInTexture.add(pict.getPicturePlane());

		endShader = new ShaderProgram("vEndStandard.glsl", "fEndStandard.glsl");
		endShader.compileShaders(this.getClass(), gl);

		endShaderToon = new ShaderProgram("vEndToon.glsl", "fEndToon.glsl");
		endShaderToon.compileShaders(this.getClass(), gl);

		endShaderTexture = new ShaderProgram("vEndTexture.glsl", "fEndTexture.glsl");
		endShaderTexture.compileShaders(this.getClass(), gl);

		endShaderReflection = new ShaderProgram("vEndReflection.glsl", "fEndReflection.glsl");
		endShaderReflection.compileShaders(this.getClass(), gl);

		endShaderReflectionToon = new ShaderProgram("vEndReflectionToon.glsl", "fEndReflectionToon.glsl");
		endShaderReflectionToon.compileShaders(this.getClass(), gl);

		GLCuboid help = new GLCuboid(new GLVertex(2.0f, 0.1f, 2.0f), 0.2f);
		help.setColor(new GLVertex(Color.YELLOW));
		endInObj.add(help);

		endP = new WaterParticleSystem(50);

		
		// Outer
		outerDirectLight = new GLDirectionLight(GL2.GL_LIGHT7, new GLVertex(2.0f, -1.0f, 2.0f));
		outerDirectLight.setUseColorMaterial(true);
		outerDirectLight.setAmbient(new GLVertex(0.2f, 0.2f, 0.2f));
		outerDirectLight.setDiffuse(new GLVertex(0.4f, 0.4f, 0.4f));
		outerDirectLight.setSpecular(new GLVertex(0.2f, 0.2f, 0.2f));

		outerShader = new ShaderProgram("vOuterHeight.glsl", "fOuterHeight.glsl");
		outerShader.compileShaders(this.getClass(), gl);

		outerHeightMap = new GLHeightMapTriangleStrip(50);
		outerHeightMap.setColor(new GLVertex(1,1,1));

		GLTexture2D texHeightMap = new GLTexture2D(outerTexFileHeightMap);
		outerHeightMap.setTexture(texHeightMap);

		GLTexture2D texMountain = new GLTexture2D(outerTexFileMountain);
		outerHeightMap.setTexture(texMountain, 2);

		GLTexture2D texLand = new GLTexture2D(outerTexFileLand);
		outerHeightMap.setTexture(texLand, 3);

		GLTexture2D texWater = new GLTexture2D(outerTexFileWater);
		outerHeightMap.setTexture(texWater, 4);
		

		//-----------
		TimeIntervallSender sender = new TimeIntervallSender(this.getKonfiguration().getTime(), 15.0f);
		sender.addTimeIntervallObserver(this);
	}

	@Override
	protected void initFrame() {
		super.initFrame();
		if (scene == 0) { // house
			this.setDrawShadow(false);
			physicAni.pauseAnimation();
		}
		if (scene == 1) { // physik
			this.setDrawShadow(false);
			physicAni.resumeAnimation();
		}
		if (scene == 2) { // shadow
			this.setDrawShadow(true);
			physicAni.pauseAnimation();
		}
		if (scene == 3) { // shadow
			this.setDrawShadow(false);
			physicAni.pauseAnimation();
		}
		if (scene == 4) { // outer
			this.setDrawShadow(false);
			physicAni.pauseAnimation();
		}
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);

		if (scene == 0) {
			outerBeginGL(gl);
		}
		if (scene == 1) {
			houseBeginGL(gl);
		}
		if (scene == 2) {
			physicBeginGL(gl);
		}
		if (scene == 3) {
			shadowBeginGL(gl);
		}
		if (scene == 4) {
			endeBeginGL(gl);
		}
	}

	private void outerBeginGL(GL2 gl) {
		outerDirectLight.beginGL(gl);

		outerShader.useProgram(gl);
		outerShader.setTextureUnit(gl, "texHeightMap", 0);
		outerShader.setTextureUnit(gl, "texMountain", 2);
		outerShader.setTextureUnit(gl, "texLand", 3);
		outerShader.setTextureUnit(gl, "texWater", 4);

		outerHeightMap.glScaleNow(10, 4, 10); // 30, 10, 30
		outerHeightMap.draw(gl);

		ShaderProgram.clearShaderProgram(gl);
	}

	private void houseBeginGL(GL2 gl) {
		housePointLight.beginGL(gl);
		houseSpot.beginGL(gl);

		houseShaderTexture.useProgram(gl);
		houseShaderTexture.setTextureUnit(gl, "texUnit", 0);
		for (GLObject o : houseInTexture) {
			o.draw(gl);
		}
		for (GLObject o : houseOutTexture) {
			o.draw(gl);
		}

		houseShader.useProgram(gl);
		for (GLObject o : houseInObj) {
			o.draw(gl);
		}
		for (GLObject o : houseOutObj) {
			o.draw(gl);
		}
		gl.glColor3f(1.0f, 0.5f, 0.5f);
		J_Utility.renderTeaPot(gl, houseTable.getTableDesk().getRectTop().getAbsoluteCenter(), 270.0f, houseM);

		ShaderProgram.clearShaderProgram(gl);

		// Explosion
		float picsPerSecond = 20;
		float time = this.getKonfiguration().getTime().getTime();
		int texCols = houseExplosionTexture.getTexGrid().getColCount();
		int texRows = houseExplosionTexture.getTexGrid().getRowCount();
		int texNr = Math.round(time * picsPerSecond) % (texCols*texRows);
		int texCol = texNr % texCols;
		int texRow = texNr / texRows;
		houseRectExplosion.setTexRect(texCol, texRow);

		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glDepthMask(false);
		
		houseRectExplosion.draw(gl);
		
		gl.glDepthMask(true);
		gl.glDisable(GL2.GL_BLEND);		
	}

	private void physicBeginGL(GL2 gl) {
		physicLight.beginGL(gl);
		shaderTexture.useProgram(gl);
		shaderTexture.setTextureUnit(gl, "texUnit", 0);
		for (GLObject o : inTexture) {
			o.draw(gl);
		}
		for (GLObject o : outTexture) {
			o.draw(gl);
		}

		shader.useProgram(gl);
		for (GLObject o : inObj) {
			o.draw(gl);
		}
		for (GLObject o : outObj) {
			o.draw(gl);
		}
		ShaderProgram.clearShaderProgram(gl);		
	}

	private void shadowBeginGL(GL2 gl) {
		shadowSpotLight.beginGL(gl);
		shadowPointLight.beginGL(gl);

		shadowShaderShadowBump.useProgram(gl);
		shadowShaderShadowBump.setTextureUnit(gl, "renderTexture", 0);
		shadowShaderShadowBump.setTextureUnit(gl, "normalTexture", 1);
		shadowShaderShadowBump.setTextureUnit(gl, "shadowMap", 2);
		for (GLObject o : shadowInShadowBump) {
			o.draw(gl);
		}
		for (GLObject o : shadowOutBump) {
			o.draw(gl);
		}

		shadowShaderShadow.useProgram(gl);
		shadowShaderShadow.setTextureUnit(gl, "shadowMap", 2);
		for (GLObject o : shadowInShadow) {
			o.draw(gl);
		}

//		for (GLObject o : shadowOutObj) { // TODO!!
//			o.draw(gl);
//		}

		ShaderProgram.clearShaderProgram(gl);		
	}

	private void endeBeginGL(GL2 gl) {
		endPointLight.beginGL(gl);
		endSpotLight.beginGL(gl);

		endShaderTexture.useProgram(gl);
		endShaderTexture.setTextureUnit(gl, "texUnit", 0);
		for (GLObject o : endInTexture) {
			o.draw(gl);
		}
		for (GLObject o : endOutTexture) {
			o.draw(gl);
		}
		ShaderProgram.clearShaderProgram(gl);


		if (endToon) {
			endShaderToon.useProgram(gl);
		} else {
			endShader.useProgram(gl);
		}
		for (GLObject o : endInObj) {
			o.draw(gl);
		}
		for (GLObject o : endOutObj) {
			o.draw(gl);
		}
		gl.glColor3f(1.0f, 0.5f, 0.5f);
		J_Utility.renderTeaPot(gl, endTable.getTableDesk().getRectTop().getAbsoluteCenter(), 270.0f, 1.0f);

		gl.glPushMatrix();
			gl.glTranslatef(2.0f, 0.0f, 2.0f);
			gl.glScalef(20.0f, 20.0f, 20.0f);
			endP.beginGL(gl);
		gl.glPopMatrix();		// TODO: Spotlight ?!

		// Reflection Mapping Start
		if (endToon) {
			endShaderReflectionToon.useProgram(gl);
			endShaderReflectionToon.setTextureUnit(gl, "textureCubeMap", 0);			
		} else {
			endShaderReflection.useProgram(gl);
			endShaderReflection.setTextureUnit(gl, "textureCubeMap", 0);
		}

		gl.glEnable(GL2.GL_TEXTURE_CUBE_MAP);
		endTextureCube.bind(gl);

		GLUT glut = new GLUT();
		gl.glPushMatrix();
			gl.glTranslatef(0.0f, 3.0f, 0.0f);
			glut.glutSolidSphere(1, 100, 100);
		gl.glPopMatrix();		

		gl.glDisable(GL2.GL_TEXTURE_CUBE_MAP);
		// Reflection Mapping Ende

		ShaderProgram.clearShaderProgram(gl);
	}

	@Override
	public void endGL(GL2 gl) {
		if (scene == 0) {
			outerEndGL(gl);			
		}
		if (scene == 1) {
			houseEndGL(gl);
		}
		if (scene == 2) {
			physicEndGL(gl);
		}
		if (scene == 3) {
			shadowEndGL(gl);
		}
		if (scene == 4) {
			endeEndGL(gl);
		}
		super.endGL(gl);
	}

	private void outerEndGL(GL2 gl) {
		
	}

	private void houseEndGL(GL2 gl) {
		houseShaderTexture.useProgram(gl);
		houseShaderTexture.setTextureUnit(gl, "texUnit", 0);
		houseBbManager.drawBillBoards(gl);
		ShaderProgram.clearShaderProgram(gl);
	}

	private void physicEndGL(GL2 gl) {
		
	}

	private void shadowEndGL(GL2 gl) {
		
	}

	private void endeEndGL(GL2 gl) {
		
	}

	@Override
	public void updateTimeIntervall(TimeIntervallObservable changed) {
		physicAni.stopAnimation();
		physicAni.startAnimation();

		endToon = ! endToon;
	}

	@Override
	protected void drawShadowRendering(GL2 gl) {
		super.drawShadowRendering(gl);

		for (GLObject o : shadowInShadowBump) {
			o.draw(gl);
		}
		for (GLObject o : shadowInShadow) {
			o.draw(gl);
		}
	}

	@Override
	public void cameraMode1() {
		super.cameraMode1();
		scene = 1;
		this.getKonfiguration().getCamera().setPosition(new GLVertex());
	}

	@Override
	public void cameraMode2() {
		super.cameraMode2();
		scene = 2;
		this.getKonfiguration().getCamera().setPosition(new GLVertex());
	}

	@Override
	public void cameraMode3() {
		super.cameraMode3();
		scene = 3;
		this.getKonfiguration().getCamera().setPosition(new GLVertex());
	}

	@Override
	public void cameraMode4() {
		super.cameraMode4();
		scene = 4;
		this.getKonfiguration().getCamera().setPosition(new GLVertex());
	}

	@Override
	public void keyO() {
		super.keyO();
		scene = 0;
		this.getKonfiguration().getCamera().setPosition(new GLVertex());
	}

	// -------------

}
