package johannes.old;

import java.util.List;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.animation.AbstractAnimation;
import falk_guenther_meier.model.light.GLAbstractLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.world.JoglAppShadowMapping;

/**
 * @author Meier
 *
 */
public abstract class MiniScene {
	private List<GLAbstractLight> lights;
	private List<AbstractAnimation> animations;
	private JoglAppShadowMapping app;

	public abstract void constructor();
	public abstract void init(GL2 gl);
	public abstract void beginGL(GL2 gl);
	public abstract void endGL(GL2 gl);

	public abstract GLVertex getPosition();
	public abstract boolean isCameraInScene();

	public List<GLAbstractLight> getLights() {
		return lights;
	}
	public List<AbstractAnimation> getAnimations() {
		return animations;
	}
	public void setApp(JoglAppShadowMapping app) {
		this.app = app;
	}

	
}
