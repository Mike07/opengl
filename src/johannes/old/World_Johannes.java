package johannes.old;

import java.awt.Color;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.animation.PositionAnimation;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.interpolation.LinearInterpolation;
import falk_guenther_meier.model.light.GLSpotLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.world.JoglAppShadowMapping;
import falk_guenther_meier.util.Utility;

/**
 * Welt / Szene
 * @author Johannes
 */
public class World_Johannes extends JoglAppShadowMapping {
	private GLOpenRoom room;
	private GLCuboid cubeBig;
	private GLCuboid cubeLittle;
	private GLCuboid plane;
	private ShaderProgram shadowShader;
	private ShaderProgram standardShader;
	private GLCustomComplexObject spheres;

	public static void main(String[] args) {
    	new World_Johannes();
	}

	public World_Johannes() {
		super("Einzelprojekt Johannes", 800, 600, true);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();

		GLVertex lightPos = new GLVertex(2.0f, 10.0f, 0.0f);
		GLSpotLight spotLight = new GLSpotLight(GL2.GL_LIGHT0, lightPos, GLVertex.invert(lightPos));

		this.setLightForShadowMapping(spotLight);
		this.addLight(spotLight);

		room = new GLOpenRoom(new GLVertex(0.0f, -10.0f, 0.0f), 30.0f, 20.0f, 2.0f, 0.5f);
		room.setColor(Color.GREEN);

		cubeBig = new GLCuboid(new GLVertex(10.0f, -0.0f, 0.0f), 2.0f);
		cubeBig.setColor(Color.BLUE);

		cubeLittle = new GLCuboid(new GLVertex(0.0f, -3.0f, 1.0f), 0.5f);
		cubeLittle.setColor(Color.YELLOW);

		plane = new GLCuboid(new GLVertex(0.0f, -1.5f, 0.0f), 10.0f, 0.5f, 10.0f);
		plane.setColor(Color.BLACK);

		PositionAnimation ani = new PositionAnimation(this.konfiguration.getTime(), this.cubeBig);
		ani.addInterpolation(new LinearInterpolation(cubeBig.getPosition().clone(), new GLVertex(0.0f, 5.0f, 0.0f), false), 6.0f);
		ani.setLoop(true);
		this.addAnimation(ani);

		ani = new PositionAnimation(this.konfiguration.getTime(), this.cubeLittle);
		ani.addInterpolation(new LinearInterpolation(cubeLittle.getPosition().clone(), new GLVertex(0.0f, 8.0f, 0.0f), false), 3.0f);
		ani.addInterpolation(new LinearInterpolation(ani.getActuallEndPoint(), new GLVertex(0.0f, -8.0f, 0.0f), false), 3.0f);
		ani.setLoop(true);
		this.addAnimation(ani);

		this.shadowShader = new ShaderProgram("vertexShadow.glsl", "fragmentShadow.glsl");
		this.shadowShader.compileShaders(this.getClass(), gl);

		this.standardShader = new ShaderProgram("vertexStandard.glsl", "fragmentStandard.glsl");
		this.standardShader.compileShaders(this.getClass(), gl);

		this.spheres = new GLCustomComplexObject();
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				GLObject o = new GLCuboid(new GLVertex(i, 0, j), 0.578f);
				Utility.setRandomColor(o);
				this.spheres.add(o);
			}
		}
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);

		if (this.isDrawShadow()) {
			this.shadowShader.useProgram(gl);
			this.shadowShader.setTextureUnit(gl, "shadowMap2", 2);
		} else {
			this.standardShader.useProgram(gl);
		}

		this.spheres.draw(gl);
		this.room.draw(gl);
		this.cubeBig.draw(gl);
		this.cubeLittle.draw(gl);
		this.plane.draw(gl);

		ShaderProgram.clearShaderProgram(gl);
	}

	@Override
	protected void drawShadowRendering(GL2 gl) {
		super.drawShadowRendering(gl);

		this.spheres.draw(gl);
		this.room.draw(gl);
		this.cubeBig.draw(gl);
		this.cubeLittle.draw(gl);
		this.plane.draw(gl);
	}

	@Override
	protected void initFrame() {
		super.initFrame();
	}
}
