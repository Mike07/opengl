// Aufgaben 33, 35
varying vec3 position;
varying vec3 normal;

void main() {	
	vec4 position2 = gl_ModelViewMatrix * gl_Vertex;
	position = position2.xyz / position2.w;

	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	normal = gl_Normal;
}
