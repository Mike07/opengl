varying vec3 normal;
varying vec3 position;
varying vec4 texCoordShadow;

void main() {
	normal =  gl_NormalMatrix * gl_Normal;

	vec4 position2 = gl_ModelViewMatrix * gl_Vertex;
	position = position2.xyz / position2.w;

	texCoordShadow =  gl_TextureMatrix[5] * gl_ModelViewMatrix * gl_Vertex;
	texCoordShadow = texCoordShadow / (texCoordShadow.w);

	gl_Position =  gl_ModelViewProjectionMatrix * gl_Vertex;
}
