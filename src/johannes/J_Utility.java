package johannes;

import java.awt.Color;

import javax.media.opengl.GL2;

import com.jogamp.opengl.util.gl2.GLUT;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.textures.GLTexture2D;

public class J_Utility {
	public static void initObj(GLObject o) {
		o.setColor(new GLVertex(1.0f, 1.0f, 1.0f));
		o.setMaterial(GLMaterial.DEFAULT_OWN);
	}

	public static void renderTeaPot(GL2 gl, GLVertex pos, float yRotate, float m) {
		GLUT glut = new GLUT();
		gl.glPushMatrix();
			gl.glTranslatef(pos.getX(), pos.getY(), pos.getZ());
			gl.glRotatef(-90.0f, 1, 0, 0);
			gl.glRotatef(yRotate, 0, 0, 1);
			GLMaterial.JADE.toGL(gl);
			glut.glutSolidTeapot(0.1f * m, false);
		gl.glPopMatrix();		
	}

	public static GLCuboid createCube(GLVertex pos, float yRotate, GLTexture2D color, GLTexture2D normal) {
		GLCuboid result = new GLCuboid(pos, 1.0f);
		result.setColor(Color.WHITE);
		result.setMaterial(GLMaterial.createTextureMaterial());
		result.setTexture(color, 0);
		result.setTexture(normal, 1);
		result.rotate(0.0f, yRotate, 0.0f);
		return result;
	}
}
