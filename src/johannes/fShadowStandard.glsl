varying vec3 normal;
varying vec3 position;
varying vec4 texCoordShadow;
uniform sampler2D shadowMap;

vec4 ambientColor(int nr, vec4 front) // ambient
{
	return front * gl_LightSource[nr].ambient + front * gl_LightModel.ambient;
}

vec4 diffuseColor(int nr, vec3 direction, float diffuseIntensity, vec4 front) // diffuse
{
	return front * diffuseIntensity * gl_LightSource[nr].diffuse;
}

vec4 specularColor(int nr, vec3 direction, float diffuseIntensity, vec3 normal, vec4 front, float shininess) // specular
{
	// "reflect"-Operator: Phong-Modell (ohne Blinn (Blinn-Phong==Phnong vereinfacht)!!)
	vec3 reflection = normalize(reflect( - normalize(direction), normal));
	float specAngle = max(0.0, dot(normal, reflection));

	vec4 sp = vec4(0.0);
	if (diffuseIntensity != 0.0) {
		float specularValue = pow(specAngle, shininess); // / (direction * direction)
		sp += gl_LightSource[nr].specular * front * specularValue; // specular
	}
	return sp;
}

vec4 phong(int nr, vec3 direction, vec3 normal, float shadow) // Hilfsmethode: Licht und Material
{
	float inten = max(0.0, dot(normal, normalize(direction)));
	vec4 color = shadow * diffuseColor(nr, direction, inten, gl_FrontMaterial.diffuse);
	color += ambientColor(nr, gl_FrontMaterial.ambient);
	color += shadow * specularColor(nr, direction, inten, normal, gl_FrontMaterial.specular, gl_FrontMaterial.shininess);
	return color;
}

vec4 phongTexture(int nr, vec3 direction, vec3 normal, vec4 textureColor, float shadow) // Hilfsmethode: Licht und Textur
{
	float inten = max(0.0, dot(normal, normalize(direction)));
	vec4 color = shadow * diffuseColor(nr, direction, inten, textureColor);
	color += ambientColor(nr, textureColor);
	color += shadow * specularColor(nr, direction, inten, normal, textureColor, 0.6); // TODO: shininess ?! vec4(1.0) ??
	return color;
}


vec4 directionalLight(int nr, vec3 normal) // ambient, diffuse, specular of a directional light
{
	return phong(nr, - gl_LightSource[nr].position.xyz, normal, 1.0);
}

vec4 directionalLightTexture(int nr, vec3 normal, vec4 textureColor)
{
	return phongTexture(nr, - gl_LightSource[nr].position.xyz, normal, textureColor, 1.0);
}

vec4 pointLight(int nr, vec3 normal, float shadow) // ambient, diffuse, specular of a point light
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// color
	vec4 color = phong(nr, direction, normal, shadow);
	// attenuation
	float dist = length(direction); // Abstand zwischen Lichtquelle und Vertex
	color *= 1.0 / (gl_LightSource[nr].constantAttenuation + gl_LightSource[nr].linearAttenuation * dist
		+ gl_LightSource[nr].quadraticAttenuation * dist * dist); // Abschwächung einrechnen
	return color;
}

vec4 pointLightTexture(int nr, vec3 normal, vec4 textureColor, float shadow)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// color
	vec4 color = phongTexture(nr, direction, normal, textureColor, shadow);
	// attenuation
	float dist = length(direction); // Abstand zwischen Lichtquelle und Vertex
	color *= 1.0 / (gl_LightSource[nr].constantAttenuation + gl_LightSource[nr].linearAttenuation * dist
		+ gl_LightSource[nr].quadraticAttenuation * dist * dist); // Abschwächung einrechnen
	return color;
}

vec4 spotLight(int nr, vec3 normal, float shadow)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// spot calculation
	vec3 actDir = normalize(direction).xyz;
	float attenuation = 0.0;
	float spotEffect = dot(normalize(gl_LightSource[nr].spotDirection), - actDir);
	if (spotEffect > gl_LightSource[nr].spotCosCutoff) {
		// TODO: zwei verschiedene Varianten ?!
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent); // Intensität berechnen
		//attenuation = pow(spotEffect, gl_LightSource[nr].spotExponent); // Intensität berechnen
	}
	// color
	return attenuation * pointLight(nr, normal, shadow); // Abschwächung ist hier bereits eingerechnet!
}

vec4 spotLightTexture(int nr, vec3 normal, vec4 textureColor, float shadow)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// spot calculation
	vec3 actDir = normalize(direction).xyz;
	float attenuation = 0.0;
	float spotEffect = dot(normalize(gl_LightSource[nr].spotDirection), - actDir);
	if (spotEffect > gl_LightSource[nr].spotCosCutoff) {
		// TODO: zwei verschiedene Varianten ?!
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent); // Intensität berechnen
		//attenuation = pow(spotEffect, gl_LightSource[nr].spotExponent); // Intensität berechnen
	}
	// color
	return attenuation * pointLightTexture(nr, normal, textureColor, shadow); // Abschwächung ist hier bereits eingerechnet!
}

vec4 spotLightDX(int nr, vec3 normal, float shadow)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	vec3 actDir = normalize(direction).xyz;
	// falloff factor calculation
		// aktueller Winkel zwischen Lichtrichtung und Vektor zwischen Objekt und Lichtquelle
	float cos_cur_angle = dot(normalize(gl_LightSource[nr].spotDirection), - actDir);
		// halber Strahlwinkel des Spotlights
	float cos_inner_cone_angle = gl_LightSource[nr].spotCosCutoff;
		// halber Winkel des inneren nicht abgeschwächten Bereichs (Kosinus-Wert) 
	float cos_outer_cone_angle = sqrt((1.0 + cos_inner_cone_angle) / 2.0); // entspricht dem halben Spot-Winkel
		// Abschwächungs-Faktor
	float fallOff = 1.0 - clamp((cos_cur_angle - cos_outer_cone_angle) / (cos_inner_cone_angle - cos_outer_cone_angle), 0.0, 1.0);
	// spot calculation
	float attenuation = 0.0;
	if (cos_cur_angle > cos_inner_cone_angle) { // liegt das Objekt innerhalb des Spot-Kegels?
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent);
	}
	// color calculation
	return attenuation * pointLight(nr, normal, shadow) * fallOff;
}

vec4 spotLightDXTexture(int nr, vec3 normal, vec4 textureColor, float shadow)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	vec3 actDir = normalize(direction).xyz;
	// falloff factor calculation
		// aktueller Winkel zwischen Lichtrichtung und Vektor zwischen Objekt und Lichtquelle
	float cos_cur_angle = dot(normalize(gl_LightSource[nr].spotDirection), - actDir);
		// halber Strahlwinkel des Spotlights
	float cos_inner_cone_angle = gl_LightSource[nr].spotCosCutoff;
		// halber Winkel des inneren nicht abgeschwächten Bereichs (Kosinus-Wert) 
	float cos_outer_cone_angle = sqrt((1.0 + cos_inner_cone_angle) / 2.0); // entspricht dem halben Spot-Winkel
		// Abschwächungs-Faktor
	float fallOff = 1.0 - clamp((cos_cur_angle - cos_outer_cone_angle) / (cos_inner_cone_angle - cos_outer_cone_angle), 0.0, 1.0);
	// spot calculation
	float attenuation = 0.0;
	if (cos_cur_angle > cos_inner_cone_angle) { // liegt das Objekt innerhalb des Spot-Kegels?
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent);
	}
	// color calculation
	return attenuation * pointLightTexture(nr, normal, textureColor, shadow) * fallOff;
}

float shadow() { // 1.0: kein Schatten ; 0.0: Schatten
	float tmp1 = texture2DProj(shadowMap, texCoordShadow).r;
	float tmp = 0.0;
	tmp = tmp1 >= texCoordShadow.z ? 1.0 : 0.0;
	return tmp;
}

void main() {
	// lighting
	vec3 n = normalize(normal);

	// shadow mapping
	float tmp = shadow();
	

	// spot light
	vec4 color = spotLight(4, n, tmp);
	color += pointLight(3, n, 1.0);

	gl_FragColor = color;
}
