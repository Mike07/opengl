package johannes.test;

import java.awt.Color;
import java.io.File;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.buildings.GLHouse;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.light.GLDirectionLight;
import falk_guenther_meier.model.light.GLSpotLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.Utility;

public class House_Johannes extends JoglApp {
	private GLComplexObject room;
	private GLHouse house;
	private GLTexture2D outerTexture;
	private GLTexture2D innerTexture;
	private GLTexture2D outerRoofTexture;
	private GLTexture2D innerRoofTexture;
	private ShaderProgram shader;
	private ShaderProgram shaderTexture;
	private int unit;
	private GLCuboid help;

	public House_Johannes(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
//		File textureFile = FileLoader.getFileFromFilesFolder("texture/bretter_small.jpg");
		File textureFile = FileLoader.getFileFromFilesFolder("texture/Mauertextur_001.jpg");
		outerTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Holztextur_1.jpg");
		innerTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Dachtextur_2.jpg");
		outerRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Holztextur_3.jpg");
		innerRoofTexture = new GLTexture2D(textureFile);
		unit = 0;
		start();
	}
	
	public static void main(String[] args) {
		new House_Johannes("House Test Johannes", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();

		this.getKonfiguration().getCamera().setPosition(new GLVertex(5.5f, 1.5f, 5.5f));
		this.getKonfiguration().getLight().setPosition(new GLVertex(0.0f, 2.0f, 2.0f));
		this.getKonfiguration().getLight().setUseColorMaterial(true);
		this.getKonfiguration().getLight().setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setAttenuation(0.3f, 0.1f, 0.01f); // c, l, q
		
		GLSpotLight spot = new GLSpotLight(GL2.GL_LIGHT3, new GLVertex(2.0f, 2.0f, 2.0f),
		new GLVertex(-1.0f, -0.1f, 0.0f));
		spot.setSpotCutoff(20.0f);
		spot.setSpotExponent(0.9f);
		spot.setUseColorMaterial(true);
		spot.setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		spot.setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		spot.setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		this.addLight(spot);
		
		GLDirectionLight direct = new GLDirectionLight(GL2.GL_LIGHT5, new GLVertex(1.0f, -1.0f, 0.0f));
		direct.setUseColorMaterial(true);
		direct.setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		direct.setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		direct.setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		this.addLight(direct);

		room = new GLOpenRoom(new GLVertex(), 20.0f, 15.0f, 2.5f, 0.04f);
		room.setColor(new GLVertex());
		room.setMaterial(new GLMaterial());
		room.rotate(0.0f, 45.0f, 0.0f);
		Utility.setRandomColor(room);
		
		house = new GLHouse(new GLVertex(), 2.0f, 1.0f, 2.0f, 1.0f);
		house.setMaterial(new GLMaterial());
		house.getRoofLeft().setColor(Color.WHITE);
		house.getRoofRight().setColor(Color.WHITE);
		house.getRoofRidge().setColor(Color.WHITE);
		house.setTextureOuterWall(outerTexture, unit);
		house.setTextureInnerWall(innerTexture, unit);
		house.setTextureOuterRoof(outerRoofTexture, unit);
		house.setTextureInnerRoof(innerRoofTexture, unit);

		shader = new ShaderProgram("vertexHouse.glsl", "fragmentHouse.glsl");
		shader.compileShaders(this.getClass(), gl);

		shaderTexture = new ShaderProgram("vertexHouseTexture.glsl", "fragmentHouseTexture.glsl");
		shaderTexture.compileShaders(this.getClass(), gl);

		help = new GLCuboid(new GLVertex(
				GLVertex.add(house.getRoofRidge().getRectRight().getNodes()[1], house.getPosition())), 0.05f);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
//		shaderTexture.useProgram(gl);
//		gl.glActiveTexture(GL2.GL_TEXTURE0); // 0: ist standardm��ig aktiviert
//		shader.setTextureUnit(gl, "texUnit", unit);
		house.draw(gl);
//		ShaderProgram.clearShaderProgram(gl);

//		help.draw(gl);

//		shader.useProgram(gl);
		room.draw(gl);
//		ShaderProgram.clearShaderProgram(gl);
	}
}
