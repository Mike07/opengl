uniform sampler2D texHeightMap;
uniform sampler2D texMountain;
uniform sampler2D texLand;
uniform sampler2D texWater;

varying vec3 position;
varying vec3 normal;

vec4 ambientColor(int nr, vec4 front) // ambient
{
	return front * gl_LightSource[nr].ambient + front * gl_LightModel.ambient;
}

vec4 diffuseColor(int nr, vec3 direction, float diffuseIntensity, vec4 front) // diffuse
{
	return front * diffuseIntensity * gl_LightSource[nr].diffuse;
}

vec4 specularColor(int nr, vec3 direction, float diffuseIntensity, vec3 normal, vec4 front, float shininess) // specular
{
	// "reflect"-Operator: Phong-Modell (ohne Blinn (Blinn-Phong==Phnong vereinfacht)!!)
	vec3 reflection = normalize(reflect( - normalize(direction), normal));
	float specAngle = max(0.0, dot(normal, reflection));

	vec4 sp = vec4(0.0);
	if (diffuseIntensity != 0.0) {
		float specularValue = pow(specAngle, shininess); // / (direction * direction)
		sp += gl_LightSource[nr].specular * front * specularValue; // specular
	}
	return sp;
}

vec4 phong(int nr, vec3 direction, vec3 normal) // Hilfsmethode: Licht und Material
{
	float inten = max(0.0, dot(normal, normalize(direction)));
	vec4 color = diffuseColor(nr, direction, inten, gl_FrontMaterial.diffuse);
	color += ambientColor(nr, gl_FrontMaterial.ambient);
	color += specularColor(nr, direction, inten, normal, gl_FrontMaterial.specular, gl_FrontMaterial.shininess);
	return color;
}

vec4 phongTexture(int nr, vec3 direction, vec3 normal, vec4 textureColor) // Hilfsmethode: Licht und Textur
{
	float inten = max(0.0, dot(normal, normalize(direction)));
	vec4 color = diffuseColor(nr, direction, inten, textureColor);
	color += ambientColor(nr, textureColor);
	color += specularColor(nr, direction, inten, normal, textureColor, 0.6); // TODO: shininess ?! vec4(1.0) ??
	return color;
}


vec4 directionalLight(int nr, vec3 normal) // ambient, diffuse, specular of a directional light
{
	return phong(nr, - gl_LightSource[nr].position.xyz, normal);
}

vec4 directionalLightTexture(int nr, vec3 normal, vec4 textureColor)
{
	return phongTexture(nr, - gl_LightSource[nr].position.xyz, normal, textureColor);
}

vec4 pointLight(int nr, vec3 normal) // ambient, diffuse, specular of a point light
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// color
	vec4 color = phong(nr, direction, normal);
	// attenuation
	float dist = length(direction); // Abstand zwischen Lichtquelle und Vertex
	color *= 1.0 / (gl_LightSource[nr].constantAttenuation + gl_LightSource[nr].linearAttenuation * dist
		+ gl_LightSource[nr].quadraticAttenuation * dist * dist); // Abschwächung einrechnen
	return color;
}

vec4 pointLightTexture(int nr, vec3 normal, vec4 textureColor)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// color
	vec4 color = phongTexture(nr, direction, normal, textureColor);
	// attenuation
	float dist = length(direction); // Abstand zwischen Lichtquelle und Vertex
	color *= 1.0 / (gl_LightSource[nr].constantAttenuation + gl_LightSource[nr].linearAttenuation * dist
		+ gl_LightSource[nr].quadraticAttenuation * dist * dist); // Abschwächung einrechnen
	return color;
}

vec4 spotLight(int nr, vec3 normal)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// spot calculation
	vec3 actDir = normalize(direction).xyz;
	float attenuation = 0.0;
	float spotEffect = dot(normalize(gl_LightSource[nr].spotDirection), - actDir);
	if (spotEffect > gl_LightSource[nr].spotCosCutoff) {
		// TODO: zwei verschiedene Varianten ?!
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent); // Intensität berechnen
		//attenuation = pow(spotEffect, gl_LightSource[nr].spotExponent); // Intensität berechnen
	}
	// color
	return attenuation * pointLight(nr, normal); // Abschwächung ist hier bereits eingerechnet!
}

vec4 spotLightTexture(int nr, vec3 normal, vec4 textureColor)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// spot calculation
	vec3 actDir = normalize(direction).xyz;
	float attenuation = 0.0;
	float spotEffect = dot(normalize(gl_LightSource[nr].spotDirection), - actDir);
	if (spotEffect > gl_LightSource[nr].spotCosCutoff) {
		// TODO: zwei verschiedene Varianten ?!
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent); // Intensität berechnen
		//attenuation = pow(spotEffect, gl_LightSource[nr].spotExponent); // Intensität berechnen
	}
	// color
	return attenuation * pointLightTexture(nr, normal, textureColor); // Abschwächung ist hier bereits eingerechnet!
}

vec4 spotLightDX(int nr, vec3 normal)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	vec3 actDir = normalize(direction).xyz;
	// falloff factor calculation
		// aktueller Winkel zwischen Lichtrichtung und Vektor zwischen Objekt und Lichtquelle
	float cos_cur_angle = dot(normalize(gl_LightSource[nr].spotDirection), - actDir);
		// halber Strahlwinkel des Spotlights
	float cos_inner_cone_angle = gl_LightSource[nr].spotCosCutoff;
		// halber Winkel des inneren nicht abgeschwächten Bereichs (Kosinus-Wert) 
	float cos_outer_cone_angle = sqrt((1.0 + cos_inner_cone_angle) / 2.0); // entspricht dem halben Spot-Winkel
		// Abschwächungs-Faktor
	float fallOff = 1.0 - clamp((cos_cur_angle - cos_outer_cone_angle) / (cos_inner_cone_angle - cos_outer_cone_angle), 0.0, 1.0);
	// spot calculation
	float attenuation = 0.0;
	if (cos_cur_angle > cos_inner_cone_angle) { // liegt das Objekt innerhalb des Spot-Kegels?
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent);
	}
	// color calculation
	return attenuation * pointLight(nr, normal) * fallOff;
}

vec4 spotLightDXTexture(int nr, vec3 normal, vec4 textureColor)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	vec3 actDir = normalize(direction).xyz;
	// falloff factor calculation
		// aktueller Winkel zwischen Lichtrichtung und Vektor zwischen Objekt und Lichtquelle
	float cos_cur_angle = dot(normalize(gl_LightSource[nr].spotDirection), - actDir);
		// halber Strahlwinkel des Spotlights
	float cos_inner_cone_angle = gl_LightSource[nr].spotCosCutoff;
		// halber Winkel des inneren nicht abgeschwächten Bereichs (Kosinus-Wert) 
	float cos_outer_cone_angle = sqrt((1.0 + cos_inner_cone_angle) / 2.0); // entspricht dem halben Spot-Winkel
		// Abschwächungs-Faktor
	float fallOff = 1.0 - clamp((cos_cur_angle - cos_outer_cone_angle) / (cos_inner_cone_angle - cos_outer_cone_angle), 0.0, 1.0);
	// spot calculation
	float attenuation = 0.0;
	if (cos_cur_angle > cos_inner_cone_angle) { // liegt das Objekt innerhalb des Spot-Kegels?
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent);
	}
	// color calculation
	return attenuation * pointLightTexture(nr, normal, textureColor) * fallOff;
}

// Normalen ("normal"-Parameter) müssen im normalized übergeben werden!!
//

vec3 getNormal()
{
	vec2 texCoord = gl_TexCoord[0].st;
	float offset = 0.005;
    
    float texelLeft = texture2D(texHeightMap, texCoord + vec2(-offset, 0)).r;
    float texelRight = texture2D(texHeightMap, texCoord + vec2(offset, 0)).r;
    float texelUp = texture2D(texHeightMap, texCoord + vec2(0, -offset)).r;
    float texelBottom = texture2D(texHeightMap, texCoord + vec2(0, offset)).r;
    vec3 vRight = vec3(2.0*offset, texelRight - texelLeft, 0);
    vec3 vUp = vec3(0, texelUp - texelBottom, -2.0*offset);
    
    return normalize(cross(vRight,vUp));
}

float getHeight()
{
	return texture2D(texHeightMap, gl_TexCoord[0].st).r;
}

float getDiffuseIntensity() {
	vec3 normal = getNormal();
	vec3 lightPos = vec3(1.0, 1.0, 0.0);
	
	return max(0.3, dot(normal, normalize(lightPos))); // und min. 0.3 ambientes Licht
}

vec4 fog(vec4 color) {
	float l = length(position);
	l = min(l - 6.0, 9.0);
	l = max(l, 0.0);
	l = l / 9.0;
	vec4 fog = color;
	fog = fog + vec4(0.33, 0.33, 0.66, 1.0) * l;
	return fog;
}

void main() {
	vec2 texCoord = gl_TexCoord[0].st;

	// Texel-Farbe für die verschiedenen Texturen ermitteln
	vec4 texMountainColor = texture2D(texMountain, texCoord);
	vec4 texLandColor = texture2D(texLand, texCoord); // 2.0 * -> feinere Auflösung!!
	vec4 beachColor = vec4(1.0, 0.90, 0.65, 1.0); 
	vec4 texWaterColor = texture2D(texWater, texCoord);
//	texWaterColor = texWaterColor * vec4(0.2, 0.4, 1.0, 1.0) + 0.1; // Wasser blau einfärben
	texWaterColor = texWaterColor * vec4(0.7, 0.7, 1.0, 1.0);

	float texHeight = getHeight();

	// Texturen auf Ebenenen entsprechend der texHeight auswählen und mischen
	vec4 texColor = mix(texWaterColor, beachColor, smoothstep(0.32,0.38, texHeight)); // Übergang Water -> Beach
	texColor = mix(texColor, texLandColor, smoothstep(0.38,0.45, texHeight));        // Übergang Beach -> Land
	texColor = mix(texColor, texMountainColor, smoothstep(0.45,0.85, texHeight));     // Übergang Land -> Mountain

	vec4 color = directionalLightTexture(7, normalize(normal), texColor);
	gl_FragColor = fog(color);
//	gl_FragColor = color;
}
