package falk_guenther_meier.tasks;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.buildings.DemoLandscape;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.world.JoglApp;

public class Aufgabe14 extends JoglApp {

	GLCustomComplexObject complexObject = new GLCustomComplexObject();
	
	boolean keySpace = false;
	boolean keyEnter = false;
	
	GLVertex pos = new GLVertex(-1, -0.1f, -1);
	DemoLandscape land;
	GLPointLight light1;

	public Aufgabe14(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
		land = new DemoLandscape(pos);
		start();
	}

	public static void main(String[] args) {
		new Aufgabe14("Aufgabe 14", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		// ----- Punktlicht -----
		// Position, Punktlicht, da w=1
		GLVertex position = new GLVertex(-4.5f, 0.1f, -3.0f, 1.0f);
		GLVertex ambient = new GLVertex(0.6f, 0.6f, 0.8f, 1.0f);
		GLVertex diffuse = new GLVertex(0.6f, 0.6f, 1f, 1.0f);
		GLVertex specular = new GLVertex(0.8f, 0.8f, 0.8f, 1.0f);
		GLObject lightObject = new GLCuboid(position, 0.6f);
		lightObject.setColor(new GLVertex(1,1,1));
		light1 = new GLPointLight(GL2.GL_LIGHT1, lightObject, position, ambient, diffuse, specular);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		gl.glEnable(GL2.GL_CULL_FACE);

		// Licht an/aus
		if (!keySpace) {
			gl.glEnable(GL2.GL_LIGHTING);
			gl.glEnable(GL2.GL_LIGHT0);
			gl.glEnable(GL2.GL_LIGHT1);
			gl.glEnable(GL2.GL_LIGHT2);
		}
		gl.glEnable(GL2.GL_COLOR_MATERIAL);
		gl.glEnable(GL2.GL_NORMALIZE);
		// Shading-Mode umschalten
		if (keyEnter) {
			gl.glShadeModel(GL2.GL_FLAT);
		}
		else  {
			// Default
			gl.glShadeModel(GL2.GL_SMOOTH);
		}

		// globales ambientes Licht
		float[] gAmbient = { 0.0f, 0.0f, 0.0f, 1.0f };
		gl.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, gAmbient, 0);
		
		// ----- direktionales Licht -----
		// Position, direktionales Licht, da w=0
		{
			float[] position = { 1.0f, 1.0f, 0.0f, 0.0f };
			gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, position, 0);
		}

		// ambientes Licht
		{
			float[] ambient = { 0.8f, 0.8f, 0.8f, 1.0f };
			gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, ambient, 0);
		}
		// diffuses Licht
		{
			float[] diffus = { 0.8f, 0.8f, 0.8f, 1.0f };
			gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, diffus, 0);
		}
		// spekulares Licht
		{
			float[] specular = { 0.6f, 0.6f, 0.6f, 1.0f };
			gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, specular, 0);
		}
		
		// Licht wird um (0,0,0) gedreht
		light1.getPosition().mult(GLMatrix.createRotateMatrix(0, 2f, 0));
		light1.beginGL(gl);

		// ----- Spotlight -----
		gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_POSITION, getKonfiguration().getCamera().getPosition().getValues(), 0);
		// Spotlight-Winkel
		gl.glLightf(GL2.GL_LIGHT2, GL2.GL_SPOT_CUTOFF, 180);
		gl.glLightf(GL2.GL_LIGHT2, GL2.GL_SPOT_EXPONENT, 0.4f);
		gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_SPOT_DIRECTION, getKonfiguration().getCamera().getDirection().getValues(), 0);
		float[] ambient = { 0.8f, 0.3f, 0.3f, 1.0f };
		gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_AMBIENT, ambient, 0);
		float[] diffus = { 1.0f, 0.3f, 0.3f, 1.0f };
		gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_DIFFUSE, diffus, 0);
		float[] specular = { 1.0f, 0.6f, 0.6f, 1.0f };
		gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_SPECULAR, specular, 0);

		// Attenuation = Abschwächung des Lichtes mit der Entfernung
		// Faktor: 1 / (k_c + k_l * d + k_q * d^2)
		gl.glLightf(GL2.GL_LIGHT2, GL2.GL_CONSTANT_ATTENUATION, 1.0f);
		gl.glLightf(GL2.GL_LIGHT2, GL2.GL_LINEAR_ATTENUATION, 0.009f);
		gl.glLightf(GL2.GL_LIGHT2, GL2.GL_QUADRATIC_ATTENUATION, 0.02f);

		// Materialeigenschaften
		{
			float[] matAmbient = { 0.25f, 0.2f, 0.2f, 1.0f };
			float[] matDiffuse = { 1.0f, 0.8f, 0.8f, 1.0f };
			float[] matSpecular = { 0.3f, 0.3f, 0.3f, 1.0f };
			float[] matEmission = { 0.0f, 0.0f, 0.0f, 1.0f };
			float matShininess = 10;
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, matAmbient, 0);
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, matDiffuse, 0);
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, matSpecular, 0);
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_EMISSION, matEmission, 0);
			gl.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, matShininess);
		}
		
		land.draw(gl);

		// Material: black rubber
		float[] matAmbient = { 0.02f, 0.02f, 0.02f, 1.0f };
		float[] matDiffuse = { 0.01f, 0.01f, 0.01f, 1.0f };
		float[] matSpecular = { 0.4f, 0.4f, 0.4f, 1.0f };
		float[] matEmission = { 0.0f, 0.0f, 0.0f, 1.0f };
		float matShininess = 0.78125f;
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, matAmbient, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, matDiffuse, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, matSpecular, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_EMISSION, matEmission, 0);
		gl.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, matShininess);
	}
	
	@Override
	public void endGL(GL2 gl) {
		gl.glDisable(GL2.GL_LIGHT0);
		gl.glDisable(GL2.GL_LIGHT1);
		gl.glDisable(GL2.GL_LIGHT2);
		gl.glDisable(GL2.GL_LIGHTING);
		super.endGL(gl);
	}

	@Override
	public void keyU() {
		keyEnter = !keyEnter;
		String text = "Shading Mode " + (keyEnter? "FLAT" : "SMOOTH") + " eingeschaltet";
		System.out.println(text);
	}

	@Override
	public void swapLightMode() {
		String text = "Licht " + (keySpace? "ein" : "aus") + "geschaltet";
		System.out.println(text);
		keySpace = !keySpace;
	}

	@Override
	public void printMyKeys() {
		System.out.println("Licht an/aus: Leertaste");
		System.out.println("Shading FLAT/SMOOTH: Enter");
	}
}
