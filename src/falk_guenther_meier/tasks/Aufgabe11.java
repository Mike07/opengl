package falk_guenther_meier.tasks;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.ObjLoader;

public class Aufgabe11 extends JoglApp {
		
	GLCustomComplexObject complexObject = new GLCustomComplexObject();
	
	public Aufgabe11(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
		complexObject = ObjLoader.objFile2ComplexObj();
	}

	public static void main(String[] args) {
		new Aufgabe11("Aufgabe 11", 800, 600);
	}

	@Override
	public void beginGL(GL2 gl) {
		
		gl.glEnable(GL2.GL_DEPTH_TEST);
		gl.glEnable(GL2.GL_CULL_FACE);
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);

		super.beginGL(gl);
		
		gl.glEnable(GL2.GL_LIGHTING);
		gl.glEnable(GL2.GL_LIGHT0);
		
		/*float[] v = {300.0f, 300.0f, 300.0f, 1.0f}; //
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, v, 1);
		
		float[] d = {1.0f, 1.0f, 1.0f, 1.0f};
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT_AND_DIFFUSE, d, 1);
		gl.glLightf(GL2.GL_LIGHT0 , GL2.GL_CONSTANT_ATTENUATION, 20.0f);
		gl.glLightf(GL2.GL_LIGHT0 , GL2.GL_LINEAR_ATTENUATION, 1.0f);
		gl.glLightf(GL2.GL_LIGHT0 , GL2.GL_QUADRATIC_ATTENUATION, 0.0040f);*/
		
		
		gl.glEnable(GL2.GL_LIGHTING);
		gl.glEnable(GL2.GL_LIGHT0);
		gl.glEnable(GL2.GL_COLOR_MATERIAL);
		gl.glEnable(GL2.GL_NORMALIZE);
		gl.glShadeModel(GL2.GL_SMOOTH);
		
		// Position
		float[] position = {100.0f, 100.0f, 0.0f, 0.0f}; // w=0 bedeutet, das Licht wird hier nur als Richtungsvektor angegeben
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, position, 0);
		
		// globales ambientes Licht
		float[] gAmbient = {0.0f, 0.0f, 0.0f, 1.0f}; // w=1 Standard
		gl.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, gAmbient, 0);
		
		// ambientes Licht
		float[] ambient = {1.0f, 1.0f, 1.0f, 1.0f};
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, ambient, 0);
		// diffuses Licht
		float[] diffus = {1.0f, 1.0f, 1.0f, 1.0f};
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, diffus, 1);
		// spekulares Licht
		float[] specular = {1.0f, 1.0f, 1.0f, 1.0f};
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, specular, 1);
		gl.glLightf(GL2.GL_LIGHT0 , GL2.GL_CONSTANT_ATTENUATION, 0.0f);
		
		/*
		// Materialeigenschaften
		float[] matAmbient = {0.25f, 0.2f, 0.2f, 1.0f};
		float[] matDiffuse = {1.0f, 0.8f, 0.8f, 1.0f};
		float[] matSpecular = {0.3f, 0.3f, 0.3f, 1.0f};
		float[] matEmission = {0.0f, 0.0f, 0.0f, 1.0f};
		float matShininess = 0.0f;
		
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, matAmbient, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, matDiffuse, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, matSpecular, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_EMISSION, matEmission, 0);
		gl.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, matShininess);*/
		
		gl.glPushMatrix();
		
		gl.glColor3f(0.5f, 0.4f, 0.2f);
		gl.glScalef(10, 10, 10);
		
		//complexObject.setDrawModeNormals(true);
		//complexObject.setDrawModeWireframe(true);

		if (complexObject != null && complexObject.hasObjects()) {
			complexObject.draw(gl);
		}

		gl.glPopMatrix();
				
	}	
	
	@Override
	public void endGL(GL2 gl) {
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glDisable(GL2.GL_LIGHT0);
		super.endGL(gl);
	}
}
