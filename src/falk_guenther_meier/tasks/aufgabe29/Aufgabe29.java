package falk_guenther_meier.tasks.aufgabe29;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.buildings.GLHouse;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.light.GLDirectionLight;
import falk_guenther_meier.model.light.GLSpotLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.Utility;

public class Aufgabe29 extends JoglApp {
	private ShaderProgram shaderPhong;
	private GLOpenRoom room;
	private GLHouse house;

	public Aufgabe29(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);		
	}

	public static void main(String[] args) {
		new Aufgabe29("Aufgabe 29", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
//		this.getKonfiguration().getLight().setPosition(new GLVertex(0.0f, 1.0f, 1.0f, 1.0f));
		this.getKonfiguration().getLight().setPosition(new GLVertex(0.0f, 2.0f, 2.0f));
		this.getKonfiguration().getLight().setUseColorMaterial(true);
		this.getKonfiguration().getLight().setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setAttenuation(0.3f, 0.1f, 0.01f); // c, l, q
//		this.getKonfiguration().getLight().setLightOn(false);

		shaderPhong = new ShaderProgram("vertexPhong.glsl", "fragmentPhong.glsl");
		shaderPhong.compileShaders(this.getClass(), drawable.getGL().getGL2());

//		GLVertex h = new GLVertex(0.0f, 2.0f, 6.0f);
//		GLSpotLight spot = new GLSpotLight(GL2.GL_LIGHT3, h,
//				GLVertex.invert(h));
//		GLSpotLight spot = new GLSpotLight(GL2.GL_LIGHT3, new GLVertex(2.0f, 4.0f, 2.0f),
//				new GLVertex(0.0f, -1.0f, 0.0f));
		GLSpotLight spot = new GLSpotLight(GL2.GL_LIGHT3, new GLVertex(2.0f, 2.0f, 2.0f),
				new GLVertex(-1.0f, -0.1f, 0.0f));
		spot.setSpotCutoff(20.0f);
		spot.setSpotExponent(0.9f);
		spot.setUseColorMaterial(true);
		spot.setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		spot.setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		spot.setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
//		spot.setLightOn(false);
		this.addLight(spot);

		GLDirectionLight direct = new GLDirectionLight(GL2.GL_LIGHT5, new GLVertex(1.0f, -1.0f, 0.0f));
		direct.setUseColorMaterial(true);
		direct.setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		direct.setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		direct.setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		this.addLight(direct);

		room = new GLOpenRoom(new GLVertex(), 20.0f, 15.0f, 2.5f, 0.04f);
		room.setColor(new GLVertex());
		room.setMaterial(new GLMaterial());
		room.rotate(0.0f, 45.0f, 0.0f);
		Utility.setRandomColor(room);

		house = new GLHouse(new GLVertex(), 2.0f, 1.0f, 2.0f, 1.0f);
		house.setMaterial(new GLMaterial());
//		Utility.setRandomColor(house);
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);

		gl.glEnable(GL2.GL_CULL_FACE);

		// ben�tigt ??
		gl.glEnable(GL2.GL_COLOR_MATERIAL);
		gl.glEnable(GL2.GL_NORMALIZE);
		gl.glShadeModel(GL2.GL_SMOOTH);

		shaderPhong.useProgram(gl);

		room.draw(gl);
		house.draw(gl);

		ShaderProgram.clearShaderProgram(gl);
	}

	@Override
	public void endGL(GL2 gl) {
		super.endGL(gl);
	}
}
