package falk_guenther_meier.tasks;

import java.io.File;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLSkyBox;
import falk_guenther_meier.model.textures.GLTexGrid;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;

public class Aufgabe19 extends JoglApp {
	private GLSkyBox skyBox;
	private GLCuboid cube;

	public Aufgabe19(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
		getKonfiguration().setShowSkybox(false);
		skyBox = new GLSkyBox(this.getKonfiguration().getCamera(), 100.0f, 0.008f);
		File textureFile = FileLoader.getFileFromFilesFolder("skybox/skybox_texture.jpg", true);	
//		File textureFile = FileLoader.getFileFromFilesFolder("skybox/skyboxsun25degtest.png", true);
		GLTexture2D skyBoxTexture = new GLTexture2D(textureFile); // Textur mit Gitter aus 4 Spalten und 3 Zeilen
		skyBoxTexture.setTexGrid(new GLTexGrid(4,3));
		skyBox.setTexture(skyBoxTexture);

		// jetzt Texturen aus Zeilen und Spalten richtig zuordnen
		skyBox.getRectFront().setTexRect(3,1);
		skyBox.getRectBack().setTexRect(1,1);
		skyBox.getRectLeft().setTexRect(0,1);
		skyBox.getRectRight().setTexRect(2,1);
		skyBox.getRectTop().setTexRect(1,0);
		skyBox.getRectDown().setTexRect(1,2);
		
		cube = new GLCuboid(new GLVertex(), 1.0f);
		start();
	}

	public static void main(String[] args) {
		new Aufgabe19("Aufgabe 19", 800, 600);
	}

	@Override
	public synchronized void init(GLAutoDrawable drawable) {
		super.init(drawable);
	}

	@Override
	public void beginGL(GL2 gl) {
		
		getKonfiguration().setShowSkybox(false);
		super.beginGL(gl);

		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glEnable(GL2.GL_CULL_FACE);
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
		skyBox.draw(gl);
		gl.glColor3f(1, 1, 1);
		cube.draw(gl);
	}
}
