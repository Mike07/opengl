varying vec3 lightDirection;
varying vec3 normal;

void main()
{
normal = gl_NormalMatrix * gl_Normal;

vec4 position4 = gl_ModelViewMatrix * gl_Vertex;
vec3 position3 = position4.xyz / position4.w;

lightDirection = normalize(gl_LightSource[7].position.xyz - position3);

gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
