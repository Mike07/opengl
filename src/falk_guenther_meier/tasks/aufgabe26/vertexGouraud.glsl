varying vec4 color;
void main()
{
vec3 eyeNormal = gl_Normal * gl_NormalMatrix; // evtl. bei uns überflüssig

vec4 position4 = gl_ModelViewMatrix * gl_Vertex;
vec3 position3 = position4.xyz / position4.w;

vec3 lightDirection = normalize(gl_LightSource[7].position.xyz - position3);


vec3 ambientColor = gl_FrontMaterial.ambient.xyz * gl_LightSource[7].ambient.xyz;

float diffuse = max(0.0, dot(eyeNormal, lightDirection));
vec3 diffuseColor = gl_FrontMaterial.diffuse.xyz * diffuse * gl_LightSource[7].diffuse.xyz;

float shininess = 128.0;
vec3 reflection = reflect( - lightDirection, eyeNormal); // normalize ??
float specAngle = max(0.0, dot(eyeNormal, reflection));
//if (diffuse != 0.0) { // warum diese Abfrage ??
	float specularValue = pow(specAngle, shininess);
	vec3 specularColor = gl_LightSource[7].specular.xyz * gl_FrontMaterial.specular.xyz * specularValue;
//}

vec3 help = ambientColor + diffuseColor + specularColor;
color = vec4(help.x, help.y, help.z, 1.0);
//color += gl_Color; // soll das??

gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
