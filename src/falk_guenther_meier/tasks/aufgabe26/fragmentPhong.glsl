varying vec3 lightDirection;
varying vec3 normal;

void main()
{
float diffuseIntensity = max(0.0, dot(normalize(normal), normalize(lightDirection)));

vec3 diffuseColor = gl_FrontMaterial.diffuse.xyz * diffuseIntensity * gl_LightSource[7].diffuse; // diffuse light
//gl_FragColor += vec4(diffuseColor.x, diffuseColor.y, diffuseColor.z, 1.0);
gl_FragColor += diffuseColor;


vec3 ambientColor = gl_FrontMaterial.ambient.xyz * gl_LightSource[7].ambient; // ambient light
//gl_FragColor += vec4(ambientColor.x, ambientColor.y, ambientColor.z, 1.0);
gl_FragColor += ambientColor;


vec3 reflection = normalize(reflect( - normalize(lightDirection), normalize(normal)));
float specAngle = max(0.0, dot(normalize(normal), reflection));

float shininess = 128.0;
if (diffuseIntensity != 0.0) {
	float specularValue = pow(specAngle, shininess);
	vec3 specularColor = gl_LightSource[7].specular.xyz * gl_FrontMaterial.specular.xyz * specularValue; // specular light
//	gl_FragColor += vec4(specularColor.x, specularColor.y, specularColor.z, 1.0);
	gl_FragColor += specularColor;
}
}
