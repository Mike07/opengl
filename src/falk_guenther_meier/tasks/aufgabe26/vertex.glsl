varying vec4 vertex;
varying vec3 normal;
void main()
{
//gl_Position = gl_Vertex; // ergibt nichts Sinnvolles (?)
//gl_Position = ftransform(); // geht anscheinend auch
gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex; // geht: soll wohl die Standard-Angabe sein!
gl_FrontColor = gl_Color; // "Lichtberechnung" ??
vertex = gl_Vertex;
normal = gl_Normal;
}
