package falk_guenther_meier.tasks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.media.opengl.GL2;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLTriangle;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.world.JoglApp;

public class Aufgabe06 extends JoglApp {
	
	private Object drawObjListLock = new Object();
	private ArrayList<GLObject> drawObjList = new ArrayList<GLObject>(); // eine Liste von Objekten, die bei display(...) gezeichnet werden sollen
	
	public Aufgabe06(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
		
		// Dateidialog f�r OBJ-Datei
        JFileChooser fileChooser = new JFileChooser("OBJ-Datei �ffnen");
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.isDirectory() || f.getName().matches(".*\\.(obj)");
            }
 
            @Override
            public String getDescription() {
                return "*.obj";
            }
        });
		
        
        // Datei verf�gbar? -> dann parsen
        if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION && fileChooser.getSelectedFile().exists()) {
        	parseObjFile(fileChooser.getSelectedFile().getAbsolutePath());
        }

	}
	
	private void parseObjFile(String fileName) {
		try {
			ArrayList<GLVertex> vertexList = new ArrayList<GLVertex>();
			ArrayList<GLObject> newObjList = new ArrayList<GLObject>();
			
			FileReader fileReader = new FileReader(fileName);
	    	BufferedReader bufferedReader = new BufferedReader(fileReader);
	    	
	    	String line = bufferedReader.readLine();
	    	while (line != null) {
	    		
	    		String[] lineParts = line.split(" ");
	    		if (lineParts.length > 0)
	    		{
		    		if (lineParts[0].equals("v")) {
		    			GLVertex vertex = obj2Vertex(lineParts);
		    			vertexList.add(vertex);
		    		} else if (lineParts[0].equals("f")) {
		    			GLTriangle triangle = obj2Triangle(lineParts, vertexList);
		    			newObjList.add(triangle);
		    		}
	    		}
	    			
	    		line = bufferedReader.readLine();
	    	}
	    	
	    	bufferedReader.close();
	    	fileReader.close();	
	    	
	    	// jetzt alle Objekte der Liste zum Zeichnen hinzuf�gen
	    	synchronized(drawObjListLock) {
	    		drawObjList.addAll(newObjList);
	    	}
        } catch (IOException e) {
        	System.out.println("Fehler beim Lesen der Datei");
        }
	}
	
	
	private float str2FloatDef(String s, float defValue) {
		try {
			return Float.valueOf(s);
		} catch (NumberFormatException e) {
			return defValue;
		}
	}
	
	private GLVertex obj2Vertex(String[] lineParts) {
		float x = str2FloatDef(lineParts[1], 0);
		float y = str2FloatDef(lineParts[2], 0);
		float z = str2FloatDef(lineParts[3], 0);
		float w = 1;
		if (lineParts.length>4) {
			w = str2FloatDef(lineParts[4], 1);
		}
		return new GLVertex(x,y,z,w);
	}
	
	private GLTriangle obj2Triangle(String[] lineParts, ArrayList<GLVertex> vertexArray) {	
		GLVertex[] triangleVertexArr= new GLVertex[3];
		for (int i = 0; i < 3; i++) {
			String sVertIndex = lineParts[i+1].split("/")[0];
			triangleVertexArr[i] = vertexArray.get(Integer.valueOf(sVertIndex)-1);
		}
		return new GLTriangle(triangleVertexArr, true);
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		new Aufgabe06("Aufgabe 6", 800, 600);
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		
		gl.glEnable(GL2.GL_DEPTH_TEST);
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
		
		synchronized(drawObjListLock) {
			gl.glPushMatrix();
			gl.glColor3f(1, 1, 1);
			gl.glScalef(10f, 10f, 10f);
			for (GLObject obj : drawObjList) {
				obj.draw(gl);
			}
			gl.glPopMatrix();
		}
		// eigenes Programm
		// ...
	}	
}
