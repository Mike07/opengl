package falk_guenther_meier.tasks.aufgabe40b;

import java.io.File;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.textures.GLTexGrid;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;

public class Aufgabe40b extends JoglApp {
	private GLCuboid cube;
	private GLCuboid cube2;
	private GLRectangle rect;
	private GLOpenRoom room;
	private GLPointLight pointLight;
	private GLTexture2D explosionTexture;
	
	public Aufgabe40b(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);

		File explosionTextureFile = FileLoader.getFileFromFilesFolder("texture/explosion2.png");
		explosionTexture = new GLTexture2D(explosionTextureFile);
		explosionTexture.setTexGrid(new GLTexGrid(4,4));
		
		start();
	}

	public static void main(String[] args) {
		new Aufgabe40b("Aufgabe 40b - Texturkoordinaten - Explosion", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		this.getKonfiguration().getCamera().setPosition(new GLVertex(10.0f, 4.0f, 13.0f));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(-0.8f, 0.0f, -1.0f));
		
		pointLight = new GLPointLight(GL2.GL_LIGHT7, new GLVertex(5f, 7f, 0f)); // new GLVertex(-2f, 5f, 3f)
		pointLight.setUseColorMaterial(true);
		pointLight.setAmbient(new GLVertex(0.7f, 0.7f, 0.7f, 1.0f));
		pointLight.setDiffuse(new GLVertex(0.8f, 0.8f, 0.8f, 1.0f));
		pointLight.setSpecular(new GLVertex(0.8f, 0.8f, 0.8f, 1.0f));
		pointLight.setAttenuation(1.0f, 0.00f, 0.001f);
		GLCuboid lightCube = new GLCuboid(new GLVertex(0f, 0f, 0f), 0.5f);
		lightCube.setColor(new GLVertex(1f, 1f, 1f));
		pointLight.setLightObject(lightCube);
		this.addLight(pointLight);

		rect = new GLRectangle(new GLVertex(2, 2, 2), 2f);
		rect.setColor(new GLVertex(1.0f, 1.0f, 1.0f, 1.0f));
		rect.setTexture(explosionTexture);
		
		cube = new GLCuboid(new GLVertex(2, 2, -4), 1.5f);
		GLMaterial material = new GLMaterial();
		material.setMatShininess(0.6f);
		cube.setMaterial(material);
		cube.setColor(new GLVertex(0.4f, 0.3f, 0.5f));
		
		cube2 = new GLCuboid(new GLVertex(-2,2,3), 2f);
		cube2.rotate(45, 45, 45);
		cube2.setColor(new GLVertex(0.4f, 0.6f, 0.2f));

		room = new GLOpenRoom(new GLVertex(), 60.0f, 60.0f, 3f, 0.04f);
		room.setColor(new GLVertex(0.6f,0.4f,0.2f));
		
	}
	
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		
		float picsPerSecond = 20;
		float time = this.getKonfiguration().getTime().getTime();
		int texCols = explosionTexture.getTexGrid().getColCount();
		int texRows = explosionTexture.getTexGrid().getRowCount();
		int texNr = Math.round(time * picsPerSecond) % (texCols*texRows);
		int texCol = texNr % texCols;
		int texRow = texNr / texRows;
		rect.setTexRect(texCol, texRow);
		
//		gl.glEnable(GL2.GL_CULL_FACE);
//
//		// ben�tigt ??
//		gl.glEnable(GL2.GL_COLOR_MATERIAL);
//		gl.glEnable(GL2.GL_NORMALIZE);
//		gl.glShadeModel(GL2.GL_SMOOTH);
		
		//shader.useProgram(gl);
		
		// PointLight rotieren
		//GLVertex pointLightPos = pointLight.getPosition();
		//pointLightPos.mult(GLMatrix.createRotateYMatrix(1f));
		//pointLight.setPosition(pointLightPos);
		
		
		cube.draw(gl);
		
		cube2.draw(gl);
		
		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glDepthMask(false);
		
		rect.draw(gl);
		
		gl.glDepthMask(true);
		gl.glDisable(GL2.GL_BLEND);
		
		
		//room.draw(gl); // Bodenplatte zeichnen -> Aufw�ndig!
		
		//ShaderProgram.clearShaderProgram(gl);
	}
}