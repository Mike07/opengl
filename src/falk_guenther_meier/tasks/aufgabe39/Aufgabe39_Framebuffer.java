package falk_guenther_meier.tasks.aufgabe39;

import java.awt.Color;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLFramebuffer;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.Utility;

public class Aufgabe39_Framebuffer extends JoglApp {
	private ShaderProgram shader;
	
	private GLCuboid cube;
	private GLCuboid cube2;
	private GLRectangle rect;
	private GLOpenRoom room;
	
	private GLPointLight pointLight;
	private GLCamera lightCam;
	
	private GLFramebuffer framebuffer;
	
	private int shadowSizeX;
	private int shadowSizeY;
	private float[] lightProjection = new float[16];
	private float[] lightModelview = new float[16]; // == inverse ModelView Matrix f�r Licht-Sicht
	private float[] shadowTextureMatrix = new float[16];
	
	public Aufgabe39_Framebuffer(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, true);
	}

	public static void main(String[] args) {
		new Aufgabe39_Framebuffer("Aufgabe 39 Framebuffer", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();
		this.getKonfiguration().getCamera().setPosition(new GLVertex(10f, 10f, 10f));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(-5f, -5f, -5f));
		
		shader = new ShaderProgram("fboVertex.glsl", "fboFragment.glsl");
		shader.compileShaders(this.getClass(), gl);
		
		pointLight = new GLPointLight(GL2.GL_LIGHT7, new GLVertex(5f, 7f, 0f)); // new GLVertex(-2f, 5f, 3f)
		pointLight.setUseColorMaterial(true);
		pointLight.setAmbient(new GLVertex(0.7f, 0.7f, 0.7f, 1.0f));
		pointLight.setDiffuse(new GLVertex(0.8f, 0.8f, 0.8f, 1.0f));
		pointLight.setSpecular(new GLVertex(0.8f, 0.8f, 0.8f, 1.0f));
		pointLight.setAttenuation(1.0f, 0.00f, 0.001f);
		GLCuboid lightCube = new GLCuboid(new GLVertex(0f, 0f, 0f), 0.5f);
		lightCube.setColor(new GLVertex(1f, 1f, 1f));
		pointLight.setLightObject(lightCube);
		this.addLight(pointLight);
		
		lightCam = new GLCamera(pointLight.getPosition(), new GLVertex(-5f, -7f, 0f));

		rect = new GLRectangle(new GLVertex(0, 0, 0), 2f);
		rect.setColor(Color.WHITE);
		
		cube = new GLCuboid(new GLVertex(2, 2, -4), 1.5f);
		GLMaterial material = new GLMaterial();
		material.setMatShininess(0.6f);
		cube.setMaterial(material);
		cube.setColor(new GLVertex(0.4f, 0.3f, 0.5f));
		
		cube2 = new GLCuboid(new GLVertex(-2,2,3), 2f);
		cube2.rotate(45, 45, 45);
		cube2.setColor(new GLVertex(0.4f, 0.6f, 0.2f));

		room = new GLOpenRoom(new GLVertex(), 60.0f, 60.0f, 3f, 0.04f);
		room.setColor(new GLVertex(0.6f,0.4f,0.2f));
		room.setMaterial(GLMaterial.PEARL);
		
		// Textur generieren und auf W�rfel setzen
		shadowSizeX = 800; // this.getWindowWidth();
		shadowSizeY = 600; // this.getWindowHeight();
		framebuffer = new GLFramebuffer(shadowSizeX, shadowSizeY, GLFramebuffer.DrawMode.DRAW_DEPTH);
		
		rect.setTexture(framebuffer.getTexture(), 0);
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		// PointLight rotieren
		GLVertex pointLightPos = pointLight.getPosition();
		pointLightPos.mult(GLMatrix.createRotateYMatrix(1f));
		pointLight.setPosition(pointLightPos);

		gl.glPushAttrib(GL2.GL_ALL_ATTRIB_BITS);

		gl.glLoadIdentity();
		// Projektion setzen
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glPushMatrix(); {
			gl.glLoadIdentity();
			int left = -10, right = 10, top = 10, bottom = -10, near = 0, far = 40;
			gl.glOrthof(left, right, bottom, top, near, far);
			gl.glGetFloatv(GL2.GL_PROJECTION_MATRIX, lightProjection, 0);

			// Modelview auf Lichtposition setzen
			gl.glMatrixMode(GL2.GL_MODELVIEW);
			gl.glPushMatrix(); {
				gl.glLoadIdentity();
				lightCam.refreshCamera();

				gl.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, lightModelview, 0);

				gl.glActiveTexture(GL2.GL_TEXTURE0); // gl.glActiveTexture(GL2.GL_TEXTURE3);
				framebuffer.beginGL(gl); {
					
					// Rendern vorbereiten
					gl.glClear(GL.GL_DEPTH_BUFFER_BIT);
					// au�er Tiefenpuffer wird nichts gebraucht
					gl.glShadeModel(GL2.GL_FLAT);
					gl.glDisable(GL2.GL_LIGHTING);
					gl.glDisable(GL2.GL_TEXTURE_2D);
					gl.glDisable(GL2.GL_NORMALIZE);
					gl.glEnable(GL2.GL_CULL_FACE);
					gl.glColorMask(false, false, false, false);
					// Genauigkeit
					float polygonOffsetFactor = 1.5f, polygonOffsetUnit = 4;
					gl.glPolygonOffset(polygonOffsetFactor, polygonOffsetUnit);
					gl.glEnable(GL2.GL_POLYGON_OFFSET_FILL);
					
					//this.draw(gl); // Welt zeichnen
					cube.draw(gl);
					cube2.draw(gl);
				} framebuffer.endGL(gl);

				// Texture matrix f�r Schattenprojektion (gl_TextureMatrix[0])
				gl.glMatrixMode(GL.GL_TEXTURE);
				gl.glLoadIdentity();
				gl.glTranslatef(0.5f, 0.5f, 0.5f);
				gl.glScalef(0.5f, 0.5f, 0.5f);
				gl.glMultMatrixf(lightProjection, 0);
				gl.glMultMatrixf(lightModelview, 0);
				//gl.glRotatef(180f, 1f, 0f, 0f);
				gl.glGetFloatv(GL2.GL_TEXTURE_MATRIX, shadowTextureMatrix, 0);
				gl.glActiveTexture(GL2.GL_TEXTURE0);

				// Matrizen auf alten Status setzen
				gl.glMatrixMode(GL2.GL_MODELVIEW);
			} gl.glPopMatrix();
			gl.glMatrixMode(GL2.GL_PROJECTION);
		} gl.glPopMatrix();
		gl.glMatrixMode(GL2.GL_MODELVIEW);

		gl.glPopAttrib(); // Alle ge�nderten Einstellungen wieder r�ckg�ngig machen
		
		/*// Framebuffer-TEST
		framebuffer.beginGL(gl); {
			cube.draw(gl);
			cube2.draw(gl);
		} framebuffer.endGL(gl);*/

		super.display(drawable);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
//		GLU glu = new GLU();
	    // .. some variable parameters of the shadow map
//	    gl.glActiveTexture(GL2.GL_TEXTURE3);
//	    gl.glEnable(GL2.GL_TEXTURE_2D);
//	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_MODE, GL2.GL_COMPARE_R_TO_TEXTURE);
//	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_FUNC, GL2.GL_LESS);
//	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_DEPTH_TEXTURE_MODE, GL2.GL_INTENSITY);
//	    gl.glActiveTexture(GL2.GL_TEXTURE0);
//	    gl.glEnable(GL2.GL_TEXTURE_2D);
		
		// inverse View-Matrix berechnen und setzen
		float[] invertMatrix = Utility.invertMatrix(this.getViewMatrix());
		//shader.setUniformMatrix(gl, "inversViewMatrix", invertMatrix); // JM: macht Kowalk genauso!
		for (float f : invertMatrix)
			System.out.println(f);
		System.out.println();
//		shader.setUniformMatrix(gl, "shadowMatrix", shadowTextureMatrix); // macht keinen Unterschied
		shader.useProgram(gl);
		shader.setTextureUnit(gl, "shadowMap", 0);
		
		cube.draw(gl);
		cube2.draw(gl);
		
		rect.glRotateNow(180, 0, 0); // Hack, damit Welt richtig auf dem Rechteck angezeigt wird
		rect.glTranslateNow(2, 2, 2);
		rect.draw(gl);
		
		room.draw(gl); // Bodenplatte zeichnen -> Aufw�ndig!
		
		ShaderProgram.clearShaderProgram(gl);
	}
}
