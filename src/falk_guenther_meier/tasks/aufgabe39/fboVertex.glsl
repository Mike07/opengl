// Aufgaben 33, 35
varying vec3 vNormal;
varying vec3 position;
varying vec4 vShadowCoord;
//varying vec4 vShadowCoordNoWDivide;
uniform mat4 inversViewMatrix;
//uniform mat4 shadowMatrix;

void main() {
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_FrontColor = gl_Color;

	// stop for shadow map regeneration
	//if (regenerateshadowmap >= 0.5) return;
	position = (gl_ModelViewMatrix * gl_Vertex).xyz;
	vNormal = normalize(gl_NormalMatrix * gl_Normal);

	//mat4 modelMatrix = inversViewMatrix * gl_ModelViewMatrix;
	//vec4 clipVertex = gl_TextureMatrix[0] * modelMatrix * gl_Vertex;
	vec4 clipVertex = gl_TextureMatrix[0] * gl_Vertex;
	vShadowCoord = clipVertex / clipVertex.w;
	
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
