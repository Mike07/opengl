package falk_guenther_meier.tasks.aufgabe39;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.light.GLSpotLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.Utility;

public class Aufgabe39_Michi extends JoglApp {
	private static final int DEPTH_MAP_SIZE = 512;
	private ShaderProgram shader;
	private GLCuboid cube;
	private GLCuboid cube2;
	private GLCustomComplexObject objects;
	private GLObject lightCube;
	
	private GLOpenRoom room;
	
	private GLSpotLight spotLight;
	private GLCamera lightCam;
	
	private GLTexture2D shadowTexture;
	private int shadowSizeX;
	private int shadowSizeY;
	
	private float[] cameraProjectionMatrix = new float[16];
	private float[] cameraModelviewMatrix = new float[16];
	private float[] lightProjectionMatrix = new float[16];
	private float[] lightModelviewMatrix = new float[16];
	private float[] cameraInverse = new float[16];
	
	private float[] shadowTextureMatrix = new float[16];
	
	private int depthtexture = 0;
	private int framebuffer = 0;
	
	public Aufgabe39_Michi(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, true);
	}

	public static void main(String[] args) {
		new Aufgabe39_Michi("Aufgabe 39", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();
		this.getKonfiguration().getCamera().setPosition(new GLVertex(10f, 10f, 10f));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(-5f, -5f, -5f));
		
		shader = new ShaderProgram("vertexLightTexture2.glsl", "fragmentLightTexture2.glsl");
		shader.compileShaders(this.getClass(), gl);
		
		GLVertex lightPos = new GLVertex(10f, 10f, 10f);
		GLVertex lightDirection = new GLVertex(-5, -5, -5);
		spotLight = new GLSpotLight(GL2.GL_LIGHT2, lightPos, lightDirection);
		spotLight.setAmbient(new GLVertex(0.7f, 0.7f, 0.7f));
		spotLight.setDiffuse(new GLVertex(1f, 0.5f, 0.5f));
		spotLight.setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		spotLight.setSpotCutoff(40f);
		spotLight.setSpotExponent(0.9f);
		spotLight.setAttenuation(1.0f, 0.01f, 0.00f);
		
		lightCam = new GLCamera(lightPos, lightDirection);
		
		this.addLight(spotLight);
		
		cube = new GLCuboid(new GLVertex(2, 2, -4), 1.5f);
		GLMaterial material = new GLMaterial();
		material.setMatShininess(0.6f);
		cube.setMaterial(material);
		
		cube2 = new GLCuboid(new GLVertex(-2,2,3), 2f);
		cube2.rotate(45, 45, 45);
		
		cube.setColor(new GLVertex(0.2f, 0.1f, 0.3f));
		cube2.setColor(new GLVertex(0.4f, 0.4f, 0.2f));
		
		objects = new GLCustomComplexObject();
		objects.add(cube);
		objects.add(cube2);

		room = new GLOpenRoom(new GLVertex(), 60.0f, 60.0f, 3f, 0.04f);
		room.setColor(new GLVertex(0.4f,0.2f,0.2f));
		room.setMaterial(GLMaterial.PEARL);
		
		// Textur generieren und auf W�rfel setzen
		shadowSizeX = this.getWindowWidth();
		shadowSizeY = this.getWindowHeight();
		shadowTexture = new GLTexture2D(gl, shadowSizeX, shadowSizeY);
//		objects.getObjects().get(0).setTexture(shadowTexture, 0);
        
		//Initialize shadow map
		createDepthTexture(gl);
		
		//use normal framebuffer (screen)
		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, 0);
	}
	
	public void checkFrameBuffer(GL gl) {
		int status = gl.glCheckFramebufferStatus(GL.GL_FRAMEBUFFER);
		System.out.println(status);
		switch (status) {
		case GL.GL_FRAMEBUFFER_COMPLETE:
			System.out.println("FRAMEBUFFER OK");
			break;
		case GL.GL_FRAMEBUFFER_UNSUPPORTED:
			System.out.println("<!> Unsupported framebuffer format\n");
			break;
		case GL.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			System.out
					.println("<!> Framebuffer incomplete, missing attachment\n");
			break;
		case GL.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			System.out
					.println("<!> Framebuffer incomplete attachement\n");
			break;			
		case GL.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
			System.out
					.println("<!> Framebuffer incomplete, attached images must have same dimensions\n");
			break;
		case GL.GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
			System.out
					.println("<!> Framebuffer incomplete, attached images must have same format\n");
			break;
		default:
			System.out
			.println("<!> Framebuffer incomplete, other reason\n");
			return;
		}
	}
	
	private void createDepthTexture(GL2 gl) {

		// FramebufferObjekt erstellen
		int[] tmp = new int[1];
		gl.glGenFramebuffers(1, tmp, 0);
		framebuffer = tmp[0];

		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, framebuffer);

		// Tiefentexture
	    gl.glGenTextures(1, tmp, 0);
	    depthtexture=tmp[0];

		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glActiveTexture(GL.GL_TEXTURE2);
		gl.glBindTexture(GL.GL_TEXTURE_2D, depthtexture);
		gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL2.GL_DEPTH_COMPONENT32,
				DEPTH_MAP_SIZE, DEPTH_MAP_SIZE, 0, GL2.GL_DEPTH_COMPONENT,
				GL.GL_UNSIGNED_BYTE, null);
		
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);

		checkFrameBuffer(gl);
		gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER,
				GL.GL_DEPTH_ATTACHMENT, GL.GL_TEXTURE_2D, depthtexture, 0);
		gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
		checkFrameBuffer(gl);
		gl.glDrawBuffer(0);
		gl.glReadBuffer(0);
	}
	
	private void saveMatrices(GL2 gl) {
		// Kameraprojektion
		gl.glMatrixMode(GL2.GL_PROJECTION);
	    gl.glLoadIdentity();
	    glu.gluPerspective(45.0f, ((float) this.getWindowWidth() / (float) this.getWindowHeight()),
				0.1f, 200.0f);
	    gl.glGetFloatv(GL2.GL_PROJECTION_MATRIX, cameraProjectionMatrix, 0);
	    gl.glLoadIdentity();
	    
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
		this.getKonfiguration().getCamera().refreshCamera();
		gl.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, cameraModelviewMatrix, 0);
		gl.glLoadIdentity();
	    
	    // Lichtprojektion
		gl.glMatrixMode(GL2.GL_PROJECTION);
//	    int left = -10, right = 10, top = 10, bottom = -10, near = 0, far = 40;
//	    gl.glOrthof(left, right, bottom, top, near, far);
	    glu.gluPerspective(42.0f, 1.0f, 0.01f, 28.0f);
	    gl.glGetFloatv(GL2.GL_PROJECTION_MATRIX, lightProjectionMatrix, 0);
	    gl.glLoadIdentity();
	    glu.gluPerspective(45.0f, ((float) this.getWindowWidth() / (float) this.getWindowHeight()),0.1f, 200.0f);
	    
	    // Modelview auf Lichtposition setzen
	    gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
	    lightCam.refreshCamera();
	    gl.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, lightModelviewMatrix, 0);
		gl.glLoadIdentity();
	}
	
	// Render the shadow map to the depth texture
	// "Aus der Sicht des Lichtes"
	public void makeShadowMap(GL2 gl){
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		gl.glLoadIdentity();
		
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glLoadMatrixf(lightProjectionMatrix, 0);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();		
		gl.glLoadMatrixf(lightModelviewMatrix, 0);
		
		startDepthMapRendering(gl);
		gl.glUseProgram(0); //No shader necessary
		// draw objects
	    objects.draw(gl);
	    room.draw(gl);
		endDepthMapRendering(gl);
		//The calculated Matrix transforms coordinates from the cameras clipspace into the
		//lights clipspace. In order to use the coordinates as texture coordinates, it will
		//be mutliplied by 0.5 and 0.5 will be added: coordinates between (0.0,0.0) and (1.0,1.0) 
		calculateTextureMatrix(gl);
	}

	private void calculateTextureMatrix(GL2 gl) {
		//instead of using texture matrix you can also use a uniform!
		gl.glActiveTexture(GL.GL_TEXTURE2);
		gl.glMatrixMode(GL.GL_TEXTURE);
		gl.glActiveTexture(GL.GL_TEXTURE2);
		
		float texMat[]= { 0.5F, 0F, 0F, 0.0F,
						   0.0F, 0.5F, 0F, 0.0F,
						   0.0F, 0F, 0.5F, 0.0F,
						   0.5F, 0.5F, 0.5F, 1.0F };
		gl.glLoadMatrixf(texMat,0);
		gl.glMultMatrixf(lightProjectionMatrix, 0);
		gl.glMultMatrixf(lightModelviewMatrix, 0);
		
		// So you ask, why do i have to invert the Cameras View Matrix?
		// This is only neccesary if you use the Modelviewmatrix for the
		// translation and rotaiton of objects: if you do so,
		// you cannot distinguish between Model and View! (look at the vertex shader)
		// So in order to compare the correct coordinates
		// you have to separate the Model and the view matrix.
		// You can also do this in a different manner, 
		// but doing it this way is straight forward if
		// you use glRotate and glTranslate.
		Utility.fastinvert(cameraModelviewMatrix, cameraInverse);
		gl.glMultMatrixf(cameraInverse, 0);
		
		gl.glMatrixMode(GL2.GL_MODELVIEW_MATRIX);
		// Ende Texture-Matrix berechnen
	}

	private void startDepthMapRendering(GL2 gl) {
		gl.glShadeModel(GL2.GL_FLAT);
		gl.glColorMask(false, false, false, false);
		gl.glEnable(GL.GL_POLYGON_OFFSET_FILL);
		gl.glPolygonOffset(1.0F, 4.0F);

		gl.glEnable(GL.GL_CULL_FACE);
		gl.glCullFace(GL.GL_FRONT);
		gl.glFrontFace(GL.GL_CCW);

		
		gl.glClearDepth(1.0f);
		gl.glUseProgram(0);
		gl.glViewport(0, 0, DEPTH_MAP_SIZE, DEPTH_MAP_SIZE);
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, framebuffer);
		gl.glClear(GL.GL_DEPTH_BUFFER_BIT);
//		gl.glDrawBuffer(0);
//		gl.glReadBuffer(0);
	}
	
	private void endDepthMapRendering(GL2 gl) {
		gl.glViewport(0, 0, this.getWindowWidth(), this.getWindowHeight());
		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glActiveTexture(GL.GL_TEXTURE2);
		gl.glBindTexture(GL.GL_TEXTURE_2D, depthtexture );
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);

		gl.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER,
				GL.GL_NEAREST);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER,
				GL.GL_NEAREST);
       
	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_FUNC, GL2.GL_LESS);
 	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_MODE, GL2.GL_NONE);
	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_DEPTH_TEXTURE_MODE, GL2.GL_INTENSITY);
		
		gl.glCullFace(GL.GL_BACK);

		gl.glDisable(GL.GL_CULL_FACE);
		gl.glShadeModel(GL2.GL_SMOOTH);
		gl.glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
		gl.glClearDepth(1.0f);
		gl.glColorMask(true, true, true, true);	
	}

	@Override
	public void display(GLAutoDrawable drawable) {
	    GL2 gl = drawable.getGL().getGL2();
	    
	    saveMatrices(gl);
	    makeShadowMap(gl);
	    gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, 0);
	    
	    gl.glMatrixMode(GL2.GL_MODELVIEW);
		super.display(drawable);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
//		GLU glu = new GLU();
	    // .. some variable parameters of the shadow map
//	    gl.glActiveTexture(GL2.GL_TEXTURE3);
//	    gl.glEnable(GL2.GL_TEXTURE_2D);
//	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_MODE, GL2.GL_COMPARE_R_TO_TEXTURE);
//	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_FUNC, GL2.GL_LESS);
//	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_DEPTH_TEXTURE_MODE, GL2.GL_INTENSITY);
//	    gl.glActiveTexture(GL2.GL_TEXTURE0);
//	    gl.glEnable(GL2.GL_TEXTURE_2D);
		
        // now the scene will be rendered to the screen
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glLoadMatrixf(cameraProjectionMatrix, 0);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadMatrixf(cameraModelviewMatrix, 0);
		
//		shader.setUniformMatrix(gl, "shadowMatrix", shadowTextureMatrix); // macht keinen Unterschied
		shader.useProgram(gl);
		//bind texture unit 2 to shadowMap-Sampler in the Shader.
	  	shader.setTextureUnit(gl, "shadowMap", 2);
		
		objects.draw(gl);
		
		room.draw(gl); // Bodenplatte zeichnen -> Aufw�ndig!
		
		ShaderProgram.clearShaderProgram(gl);
	}
}
