// Aufgaben 33, 35
varying vec3 vNormal;
varying vec3 position;
varying vec4 vShadowCoord;

void main() {
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

	// stop for shadow map regeneration
	//if (regenerateshadowmap >= 0.5) return;
	position = (gl_ModelViewMatrix * gl_Vertex).xyz;
	vNormal = normalize(gl_NormalMatrix * gl_Normal);

	//vec4 clipVertex = gl_TextureMatrix[3] * gl_ModelViewMatrix * gl_Vertex;
	vec4 clipVertex = gl_TextureMatrix[2] * gl_ModelViewMatrix * gl_Vertex;
	//vec4 clipVertex = gl_TextureMatrix[3] * gl_Vertex;
	//vShadowCoordNoWDivide = clipVertex;
	vShadowCoord = clipVertex / clipVertex.w; // werden vielleicht noch falsch berechnet! JM: ich glaube, dass passt alles
}
