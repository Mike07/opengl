package falk_guenther_meier.tasks;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

public class Aufgabe15 extends Aufgabe14 {

	public Aufgabe15(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		new Aufgabe15("Aufgabe 15", 800, 600);
		System.out.println("Taste \"S\" gedr�ckt halten, um Nebeleffekt zu sehen!");
	}

	
	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
	}
	
	@Override
	public void beginGL(GL2 gl) {
				
		
		gl.glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		
		gl.glEnable(GL2.GL_FOG);
		
		
		float[] fogColor =  {0.2f, 0.3f, 0.3f, 1.0f}; 
		gl.glFogfv(GL2.GL_FOG_COLOR, fogColor, 0);
		gl.glFogf(GL2.GL_FOG_START, 10f);
		gl.glFogf(GL2.GL_FOG_END, 25f);
		gl.glFogf(GL2.GL_FOG_DENSITY, 0.08f);
		gl.glFogi(GL2.GL_FOG_MODE, GL2.GL_LINEAR);
		//gl.glFogi(GL2.GL_FOG_MODE, GL2.GL_EXP);
		//gl.glFogi(GL2.GL_FOG_MODE, GL2.GL_EXP2);
		//gl.glHint (GL2.GL_FOG_HINT, GL2.GL_NICEST);
		gl.glHint (GL2.GL_FOG_HINT, GL2.GL_FASTEST);
		//gl.glFogi( GL2.GL_FOG_COORD_SRC, GL2.GL_FRAGMENT_DEPTH);
		
		super.beginGL(gl);
		
		
	}
	
	@Override
	public void endGL(GL2 gl) {
		gl.glDisable(GL2.GL_FOG);
		super.endGL(gl);
	}

}
