package falk_guenther_meier.tasks.aufgabe27;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.ObjLoader;

public class Aufgabe27 extends JoglApp {
	private ShaderProgram shader;
//	private GLCuboid cuboid;
//	private GLCuboid second;
	GLCuboid right;
	GLCuboid bottom;
	GLCuboid center;
	GLCustomComplexObject collection;
//	private int count = 0;
	GLObject bunny;
	private GLRectangle rect;
	
	public Aufgabe27(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);	
		bunny = ObjLoader.objFile2ComplexObj(FileLoader.getFileFromFilesFolder("obj/bunny.obj", true));
		start();
	}

	public static void main(String[] args) {
		new Aufgabe27("Aufgabe 27", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		drawable.getGL().getGL2().glEnable(GL2.GL_CULL_FACE);
		this.getKonfiguration().getLight().setPosition(new GLVertex(-5.0f, 5.0f, 5.0f));
		this.getKonfiguration().getLight().setUseColorMaterial(false);
//		this.getKonfiguration().getLight().setLightNumber(0);
		this.getKonfiguration().getLight().setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));

//		shader = new ShaderProgram("vertexToon.glsl", "fragmentToon.glsl");
		shader = new ShaderProgram("vertex.glsl", "fragment.glsl");
		shader.compileShaders(this.getClass(), drawable.getGL().getGL2());

		bunny.scale(10, 10, 10);
		
		rect = new GLRectangle(new GLVertex(2.0f, 2.0f, 0.0f), 1.0f, 1.0f);
		rect.setColor(new GLVertex(0.2f, 0.8f, 0.5f));
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glFrontFace(GL2.GL_CW);
		gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_LINE);
		gl.glColor4f(0.0f,0.0f,0.0f,1.0f);
		gl.glLineWidth(3.0f);
		this.getKonfiguration().getLight().setUseColorMaterial(false);
		drawScene(gl);
		
		gl.glEnable(GL2.GL_LIGHTING);
		gl.glFrontFace(GL2.GL_CCW);
		gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_FILL);
		gl.glLineWidth(1.0f);
		
		shader.useProgram(gl);
		drawScene(gl);
		ShaderProgram.clearShaderProgram(gl);
	}
	
	private void drawScene(GL2 gl) {
		bunny.rotate(1f, 1f, 1f);
		bunny.draw(gl);
//		rect.draw(gl);
//		gl.glBegin(GL.GL_TRIANGLES);
//			gl.glColor3f(1.0f, 0.0f, 0.0f);
//			gl.glVertex3f(2.0f, 2.0f, 0.0f);
//			gl.glColor3f(0.0f, 1.0f, 0.0f);
//			gl.glVertex3f(3.0f, 2.0f, 0.0f);
//			gl.glColor3f(0.0f, 0.0f, 1.0f);
//			gl.glVertex3f(2.0f, 3.0f, 0.0f);
//		gl.glEnd();
	}
}
