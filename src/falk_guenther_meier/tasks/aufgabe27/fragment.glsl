varying vec3 normal;
varying vec3 lightDirection;
float intensity;
vec4 color;
void main() {
	//gl_FragColor = gl_Color; // Standard-Befehl
	
	// Intensitšt des Lichtes berechnen, bestimmt den cos zwischen 2 Vektoren [0..1]
	intensity = dot(normal, lightDirection);
	if (intensity > 0.90) {
		color = vec4 (1.0, 0.56, 0.0, 1.0);
	}
	else if (intensity > 0.60) {
		color = vec4 (0.64, 0.35, 0.0, 1.0);
	}
	else if (intensity > 0.45) {
		color = vec4 (0.50, 0.25, 0.0, 1.0);
	}
	else if (intensity > 0.30) {
		color = vec4 (0.37, 0.17, 0.0, 1.0);
	}
	else if (intensity > 0.15) {
		color = vec4 (0.20, 0.10, 0.0, 1.0);
	}
	else {
		color = vec4 (0.12, 0.07, 0.0, 1.0);
	}
	gl_FragColor = color;
}
