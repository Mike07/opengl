varying vec3 normal;
varying vec3 lightDirection;
void main() {
	normal = normalize(gl_NormalMatrix * gl_Normal);
	vec4 position = gl_ModelViewMatrix * gl_Vertex;
	vec3 position2 = position.xyz / position.w;

	lightDirection = normalize(gl_LightSource[7].position.xyz - position2);
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
