package falk_guenther_meier.tasks.aufgabe36;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.world.JoglApp;

public class Aufgabe36 extends JoglApp {
	private ShaderProgram shader;
	private GLCuboid cube;
	private GLCuboid cube2;
	private GLCustomComplexObject objects;
	
	private GLOpenRoom room;
	
	private GLPointLight pointLight;
	
	public Aufgabe36(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, true);
		//start();
	}

	public static void main(String[] args) {
		new Aufgabe36("Aufgabe 36", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();
		this.getKonfiguration().getCamera().setPosition(new GLVertex(20.0f, 20.0f, 20.0f));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(-1.0f, -1.0f, -1.0f));
//		this.getKonfiguration().getLight().setPosition(new GLVertex(0.0f, 3.0f, 9.0f));
//		this.getKonfiguration().getLight().setUseColorMaterial(true);
//		this.getKonfiguration().getLight().setAmbient(new GLVertex(0.1f, 0.0f, 0.0f));
//		this.getKonfiguration().getLight().setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
//		this.getKonfiguration().getLight().setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
//		this.getKonfiguration().getLight().setAttenuation(0.1f, 0.01f, 0.009f); // c, l, q
		
		shader = new ShaderProgram("vertexPhong.glsl", "fragmentPhong.glsl");
		shader.compileShaders(this.getClass(), gl);
		
		pointLight = new GLPointLight(GL2.GL_LIGHT7, new GLVertex(5f, 7f, 0f)); // new GLVertex(-2f, 5f, 3f)
		pointLight.setUseColorMaterial(true);
		pointLight.setAmbient(new GLVertex(0.7f, 0.7f, 0.7f, 1.0f));
		pointLight.setDiffuse(new GLVertex(0.8f, 0.8f, 0.8f, 1.0f));
		pointLight.setSpecular(new GLVertex(0.8f, 0.8f, 0.8f, 1.0f));
		pointLight.setAttenuation(1.0f, 0.00f, 0.001f);
		
		GLCuboid lightCube = new GLCuboid(new GLVertex(0f, 0f, 0f), 0.5f);
		lightCube.setColor(new GLVertex(1f, 1f, 1f));
		pointLight.setLightObject(lightCube);
		
		this.addLight(pointLight);

		
		cube = new GLCuboid(new GLVertex(2, 2, -4), 1.5f);
		GLMaterial material = new GLMaterial();
		material.setMatShininess(0.6f);
		cube.setMaterial(material);
		
		cube2 = new GLCuboid(new GLVertex(-2,2,3), 2f);
		cube2.rotate(45, 45, 45);
		
		objects = new GLCustomComplexObject();
		objects.add(cube);
		objects.add(cube2);

		room = new GLOpenRoom(new GLVertex(), 60.0f, 60.0f, 3f, 0.04f);
		room.setColor(new GLVertex(0.6f,0.4f,0.2f));
	}
	
	private GLMatrix calculateShadowProjectionMatrix(GLVertex lightPos) {
		GLVertex l = lightPos;
		GLVertex n = new GLVertex(0.0f, 1.0f, 0.0f); // der Normalenvektor der Ebene
		GLVertex p = new GLVertex(0.0f, 0.0f, 0.0f); // irgendein Punkt auf der Ebene
		float d = GLVertex.scalarProduct(GLVertex.invert(n), p);
		
		float NdotL = GLVertex.scalarProduct(n, l);
		
		return new GLMatrix( new float[]{
					// 1. Spalte
					NdotL + d - l.getX() * n.getX(),
					-l.getY() * n.getX(), 
					-l.getZ() * n.getX(), 
					-n.getX(), 
					
					// 2. Spalte
					-l.getX() * n.getY(),
					NdotL + d - l.getY() * n.getY(),
					-l.getZ() * n.getY(), 
					-n.getY(), 
					
					// 3. Spalte
					-l.getX() * n.getZ(),
					-l.getY() * n.getZ(), 
					NdotL + d - l.getZ() * n.getZ(),
					-n.getZ(), 
					
					// 4. Spalte
					-l.getX() * d,
					-l.getY() * d,
					-l.getZ() * d,
					NdotL
				}
				);
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		
		
		gl.glEnable(GL2.GL_CULL_FACE);

		// ben�tigt ??
		gl.glEnable(GL2.GL_COLOR_MATERIAL);
		gl.glEnable(GL2.GL_NORMALIZE);
		gl.glShadeModel(GL2.GL_SMOOTH);
		
		shader.useProgram(gl);
		
		// PointLight rotieren
		GLVertex pointLightPos = pointLight.getPosition();
		pointLightPos.mult(GLMatrix.createRotateYMatrix(1f));
		pointLight.setPosition(pointLightPos);
		
		cube.setColor(new GLVertex(0.4f, 0.3f, 0.5f));
		cube2.setColor(new GLVertex(0.4f, 0.6f, 0.2f));
		objects.draw(gl);
		
		gl.glPushMatrix(); {
			gl.glPushAttrib(GL2.GL_ALL_ATTRIB_BITS); // aktuelle Einstellungen speichern
//			gl.glEnable(GL2.GL_STENCIL_TEST);
//			gl.glStencilFunc(GL2.GL_EQUAL, 0, 0xFFFFFFFF);
//			gl.glStencilOp(GL2.GL_KEEP,GL2.GL_KEEP,GL2.GL_INCR);
			gl.glDisable(GL2.GL_LIGHTING);
			gl.glEnable(GL2.GL_BLEND);
			objects.setColor(new GLVertex(0.0f, 0.0f, 0.0f, 0.7f));
			
			GLMatrix shadowProjectionMatrix = calculateShadowProjectionMatrix(pointLight.getPosition());
			objects.glTransformNow(shadowProjectionMatrix); //gl.glMultTransposeMatrixf(shadowProjectionMatrix.get(), 0); // liefert dasselbe Ergebnis wie bei glTransformNow
			//objects.glTranslateNow(0f, 0.2f, 0f);
			objects.draw(gl);
			
			
			gl.glPopAttrib(); // vorgenommene Einstellungen wieder zur�cksetzen
		}
		gl.glPopMatrix();
		
		
		room.draw(gl); // Bodenplatte zeichnen -> Aufw�ndig!
		
		ShaderProgram.clearShaderProgram(gl);
	}
}