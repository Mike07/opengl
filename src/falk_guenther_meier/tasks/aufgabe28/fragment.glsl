uniform sampler2D texUnit;

void main()
{
	vec4 textureColor = texture2D(texUnit, gl_TexCoord[0].st);
	gl_FragColor = textureColor;
	//gl_FragColor = (textureColor + gl_Color) * 0.5;
	//gl_FragColor = gl_Color;
}