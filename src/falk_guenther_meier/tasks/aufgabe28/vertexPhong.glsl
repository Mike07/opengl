varying vec3 normal;
varying vec3 lightDirection;
void main()
{	
	gl_Position = ftransform();
	vec3 position3 = gl_Position.xyz / gl_Position.w;
	lightDirection = normalize(gl_LightSource[7].position.xyz - position3);
	normal = normalize(gl_NormalMatrix * gl_Normal);

	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_FrontColor = gl_Color;
}
