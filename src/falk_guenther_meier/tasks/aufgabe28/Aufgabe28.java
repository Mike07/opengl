package falk_guenther_meier.tasks.aufgabe28;

import java.io.File;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;

public class Aufgabe28 extends JoglApp {
	private ShaderProgram shader;
	private GLCuboid cuboid;
	private File textureFile;
	
	public Aufgabe28(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
		textureFile = FileLoader.getFileFromFilesFolder("texture/bretter.JPG", true);	
		start();
	}

	public static void main(String[] args) {
		new Aufgabe28("Aufgabe 28", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();
		drawable.getGL().getGL2().glEnable(GL2.GL_CULL_FACE);
		this.getKonfiguration().getLight().setPosition(new GLVertex(0.0f, 1.0f, 1.0f));
		this.getKonfiguration().getLight().setUseColorMaterial(true);
		this.getKonfiguration().getLight().setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		
		shader = new ShaderProgram("vertex.glsl", "fragment.glsl");
		shader.compileShaders(this.getClass(), gl);
		
		GLTexture2D texture = new GLTexture2D(textureFile);
		
		cuboid = new GLCuboid(new GLVertex(0, 0, 0), 0.5f);
		cuboid.rotate(-10, 30, 0);
		cuboid.translate(0.1f, 0.1f, 0.1f);	
		cuboid.setColor(new GLVertex(1,1,1));
		cuboid.setTexture(texture, 0);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		
		shader.useProgram(gl);
		//gl.glActiveTexture(GL2.GL_TEXTURE1); // TextureUnit GL_TEXTUREi aktivieren - GL_TEXTURE0 ist standardm��ig aktiviert
		shader.setTextureUnit(gl, "texUnit", 0);
		cuboid.draw(gl);
		
		ShaderProgram.clearShaderProgram(gl);
	}
}
