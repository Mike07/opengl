varying vec3 normal;
varying vec3 lightDirection;

uniform sampler2D texUnit;

vec4 getLightPhong() {
	float diffuseIntensity = max(0.0, dot(normalize(normal), normalize(lightDirection)));
	
	vec4 lightColor = gl_FrontMaterial.diffuse * diffuseIntensity * gl_LightSource[7].diffuse; // diffuse 
	lightColor += gl_FrontMaterial.ambient * gl_LightSource[7].ambient; // ambient
	
	vec3 reflection = normalize(reflect( - normalize(lightDirection), normalize(normal)));
	float specAngle = max(0.0, dot(normalize(normal), reflection));
	
	float shininess = 128.0;
	if (diffuseIntensity != 0.0) {
		float specularValue = pow(specAngle, shininess);
		lightColor += gl_LightSource[7].specular * gl_FrontMaterial.specular * specularValue; // specular
	}
	return lightColor;
}

void main()
{
	vec4 textureColor = texture2D(texUnit, gl_TexCoord[0].st);
	//gl_FragColor = textureColor;
	gl_FragColor = textureColor * getLightPhong();
	//gl_FragColor = (textureColor + gl_Color) * 0.5;
	//gl_FragColor = gl_Color;
}