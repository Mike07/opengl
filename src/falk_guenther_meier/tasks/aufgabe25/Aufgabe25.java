package falk_guenther_meier.tasks.aufgabe25;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.tasks.Aufgabe24;

public class Aufgabe25 extends Aufgabe24 {	
	private ShaderProgram shader;
	
	public Aufgabe25(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);		
	}

	public static void main(String[] args) {
		new Aufgabe25("Aufgabe 25", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();
		// Shader-Programm einmal kompilieren
		shader = new ShaderProgram("vertex.glsl", "fragment.glsl");
		shader.compileShaders(this.getClass(), gl);
	}

	@Override
	public void beginGL(GL2 gl) {
		// Skybox hat kein Shader-Programm!
		// jetzt: Shader benutzen, gilt f�r alle Objekte danach
		shader.useProgram(gl);
		super.beginGL(gl);
		// Shader entfernen
		ShaderProgram.clearShaderProgram(gl);
	}
}
