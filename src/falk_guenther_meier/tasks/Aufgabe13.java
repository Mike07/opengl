package falk_guenther_meier.tasks;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.objects.GLSphere;
import falk_guenther_meier.model.world.JoglApp;

public class Aufgabe13 extends JoglApp {
	private GLSphere ball;

	public Aufgabe13(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}

	public static void main(String[] args) {
		new Aufgabe13("Sphere", 800, 600);
	}

	@Override
	public synchronized void init(GLAutoDrawable drawable) {
		super.init(drawable);
		int iterations = 6;
		ball = new GLSphere(0.5f, iterations); // Grenze liegt bei ca. 12+ Iterationen
//		for (GLObject o : ball.getObjects()) {
//			((GLPrimitiveObject) o).setColor(new GLVertex((float) Math.random(), (float) Math.random(), (float) Math.random()));
//		}
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);

		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glEnable(GL2.GL_CULL_FACE);
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);

		gl.glEnable(GL2.GL_LIGHTING);
		gl.glEnable(GL2.GL_LIGHT0);

		gl.glEnable(GL2.GL_COLOR_MATERIAL);
		gl.glEnable(GL2.GL_NORMALIZE);
		gl.glShadeModel(GL2.GL_SMOOTH);

		// Position
		float[] position = {100.0f, 100.0f, 0.0f, 0.0f}; // w=0 bedeutet, das Licht wird hier nur als Richtungsvektor angegeben
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, position, 0);
		
		// globales ambientes Licht
		float[] gAmbient = {0.0f, 0.0f, 0.0f, 1.0f}; // w=1 Standard
		gl.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, gAmbient, 0);
		
		// ambientes Licht
		float[] ambient = {1.0f, 1.0f, 1.0f, 1.0f};
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, ambient, 0);
		// diffuses Licht
		float[] diffus = {1.0f, 1.0f, 1.0f, 1.0f};
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, diffus, 1);
		// spekulares Licht
		float[] specular = {1.0f, 1.0f, 1.0f, 1.0f};
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, specular, 1);
		gl.glLightf(GL2.GL_LIGHT0 , GL2.GL_CONSTANT_ATTENUATION, 0.0f);
		
//		gl.glPushMatrix();
		
		gl.glColor3f(0.5f, 0.4f, 0.2f);
		
		//ball.setDrawModeWireframe(true);
		//ball.setDrawModeNormals(true);
		ball.draw(gl);
		
//		gl.glPopMatrix();

	}	
	
	@Override
	public void endGL(GL2 gl) {
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glDisable(GL2.GL_LIGHT0);
		super.endGL(gl);
	}
}
