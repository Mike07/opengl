package falk_guenther_meier.tasks;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.buildings.DemoLandscape;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.ObjLoader;

public class Aufgabe10 extends JoglApp {
	GLCustomComplexObject complexObject = new GLCustomComplexObject();

	GLCamera camera = new GLCamera();
	
	boolean keySpace = false;
	
	GLVertex pos = new GLVertex(-1, -0.1f, -1);

	public Aufgabe10(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
		complexObject = ObjLoader.objFile2ComplexObj();
	}

	public static void main(String[] args) {
		new Aufgabe10("Aufgabe 10", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);

		gl.glEnable(GL2.GL_CULL_FACE);

		DemoLandscape land = new DemoLandscape(pos);
		if (!keySpace) {
			gl.glEnable(GL2.GL_LIGHTING);
			gl.glEnable(GL2.GL_LIGHT0);
			gl.glEnable(GL2.GL_LIGHT1);
			gl.glEnable(GL2.GL_LIGHT2);
		}
		gl.glEnable(GL2.GL_COLOR_MATERIAL);
		gl.glEnable(GL2.GL_NORMALIZE);
		gl.glShadeModel(GL2.GL_SMOOTH);

		// ----- direktionales Licht -----
		// Position, direktionales Licht, da w=0
		{
			float[] position = { 1.0f, 1.0f, 0.0f, 0.0f };
			gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, position, 0);
		}

		// globales ambientes Licht
		float[] gAmbient = { 0.0f, 0.0f, 0.0f, 1.0f };
		gl.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, gAmbient, 0);

		// ambientes Licht
		{
			float[] ambient = { 0.8f, 0.8f, 0.8f, 1.0f };
			gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, ambient, 0);
		}
		// diffuses Licht
		{
			float[] diffus = { 0.8f, 0.8f, 0.8f, 1.0f };
			gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, diffus, 1);
		}
		// spekulares Licht
		{
			float[] specular = { 0.6f, 0.6f, 0.6f, 1.0f };
			gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, specular, 1);
		}

		// ----- Punktlicht -----
		// Position, Punktlicht, da w=1
		float[] position = { -5.0f, 1.0f, -3.0f, 1.0f };
		gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_POSITION, position, 0);

		// ambientes Licht
		{
			float[] ambient = { 0.2f, 0.2f, 0.6f, 1.0f };
			gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, ambient, 0);
		}
		// diffuses Licht
		{
			float[] diffus = { 0.4f, 0.4f, 1f, 1.0f };
			gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_DIFFUSE, diffus, 1);
		}
		// spekulares Licht
		{
			float[] specular = { 0.6f, 0.6f, 0.6f, 1.0f };
			gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_SPECULAR, specular, 1);
		}

		// ----- Spotlight -----
		gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_POSITION, camera.getPosition()
				.getValues(), 0);
		// Spotlight-Winkel
		gl.glLightf(GL2.GL_LIGHT2, GL2.GL_SPOT_CUTOFF, 180);
		gl.glLightf(GL2.GL_LIGHT2, GL2.GL_SPOT_EXPONENT, 0.4f);
		gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_SPOT_DIRECTION, camera
				.getDirection().getValues(), 0);
		float[] ambient = { 0.8f, 0.3f, 0.3f, 1.0f };
		gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_AMBIENT, ambient, 0);
		float[] diffus = { 1.0f, 0.3f, 0.3f, 1.0f };
		gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_DIFFUSE, diffus, 1);
		float[] specular = { 1.0f, 0.6f, 0.6f, 1.0f };
		gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_SPECULAR, specular, 1);

		// Attenuation = Abschwächung des Lichtes mit der Entfernung
		// Faktor: 1 / (k_c + k_l * d + k_q * d^2)
		gl.glLightf(GL2.GL_LIGHT2, GL2.GL_CONSTANT_ATTENUATION, 1.0f);
		gl.glLightf(GL2.GL_LIGHT2, GL2.GL_LINEAR_ATTENUATION, 0.009f);
		gl.glLightf(GL2.GL_LIGHT2, GL2.GL_QUADRATIC_ATTENUATION, 0.02f);

		// Materialeigenschaften
		{
			float[] matAmbient = { 0.25f, 0.2f, 0.2f, 1.0f };
			float[] matDiffuse = { 1.0f, 0.8f, 0.8f, 1.0f };
			float[] matSpecular = { 0.3f, 0.3f, 0.3f, 1.0f };
			float[] matEmission = { 0.0f, 0.0f, 0.0f, 1.0f };
			float matShininess = 10;
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, matAmbient, 0);
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, matDiffuse, 0);
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, matSpecular, 0);
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_EMISSION, matEmission, 0);
			gl.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, matShininess);
		}

		land.draw(gl);

		// Material: black rubber
		float[] matAmbient = { 0.02f, 0.02f, 0.02f, 1.0f };
		float[] matDiffuse = { 0.01f, 0.01f, 0.01f, 1.0f };
		float[] matSpecular = { 0.4f, 0.4f, 0.4f, 1.0f };
		float[] matEmission = { 0.0f, 0.0f, 0.0f, 1.0f };
		float matShininess = 0.78125f;
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, matAmbient, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, matDiffuse, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, matSpecular, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_EMISSION, matEmission, 0);
		gl.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, matShininess);

		gl.glScalef(40, 40, 40);
		complexObject.draw(gl);
	}
	
	@Override
	public void endGL(GL2 gl) {
		gl.glDisable(GL2.GL_LIGHT0);
		gl.glDisable(GL2.GL_LIGHT1);
		gl.glDisable(GL2.GL_LIGHT2);
		gl.glDisable(GL2.GL_LIGHTING);
		super.endGL(gl);
	}

	@Override
	public void swapLightMode() {
		keySpace = !keySpace;
	}
}
