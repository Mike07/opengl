package falk_guenther_meier.tasks;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLHeightMapRectangle;
import falk_guenther_meier.model.world.JoglApp;

public class Aufgabe24 extends JoglApp {
	private GLHeightMapRectangle map;
	private GLPointLight light1;
	
	public Aufgabe24(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);

		map = new GLHeightMapRectangle("heightmap/height_little.png", 1.0f, 1.0f, 0.5f, "texture/grass-texture.png", 1);
		map.scale(4.0f, 2.0f, 4.0f);

		start();
	}

	public static void main(String[] args) {
		new Aufgabe24("Aufgabe 24", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		// ----- Punktlicht -----
		// Position, Punktlicht, da w=1
		GLVertex position = new GLVertex(0.0f, 2.0f, 0.0f, 1.0f);
		GLVertex ambient = new GLVertex(0.6f, 0.6f, 0.8f, 1.0f);
		GLVertex diffuse = new GLVertex(0.6f, 0.6f, 1f, 1.0f);
		GLVertex specular = new GLVertex(0.8f, 0.8f, 0.8f, 1.0f);
		light1 = new GLPointLight(GL2.GL_LIGHT1, null, position, ambient, diffuse, specular);
		// Wichtig, da sonst die Landschaft starke Blaut�ne enth�lt !!
		this.getKonfiguration().getLight().setUseColorMaterial(false);
		drawable.getGL().getGL2().glEnable(GL2.GL_CULL_FACE);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		
		light1.beginGL(gl);

		map.draw(gl);
	}
	
	@Override
	public void endGL(GL2 gl) {
		light1.endGL(gl);
		super.endGL(gl);
	}
}
