package falk_guenther_meier.tasks;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.observers.TranslateObservable;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.Utility;

public class Aufgabe21 extends JoglApp {
	GLCuboid cuboid;
	GLCuboid second;
	GLCuboid right;
	GLCuboid bottom;
	GLCuboid center;
	int count = 0;
	GLCustomComplexObject collection;

	public Aufgabe21(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}

	public static void main(String[] args) {
		new Aufgabe21("Aufgabe 21", 800, 600);
	}

	@Override
	public synchronized void init(GLAutoDrawable drawable) {
		super.init(drawable);
		this.getKonfiguration().getLight().setPosition(new GLVertex(-2.0f, 2.0f, 2.0f));
		this.getKonfiguration().getLight().setUseColorMaterial(true);
//		this.getKonfiguration().getLight().setLightNumber(0);
		this.getKonfiguration().getLight().setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));

		cuboid = new GLCuboid(new GLVertex(0, 0, 0), 0.5f);
		cuboid.rotate(45, 0, 0);
		cuboid.translate(0.1f, 0.1f, 0.1f);
		cuboid.setMaterial(new GLMaterial());
		Utility.setRandomColor(cuboid);

		second = new GLCuboid(new GLVertex(0, 0, 0), 0.5f) {
			@Override
			public void updateTranslate(TranslateObservable changed, float x, float y, float z) {
				super.updateTranslate(changed, x, y, z);
				this.translate(x, 2*y, z);
			}
		};
		second.rotate(45, 0, 0);
		second.translate(0.1f, 1.1f, 0.1f);
		Utility.setRandomColor(second);
		cuboid.addTranslateObserver(second);

		right = new GLCuboid(new GLVertex(0, 0, 0), 0.5f);
		right.rotate(45, 0, 0);
		right.translate(3.1f, 0.1f, 0.1f);
		Utility.setRandomColor(right);

		bottom = new GLCuboid(new GLVertex(0, 0, 0), 0.5f) {
			@Override
			public void updateTranslate(TranslateObservable changed, float x, float y, float z) {
				super.updateTranslate(changed, x, y, z);
				this.translate(x, 0.0f, 0.0f);
			}
		};
		bottom.rotate(45, 0, 0);
		bottom.translate(3.1f, -1.1f, 0.1f);
		Utility.setRandomColor(bottom);
		right.addTranslateObserver(bottom);

		center = new GLCuboid(new GLVertex(0, 0, 0), 0.5f) {
			private List<TranslateObservable> list = new ArrayList<TranslateObservable>();
			@Override
			public void updateTranslate(TranslateObservable changed, float x, float y, float z) {
				super.updateTranslate(changed, x, y, z);
				if (!list.contains(changed)) {
					list.add(changed);
				}
				int count = 0;
				GLVertex pos = new GLVertex();
				for (TranslateObservable o : list) {
					count++;
					pos.add(o.getPosition());
				}
				pos.mult(1.0f / count);
				this.setPosition(pos);
			}
		};
		center.rotate(45, 0, 0);
		Utility.setRandomColor(center);
		bottom.addTranslateObserver(center);
		second.addTranslateObserver(center);

		collection = new GLCustomComplexObject();
		collection.add(cuboid);
		collection.add(second);
		collection.setColor(new GLVertex(0.8f, 0.6f, 0.7f));
		collection.setReferencePoint(new GLVertex());
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);

		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glEnable(GL2.GL_CULL_FACE);
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);

		if (count > 0 && count < 51) {
			cuboid.translate(0.0f, 0.015f, 0.0f);
			right.translate(0.025f, 0.025f, 0.0f);
		} else {
			cuboid.translate(-0.0f, -0.015f, -0.0f);
			right.translate(-0.025f, -0.025f, -0.0f);
		}

		collection.rotate(0, 0, 1.0f);
		bottom.rotate(0.0f, 1.0f, 0.0f);
		center.rotate(1.0f, 0.0f, 0.0f);

		cuboid.draw(gl);
		second.draw(gl);
		right.draw(gl);
		bottom.draw(gl);
		center.draw(gl);

		count = (count + 1) % 101;
	}	
}
