package falk_guenther_meier.tasks.aufgabe30;

import java.io.File;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLHeightMapTriangleStrip;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;

public class Aufgabe30 extends JoglApp {
	private ShaderProgram shader;
	private GLHeightMapTriangleStrip heightMap;
	private File texFileHeightMap;
	private File texFileMountain;
	private File texFileLand;
	private File texFileWater;
	
	public Aufgabe30(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
//		texFileHeightMap = FileLoader.getFileFromFilesFolder("heightmap/mountainlake.jpg", true);
//		texFileHeightMap = FileLoader.getFileFromFilesFolder("heightmap/big_hills.jpg", true);
//		texFileHeightMap = FileLoader.getFileFromFilesFolder("heightmap/craterlake.jpg", true);
		texFileHeightMap = FileLoader.getFileFromFilesFolder("heightmap/Heightmap_Michi.png", true);
		texFileMountain = FileLoader.getFileFromFilesFolder("texture/mauerwerk_small.JPG", true); // TODO get a better one
		texFileLand = FileLoader.getFileFromFilesFolder("texture/gras_small.jpg", true); // TODO get a better one
		texFileWater = FileLoader.getFileFromFilesFolder("heightmap/waterworld.jpg", true); // TODO get a better one
		
//		map = new GLHeightMapRectangle("heightmap/height_little.png", 1.0f, 1.0f, 0.5f, "texture/grass-texture.png", 1);
		//map.scale(4.0f, 2.0f, 4.0f);
		
		start();
	}

	public static void main(String[] args) {
		new Aufgabe30("Aufgabe 30", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();
		gl.glEnable(GL2.GL_CULL_FACE);
		this.getKonfiguration().setCoordinates(false);
		this.getKonfiguration().getCamera().setPosition(new GLVertex(20,20,20));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(-1,-0.5f,-1));
		this.getKonfiguration().getLight().setPosition(new GLVertex(0.0f, 1.0f, 1.0f));
		this.getKonfiguration().getLight().setUseColorMaterial(true);
		this.getKonfiguration().getLight().setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		
		shader = new ShaderProgram("vertexHeightMap.glsl", "fragmentHeightMap.glsl");
		shader.compileShaders(this.getClass(), gl);
		
		heightMap = new GLHeightMapTriangleStrip(50);
		heightMap.setColor(new GLVertex(1,1,1));
		
		GLTexture2D texHeightMap = new GLTexture2D(texFileHeightMap);
		heightMap.setTexture(texHeightMap);
			
		GLTexture2D texMountain = new GLTexture2D(texFileMountain);
		texMountain.setGLTextureWrap(GL2.GL_REPEAT);
		heightMap.setTexture(texMountain, 2);
		
		GLTexture2D texLand = new GLTexture2D(texFileLand);
		texLand.setGLTextureWrap(GL2.GL_REPEAT);
		heightMap.setTexture(texLand, 3);
		
		GLTexture2D texWater = new GLTexture2D(texFileWater);
		texWater.setGLTextureWrap(GL2.GL_REPEAT);
		heightMap.setTexture(texWater, 4);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		
		shader.useProgram(gl);
		shader.setTextureUnit(gl, "texHeightMap", 0);
		shader.setTextureUnit(gl, "texMountain", 2);
		shader.setTextureUnit(gl, "texLand", 3);
		shader.setTextureUnit(gl, "texWater", 4);
		
		//heightMap.glRotateNow(45, 0, 0);
		heightMap.glScaleNow(30, 10, 30);
		heightMap.draw(gl);

		
		ShaderProgram.clearShaderProgram(gl);	
		
		
	}
}