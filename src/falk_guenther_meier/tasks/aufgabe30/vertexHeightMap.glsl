uniform sampler2D texHeightMap;
uniform sampler2D texMountain;
uniform sampler2D texLand;
uniform sampler2D texWater;

void main()
{	
	// Berechnung im Shader Vertex -> TexCoord; bringt aber keinen nennenswerten Geschwindigkeitsvorteil
	//gl_TexCoord[0] = vec4((gl_Vertex.x + 1.0)/2.0, (gl_Vertex.z + 1.0)/2.0, 0.0, 1.0);
	
	// H�he aus der HeightMap auslesen
	gl_TexCoord[0] = gl_MultiTexCoord0;
	vec2 hmTexCoord = vec2( clamp(gl_MultiTexCoord0.s, 0.0000001, 0.9999999), clamp(gl_MultiTexCoord0.t, 0.0000001, 0.9999999) ); // manuelles Clamp, funktioniert sonst nicht
	//vec2 hmTexCoord = gl_TexCoord[0].st;
	float texHeight = texture2D(texHeightMap, hmTexCoord).r;
	
	// Erh�hten Vertex setzen
	vec4 vertex = gl_Vertex;
	vertex.y = texHeight;
	gl_Position = gl_ModelViewProjectionMatrix * vertex;
	
	gl_FrontColor = gl_Color;
}