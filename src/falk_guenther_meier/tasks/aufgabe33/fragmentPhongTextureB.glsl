// Quelle (u.a.): http://www.lighthouse3d.com/tutorials/glsl-tutorial/spot-light-per-pixel/ (03.01.2013)
// http://www.lighthouse3d.com/tutorials/glsl-tutorial/?lights (05.01.2013)
// https://www.opengl.org/sdk/docs/manglsl/xhtml/reflect.xml (05.01.2013)
// http://www.ozone3d.net/tutorials/glsl_lighting_phong_p3.php (05.01.2013)

varying vec3 position;
varying mat3 mTBN;

uniform sampler2D renderTexture;
uniform sampler2D normalTexture;

vec4 ambientColor(int nr) // ambient
{
//	return gl_FrontMaterial.ambient * gl_LightSource[nr].ambient + gl_FrontMaterial.ambient * gl_LightModel.ambient;
	return gl_FrontMaterial.ambient * gl_LightSource[nr].ambient;
}

vec4 diffuseColor(int nr, vec3 direction, float diffuseIntensity) // diffuse
{
	return gl_FrontMaterial.diffuse * diffuseIntensity * gl_LightSource[nr].diffuse;
}

vec4 specularColor(int nr, vec3 direction, float diffuseIntensity, vec3 normal) // specular
{
	// "reflect"-Operator: Phong-Modell (ohne Blinn (Blinn-Phong==Phnong vereinfacht)!!)
	vec3 reflection = normalize(reflect( - normalize(direction), normal));
	float specAngle = max(0.0, dot(normal, reflection));

	vec4 sp = vec4(0.0);
	if (diffuseIntensity != 0.0) {
		float specularValue = pow(specAngle, gl_FrontMaterial.shininess);
		sp += gl_LightSource[nr].specular * gl_FrontMaterial.specular * specularValue; // specular
	}
	return sp;
}

vec4 phong(int nr, vec3 direction, vec3 normal) // ist nur eine Hilfsmethode!
{
	float inten = max(0.0, dot(normal, normalize(direction)));
	vec4 color = diffuseColor(nr, direction, inten);
	color += ambientColor(nr);
	color += specularColor(nr, direction, inten, normal);
	return color;
}


vec4 directionalLight(int nr, vec3 normal) // ambient, diffuse, specular of a directional light
{
	// only color
	return phong(nr, - gl_LightSource[nr].position.xyz, normal);
}

vec4 pointLight(int nr, vec3 normal) // ambient, diffuse, specular of a point light
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// color
	vec4 color = phong(nr, direction, normal);
	// attenuation
	float dist = length(direction); // Abstand zwischen Lichtquelle und Vertex
	color *= 1.0 / (gl_LightSource[nr].constantAttenuation + gl_LightSource[nr].linearAttenuation * dist
		+ gl_LightSource[nr].quadraticAttenuation * dist * dist); // Abschwächung einrechnen
	return color;
}

vec4 spotLight(int nr, vec3 normal)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// spot calculation
	vec3 actDir = normalize(direction).xyz;
	float attenuation = 0.0;
	float spotEffect = dot(normalize(gl_LightSource[nr].spotDirection), - actDir);
	if (spotEffect > gl_LightSource[nr].spotCosCutoff) {
		// zwei verschiedene Varianten ?!
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent); // Intensität berechnen
		//attenuation = pow(spotEffect, gl_LightSource[nr].spotExponent); // Intensität berechnen
	}
	// color
	return attenuation * pointLight(nr, normal); // Abschwächung ist hier bereits eingerechnet!
}

vec4 spotLightDX(int nr, vec3 normal)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	vec3 actDir = normalize(direction).xyz;
	// falloff
	float cos_cur_angle = dot(normalize(gl_LightSource[nr].spotDirection), - actDir); // aktueller Winkel zwischen 
	float cos_outer_cone_angle = 0.866; // 36 degrees -> ist eine Konstante!!
	float cos_inner_cone_angle = gl_LightSource[nr].spotCosCutoff; // halber Strahlwinkel des Spotlights
	float fallOff = clamp((cos_outer_cone_angle - cos_cur_angle) / (cos_inner_cone_angle - cos_outer_cone_angle), 0.0, 1.0);
	// spot calculation
	float attenuation = 0.0;
	if (cos_cur_angle > cos_inner_cone_angle) {
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent); // Intensität berechnen
	}
	// color
	return attenuation * pointLight(nr, normal) * fallOff; // Abschwächung ist hier bereits eingerechnet!
}

// Normalen ("normal"-Parameter) müssen im normalized übergeben werden!!


void main() // eigentliche Start-Methode: Zusammenstellen der einzelnen Lichtquellen
{
	vec3 normalMap = vec3(texture2D(normalTexture, gl_TexCoord[0].st));
	//  Expand the normalMap into a normalized signed vector
    normalMap = normalMap * 2.0 - 1.0;
    normalMap = normalize(normalMap * mTBN);
	
	// get the texture color
	vec4 colorTexture = vec4(texture2D(renderTexture, gl_TexCoord[0].st).rgb, 1.0);
	
	vec3 direction = normalize(gl_LightSource[7].position.xyz - position);
	float intensity = max(0.0, dot(direction, normalMap));
	
	// ambientes und diffuses Licht mit Textur
	vec4 color = colorTexture * (gl_LightSource[7].ambient + gl_LightSource[7].diffuse * intensity);
	
	vec3 reflection = normalize(reflect( - normalize(direction), normalMap));
	float specAngle = max(0.0, dot(normalMap, reflection));
	
	// spekulares Licht mit Textur
	vec4 spec = vec4(0.0);
	if (intensity != 0.0) {
		float specularValue = pow(specAngle, 0.6);
		spec += gl_LightSource[7].specular * colorTexture * specularValue; // specular
	}
	
	gl_FragColor = color + spec;
	
}
