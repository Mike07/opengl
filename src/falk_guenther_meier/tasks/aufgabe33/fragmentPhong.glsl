// Quelle (u.a.): http://www.lighthouse3d.com/tutorials/glsl-tutorial/spot-light-per-pixel/ (03.01.2013)
// http://www.lighthouse3d.com/tutorials/glsl-tutorial/?lights (05.01.2013)
// https://www.opengl.org/sdk/docs/manglsl/xhtml/reflect.xml (05.01.2013)
// http://www.ozone3d.net/tutorials/glsl_lighting_phong_p3.php (05.01.2013)
varying vec3 normal; // Normale
varying vec3 position; // eye ??

vec4 ambientColor(int nr) // ambient
{
//	return gl_FrontMaterial.ambient * gl_LightSource[nr].ambient + gl_FrontMaterial.ambient * gl_LightModel.ambient;
	return gl_FrontMaterial.ambient * gl_LightSource[nr].ambient;
}

vec4 diffuseColor(int nr, vec3 direction, float diffuseIntensity) // diffuse
{
	return gl_FrontMaterial.diffuse * diffuseIntensity * gl_LightSource[nr].diffuse;
}

vec4 specularColor(int nr, vec3 direction, float diffuseIntensity) // specular
{
	vec3 reflection = normalize(reflect( - normalize(direction), normalize(normal))); // reflect: Phong-Modell (ohne Blinn (Blinn-Phong==Phnong vereinfacht)!!)
	float specAngle = max(0.0, dot(normalize(normal), reflection));

	vec4 sp = vec4(0.0);
	if (diffuseIntensity != 0.0) {
		float specularValue = pow(specAngle, gl_FrontMaterial.shininess);
		sp += gl_LightSource[nr].specular * gl_FrontMaterial.specular * specularValue; // specular
	}
	return sp;
}

vec4 phong(int nr, vec3 direction) // ist nur eine Hilfsmethode!
{
	float inten = max(0.0, dot(normalize(normal), normalize(direction)));
	vec4 color = diffuseColor(nr, direction, inten);
	color += ambientColor(nr);
	color += specularColor(nr, direction, inten);
	return color;
}


vec4 directionalLight(int nr) // ambient, diffuse, specular of a directional light
{
	// only color
	return phong(nr, - gl_LightSource[nr].position.xyz);
}

vec4 pointLight(int nr) // ambient, diffuse, specular of a point light
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// color
	vec4 color = phong(nr, direction);
	// attenuation
	float dist = length(direction); // Abstand zwischen Lichtquelle und Vertex
	color *= 1.0 / (gl_LightSource[nr].constantAttenuation + gl_LightSource[nr].linearAttenuation * dist
		+ gl_LightSource[nr].quadraticAttenuation * dist * dist); // Abschwächung einrechnen
	return color;
}

vec4 spotLight(int nr)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	// spot calculation
	vec3 actDir = normalize(direction).xyz;
	float attenuation = 0.0;
	float spotEffect = dot(normalize(gl_LightSource[nr].spotDirection), - actDir);
	if (spotEffect > gl_LightSource[nr].spotCosCutoff) {
		// zwei verschiedene Varianten ?!
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent); // Intensität berechnen
		//attenuation = pow(spotEffect, gl_LightSource[nr].spotExponent); // Intensität berechnen
	}
	// color
	return attenuation * pointLight(nr); // Abschwächung ist hier bereits eingerechnet!
}

vec4 spotLightDX(int nr)
{
	vec3 direction = gl_LightSource[nr].position.xyz - position;
	vec3 actDir = normalize(direction).xyz;
	// falloff
	float cos_cur_angle = dot(normalize(gl_LightSource[nr].spotDirection), - actDir); // aktueller Winkel zwischen 
	float cos_outer_cone_angle = 0.866; // 36 degrees -> ist eine Konstante!!
	float cos_inner_cone_angle = gl_LightSource[nr].spotCosCutoff; // halber Strahlwinkel des Spotlights
	float fallOff = clamp((cos_outer_cone_angle - cos_cur_angle) / (cos_inner_cone_angle - cos_outer_cone_angle), 0.0, 1.0);
	// spot calculation
	float attenuation = 0.0;
	if (cos_cur_angle > cos_inner_cone_angle) {
		attenuation = pow(max(0.0, dot(actDir, normal)), gl_LightSource[nr].spotExponent); // Intensität berechnen
	}
	// color
	return attenuation * pointLight(nr) * fallOff; // Abschwächung ist hier bereits eingerechnet!
}


void main() // eigentliche Start-Methode: Zusammenstellen der einzelnen Lichtquellen
{
vec4 color = vec4(0.0);

// point light 7
color += pointLight(7);

// spot light 3
//color += spotLightDX(3);
//color += spotLight(3);

// directional light 5
//color += directionalLight(5);

gl_FragColor = color;
}
