package falk_guenther_meier.tasks.aufgabe33;

import java.io.File;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.light.GLDirectionLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.Utility;

public class Aufgabe33a extends JoglApp {
	private ShaderProgram shader;
	private ShaderProgram shader2;
	private GLCuboid cube;
	private GLRectangle rect;
	private GLOpenRoom room;
	private File texture_c;
	private File texture_n;
	
	public Aufgabe33a(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
		texture_c = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-c.jpg");
		texture_n = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-n.jpg");
		start();
	}

	public static void main(String[] args) {
		new Aufgabe33a("Aufgabe 33a", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();
		this.getKonfiguration().getCamera().setPosition(new GLVertex(8.0f, 3.0f, 9.0f));
		this.getKonfiguration().getLight().setPosition(new GLVertex(0.0f, 3.0f, 9.0f));
		this.getKonfiguration().getLight().setUseColorMaterial(true);
		this.getKonfiguration().getLight().setAmbient(new GLVertex(0.1f, 0.0f, 0.0f));
		this.getKonfiguration().getLight().setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setAttenuation(0.1f, 0.01f, 0.009f); // c, l, q
		
		GLDirectionLight direct = new GLDirectionLight(GL2.GL_LIGHT5, new GLVertex(1.0f, -1.0f, 0.0f));
		direct.setUseColorMaterial(true);
		direct.setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		direct.setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		direct.setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		this.addLight(direct);
		
		shader = new ShaderProgram("vertexPhongTextureA.glsl", "fragmentPhongTextureA.glsl");
		shader.compileShaders(this.getClass(), gl);
		
		shader2 = new ShaderProgram("vertexPhong.glsl", "fragmentPhong.glsl");
		shader2.compileShaders(this.getClass(), gl);
		
//		loc = gl.glGetUniformLocation(shader.getShaderprogram(), "cameraPos");
		
		// Gebilde wird in viele Teile aufgeteilt
		room = new GLOpenRoom(new GLVertex(), 60.0f, 60.0f, 3f, 0.04f);
		
		cube = new GLCuboid(new GLVertex(2, 2, -4), 1.5f);
		GLMaterial material = new GLMaterial();
		material.setMatShininess(0.6f);
		cube.setMaterial(material);
		cube.setColor(new GLVertex(0.4f, 0.3f, 0.5f));
		
		rect = new GLRectangle(new GLVertex(6,2,0), 2f);
		
		GLTexture2D renderTexture = new GLTexture2D(texture_c);
		GLTexture2D normalTexture = new GLTexture2D(texture_n);
		cube.setTexture(renderTexture, 0);
		cube.setTexture(normalTexture, 1);
		rect.setTexture(renderTexture, 0);
		rect.setTexture(normalTexture, 1);

		Utility.setRandomColor(room);
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		
		gl.glEnable(GL2.GL_CULL_FACE);

		// ben�tigt ??
		gl.glEnable(GL2.GL_COLOR_MATERIAL);
		gl.glEnable(GL2.GL_NORMALIZE);
		gl.glShadeModel(GL2.GL_SMOOTH);

		shader.useProgram(gl);
		shader.setTextureUnit(gl, "renderTexture", 0);
		shader.setTextureUnit(gl, "normalTexture", 1);
		
		rect.draw(gl);
		cube.draw(gl);
		
		shader2.useProgram(gl);
		room.draw(gl);
		
		ShaderProgram.clearShaderProgram(gl);
	}
}
