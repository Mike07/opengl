package falk_guenther_meier.tasks;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import falk_guenther_meier.model.world.JoglApp;

/**
 * @author Michael
 */
public class Aufgabe04 extends JoglApp {
	
	static final long startTime = System.currentTimeMillis();
	
	// GL2-Kontext
	private GL2 gl;
	
	// Zustandsvariablen
	private boolean rotation = true;
	private boolean backfaceCulling = true;
	private boolean depthTest = true;
	private boolean counterClockwiseWinding = true;
	private float cubeAngle = 0;
	private float lastTime = (System.currentTimeMillis() - startTime) / 1000f;
	
	public Aufgabe04(String Name_value, int width, int height) {
		super(Name_value, width, height);
	}

	public static void main(String[] args) {
		new Aufgabe04("Aufgabe 4", 800, 600);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		this.gl = gl;
		
		// Tiefentest aktivieren !!
		if (depthTest)
			gl.glEnable(GL.GL_DEPTH_TEST);
		else
			gl.glDisable(GL.GL_DEPTH_TEST);
		
		// Cullfacing aktivieren
		if (backfaceCulling)
			gl.glEnable(GL.GL_CULL_FACE);
		else
			gl.glDisable(GL.GL_CULL_FACE);
		//glCullface(GL_BACK);
		
		// Counter Clockwise oder Clockwise Winding einstellen
		if (counterClockwiseWinding)
			gl.glFrontFace(GL.GL_CCW); // Counter Clock-Wise
		else
			gl.glFrontFace(GL.GL_CW); //Clock-Wise
		
		// Tiefenpuffer leeren
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
		//gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		
		float[] cubePos = { 0.2f, 0.2f, 0.2f };
		float cubeLength = 0.6f;
		float diffTime = ((System.currentTimeMillis() - startTime) / 1000f) - lastTime;
		
		gl.glPushMatrix();
		if (rotation && diffTime > 0.01) {
			cubeAngle += 1.5;
			// lastTime aktualisieren
			lastTime = (System.currentTimeMillis() - startTime) / 1000f;
		}
		// R�ckverschiebung
		gl.glTranslatef((cubePos[0] + (cubeLength / 2)), (cubePos[1] + (cubeLength / 2)), 0);
		gl.glRotatef(cubeAngle, 0, 0, 1);
		// Verschiebung in Position (0,0,z)
		gl.glTranslatef(-(cubePos[0] + (cubeLength / 2)), -(cubePos[1] + (cubeLength / 2)), 0);
		
		drawCube(cubePos[0], cubePos[1], cubePos[2], cubeLength);
		gl.glPopMatrix();			
	}

	@Override
	public void swapAnimationMode() {
		super.swapAnimationMode();
		rotation = !rotation;
		System.out.println(rotation? "Rotation ein" : "Rotation aus");
	}

	@Override
	public void keyI() {
		backfaceCulling = !backfaceCulling;
		System.out.println(backfaceCulling? "Backface Culling ein" : "Backface Culling aus");
	}

	@Override
	public void keyO() {
		depthTest = !depthTest;
		System.out.println(depthTest? "Depth Test ein" : "Depth Test aus");
	}

	@Override
	public void keyP() {
		counterClockwiseWinding = !counterClockwiseWinding;
		System.out.println(counterClockwiseWinding? "CCW-Winding ein" : "CW-Winding ein");
	}

	/**
	 * Zeichnet einen Quader an die Position (x,y,z) mit den entsprechenden Ma�en
	 * und vordefinierten Farben f�r die Seiten.
	 * @param x
	 * @param y
	 * @param z
	 * @param width
	 * @param height
	 * @param depth
	 */
	public void drawCuboid(float x, float y, float z, float width, float height, float depth) {
		gl.glPushMatrix();
		gl.glBegin(GL2.GL_QUADS);
			// Vorderseite
			gl.glColor3f(0, 1, 0);
			gl.glVertex3f(x, y, z);
			gl.glVertex3f(x + width, y, z);
			gl.glVertex3f(x + width, y + height, z);
			gl.glVertex3f(x, y + height, z);
			
			// Mittlere Fl�chen
			gl.glColor3f(1, 0, 0);
			gl.glVertex3f(x, y, z);
			gl.glVertex3f(x, y + height, z);
			gl.glVertex3f(x, y + height, z - depth);
			gl.glVertex3f(x, y, z - depth);
			
			gl.glColor3f(0, 0, 1);
			gl.glVertex3f(x + width, y, z - depth);
			gl.glVertex3f(x + width, y + height, z - depth);
			gl.glVertex3f(x + width, y + height, z);
			gl.glVertex3f(x + width, y, z);

			gl.glColor3f(0.5f, 0.5f, 0.8f);
			gl.glVertex3f(x, y, z);
			gl.glVertex3f(x, y, z - depth);
			gl.glVertex3f(x + width, y, z - depth);
			gl.glVertex3f(x + width, y, z);
			
			gl.glColor3f(0.8f, 0.5f, 0.5f);
			gl.glVertex3f(x + width, y + height, z);
			gl.glVertex3f(x + width, y + height, z - depth);
			gl.glVertex3f(x, y + height, z - depth);
			gl.glVertex3f(x, y + height, z);
			
			// R�ckseite
			gl.glColor3f(0.5f, 0.8f, 0.5f);
			gl.glVertex3f(x, y + height, z - depth);
			gl.glVertex3f(x + width, y + height, z - depth);
			gl.glVertex3f(x + width, y, z - depth);
			gl.glVertex3f(x, y, z - depth);		
		gl.glEnd();
		gl.glPopMatrix();
	}
	
	/**
	 * Zeichnet einen W�rfel an der Position (x,y,z) mit entsprechender Seitenl�nge
	 * und vordefinierten Farben f�r die Seiten.
	 * @param x
	 * @param y
	 * @param z
	 * @param length
	 */
	public void drawCube(float x, float y, float z, float length) {
		drawCuboid(x, y, z, length, length, length);
	}
	
	/**
	 * Gibt die Belegung der Tasten aus.
	 */
	@Override
	public void printMyKeys() {
		System.out.println("Backface Culling ein/aus: i");
		System.out.println("Depth Test ein/aus: o");
		System.out.println("CCW oder CW-Winding: p");
		System.out.println("Rotation ein/aus: SPACE");
	}
}
