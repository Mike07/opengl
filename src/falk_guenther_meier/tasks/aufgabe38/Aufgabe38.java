package falk_guenther_meier.tasks.aufgabe38;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.light.GLSpotLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;

public class Aufgabe38 extends JoglApp {
	private ShaderProgram shader;
	private GLCuboid cube;
	private GLCuboid cube2;
	private GLCustomComplexObject objects;
	private GLObject lightCube;
	
	private GLOpenRoom room;
	
	private GLSpotLight spotLight;
	private GLCamera lightCam;
	
	private GLTexture2D shadowTexture;
	private int shadowSizeX;
	private int shadowSizeY;
	private float[] lightProjection = new float[16];
	private float[] lightModelview = new float[16]; // == inverse ModelView Matrix f�r Licht-Sicht
	private float[] shadowTextureMatrix = new float[16];
	
	public Aufgabe38(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, true);
	}

	public static void main(String[] args) {
		new Aufgabe38("Aufgabe 38", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();
		this.getKonfiguration().getCamera().setPosition(new GLVertex(10f, 10f, 10f));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(-5f, -5f, -5f));
		
		shader = new ShaderProgram("vertexOut.glsl", "fragmentOut.glsl");
		shader.compileShaders(this.getClass(), gl);
		
		GLVertex lightPos = new GLVertex(10f, 10f, 10f);
		GLVertex lightDirection = new GLVertex(-5, -5, -5);
		spotLight = new GLSpotLight(GL2.GL_LIGHT2, lightPos, lightDirection);
		spotLight.setAmbient(new GLVertex(0.7f, 0.7f, 0.7f));
		spotLight.setDiffuse(new GLVertex(1f, 0.5f, 0.5f));
		spotLight.setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		spotLight.setSpotCutoff(40f);
		spotLight.setSpotExponent(0.9f);
		spotLight.setAttenuation(1.0f, 0.01f, 0.00f);
		lightCube = new GLCuboid(lightPos, 0.5f);
		lightCube.setColor(new GLVertex(1f, 1f, 1f));
		
		lightCam = new GLCamera(lightPos, lightDirection);
		
		this.addLight(spotLight);
		
		cube = new GLCuboid(new GLVertex(2, 2, -4), 1.5f);
		GLMaterial material = new GLMaterial();
		material.setMatShininess(0.6f);
		cube.setMaterial(material);
		
		cube2 = new GLCuboid(new GLVertex(-2,2,3), 2f);
		cube2.rotate(45, 45, 45);
		
		cube.setColor(new GLVertex(0.2f, 0.1f, 0.3f));
		cube2.setColor(new GLVertex(0.4f, 0.4f, 0.2f));
		
		objects = new GLCustomComplexObject();
		objects.add(cube);
		objects.add(cube2);

		room = new GLOpenRoom(new GLVertex(), 60.0f, 60.0f, 3f, 0.04f);
		room.setColor(new GLVertex(1f,0.5f,0.5f));
//		room.setMaterial(GLMaterial.PEARL);
		
		// Textur generieren und auf W�rfel setzen
		shadowSizeX = 512;
		shadowSizeY = 512;
		shadowTexture = new GLTexture2D(gl, shadowSizeX, shadowSizeY);
		objects.setTexture(shadowTexture, 0);
	}

	@Override
	public void display(GLAutoDrawable drawable) {
	    GL2 gl = drawable.getGL().getGL2();

		gl.glViewport(0, 0, this.getWindowWidth(), this.getWindowHeight());
	    gl.glLoadIdentity();
	    // Projektion setzen
		gl.glMatrixMode(GL2.GL_PROJECTION);
	    gl.glPushMatrix();
	    gl.glLoadIdentity();
	    int left = -10, right = 10, top = 10, bottom = -10, near = 0, far = 40;
	    gl.glOrthof(left, right, bottom, top, near, far);
	    gl.glGetFloatv(GL2.GL_PROJECTION_MATRIX, lightProjection, 0);
	    
	    // Modelview auf Lichtposition setzen
	    gl.glMatrixMode(GL2.GL_MODELVIEW);
	    gl.glPushMatrix();
	    gl.glLoadIdentity();
	    lightCam.refreshCamera();
	    gl.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, lightModelview, 0);
	    
	    gl.glPushAttrib(GL2.GL_VIEWPORT_BIT);
		gl.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, lightModelview, 0);
		gl.glViewport(0, 0, shadowSizeX, shadowSizeY);
	    
	    // Rendern vorbereiten
	    gl.glClear(GL.GL_DEPTH_BUFFER_BIT);
	    // au�er Tiefenpuffer wird nichts gebraucht
	    gl.glShadeModel(GL2.GL_FLAT);
	    gl.glDisable(GL2.GL_LIGHTING);
	    gl.glDisable(GL2.GL_TEXTURE_2D);
	    gl.glDisable(GL2.GL_NORMALIZE);
	    gl.glEnable(GL2.GL_CULL_FACE);
	    gl.glColorMask(false, false, false, false);
	    // Genauigkeit
	    float polygonOffsetFactor = 1.5f, polygonOffsetUnit = 4;
	    gl.glPolygonOffset(polygonOffsetFactor, polygonOffsetUnit);
	    gl.glEnable(GL2.GL_POLYGON_OFFSET_FILL);
	    
	    // Objekte erstellen und Tiefentextur erstellen (Texture-Unit 3)
	    objects.draw(gl);
	    room.draw(gl);
		
	    gl.glActiveTexture(GL.GL_TEXTURE3);
	    shadowTexture.bind(gl);
	    gl.glCopyTexImage2D(GL.GL_TEXTURE_2D, 0, GL2.GL_DEPTH_COMPONENT, 0, 0, shadowSizeX, shadowSizeY, 0);
	    
	    // Einstellungen r�ck�ngig machen
	    gl.glShadeModel(GL2.GL_SMOOTH);
	    gl.glEnable(GL2.GL_LIGHTING);
	    gl.glEnable(GL2.GL_TEXTURE_2D);
	    gl.glEnable(GL2.GL_NORMALIZE);
	    gl.glColorMask(true, true, true, true);
	    gl.glDisable(GL.GL_POLYGON_OFFSET_FILL);
	    
	    // Texture matrix f�r Schattenprojektion (gl_TextureMatrix[3])
	    gl.glMatrixMode(GL.GL_TEXTURE);
	    gl.glLoadIdentity();
	    gl.glTranslatef(0.5f, 0.5f, 0.5f);
	    gl.glScalef(0.5f, 0.5f, 0.5f);
	    gl.glMultMatrixf(lightProjection, 0);
	    gl.glMultMatrixf(lightModelview, 0);
	    gl.glGetFloatv(GL2.GL_TEXTURE_MATRIX, shadowTextureMatrix, 0);
	    gl.glActiveTexture(GL2.GL_TEXTURE0);
	    
	    // alternativer Versuch
//	    gl.glMatrixMode(GL.GL_TEXTURE);
//	    float[] bias = {
//			0.5f, 0.0f, 0.0f, 0.0f, 
//			0.0f, 0.5f, 0.0f, 0.0f,
//			0.0f, 0.0f, 0.5f, 0.0f,
//		0.5f, 0.5f, 0.5f, 1.0f};
//	    gl.glLoadMatrixf(bias, 0);
//	    gl.glMultMatrixf(lightProjection, 0);
//	    gl.glMultMatrixf(lightModelview, 0);
//	    gl.glActiveTexture(GL2.GL_TEXTURE0);
	    
	    // Matrizen auf alten Status setzen
	    gl.glActiveTexture(GL2.GL_TEXTURE0);
	    gl.glMatrixMode(GL2.GL_MODELVIEW);
	    gl.glPopMatrix();
	    gl.glMatrixMode(GL2.GL_PROJECTION);
	    gl.glPopMatrix();
		gl.glMatrixMode(GL2.GL_MODELVIEW);
	    gl.glViewport(0, 0, this.getWindowWidth(), this.getWindowHeight());
//	    gl.glMatrixMode(GL2.GL_MODELVIEW);
		super.display(drawable);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
//		GLU glu = new GLU();
	    // .. some variable parameters of the shadow map
//	    gl.glActiveTexture(GL2.GL_TEXTURE3);
//	    gl.glEnable(GL2.GL_TEXTURE_2D);
//	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_MODE, GL2.GL_COMPARE_R_TO_TEXTURE);
//	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_FUNC, GL2.GL_LESS);
//	    gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_DEPTH_TEXTURE_MODE, GL2.GL_INTENSITY);
//	    gl.glActiveTexture(GL2.GL_TEXTURE0);
//	    gl.glEnable(GL2.GL_TEXTURE_2D);
		
		room.draw(gl); // Bodenplatte zeichnen -> Aufw�ndig!
		lightCube.draw(gl);
		
		shader.useProgram(gl);
		shader.setTextureUnit(gl, "shadowMap", 0);
		
		objects.draw(gl);
		
		ShaderProgram.clearShaderProgram(gl);
	}
}
