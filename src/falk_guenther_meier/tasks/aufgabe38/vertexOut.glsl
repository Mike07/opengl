varying vec3 st;
varying vec3 vNormal;
varying vec3 position;

void main() {
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	st = vec3(gl_MultiTexCoord0.xyz);
	position = (gl_ModelViewMatrix * gl_Vertex).xyz;
	vNormal = normalize(gl_NormalMatrix * gl_Normal);
}
