package falk_guenther_meier.tasks;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.world.JoglApp;

public class Aufgabe16 extends JoglApp {
	GLCustomComplexObject complexObject = new GLCustomComplexObject();
	GLCuboid front;
	GLCuboid back;
	GLCuboid cuboid;

	public Aufgabe16(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
		getKonfiguration().changeCamera(new GLCamera(new GLVertex(0.0f, 0.0f, 6.0f), new GLVertex(0.1f, 0.2f, -2.0f)));
		this.addKeyboardObserver(this.getKonfiguration().getCamera());
		complexObject = new GLCustomComplexObject();
		cuboid = new GLCuboid(new GLVertex(1.0f, 1.0f, -2.0f), 1.0f);
		cuboid.setColor(new GLVertex(0.f, 0.5f, 0.5f));
		complexObject.add(cuboid);

		front = new GLCuboid(new GLVertex(1.2f, 1.2f, 0.0f), 1.0f, 1.0f, 0.1f);
		back = new GLCuboid(new GLVertex(1.1f, 1.1f, -1.0f), 1.0f, 1.0f, 0.1f);

		front.setColor(new GLVertex(1.0f, 0.0f, 0.0f, 0.5f));
		back.setColor(new GLVertex(1.0f, 0.0f, 1.0f, 0.5f));

		complexObject.add(front);
		complexObject.add(back);
	}

	public static void main(String[] args) {
		new Aufgabe16("Aufgabe 16", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		//gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
		gl.glEnable(GL2.GL_CULL_FACE);
		gl.glEnable(GL.GL_DEPTH_TEST);

		cuboid.draw(gl);

		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glDepthMask(false);

//		complexObject.draw(gl);
		front.draw(gl);
		back.draw(gl);

		gl.glDepthMask(true);
		
	}
	
	@Override
	public void endGL(GL2 gl) {
		gl.glDisable(GL2.GL_BLEND);
		super.endGL(gl);
	}
	
}
