package falk_guenther_meier.tasks.aufgabe23;

import java.io.File;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLBillBoard;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.Utility;

public class Aufgabe23 extends JoglApp {
	private GLCuboid cube;
	private GLBillBoard bb;
	private ShaderProgram shader;

	public Aufgabe23(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false); // false -> noch nicht zeichen

		File textureFile = FileLoader.getFileFromFilesFolder("billboard/baum.png", true);
		GLTexture2D rectTexture = new GLTexture2D(textureFile);
		bb = new GLBillBoard(this.getKonfiguration().getCamera(), rectTexture, new GLVertex(), 0.5f, 1.0f);
		bb.scale(3.5f, 3.5f, 3.5f);
		bb.translate(1, 1, 1);

		cube = new GLCuboid(new GLVertex(0,0,0.0f), 1.0f);

		GLVertex a = GLVertex.add(cube.getRectFront().getNodes()[0], cube.getRectFront().getPosition()); // ein Eckpunkt des Rechtecks
		GLVertex c = GLVertex.add(cube.getRectFront().getNodes()[2], cube.getRectFront().getPosition()); // gegen�ber liegender Eckpunkt
		GLVertex center = GLVertex.add(a, GLVertex.mult(GLVertex.sub(c, a), 0.5f)); // Mittelpunkt des Rechtecks
		cube.setReferencePoint(center.clone());
		
		cube.setColor(new GLVertex(1.0f, 1.0f, 0.0f));
		Utility.setRandomColor(cube);
		
		start(); // erst ab jetzt darf gezeichnet werden, da jetzt erst die Texturen verf�gbar sind
	}

	public static void main(String[] args) {
		new Aufgabe23("Aufgabe 23", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		this.getKonfiguration().setCoordinates(false);
		GL2 gl = drawable.getGL().getGL2();
		shader = new ShaderProgram("vertexLightTexture.glsl", "fragmentLightTexture.glsl");
		shader.compileShaders(this.getClass(), gl);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
//		gl.glEnable(GL.GL_DEPTH_TEST);
//		gl.glEnable(GL2.GL_CULL_FACE);
//		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
		
		shader.setTextureUnit(gl, "texture", 0);
		shader.useProgram(gl);
		cube.draw(gl);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glEnable(GL2.GL_BLEND);
		bb.draw(gl);
		gl.glDisable(GL2.GL_BLEND);
		ShaderProgram.clearShaderProgram(gl);
	}	
}
