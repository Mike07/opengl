// Aufgaben 33, 35
varying vec3 position;

void main()
{	
	vec4 position4 = gl_ModelViewMatrix * gl_Vertex;
	position = position4.xyz / position4.w;

	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

	gl_TexCoord[0] = gl_MultiTexCoord0;

	vec3 normal = normalize(gl_Normal);
}