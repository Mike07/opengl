package falk_guenther_meier.tasks.aufgabe35;

import java.io.File;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.Utility;

public class Aufgabe35 extends JoglApp {
	private ShaderProgram shader;
	private ShaderProgram shader2;
	private GLCuboid cube;
	private GLRectangle rect;
	private GLRectangle rect2;
	private GLRectangle rect3;
	private GLRectangle rect4;
	private GLOpenRoom room;
	private File texture_c;
	private File texture_n;
	private File texture2_c;
	private File texture2_n;

	private GLPointLight pointLight;
	
	public Aufgabe35(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
		texture_c = FileLoader.getFileFromFilesFolder("texture/bumpmapping/brickwork-c.jpg");
		texture_n = FileLoader.getFileFromFilesFolder("texture/bumpmapping/brickwork-n.jpg");
		texture2_c = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-c.jpg");
		texture2_n = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-n.jpg");
		start();
	}

	public static void main(String[] args) {
		new Aufgabe35("Aufgabe 35", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();
		this.getKonfiguration().getCamera().setPosition(new GLVertex(20.0f, 20.0f, 20.0f));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(-1.0f, -1.0f, -1.0f));
		
		pointLight = new GLPointLight(GL2.GL_LIGHT7, new GLVertex(9f, 10f, 0f)); // new GLVertex(-2f, 5f, 3f)
		pointLight.setUseColorMaterial(true);
		pointLight.setAmbient(new GLVertex(0.7f, 0.7f, 0.7f, 1.0f));
		pointLight.setDiffuse(new GLVertex(0.8f, 0.8f, 0.8f, 1.0f));
		pointLight.setSpecular(new GLVertex(0.8f, 0.8f, 0.8f, 1.0f));
		pointLight.setAttenuation(1.0f, 0.00f, 0.001f);
		
		GLCuboid lightCube = new GLCuboid(new GLVertex(0f, 0f, 0f), 0.5f);
		lightCube.setColor(new GLVertex(1f, 1f, 1f));
		pointLight.setLightObject(lightCube);
		this.addLight(pointLight);
		
		shader = new ShaderProgram("vertexPhongTextureB.glsl", "fragmentPhongTexture.glsl"); // fragmentPhongTextureB
		shader.compileShaders(this.getClass(), gl);
		
		shader2 = new ShaderProgram("vertexPhong.glsl", "fragmentPhong.glsl");
		shader2.compileShaders(this.getClass(), gl);

		// Gebilde wird in viele Teile aufgeteilt
		room = new GLOpenRoom(new GLVertex(), 60.0f, 60.0f, 3f, 0.04f);
		
		cube = new GLCuboid(new GLVertex(2, 2, -4), 1.5f);
		GLMaterial material = new GLMaterial();
		material.setMatShininess(0.2f);
		cube.setMaterial(material);
		cube.setColor(new GLVertex(0.4f, 0.3f, 0.5f));
		
		cube.setShader(shader);

		rect = new GLRectangle(new GLVertex(-4,3,0), 2f);
		rect.rotate(0, 90, 0);
		rect.setShader(shader);

		rect2 = new GLRectangle(new GLVertex(4,3,0), 2f);
		rect2.rotate(0, -90, 0);
		rect2.setShader(shader);

		rect3 = new GLRectangle(new GLVertex(0,6,-2), 2f);
		rect3.rotate(90, 0, 0);
		rect3.setShader(shader);

		rect4 = new GLRectangle(new GLVertex(-4, 4, -2), 4f);
		rect4.rotate(0, 45, 0);
		rect4.setShader(shader);
		
		GLTexture2D renderTexture = new GLTexture2D(texture_c);
		GLTexture2D normalTexture = new GLTexture2D(texture_n);
		GLTexture2D renderTexture2 = new GLTexture2D(texture2_c);
		GLTexture2D normalTexture2 = new GLTexture2D(texture2_n);
		
		cube.setTexture(renderTexture, 0);
		cube.setTexture(normalTexture, 1);
		rect.setTexture(renderTexture, 0);
		rect.setTexture(normalTexture, 1);
		rect2.setTexture(renderTexture, 0);
		rect2.setTexture(normalTexture, 1);
		rect3.setTexture(renderTexture2, 0);
		rect3.setTexture(normalTexture2, 1);
		rect4.setTexture(renderTexture2, 0);
		rect4.setTexture(normalTexture2, 1);

		Utility.setRandomColor(room);
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		
		gl.glEnable(GL2.GL_CULL_FACE);
		
		// PointLight rotieren
		GLVertex pointLightPos = pointLight.getPosition();
		pointLightPos.mult(GLMatrix.createRotateYMatrix(1f));
		pointLight.setPosition(pointLightPos);

		// ben�tigt ??
		gl.glEnable(GL2.GL_COLOR_MATERIAL);
		gl.glEnable(GL2.GL_NORMALIZE);
		gl.glShadeModel(GL2.GL_SMOOTH);

		shader.useProgram(gl);
		shader.setTextureUnit(gl, "renderTexture", 0);
		shader.setTextureUnit(gl, "normalTexture", 1);

		rect.draw(gl);		
		rect2.draw(gl);
		rect3.draw(gl);
		rect4.draw(gl);

		cube.rotate(1f, 0.5f, 0.7f);
		cube.draw(gl);

		shader2.useProgram(gl);

		room.draw(gl);
		
		ShaderProgram.clearShaderProgram(gl);
	}
}
