varying vec3 normal; // Normale
varying vec3 position; // eye ??

void main()
{
normal = gl_NormalMatrix * gl_Normal;

vec4 position4 = gl_ModelViewMatrix * gl_Vertex;
position = position4.xyz / position4.w;

gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
