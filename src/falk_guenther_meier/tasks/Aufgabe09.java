package falk_guenther_meier.tasks;

import java.awt.event.KeyEvent;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;

import falk_guenther_meier.model.buildings.DemoLandscape;
import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.util.JoglAppLight;

public class Aufgabe09 extends JoglAppLight {

	GLVertex eye = new GLVertex(6, 0, 6);
	GLVertex center = new GLVertex(0, 0, 0);
	GLVertex up = new GLVertex(0, 1, 0);
	GLVertex viewingDirection = GLVertex.sub(center, eye);
	GLVertex virtualYAxis = new GLVertex(0, 1, 0);
	float viewAngle = -45;
	float angleDelta = 0.15f;
	// Geschwindigkeit der Kamera (kann ge�ndert werden!)
	float speed = 10;
	
	boolean keyW = false;
	boolean keyS = false;
	boolean keyA = false;
	boolean keyD = false;
	boolean keyUp = false;
	boolean keyDown = false;
	boolean keyLeft = false;
	boolean keyRight = false;
	
	GLVertex cuboidPos = new GLVertex(-1, -0.1f, -1);
		
	public Aufgabe09(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
		this.printKeys();
	}
	
	public static void main(String[] args) {
		new Aufgabe09("Aufgabe 9", 800, 600);
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glEnable(GL2.GL_DEPTH_TEST);
		gl.glEnable(GL2.GL_CULL_FACE);
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
		
		gl.glLoadIdentity();
		keyControl(gl);
		super.display(drawable);
		
		//GLCuboid cuboid = new GLCuboid(cuboidPos, 6, 2, 6);
		//cuboid.draw(gl);
		DemoLandscape land = new DemoLandscape(cuboidPos);
		land.draw(gl);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_W) {
			keyW = true;
		}
		else if (e.getKeyCode() == KeyEvent.VK_S) {
			keyS = true;
		}
		else if (e.getKeyCode() == KeyEvent.VK_A) {
			keyA = true;
		} 
		else if (e.getKeyCode() == KeyEvent.VK_D) {
			keyD = true;
		}
		else if (e.getKeyCode() == KeyEvent.VK_UP) {
			keyUp = true;
		}
		else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			keyDown = true;
		}
		else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			keyLeft = true;
		} 
		else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			keyRight = true;
		}
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_W) {
			keyW = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_S) {
			keyS = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_A) {
			keyA = false;
		} 
		if (e.getKeyCode() == KeyEvent.VK_D) {
			keyD = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			keyUp = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			keyDown = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			keyLeft = false;
		} 
		if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			keyRight = false;
		}
	}
	
	/**
	 * Gibt die Belegung der Tasten aus.
	 */
	private void printKeys() {
		System.out.println();
		System.out.println("Vorw�rts: Pfeiltaste oben");
		System.out.println("R�ckw�rts: Pfeiltaste unten");
		System.out.println("Linksdrehung: Pfeiltaste links");
		System.out.println("Rechtsdrehung: Pfeiltaste rechts");
		System.out.println();
	}
	
	/**
	 * Erm�glicht das Steuern der Kamera mit der Tastatur.
	 */
	private void keyControl(GL2 gl) {
		GLU glu = new GLU();
		if (keyW) {
			// Vorw�rtsbewegung
			GLVertex step = GLVertex.mult(GLVertex.normalize(viewingDirection), 1/40f * speed);
			eye.add(step);
			center.add(step);
		}
		if (keyS) {
			// R�ckw�rtsbewegung
			GLVertex step = GLVertex.mult(GLVertex.invert(GLVertex.normalize(viewingDirection)), 1/40f * speed);
			eye.add(step);
			center.add(step);
		}
		if (keyA) {
			// Linksbewegung
			eye.addValues(viewingDirection.getZ() / 300 * speed, 0, -viewingDirection.getX() / 300 * speed);
			center.addValues(viewingDirection.getZ() / 300 * speed, 0, -viewingDirection.getX() / 300 * speed);
		}
		if (keyD) {
			// Rechtsbewegung
			eye.addValues(-viewingDirection.getZ() / 300 * speed, 0, viewingDirection.getX() / 300 * speed);
			center.addValues(-viewingDirection.getZ() / 300 * speed, 0, viewingDirection.getX() / 300 * speed);
		}
		if (keyUp) {
			// Rotation oben
			viewingDirection.add(new GLVertex(0, 1/50f * speed, 0));
			center.setValues(GLVertex.add(eye, viewingDirection));
		}
		if (keyDown) {
			// Rotation unten
			viewingDirection.add(new GLVertex(0, -1/50f * speed, 0));
			center.setValues(GLVertex.add(eye, viewingDirection));
		}
		if (keyLeft) {
			// Rotation links
			viewingDirection.mult(GLMatrix.createRotateYMatrix(angleDelta * speed));
			center.setValues(GLVertex.add(eye, viewingDirection));
		}
		if (keyRight) {
			// Rotation rechts
			viewingDirection.mult(GLMatrix.createRotateYMatrix(-angleDelta * speed));
			center.setValues(GLVertex.add(eye, viewingDirection));
		}
		glu.gluLookAt(eye.getX(), eye.getY(), eye.getZ(), center.getX(), center.getY(), center.getZ(), up.getX(), up.getY(), up.getZ());
	}	
}
