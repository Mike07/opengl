uniform sampler2D texCubeMap;
varying vec3 reflectDir;

void main() {	
	gl_Position = ftransform();   
	vec4 position = gl_ModelViewMatrix * gl_Vertex;
	vec3 normal = normalize(gl_NormalMatrix * gl_Normal);
	reflectDir = reflect(position.xyz, normal) * gl_NormalMatrix;
}