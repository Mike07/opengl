package falk_guenther_meier.tasks.aufgabe32;

import java.io.File;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import com.jogamp.opengl.util.gl2.GLUT;

import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture;
import falk_guenther_meier.model.textures.GLTextureCube;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.SkyBoxLoader;

public class Aufgabe32 extends JoglApp {
	private ShaderProgram shader;
	private File texSkyMap_right;
	private File texSkyMap_left;
	private File texSkyMap_top;
	private File texSkyMap_down;
	private File texSkyMap_front;
	private File texSkyMap_back;
	private GLTexture textureCube;
	
	public Aufgabe32(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
	
		this.getKonfiguration().setSkybox(SkyBoxLoader.getOceanInletSkyBox(this.getKonfiguration().getCamera(), 100));
		
		texSkyMap_right = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_right.png", true);
		texSkyMap_left = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_left.png", true);
		texSkyMap_top = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_top.png", true);
		texSkyMap_down = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_down.png", true);
		texSkyMap_front = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_front.png", true);
		texSkyMap_back = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_back.png", true);
		
		/*texSkyMap_right = FileLoader.getFileFromFilesFolder("skybox/sundown/jajsundown1_right.jpg", true);
		texSkyMap_left = FileLoader.getFileFromFilesFolder("skybox/sundown/jajsundown1_back.jpg", true);
		texSkyMap_top = FileLoader.getFileFromFilesFolder("skybox/sundown/jajsundown1_top.jpg", true);
		texSkyMap_down = FileLoader.getFileFromFilesFolder("skybox/sundown/jajsundown1_top.jpg", true);
		texSkyMap_front = FileLoader.getFileFromFilesFolder("skybox/sundown/jajsundown1_front.jpg", true);
		texSkyMap_back = FileLoader.getFileFromFilesFolder("skybox/sundown/jajsundown1_back.jpg", true);*/
		
		textureCube = new GLTextureCube(new File[]{texSkyMap_right, texSkyMap_left, texSkyMap_top, texSkyMap_down, texSkyMap_front, texSkyMap_back});
		//textureCube = new GLTextureCube(new File[]{texSkyMap_left, texSkyMap_right, texSkyMap_top, texSkyMap_down, texSkyMap_front, texSkyMap_back});

		start();
	}

	public static void main(String[] args) {
		new Aufgabe32("Aufgabe 32", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();
		gl.glEnable(GL2.GL_CULL_FACE);
		this.getKonfiguration().getCamera().setPosition(new GLVertex(0,1,10));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(0,0,-1));
		
		this.getKonfiguration().getLight().setPosition(new GLVertex(0.0f, 1.0f, 1.0f));
		this.getKonfiguration().getLight().setUseColorMaterial(true);
		this.getKonfiguration().getLight().setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		
		shader = new ShaderProgram("vertexCubicReflection.glsl", "fragmentCubicReflection.glsl");
		shader.compileShaders(this.getClass(), gl);
	}

	
	@Override
	public void beginGL(GL2 gl) {
		
		super.beginGL(gl);
		// etwas gehackte Drehung
		float rotateFaktor = 0.5f;
		GLVertex eye = this.getKonfiguration().getCamera().getPosition().clone();
		eye.mult(GLMatrix.createRotateMatrix(0, rotateFaktor, 0));
		this.getKonfiguration().getCamera().setPosition(eye);
		
		GLVertex viewDir = this.getKonfiguration().getCamera().getDirection();
		viewDir.mult(GLMatrix.createRotateMatrix(0, rotateFaktor, 0));
		this.getKonfiguration().getCamera().setDirection(viewDir);

		shader.useProgram(gl);
		shader.setTextureUnit(gl, "textureCubeMap", 0);
		
		gl.glEnable(GL2.GL_TEXTURE_CUBE_MAP);
		textureCube.bind(gl);
		
		GLUT glut = new GLUT();
		
		//glut.glutSolidCube(2);
		glut.glutSolidSphere(1, 100, 100);
		//glut.glutSolidCone(1f, 1f, 10, 5);

		gl.glDisable(GL2.GL_TEXTURE_CUBE_MAP);
		
		ShaderProgram.clearShaderProgram(gl);	
		
		
	}
	
}