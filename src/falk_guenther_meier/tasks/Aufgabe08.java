package falk_guenther_meier.tasks;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.ObjLoader;

public class Aufgabe08 extends JoglApp {
		
	GLCustomComplexObject complexObject = new GLCustomComplexObject();
	
	public Aufgabe08(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
		complexObject = ObjLoader.objFile2ComplexObj();
	}
	
	public static void main(String[] args) {
		new Aufgabe08("Aufgabe 8", 800, 600);
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		gl.glEnable(GL2.GL_DEPTH_TEST);
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
		
		//super.display(drawable);
		
		/*
		float[] modelViewMatrix = new float[16];
		gl.glGetFloatv(GLMatrixFunc.GL_MODELVIEW_MATRIX, modelViewMatrix, 0);
		GLMatrix glMatrix= new GLMatrix(modelViewMatrix); */

		
		//gl.glPushMatrix();
		
		
		gl.glColor3f(0, 0, 1);
		
		GLMatrix glMatrix = GLMatrix.createIdentityMatrix();
		glMatrix.rotate(45, 45, 45);
		glMatrix.translate(0, 0, -10);
		
		gl.glLoadMatrixf(glMatrix.get(), 0);
		
		if (complexObject.hasObjects()) {
			complexObject.draw(gl);
		} else {
			GLObject cuboid = new GLCuboid(new GLVertex(0,0,0),1,1,1);		
			cuboid.draw(gl);
		}
		
		//gl.glPopMatrix();
	}	
}

