package falk_guenther_meier.tasks;

import java.io.File;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.textures.GLTexGrid;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;

public class Aufgabe18 extends JoglApp {
	private GLCuboid cube;
	private GLRectangle rect1, rect2;
	private GLTexture2D cubeTexture;
	
	public Aufgabe18(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false); // false -> noch nicht zeichen
		

		
		GLVertex v0 = new GLVertex(-0.5f, -0.5f, 2.0f);	
		GLVertex v1 = new GLVertex(0.5f, -0.5f, 2.0f);
		GLVertex v2 = new GLVertex(0.5f, 0.5f, 2.0f);
		GLVertex v3 = new GLVertex(-0.5f, 0.5f, 2.0f);
		rect1 = new GLRectangle(v0, v1, v2, v3, true);
		File textureFile = FileLoader.getFileFromFilesFolder("dome256.jpg", true);	
		GLTexture2D rectTexture = new GLTexture2D(textureFile);
		rect1.setTexture(rectTexture); // Texturkoordinaten werden bei einer einfachen Textur (1 Spalte, 1 Zeile) automatisch gesetzt
		
		rect2 = new GLRectangle(new GLVertex(0,0,3), 1.2f, 1.2f);
		rect2.setTexture(rectTexture);
		rect2.setTexRect(0, 0, 1, true); // 1x horizontal spiegeln dann 1x um 90� gegen den Uhrzeigersinn drehen
		
		cube = new GLCuboid(new GLVertex(0,0,-2), 1);
		textureFile = FileLoader.getFileFromFilesFolder("skybox/skybox_texture2.jpg", true);	
		cubeTexture = new GLTexture2D(textureFile); // Textur mit Gitter aus 4 Spalten und 3 Zeilen
		cubeTexture.setTexGrid(new GLTexGrid(4,3));
		cube.setTexture(cubeTexture);
		
		// jetzt Texturen aus Zeilen und Spalten richtig zuordnen
		cube.getRectFront().setTexRect(3,1);
		cube.getRectRear().setTexRect(1,1);
		cube.getRectLeft().setTexRect(0,1);
		cube.getRectRight().setTexRect(2,1);
		cube.getRectTop().setTexRect(1,0);
		cube.getRectBottom().setTexRect(1,2);
		
		start(); // erst ab jetzt darf gezeichnet werden, da jetzt erst die Texturen verf�gbar sind
	}

	public static void main(String[] args) {
		new Aufgabe18("Aufgabe 18", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		//GL2 gl = drawable.getGL().getGL2();
		//glTexture01.load(gl); // ist aber nicht unbedingt n�tig, wird auch automatisch beim ersten display-Aufruf gemacht
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);

		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glEnable(GL2.GL_CULL_FACE);
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
		
		
		//gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_DECAL);
		
		gl.glColor3f(1, 1, 1);
		
		cube.draw(gl);
		rect1.draw(gl);
		rect2.draw(gl);
		
		// Rechteck 3 manuell mit Ausschnitt RectTop (1,0) von cubeTexture zeichen:
		cubeTexture.bind(gl);
		
		float width = 1f/4f;
		float height = 1f/3f;
		float x = 1f/4f;
		float y = 0;
		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glBegin(GL2.GL_QUADS);
			gl.glTexCoord2f(x, y+height);
			gl.glVertex4f(-0.5f, -0.5f, 0.0f, 1.0f);
			
			gl.glTexCoord2f(x+width, y+height);
			gl.glVertex4f(0.5f, -0.5f, 0.0f, 1.0f);
			
			gl.glTexCoord2f(x+width, y);
			gl.glVertex4f(0.5f, 0.5f, 0.0f, 1.0f);
			
			gl.glTexCoord2f(x, y);
			gl.glVertex4f(-0.5f, 0.5f, 0.0f, 1.0f);
		gl.glEnd();
		gl.glDisable(GL.GL_TEXTURE_2D);
	}	
}
