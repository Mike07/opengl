package falk_guenther_meier.tasks.aufgabe37;

import java.awt.Color;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLFramebuffer;
import falk_guenther_meier.model.world.JoglApp;

public class Aufgabe37 extends JoglApp {
	private ShaderProgram shader;
	private GLCuboid cube;
	private GLCuboid cube2;
	private GLRectangle rect;
	private GLOpenRoom room;
	private GLPointLight pointLight;
	private GLFramebuffer framebuffer;
	
	public Aufgabe37(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, true);
		//start();
	}

	public static void main(String[] args) {
		new Aufgabe37("Aufgabe 37", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();
		this.getKonfiguration().getCamera().setPosition(new GLVertex(10.0f, 4.0f, 13.0f));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(-0.8f, 0.0f, -1.0f));
//		this.getKonfiguration().getLight().setPosition(new GLVertex(0.0f, 3.0f, 9.0f));
//		this.getKonfiguration().getLight().setUseColorMaterial(true);
//		this.getKonfiguration().getLight().setAmbient(new GLVertex(0.1f, 0.0f, 0.0f));
//		this.getKonfiguration().getLight().setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
//		this.getKonfiguration().getLight().setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
//		this.getKonfiguration().getLight().setAttenuation(0.1f, 0.01f, 0.009f); // c, l, q
		
		shader = new ShaderProgram("vertexPhong.glsl", "fragmentPhong.glsl");
		shader.compileShaders(this.getClass(), gl);
		
		pointLight = new GLPointLight(GL2.GL_LIGHT7, new GLVertex(5f, 7f, 0f)); // new GLVertex(-2f, 5f, 3f)
		pointLight.setUseColorMaterial(true);
		pointLight.setAmbient(new GLVertex(0.7f, 0.7f, 0.7f, 1.0f));
		pointLight.setDiffuse(new GLVertex(0.8f, 0.8f, 0.8f, 1.0f));
		pointLight.setSpecular(new GLVertex(0.8f, 0.8f, 0.8f, 1.0f));
		pointLight.setAttenuation(1.0f, 0.00f, 0.001f);
		GLCuboid lightCube = new GLCuboid(new GLVertex(0f, 0f, 0f), 0.5f);
		lightCube.setColor(new GLVertex(1f, 1f, 1f));
		pointLight.setLightObject(lightCube);
		this.addLight(pointLight);

		rect = new GLRectangle(new GLVertex(0, 0, 0), 2f);
		
		cube = new GLCuboid(new GLVertex(2, 2, -4), 1.5f);
		GLMaterial material = new GLMaterial();
		material.setMatShininess(0.6f);
		cube.setMaterial(material);
		cube.setColor(new GLVertex(0.4f, 0.3f, 0.5f));
		
		cube2 = new GLCuboid(new GLVertex(-2,2,3), 2f);
		cube2.rotate(45, 45, 45);
		cube2.setColor(new GLVertex(0.4f, 0.6f, 0.2f));

		room = new GLOpenRoom(new GLVertex(), 60.0f, 60.0f, 3f, 0.04f);
		room.setColor(new GLVertex(0.6f,0.4f,0.2f));
		
		framebuffer = new GLFramebuffer(512, 512, GLFramebuffer.DrawMode.DRAW_COLOR);
		//cube.setTexture(framebuffer.getTexture());
		rect.setColor(Color.WHITE);
		rect.setTexture(framebuffer.getTexture());
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		framebuffer.beginGL(gl);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		gl.glEnable(GL2.GL_DEPTH_TEST); // will nicht funktionieren :-(
		gl.glDepthMask(true); // machts auch nicht besser
		this.draw(gl);
		framebuffer.endGL(gl);
		
		super.display(drawable);
	}
	
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		
		
//		gl.glEnable(GL2.GL_CULL_FACE);
//
//		// ben�tigt ??
//		gl.glEnable(GL2.GL_COLOR_MATERIAL);
//		gl.glEnable(GL2.GL_NORMALIZE);
//		gl.glShadeModel(GL2.GL_SMOOTH);
		
		//shader.useProgram(gl);
		
		// PointLight rotieren
		//GLVertex pointLightPos = pointLight.getPosition();
		//pointLightPos.mult(GLMatrix.createRotateYMatrix(1f));
		//pointLight.setPosition(pointLightPos);
		
		rect.glRotateNow(180, 0, 0); // Hack, damit Welt richtig angezeigt wird
		rect.glTranslateNow(2, 2, 2);
		rect.draw(gl);
		
		
		cube.draw(gl);
		cube2.draw(gl);
		
		
		//room.draw(gl); // Bodenplatte zeichnen -> Aufw�ndig!
		
		//ShaderProgram.clearShaderProgram(gl);
	}
}