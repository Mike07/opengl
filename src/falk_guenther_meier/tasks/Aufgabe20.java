package falk_guenther_meier.tasks;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.Utility;

public class Aufgabe20 extends JoglApp {

	public Aufgabe20(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}

	public static void main(String[] args) {
		new Aufgabe20("Aufgabe 20", 800, 600);
	}

	@Override
	public synchronized void init(GLAutoDrawable drawable) {
		super.init(drawable);
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);

		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glEnable(GL2.GL_CULL_FACE);
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
		
		// zeigt die FPS an
		GLDrawable drawable = gl.getContext().getGLDrawable();
		Utility.drawText(gl, Utility.DEFAULT_FONT, new GLVertex(1.0f, 0.0f, 0.0f, 1.0f), Float.toString(this.getFrameRate()) + " FPS", 30, drawable.getHeight() - 90);
		
		Utility.drawText(gl, Utility.DEFAULT_FONT, new GLVertex(0.2f, 0.3f, 0.8f, 1.0f), "Hello World!", 80, 80);
		Utility.drawText(gl, Utility.DEFAULT_FONT, new GLVertex(0.2f, 0.3f, 0.8f, 1.0f), "><", drawable.getWidth() / 2 - 14, drawable.getHeight() / 2 - 9);
	}	
}
