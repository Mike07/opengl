package falk_guenther_meier.tasks;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLRectangle;

public class Aufgabe22 extends Aufgabe14 {
	GL2 gl;
	GLCamera[] cameras;
	int currentCamIndex;
	int keyboardCamIndex;
	int textureCamIndex;
	GLRectangle rect;

	public Aufgabe22(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}

	public static void main(String[] args) {
		new Aufgabe22("Aufgabe 22", 800, 600);
	}

	@Override
	public synchronized void init(GLAutoDrawable drawable) {
		super.init(drawable);
		gl = drawable.getGL().getGL2();

		cameras = new GLCamera[3];
		cameras[0] = getKonfiguration().getCamera();
		cameras[1] = new GLCamera(new GLVertex(8, 6, 8), new GLVertex(-8, -6, -8));
		cameras[2] = new GLCamera(new GLVertex(-13, 20, -9), new GLVertex(13, -20, 9));
		currentCamIndex = 0;
		keyboardCamIndex = 0;
		textureCamIndex = 1;
		
		rect = new GLRectangle(new GLVertex(0,0,3), 2f, 1.5f);
		rect.setTexture(cameras[textureCamIndex].getTexture(gl));
		rect.setTexRect(0, 0, 2, true); // flip vertically
//		setCamera(new GLCamera(newPos, newDir));
	}

	@Override
	public synchronized void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		cameras[textureCamIndex].refreshTexture(gl, this);
		super.display(drawable);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		gl.glEnable(GL2.GL_CULL_FACE);
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
		gl.glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
	
		super.beginGL(gl);

		rect.draw(gl);
	}
	
	@Override
	public void printMyKeys() {
		System.out.println("Kamera wechseln und per Tastatur steuerbar machen: 2");
		System.out.println("Tastatursteuerung wechseln: 3");
		System.out.println("Texturkamera wechseln: 4");
	}

	@Override
	public void cameraMode2() {
		this.removeKeyboardObserver(cameras[keyboardCamIndex]);
		
		currentCamIndex = (currentCamIndex + 1) % cameras.length;
		keyboardCamIndex = currentCamIndex;
		
		this.addKeyboardObserver(cameras[keyboardCamIndex]);
		this.getKonfiguration().changeCamera(cameras[currentCamIndex]);

		System.out.println("Aktuelle Sicht und Tastatursteuerung gewechselt auf Kamera " + currentCamIndex);
	}

	@Override
	public void cameraMode3() {
		this.removeKeyboardObserver(cameras[keyboardCamIndex]);	
		keyboardCamIndex = (keyboardCamIndex + 1) % cameras.length;
		this.addKeyboardObserver(cameras[keyboardCamIndex]);
		System.out.println("Tastatursteuerung gewechselt auf Kamera " + keyboardCamIndex);
	}

	@Override
	public void cameraMode4() {
		textureCamIndex = (textureCamIndex + 1) % cameras.length;
		rect.setTexture(cameras[textureCamIndex].getTexture(gl));
		rect.setTexRect(0, 0, 2, true); // flip vertically
		System.out.println("Texturkamera gewechselt auf Kamera " + textureCamIndex);
	}	
}
