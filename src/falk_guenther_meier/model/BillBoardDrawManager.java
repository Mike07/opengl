package falk_guenther_meier.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.objects.GLBillBoard;

/**
 * BillBoardDrawManager nimmt eine Menge von Billboards auf,
 * die abh�ngig von der Position der Kamera in der richtigen Reihenfolge geszeichnet und 
 * abgeblendert werden.
 * @author Michi
 */
public class BillBoardDrawManager {
	private ArrayList<GLBillBoard> billboards;
	
	public BillBoardDrawManager() {
		billboards = new ArrayList<GLBillBoard>();
	}
	
	public void addBillBoard(GLBillBoard bb) {
		billboards.add(bb);
	}
	
	public List<GLBillBoard> getBillBoards() {
		return this.billboards;
	}
	
	public void drawBillBoards(GL2 gl) {
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glEnable(GL2.GL_BLEND);
		gl.glDepthMask(false);
		Collections.sort(billboards);
		for (GLBillBoard bb : billboards) {
			bb.draw(gl);
		}
		gl.glDepthMask(true);
		gl.glDisable(GL2.GL_BLEND);
	}
}
