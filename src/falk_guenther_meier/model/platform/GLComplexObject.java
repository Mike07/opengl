package falk_guenther_meier.model.platform;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.observers.RotateObserver;
import falk_guenther_meier.model.observers.ScaleObserver;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;

/**
 * Ein GLComplexObject besteht aus mehreren Unterobjekten, die
 * wiederum komplex sein k�nnen (Composite-Pattern).
 * Diese abstrakte Klasse ist daf�r gedacht,
 * dass man neue komplexe Strukturen als eigene Klasse erstellen kann (Beispiel: Cuboid).
 * Die Struktur soll dabei nachtr�glich nicht mehr ver�ndert werden.
 */
public abstract class GLComplexObject extends GLObject {
	/**
	 * Referenz auf Objekte, die Teil dieses Objekts sind.
	 * Diese haben auch Ortsvektoren als Position. 
	 */
	protected List<GLObject> objects;

	public GLComplexObject() {
		objects = new ArrayList<GLObject>();
	}

	@Override
	public final void multMatrix(GLMatrix matrix) {
		for (GLObject o : objects) {
			o.multMatrix(matrix);
		}		
	}

	@Override
	public final void scale(float x, float y, float z) {
		for (GLObject o : objects) {
			o.scale(x, y, z);
		}
		for (ScaleObserver o : scaleObserver) {
			o.updateScale(this, x, y, z);
		}
	}

	@Override
	protected final void rotateRealisation(float x, float y, float z) {
		for (GLObject o : objects) {
			o.rotate(x, y, z);
		}
	}

	@Override
	public final void rotate(GLVertex first, GLVertex second, float angle) {
		for (GLObject o : objects) {
			o.rotate(first, second, angle);
		}
		for (RotateObserver o : rotateObserver) {
			o.updateRotate(this, first, second, angle);
		}
	}

	@Override
	public final GLVertex[] getNodes() {
		Set<GLVertex> vertexes = new HashSet<GLVertex>();
		for (GLObject obj : objects) {
			List<GLVertex> subVertexes = Arrays.asList(obj.getNodes());
			vertexes.addAll(subVertexes);
		}
		return vertexes.toArray(new GLVertex[]{});
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		for (GLObject o : objects) {
			o.draw(gl); // Hanno @ Johannes: das ist schon richtig so, sonst werden Push- und Pop(Matrix/Attrib) innerhalb der Objekte falsch behandelt
		}
	}

	@Override
	public final void setReferencePoint(GLVertex newPosition) {
//		GLVertex help = GLVertex.sub(position, newPosition); // funktioniert so nicht
//		for (int i = 0; i < this.getNodes().length; i++) {
//			this.getNodes()[i].setValues(GLVertex.add(help, this.getNodes()[i]));
//		}
		this.position = newPosition;
		if (objects != null) {
			for (GLObject object : objects) {
				object.setReferencePoint(newPosition); // TODO: ist das richtig ?? newPosition.clone() ??!
			}
		}
	}

	/**
	 * Liefert alle Unterobjekte.
	 * @return
	 */
	public final List<GLObject> getObjects() {
		return objects;
	}

	/**
	 * F�gt ein neues GLObject in die Liste ein. -> Composite-Pattern
	 * Nur f�r abgeleitete Klassen.
	 */
	protected void add(GLObject obj) {
		obj.setParent(obj);
		objects.add(obj);
		obj.setReferencePoint(this.getReferencePoint());
	}

	/**
	 * F�gt neue GLObjects in die Liste ein. -> Composite-Pattern
	 * Nur f�r abgeleitete Klassen.
	 */
	protected void addAll(List<? extends GLObject> objects) {
		for (GLObject o : objects) {
			this.add(o);
		}
	}

	@Override
	public final void changeCCW() {
		for (GLObject o : objects) {
			o.changeCCW();
		}
	}

	/**
	 * Setzt die �bergebene Farbe f�r sich und f�r alle Unterobjekte.
	 */
	@Override
	public final void setColor(GLVertex color) {
		this.color = color;
		this.material = GLMaterial.createColor(this.color);
		for (GLObject obj : objects) {
			obj.setColor(color);
		}
	}
	
	/**
	 * Setzt das �bergebene Material f�r sich und f�r alle Unterobjekte.
	 */
	@Override
	public final void setMaterial(GLMaterial material) {
		this.material = material;
		for (GLObject obj : objects) {
			obj.setMaterial(material);
		}
	}
	
	@Override
	public void setTexture(GLTexture2D texture, int textureUnit) {
		super.setTexture(texture, textureUnit);
		for (GLObject obj : objects) {
			obj.setTexture(texture, textureUnit);
		}
	}
	
	@Override
	public final void setDrawModeNormals(boolean drawModeNormals) {
		this.drawModeNormals = drawModeNormals;
		for (GLObject obj : objects) {
			obj.setDrawModeNormals(drawModeNormals);
		}
	}
	
	@Override
	public final void setDrawNormalsColor(GLVertex color) {
		this.drawNormalsColor = color;
		for (GLObject obj : objects) {
			obj.setDrawNormalsColor(color);
		}
	}

	@Override
	public final void setShader(ShaderProgram shader) {
		super.setShader(shader);
		for (GLObject obj : objects) {
			obj.setShader(shader);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (super.equals(obj)) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			GLComplexObject other = (GLComplexObject) obj;
			if (objects == null) {
				if (other.objects != null)
					return false;
			} else if (!objects.equals(other.objects))
				return false;
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((objects == null) ? 0 : objects.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "GLComplexObject [objects=" + objects + ", position=" + position
				+ ", color=" + color + ", material=" + material + "]";
	}
}
