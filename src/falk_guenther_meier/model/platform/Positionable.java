package falk_guenther_meier.model.platform;

import falk_guenther_meier.model.math.GLVertex;

/**
 * Beschreibt die Funktion von Objekten, die animiert werden k�nnen.
 */
public interface Positionable {
	/**
	 * Liefert die aktuelle Position.
	 * @return
	 */
	public GLVertex getPosition();
	/**
	 * Setzt die aktuelle Position.
	 * @param pos
	 */
	public void setPosition(GLVertex pos);	
}
