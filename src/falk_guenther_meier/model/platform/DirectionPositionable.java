package falk_guenther_meier.model.platform;

import falk_guenther_meier.model.math.GLVertex;

/**
 * Beschreibt die Funktionen von animierbaren Objekten, die neben einer Position auch eine Richtung haben.
 * Angedacht sind Objekte wie Spotlight, Kamera
 * @author Johannes
 */
public interface DirectionPositionable extends Positionable {
	/**
	 * Liefert die aktuelle Richtung.
	 * @return
	 */
	public GLVertex getDirection();

	/**
	 * Setzt die Richtung, ohne dabei die Position zu ver�ndern.
	 * @param direction neue Richtung
	 */
	public void setDirection(GLVertex direction);

	/**
	 * Setzt die Position neu.
	 * @param position neue Position, die in jedem Fall �bernommen wird
	 * @param changeDirection Ist false angegeben, wird die Richtung nicht ver�ndert,
	 * wird true angegeben, wird die Richtung so ge�ndert, dass "position' + direction' == position + direction" gilt.
	 */
	public void setPosition(GLVertex position, boolean changeDirection);
}
