package falk_guenther_meier.model.platform;

import java.awt.Color;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.GLDrawable;
import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.observers.RotateObservable;
import falk_guenther_meier.model.observers.RotateObserver;
import falk_guenther_meier.model.observers.ScaleObservable;
import falk_guenther_meier.model.observers.ScaleObserver;
import falk_guenther_meier.model.observers.TranslateObservable;
import falk_guenther_meier.model.observers.TranslateObserver;
import falk_guenther_meier.model.operations.GLOperation;
import falk_guenther_meier.model.operations.GLRotate;
import falk_guenther_meier.model.operations.GLScale;
import falk_guenther_meier.model.operations.GLTransform;
import falk_guenther_meier.model.operations.GLTranslate;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLObjectTextureBinding;
import falk_guenther_meier.model.textures.GLTexture2D;

/**
 * Die Oberklasse f�r Objekte, insbesondere primitiven und komplexen Objekten.
 */
public abstract class GLObject implements GLDrawable, RotateObservable, RotateObserver,
		ScaleObservable, ScaleObserver, TranslateObservable, TranslateObserver, Positionable {
	/**
	 * Vektor, der die Position des Objekts bestimmt.
	 * Dieser Vektor muss immer ein Ortsvektoren sein.
	 */
	protected GLVertex position;
	/**
	 * Farbe eines Objekts.
	 */
	protected GLVertex color;
	
	/**
	 * Material eines Objekts.
	 */
	protected GLMaterial material;
	
	/**
	 * Textur eines Objekts.
	 */	
	protected List<GLObjectTextureBinding> textureBindings;
	
	/**
	 * Referenz auf Oberobjekt falls vorhanden, sonst null.
	 */
	protected GLObject parent;

	protected List<RotateObserver> rotateObserver;
	protected List<ScaleObserver> scaleObserver;
	private List<TranslateObserver> translateObserver;
	private ShaderProgram shader;

	/**
	 * Liste, die geplante Operationen wie glRotate, glScale und glTransform f�r die sp�tere Ausf�hrung speichert
	 */
	private LinkedList<GLOperation> glOperations;
	
	private boolean inPushPop;
	/**
	 * Gibt an, ob Normalen gezeichnet werden.
	 */
	protected boolean drawModeNormals;
	/**
	 * Farbe f�r die zu zeichnenden Normalen.
	 */
	protected GLVertex drawNormalsColor;

	public GLObject() {
		rotateObserver = new ArrayList<RotateObserver>();
		scaleObserver = new ArrayList<ScaleObserver>();
		translateObserver = new ArrayList<TranslateObserver>();
		glOperations = new LinkedList<GLOperation>();
		textureBindings = new ArrayList<GLObjectTextureBinding>();
		inPushPop = false;
	}

	public final LinkedList<GLOperation> getGlOperations() {
		return glOperations;
	}

	@Override
	public void beginGL(GL2 gl) {
		if (glOperations.size() > 0) {
			gl.glPushMatrix();
			inPushPop = true;
			executeGLOperations(gl);
		}
	}
	
	@Override
	public void endGL(GL2 gl) {
		if (inPushPop) {
			gl.glPopMatrix();
			inPushPop = false;
		}
	}
	
	@Override
	public final void draw(GL2 gl) {
		beginGL(gl);
		endGL(gl);
	}
	
	private final void executeGLOperations(GL2 gl) {
		for (GLOperation glOperation: glOperations) {
			glOperation.toGL(gl);
		}
		glOperations.clear();
	}
	
	public final void glScaleNow(float x, float y, float z) {
		GLOperation glOperation = new GLScale(x, y, z);
		glOperations.addFirst(glOperation);
	}
	
	public final void glRotateNow(float x, float y, float z) {
		GLOperation glOperation = new GLRotate(x, y, z);
		glOperations.addFirst(glOperation);
	}
	
	public final void glTranslateNow(float x, float y, float z) {
		GLOperation glOperation = new GLTranslate(x, y, z);
		glOperations.addFirst(glOperation);
	}

	public abstract void multMatrix(GLMatrix matrix);

	public final void glTransformNow(GLMatrix matrix) {
		GLOperation glOperation = new GLTransform(matrix);
		glOperations.addFirst(glOperation);
	}
	
	public abstract void scale(float x, float y, float z);

	/**
	 * Rotiert um ein Standard-Koordinatensystem
	 * @param x
	 * @param y
	 * @param z
	 */
	public final void rotate(float x, float y, float z) {
		this.rotateRealisation(x, y, z);
		for (RotateObserver o : rotateObserver) {
			o.updateRotate(this, x, y, z);
		}
	}
	/**
	 * Dient zur Realiserung der Rotation.
	 * Diese Methode schickt keine Benachrichtigungen.
	 * @param x
	 * @param y
	 * @param z
	 */
	protected abstract void rotateRealisation(float x, float y, float z);

	/**
	 * Rotiert um ein Koordinatensystem mit dem Ursprung am angegebenen Punkt.
	 * @param point Der Punkt ist ein Ortsvektor.
	 * @param x
	 * @param y
	 * @param z
	 */
	public final void rotate(GLVertex point, float x, float y, float z) {
		GLVertex help = this.getReferencePoint();
		this.setReferencePoint(point);
		this.rotate(x, y, z);
		this.setReferencePoint(help);
		for (RotateObserver o : rotateObserver) {
			o.updateRotate(this, point, x, y, z);
		}
	}

	/**
	 * Rotiert das Objekt um eine Gerade. Alle angegebenen Vektoren sind Ortsvektoren
	 * @param first erster St�tzpunkt der Gerade
	 * @param second zweiter St�tzpunkt der Gerade (Richtung: von first nach second; ben�tigt(?))
	 * @param angle Winkel
	 */
	public abstract void rotate(GLVertex first, GLVertex second, float angle);

	public abstract GLVertex[] getNodes();

	public final void translate(float x, float y, float z) {
		this.translateRealisation(x, y, z);
		for (TranslateObserver o : translateObserver) {
			o.updateTranslate(this, x, y, z);
		}
	}
	/**
	 * Dient nur zur Realisierung der Translation.
	 * Diese Methode schickt keine Benachrichtigungen.
	 * @param x
	 * @param y
	 * @param z
	 */
	protected final void translateRealisation(float x, float y, float z) {
		position.mult(GLMatrix.createTranslateMatrix(x, y, z));
	}
	/**
	 * Verschiebt das Objekt, wobei der Vektor die Verschiebung angibt.
	 * @param v
	 */
	public final void translate(GLVertex v) {
		this.translateRealisation(v.getX(), v.getY(), v.getZ());
		for (TranslateObserver o : translateObserver) {
			o.updateTranslate(this, v);
		}
	}

	public final void setPosition(GLVertex position) {
		this.position.setValues(position);
		for (TranslateObserver o : translateObserver) {
			o.updateAbsolutePosition(this, position);
		}
	}

	@Override
	public GLVertex getPosition() {
		return position;
	}

	public final GLVertex getReferencePoint() {
		return position;
	}
	
	/**
	 * Setzt den Referenzpunkt dieses Objekts neu.
	 * Die absolute Positionierung des Objekts ver�ndert sich dadurch nicht.
	 * @param newReferencePoint
	 */
	public abstract void setReferencePoint(GLVertex newReferencePoint);
	
	protected final void setParent(GLObject parent) {
		this.parent = parent;
	}
	
	public final GLObject getParent() {
		return parent;
	}
	
	public final boolean getDrawModeNormals() {
		return drawModeNormals;
	}
	
	public abstract void setDrawModeNormals(boolean drawModeNormals);
	
	public final GLVertex getDrawNormalsColor() {
		return drawNormalsColor;
	}
	
	public abstract void setDrawNormalsColor(GLVertex color);

	/**
	 * Dreht die Zeichenrichtung (CW / CCW) um.
	 */
	public abstract void changeCCW();

	/**
	 * Farbe des Objekts. null, wenn nicht gesetzt.
	 * @return GLVertex
	 */
	public final GLVertex getColor() {
		return color;
	}
	
	public abstract void setColor(GLVertex color);

	public final void setColor(Color color) {
		this.setColor(new GLVertex(color));
	}

	/**
	 * Material des Objekts. null, wenn nicht gesetzt.
	 * @return
	 */
	public final GLMaterial getMaterial() {
		return material;
	}
	
	public abstract void setMaterial(GLMaterial texture);
	
	public final List<GLObjectTextureBinding> getTextureBindings() {
		return textureBindings;
	}

	public final void setTexture(GLTexture2D texture) {
		this.setTexture(texture, 0);
	}

	public void setTexture(GLTexture2D texture, int textureUnit) {
		// replace existing Textures on same textureUnit
		for (GLObjectTextureBinding tempTextureBinding : this.textureBindings.toArray(new GLObjectTextureBinding[0])) {
			if (tempTextureBinding.getTextureUnit() == textureUnit) {
				textureBindings.remove(tempTextureBinding);
			}
		}
		// add new Textur
		GLObjectTextureBinding textureBinding = new GLObjectTextureBinding(texture, textureUnit);
		this.textureBindings.add(textureBinding);
		// TODO: macht das immer Sinn??
		this.setColor(new GLVertex(1.0f, 1.0f, 1.0f));
	}

	/**
	 * Setzt den Shader, mit dem dieses Objekt gezeichnet werden soll.
	 * @param shader
	 */
	public void setShader(ShaderProgram shader) {
		this.shader = shader;
	}

	/**
	 * Liefert den Shader, mit dem dieses Objekt gezeichnet wird.
	 * @return
	 */
	public final ShaderProgram getShader() {
		return this.shader;
	}

	// --------- Observer / Observable -----------------
	@Override
	public final void addRotateObserver(RotateObserver o) {
		rotateObserver.add(o);
	}
	@Override
	public final void removeRotateObserver(RotateObserver o) {
		rotateObserver.remove(o);
	}
	@Override
	public final List<RotateObserver> getRotateObservers() {
		return rotateObserver;
	}
	@Override
	public void updateRotate(RotateObservable changed, float x, float y, float z) {
		// bleibt leer
	}
	@Override
	public void updateRotate(RotateObservable changed, GLVertex point, float x, float y, float z) {
		// bleibt leer		
	}
	@Override
	public void updateRotate(RotateObservable changed, GLVertex first, GLVertex second, float angle) {
		// bleibt leer
	}
	@Override
	public final void addScaleObserver(ScaleObserver o) {
		scaleObserver.add(o);
	}
	@Override
	public final void removeScaleObserver(ScaleObserver o) {
		scaleObserver.remove(o);
	}
	@Override
	public List<ScaleObserver> getScaleObservers() {
		return scaleObserver;
	}
	@Override
	public void updateScale(ScaleObservable changed, float x, float y, float z) {
		// bleibt leer
	}
	@Override
	public final void addTranslateObserver(TranslateObserver o) {
		translateObserver.add(o);
	}
	@Override
	public final void removeTranslateObserver(TranslateObserver o) {
		translateObserver.remove(o);
	}
	@Override
	public List<TranslateObserver> getTranslateObservers() {
		return translateObserver;
	}
	@Override
	public void updateTranslate(TranslateObservable changed, float x, float y, float z) {
		// bleibt leer
	}
	@Override
	public void updateTranslate(TranslateObservable changed, GLVertex v) {
		// bleibt leer
	}
	@Override
	public void updateAbsolutePosition(TranslateObservable changed, GLVertex position) {
		// bleibt leer
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + (drawModeNormals ? 1231 : 1237);
		result = prime * result
				+ ((glOperations == null) ? 0 : glOperations.hashCode());
		result = prime * result + (inPushPop ? 1231 : 1237);
		result = prime * result
				+ ((material == null) ? 0 : material.hashCode());
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		result = prime * result
				+ ((position == null) ? 0 : position.hashCode());
		result = prime * result
				+ ((rotateObserver == null) ? 0 : rotateObserver.hashCode());
		result = prime * result
				+ ((scaleObserver == null) ? 0 : scaleObserver.hashCode());
		result = prime * result
				+ ((textureBindings == null) ? 0 : textureBindings.hashCode());
		result = prime
				* result
				+ ((translateObserver == null) ? 0 : translateObserver
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GLObject other = (GLObject) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (drawModeNormals != other.drawModeNormals)
			return false;
		/*if (glOperations == null) {
			if (other.glOperations != null)
				return false;
		} else if (!glOperations.equals(other.glOperations))
			return false;*/
		if (inPushPop != other.inPushPop)
			return false;
		if (material == null) {
			if (other.material != null)
				return false;
		} else if (!material.equals(other.material))
			return false;
		/*if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;*/
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		/*if (rotateObserver == null) {
			if (other.rotateObserver != null)
				return false;
		} else if (!rotateObserver.equals(other.rotateObserver))
			return false;
		if (scaleObserver == null) {
			if (other.scaleObserver != null)
				return false;
		} else if (!scaleObserver.equals(other.scaleObserver))
			return false;
		if (textureBindings == null) {
			if (other.textureBindings != null)
				return false;
		} else if (!textureBindings.equals(other.textureBindings))
			return false;
		if (translateObserver == null) {
			if (other.translateObserver != null)
				return false;
		} else if (!translateObserver.equals(other.translateObserver))
			return false;*/
		return true;
	}

	@Override
	public String toString() {
		return "GLObject [position=" + position + ", color=" + color
				+ ", material=" + material + ", textureBindings="
				+ textureBindings + ", parent=" + parent + ", rotateObserver="
				+ rotateObserver + ", scaleObserver=" + scaleObserver
				+ ", translateObserver=" + translateObserver
				+ ", glOperations=" + glOperations + ", inPushPop=" + inPushPop
				+ ", drawModeNormals=" + drawModeNormals + "]";
	}
}
