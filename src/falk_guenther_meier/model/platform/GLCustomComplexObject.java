package falk_guenther_meier.model.platform;

import falk_guenther_meier.model.math.GLVertex;

/**
 * Wie GLComplexObject besteht GLCustomComplexObject aus Unterobjekten,
 * die nicht relativ zur Position des Vaterobjekts positioniert sind!
 * Ein weiterer Unterschied ist, dass diese Klasse instanzierbar ist und dass
 * Objekte zur Laufzeit hinzugef�gt und entfernt werden k�nnen.
 * Dieser Ansatz wird zum Beispiel ben�tigt, um 3D-Modelle
 * (Beispiel obj-Dateien) einzulesen.  
 */
public final class GLCustomComplexObject extends GLComplexObject {
	public GLCustomComplexObject() {
		super();
		this.position = new GLVertex();
	}

	public GLCustomComplexObject(GLVertex position) {
		super();
		this.position = position;
	}

	/**
	 * Pr�ft, ob Unterobjekte vorhanden sind.
	 * @return
	 */
	public final boolean hasObjects() {
		return objects.size() > 0;
	}

	/**
	 * F�gt ein neues GLObject in die Liste ein. -> Composite-Pattern
	 * Sichtbar f�r alle.
	 */
	@Override
	public final void add(GLObject obj) {
		super.add(obj);
	}

	/**
	 * Entfernt das erste mit obj �bereinstimmende GLObject aus der Liste.
	 */
	public final void remove(GLObject obj) {
		objects.remove(obj);
	}

	/**
	 * Entfernt das GLObject am entsprechenden Index aus der Liste.
	 */
	public final void remove(int index) {
		objects.remove(index);
	}
}
