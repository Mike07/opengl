package falk_guenther_meier.model.platform;

import java.util.Arrays;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.observers.RotateObserver;
import falk_guenther_meier.model.observers.ScaleObserver;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLObjectTextureBinding;


/**
 * Abstrakte Oberklasse von primitiven Objekten wie zum Beispiel Rectangle, Triangle etc.
 */
public abstract class GLPrimitiveObject extends GLObject {
	
	/**
	 * Vektoren, die die Grundstruktur des Objekts aufspannen (und die nicht die Position des Objekts bestimmen).
	 * Ausgehend von den / der "positions".
	 */
	protected GLVertex[] nodes;
	
	/**
	 * Bei true werden Normalen automatisch gesetzt,
	 * falls Normalen nicht explizit angegeben wurden.
	 * Default: true
	 */
	protected boolean alwaysSetNormals = true;

	@Override
	public final void multMatrix(GLMatrix matrix) {
		for (int i = 0; i < nodes.length; i++) {
			nodes[i].mult(matrix);
		}
	}

	@Override
	public final void scale(float x, float y, float z) {
		this.multMatrix(GLMatrix.createScaleMatrix(x, y, z));
		for (ScaleObserver o : scaleObserver) {
			o.updateScale(this, x, y, z);
		}
	}

	@Override
	protected final void rotateRealisation(float x, float y, float z) {
		this.multMatrix(GLMatrix.createRotateMatrix(x, y, z));
	}

	@Override
	public final void rotate(GLVertex first, GLVertex second, float angle) {
		// http://www-lehre.informatik.uni-osnabrueck.de/~cg/1997/skript/13_3_1_Rotation_um.html (03.12.2012)
		GLVertex direction = GLVertex.sub(second, first);
		GLVertex inv = GLVertex.invert(first);
		this.translateRealisation(inv.getX(), inv.getY(), inv.getZ());
		GLVertex help = this.getReferencePoint();
		this.setReferencePoint(new GLVertex(0.0f, 0.0f, 0.0f));
		this.multMatrix(GLMatrix.createRotateVMatrix(direction, angle));
		this.setReferencePoint(help);
		this.translateRealisation(first.getX(), first.getY(), first.getZ());
		for (RotateObserver o : rotateObserver) {
			o.updateRotate(this, first, second, angle);
		}
	}

	@Override
	public final GLVertex[] getNodes() {
		return nodes;
	}

	@Override
	public final void setReferencePoint(GLVertex newPosition) {
		GLVertex help = GLVertex.sub(position, newPosition);
		for (int i = 0; i < nodes.length; i++) {
			nodes[i].setValues(GLVertex.add(help, nodes[i]));
		}
		this.position = newPosition;
	}
	
	/**
	 * Berechnet die Normale einer Fl�che aus den ersten 3 in Counter-Clock-Wise angegebenen Knoten.
	 */
	public final GLVertex calculateNormal() {
		if (nodes.length >= 3) {
			GLVertex calcVertex = GLVertex.mult(GLVertex.sub(nodes[1], nodes[0]), GLVertex.sub(nodes[2], nodes[0])); // 0->1 x 0->2  // Kreuzprodukt: rechte-Hand-Regel: Daumen x Zeigefinger = Mittelfinger
			return GLVertex.normalize(calcVertex);
		} else {
			return null;
		}
	}

	/**
	 * Liefert die Normale, die sich aus dem Durchschnitt der Normalen der einzelnen Vektoren ergibt
	 */
	public final GLVertex getNormal() {
		GLVertex normal = null;
		for(GLVertex node: nodes) {
			if (node.getNormal() != null) {
				if (normal == null)
					normal = new GLVertex();
				normal.add(node);
			}	
		}
		normal.normalize();
		return normal;
	}
	
	/**
	 * Setzt die Normale auf die einzelnen Vektoren
	 */
	public final void setNormal(GLVertex normal) {
		for(GLVertex node: nodes) {
			node.setNormal(normal);
		}
	}
	
	public final boolean isAlwaysSetNormals() {
		return alwaysSetNormals;
	}

	public final void setAlwaysSetNormals(boolean alwaysSetNormals) {
		this.alwaysSetNormals = alwaysSetNormals;
	}

	@Override
	public final void changeCCW() {
		if (nodes == null) {
			return;
		}
		GLVertex[] n = new GLVertex[nodes.length];
		for (int i = 0; i < n.length; i++) {
			n[i] = nodes[n.length - 1 - i];
		}
		for (int i = 0; i < n.length; i++) {
			nodes[i] = n[i];
		}
	}

	@Override
	public final void setColor(GLVertex color) {
		this.color = color;
		// TODO: macht das immer Sinn??
		this.setMaterial(GLMaterial.createColor(this.color));
	}
	
	@Override
	public final void setMaterial(GLMaterial material) {
		this.material = material;
	}
	
	@Override
	public final void setDrawModeNormals(boolean drawModeNormals) {
		this.drawModeNormals = drawModeNormals;
	}
	
	@Override
	public final void setDrawNormalsColor(GLVertex color) {
		this.drawNormalsColor = color;
	}

	@Override
	public final void setShader(ShaderProgram shader) {
		super.setShader(shader);
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		gl.glPushAttrib(GL2.GL_ALL_ATTRIB_BITS); // aktuelle Einstellungen speichern
		if (material != null) {
			material.toGL(gl);
		}
		if (nodes.length > 0 && nodes[0].getNormal() == null && this.alwaysSetNormals) {
			this.setNormal(this.calculateNormal());
		}
		if (drawModeNormals) {
			drawNormals(gl);
		}
		if (color != null) {
			gl.glColor4f(color.getX(), color.getY(), color.getZ(), color.getW());
		}
		for(GLObjectTextureBinding textureBinding: this.getTextureBindings()) {
			gl.glActiveTexture(GL2.GL_TEXTURE0 + textureBinding.getTextureUnit());
			//gl.glTexEnvi( GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);
			gl.glEnable(GL2.GL_TEXTURE_2D);
			textureBinding.getTexture().bind(gl);			
		}
	}
	
	@Override
	public void endGL(GL2 gl) {
		gl.glPopAttrib(); // vorgenommene Einstellungen wieder zur�cksetzen
		super.endGL(gl);
	}
	
	protected void drawNormals(GL2 gl) {
		if (this.drawNormalsColor != null) {
			GLVertex c = this.drawNormalsColor;
			gl.glColor4f(c.getX(), c.getY(), c.getZ(), c.getW());
		}		
		for(GLVertex fromNode: nodes) {
			if (fromNode.getNormal() != null) {		
				gl.glBegin(GL2.GL_LINES);
					fromNode.toGL(gl, position);
					GLVertex toNode = GLVertex.add(fromNode, fromNode.getNormal());
					toNode.toGL(gl, position);
				gl.glEnd();
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (alwaysSetNormals ? 1231 : 1237);
		result = prime * result + Arrays.hashCode(nodes);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GLPrimitiveObject other = (GLPrimitiveObject) obj;
		if (alwaysSetNormals != other.alwaysSetNormals)
			return false;
		if (!Arrays.equals(nodes, other.nodes))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GLPrimitiveObject [nodes=" + Arrays.toString(nodes)
				+ ", alwaysSetNormals=" + alwaysSetNormals + ", position="
				+ position + ", color=" + color + ", material=" + material
				+ "]";
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}
	
}
