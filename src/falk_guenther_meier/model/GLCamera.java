package falk_guenther_meier.model;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;

import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.observers.KeyboardObserver;
import falk_guenther_meier.model.observers.RotateObservable;
import falk_guenther_meier.model.observers.RotateObserver;
import falk_guenther_meier.model.observers.ScaleObservable;
import falk_guenther_meier.model.observers.ScaleObserver;
import falk_guenther_meier.model.observers.TranslateObservable;
import falk_guenther_meier.model.observers.TranslateObserver;
import falk_guenther_meier.model.platform.DirectionPositionable;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.util.exceptions.OutOfBoundsException;

public class GLCamera implements RotateObserver, ScaleObserver, TranslateObserver, TranslateObservable, RotateObservable,
		KeyboardObserver, DirectionPositionable { // TODO: GLable ?!
	private GLU glu;

	private GLVertex initPosition;
	private GLVertex initViewingDirection;

	private GLVertex position; // Position des Kamera-Auges 
	private GLVertex viewingDirection; // Blich-Richtung des Kamera-Auges
	private GLVertex center; // Punkt, auf den das Kamera-Auge schaut: center == position + viewingDirection

	private GLVertex up; // (0,1,0): TODO wof�r ??
	private GLVertex viewingDirectionOrthogonale; // TODO wof�r: ??
	private float angleDelta = 0.15f;
	private float speed = 8; // Geschwindigkeit der Kamera (kann ge�ndert werden!)

	private GLTexture2D texture;
	private int refreshDepth = 0;
	private final int cameraWidth = 512; // TODO: ist diese Festlegung so sinnvoll??
	private final int cameraHeight = 512;
	
	private float xMin, xMax, yMin, yMax, zMin, zMax;

	private List<TranslateObserver> translateObserver;
	private List<RotateObserver> rotateObserver;

	private boolean useBounds; // wenn true, werden Begrenzungen ber�cksichtigt
	
	public GLCamera() {
		this(new GLVertex(8, 0, 8), new GLVertex(-1, 0, -1));
	}

	public GLCamera(GLVertex pos, GLVertex viewingDirection) {
		if (this.isOutOfBounds(pos)) throw new OutOfBoundsException(pos);
		glu = new GLU();
		initPosition = pos.clone();
		initViewingDirection = viewingDirection.clone();
		translateObserver = new ArrayList<TranslateObserver>();
		rotateObserver = new ArrayList<RotateObserver>();
		up = new GLVertex(0, 1, 0);
		position = pos;
		this.viewingDirection = viewingDirection;
		center = GLVertex.add(position, viewingDirection);
		
		float upAngle= GLVertex.getAngleBetween(initViewingDirection, up);
		if (upAngle < 1 || upAngle > 179)
			throw new IllegalArgumentException("Angle between viewingDirection and up is too small"); // viewingDirectionOrthogonale kann nicht kalkuliert werden
		
		viewingDirectionOrthogonale = GLVertex.mult(initViewingDirection, up);
		useBounds = false;
	}

	public final float getSpeed() {
		return speed;
	}

	public final void setSpeed(float speed) {
		this.speed = speed;
	}
	
	/**
	 * Setzt Begrenzungen f�r die Kamerabewegungen.
	 * @param xMin
	 * @param xMax
	 * @param yMin
	 * @param yMax
	 * @param zMin
	 * @param zMax
	 */
	public final void setBounds(float xMin, float xMax, float yMin, float yMax, float zMin, float zMax) {
		this.xMin = xMin;
		this.xMax = xMax;
		this.yMin = yMin;
		this.yMax = yMax;
		this.zMin = zMin;
		this.zMax = zMax;
	}
	
	/**
	 * Pr�ft, ob pos au�erhalb der Grenzen liegt.
	 * @param pos
	 * @return
	 */
	private boolean isOutOfBounds(GLVertex pos) {
		if (!useBounds) return false;
		float x = pos.getX();
		float y = pos.getY();
		float z = pos.getZ();
		if (x < xMin || x > xMax || y < yMin || y > yMax || z < zMin || z > zMax)
			return true;
		else return false;
	}

	public boolean isUseBounds() {
		return useBounds;
	}

	public void setUseBounds(boolean useBounds) {
		this.useBounds = useBounds;
	}

	/**
	 * Liefert die Position des Kamera-Auges.
	 * @return eine neue Kopie der Position!
	 */
	@Override
	public GLVertex getPosition() {
		return position.clone();
	}

	/**
	 * Setzt die Position des Kamera-Auges.
	 * @param pos �bernimmt die Werte (nicht die �bergebene Instanz)!
	 */
	@Override
	public void setPosition(GLVertex pos) {
		if (this.isOutOfBounds(pos)) throw new OutOfBoundsException(pos);
		this.setPosition(pos, false);
	}

	@Override
	public void setPosition(GLVertex position, boolean changeDirection) {
		this.position.setValues(position);
		if (changeDirection) {
			viewingDirection.setValues(GLVertex.sub(center, position));
			viewingDirectionOrthogonale = GLVertex.mult(viewingDirection, up);
		} else {
			center = GLVertex.add(position, viewingDirection);
		}
		for (TranslateObserver o : translateObserver) {
			o.updateAbsolutePosition(this, this.getPosition());
		}
	}

	/**
	 * Liefert die Blickrichtung der Kamera.
	 * @return eine neue Instanz!
	 */
	@Override
	public GLVertex getDirection() {
		return viewingDirection.clone(); 
	}

	/**
	 * Setzt die Blickrichtung der Kamera.
	 * @param dir �bernimmt die Werte (nicht die Instanz!)
	 */
	@Override
	public void setDirection(GLVertex dir) {
		viewingDirection.setValues(dir);
		center = GLVertex.add(position, viewingDirection);
		viewingDirectionOrthogonale = GLVertex.mult(viewingDirection, up);
		for (RotateObserver o : rotateObserver) {
			o.updateRotate(this, viewingDirection.getX(), viewingDirection.getY(), viewingDirection.getZ());
		}
	}

	/**
	 * Setzt die Kamera und ihre Blickrichtung auf die Werte bei ihrer Erzeugung zur�ck.
	 */
	public void initiateCamera() {
		position = initPosition.clone();
		viewingDirection = initViewingDirection.clone();
		center = GLVertex.add(position, viewingDirection);
		viewingDirectionOrthogonale = GLVertex.mult(viewingDirection, up);
		for (TranslateObserver o : translateObserver) {
			o.updateAbsolutePosition(this, this.getPosition());
		}
		for (RotateObserver o : rotateObserver) {
			o.updateRotate(this, viewingDirection.getX(), viewingDirection.getY(), viewingDirection.getZ());
		}
	}

	/* (non-Javadoc)
	 * @see falk_guenther_meier.model.KeyboardObserver#forward()
	 */
	@Override
	public void forward() {
		// Vorw�rtsbewegung
		GLVertex step = GLVertex.mult(GLVertex.normalize(viewingDirection), 1/35f * speed);
		if (this.isOutOfBounds(GLVertex.add(position, step))) return;
		position.add(step);
		center.add(step);
		for (TranslateObserver o : translateObserver) {
			o.updateTranslate(this, step);
		}
	}
	
	/* (non-Javadoc)
	 * @see falk_guenther_meier.model.KeyboardObserver#backward()
	 */
	@Override
	public void backward() {
		// R�ckw�rtsbewegung
		GLVertex step = GLVertex.mult(GLVertex.invert(GLVertex.normalize(viewingDirection)), 1/35f * speed);
		if (this.isOutOfBounds(GLVertex.add(position, step))) return;
		position.add(step);
		center.add(step);
		for (TranslateObserver o : translateObserver) {
			o.updateTranslate(this, step);
		}
	}
	
	/* (non-Javadoc)
	 * @see falk_guenther_meier.model.KeyboardObserver#left()
	 */
	@Override
	public void left() {
		// Linksbewegung
		GLVertex add = new GLVertex(viewingDirection.getZ() / 80 * speed, 0, -viewingDirection.getX() / 80 * speed);
		if (this.isOutOfBounds(GLVertex.add(position, add))) return;
		position.add(add);
		center.add(add);
		for (TranslateObserver o : translateObserver) {
			o.updateTranslate(this, add);
		}
	}
	
	/* (non-Javadoc)
	 * @see falk_guenther_meier.model.KeyboardObserver#right()
	 */
	@Override
	public void right() {
		// Rechtsbewegung
		GLVertex add = new GLVertex(-viewingDirection.getZ() / 100 * speed, 0, viewingDirection.getX() / 100 * speed);
		if (this.isOutOfBounds(GLVertex.add(position, add))) return;
		position.add(add);
		center.add(add);
		for (TranslateObserver o : translateObserver) {
			o.updateTranslate(this, add);
		}
	}
	
	/* (non-Javadoc)
	 * @see falk_guenther_meier.model.KeyboardObserver#up()
	 */
	@Override
	public void up() {
		// Bewegung nach oben
		float y = 0.02f * speed;
		if (this.isOutOfBounds(GLVertex.add(position, new GLVertex(0, y, 0)))) return;
		position.addValues(0, y, 0);
		center.addValues(0, y, 0);
		for (TranslateObserver o : translateObserver) {
			o.updateTranslate(this, 0, y, 0);
		}
	}
	
	/* (non-Javadoc)
	 * @see falk_guenther_meier.model.KeyboardObserver#down()
	 */
	@Override
	public void down() {
		// Bewegung nach untens
		float y = 0.02f * speed;
		if (this.isOutOfBounds(GLVertex.add(position, new GLVertex(0, -y, 0)))) return;
		position.addValues(0, -y, 0);
		center.addValues(0, -y, 0);
		for (TranslateObserver o : translateObserver) {
			o.updateTranslate(this, 0, -y, 0);
		}
	}
	
	/* (non-Javadoc)
	 * @see falk_guenther_meier.model.KeyboardObserver#turnUp()
	 */
	@Override
	public void turnUp() {
		// Rotation oben
		GLMatrix rotateMatrix = GLMatrix.createRotateVMatrix(viewingDirectionOrthogonale, angleDelta * speed);
		if (GLVertex.getAngleBetween(GLVertex.mult(rotateMatrix, viewingDirection), up) > 10) {
			//up.mult(rotateMatrix);
			viewingDirection.mult(rotateMatrix);
			//viewingDirection.add(new GLVertex(0, 1/50f * speed, 0));
			center.setValues(GLVertex.add(position, viewingDirection));
			for (RotateObserver o : rotateObserver) {
				o.updateRotate(this, viewingDirection.getX(), viewingDirection.getY(), viewingDirection.getZ());
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see falk_guenther_meier.model.KeyboardObserver#turnDown()
	 */
	@Override
	public void turnDown() {
		// Rotation unten
		GLMatrix rotateMatrix = GLMatrix.createRotateVMatrix(viewingDirectionOrthogonale, -angleDelta * speed);
		if (GLVertex.getAngleBetween(GLVertex.mult(rotateMatrix, viewingDirection), up) < 170) {
			//up.mult(rotateMatrix);
			viewingDirection.mult(rotateMatrix);
			//viewingDirection.add(new GLVertex(0, -1/50f * speed, 0));
			center.setValues(GLVertex.add(position, viewingDirection));
			for (RotateObserver o : rotateObserver) {
				o.updateRotate(this, viewingDirection.getX(), viewingDirection.getY(), viewingDirection.getZ());
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see falk_guenther_meier.model.KeyboardObserver#turnLeft()
	 */
	@Override
	public void turnLeft() {
		// Rotation links
		GLMatrix rotateMatrix = GLMatrix.createRotateYMatrix(angleDelta * speed);
		viewingDirectionOrthogonale.mult(rotateMatrix);
		viewingDirection.mult(rotateMatrix);
		center.setValues(GLVertex.add(position, viewingDirection));
		for (RotateObserver o : rotateObserver) {
			o.updateRotate(this, viewingDirection.getX(), viewingDirection.getY(), viewingDirection.getZ());
		}
	}
	
	/* (non-Javadoc)
	 * @see falk_guenther_meier.model.KeyboardObserver#turnRight()
	 */
	@Override
	public void turnRight() {
		// Rotation rechts
		GLMatrix rotateMatrix = GLMatrix.createRotateYMatrix(-angleDelta * speed);
		viewingDirectionOrthogonale.mult(rotateMatrix);
		viewingDirection.mult(rotateMatrix);
		center.setValues(GLVertex.add(position, viewingDirection));
		for (RotateObserver o : rotateObserver) {
			o.updateRotate(this, viewingDirection.getX(), viewingDirection.getY(), viewingDirection.getZ());
		}
	}
	
	@Override
	public void swapWireMode() {
		// bleibt leer
	}
	
	@Override
	public void cameraMode1() {
		// bleibt leer
	}
	
	@Override
	public void cameraMode2() {
		// bleibt leer
	}

	@Override
	public void cameraMode3() {
		// bleibt leer
	}

	@Override
	public void cameraMode4() {
		// bleibt leer
	}

	@Override
	public void swapLightMode() {
		// bleibt leer
	}

	@Override
	public void keyZ() {
		// bleibt leer
	}

	public void refreshCamera() {
		glu.gluLookAt(position.getX(), position.getY(), position.getZ(), center.getX(), center.getY(), center.getZ(), up.getX(), up.getY(), up.getZ());
	}
	
	public GLTexture2D getTexture(GL2 gl) {	
		if (texture == null) {
			texture = new GLTexture2D(gl, cameraWidth, cameraHeight);
		}
		return texture;
	}

	public void refreshTexture(GL2 gl, GLDrawable app) {
		if (refreshDepth == 0) { // Sicherheitsfunktion gegen Endlos-Rekursion
			refreshDepth++;

			gl.glPushAttrib(GL2.GL_VIEWPORT_BIT); // alte ViewPort-Daten speichern

			gl.glViewport(0, 0, cameraWidth, cameraHeight);

			gl.glMatrixMode(GL2.GL_MODELVIEW);
		    gl.glPushMatrix();
		    gl.glLoadIdentity();

		    refreshCamera();
		    app.draw(gl); // Welt zeichnen

			texture.bind(gl);
			gl.glCopyTexImage2D(GL2.GL_TEXTURE_2D, 0, GL2.GL_RGB, 0,0, cameraWidth, cameraHeight, 0); // kopiert das Kamerabild auf die aktuell gebundene Textur
			gl.glPopMatrix();

			gl.glPopAttrib(); // alten ViewPort wiederherstellen

			refreshDepth--;
		}
	}

	@Override
	public final void addTranslateObserver(TranslateObserver o) {
		translateObserver.add(o);
	}

	@Override
	public final void removeTranslateObserver(TranslateObserver o) {
		translateObserver.remove(o);
	}

	@Override
	public List<TranslateObserver> getTranslateObservers() {
		return translateObserver;
	}
	
	@Override
	public void addRotateObserver(RotateObserver o) {
		rotateObserver.add(o);	
	}

	@Override
	public void removeRotateObserver(RotateObserver o) {
		rotateObserver.remove(o);
	}

	@Override
	public List<RotateObserver> getRotateObservers() {
		return rotateObserver;
	}

	@Override
	public void updateTranslate(TranslateObservable changed, float x, float y, float z) {
		// bleibt leer
	}

	@Override
	public void updateTranslate(TranslateObservable changed, GLVertex v) {
		// bleibt leer
	}

	@Override
	public void updateAbsolutePosition(TranslateObservable changed, GLVertex position) {
		// bleibt leer
	}

	@Override
	public void updateScale(ScaleObservable changed, float x, float y, float z) {
		// bleibt leer
	}

	@Override
	public void updateRotate(RotateObservable changed, float x, float y, float z) {
		// bleibt leer
	}

	@Override
	public void updateRotate(RotateObservable changed, GLVertex point, float x, float y, float z) {
		// bleibt leer
	}

	@Override
	public void updateRotate(RotateObservable changed, GLVertex first, GLVertex second, float angle) {
		// bleibt leer
	}

	@Override
	public void keyU() {
		// bleibt leer
	}

	@Override
	public void swapAnimationMode() {
		// bleibt leer
	}

	@Override
	public void swapAnimationStartMode() {
		// bleibt leer
	}

	@Override
	public void keyI() {
		// bleibt leer
	}

	@Override
	public void keyO() {
		// bleibt leer
	}

	@Override
	public void keyP() {
		// bleibt leer
	}
	
	@Override
	public void keyH() {
		// bleibt leer
	}

	@Override
	public void printMyKeys() {
		System.out.println("Vorw�rts: W");
		System.out.println("R�ckw�rts: S");
		System.out.println("Seitw�rts links: A");
		System.out.println("Seitw�rts rechts: D");
		System.out.println("Bewegung nach oben: R");
		System.out.println("Bewegung nach unten: F");
		System.out.println("Nach oben schauen: Pfeiltaste oben");
		System.out.println("Nach unten schauen: Pfeiltaste unten");
		System.out.println("Linksdrehung: Pfeiltaste links");
		System.out.println("Rechtsdrehung: Pfeiltaste rechts");
	}

}
