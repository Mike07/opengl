package falk_guenther_meier.model.buildings;

import java.awt.Color;
import java.util.LinkedList;
import java.util.List;

import falk_guenther_meier.model.math.GLStraightLine;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.objects.GLTriangle;
import falk_guenther_meier.model.objects.GLTriangleWall;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.textures.GLTexCoord;
import falk_guenther_meier.model.textures.GLTexRect;
import falk_guenther_meier.model.textures.GLTexture2D;

public class GLHouse extends GLComplexObject {
	/**
	 * Haus (ohne Grundfl�che!)
	 * @param center Mittelpunkt der Grundfl�che
	 * @param width
	 * @param height
	 * @param depth H�he des Untergeschosses
	 * @param roofHeight H�he des Obergeschosses
	 */
	public GLHouse(GLVertex center, float width, float height, float depth, float roofHeight) {
		super();
		position = center;
		float wallDepth = Math.min(Math.min(height, width), Math.min(roofHeight, depth)) / 15.0f;
		// Untergeschoss
		// links
		float dHelp = depth - 2 * wallDepth;
		GLWallWithHole wall = new GLWallWithHole(GLVertex.addValues(center, 0.0f, height / 2.0f, 0.0f),
				dHelp, dHelp / 3.0f, dHelp / 3.0f, height, height / 3.0f, height / 3.0f, wallDepth);
		wall.rotate(0.0f, 270.0f, 0.0f);
		wall.translate(- width / 2.0f + wallDepth / 2.0f, 0.0f, 0.0f);
		wall.setReferencePoint(position);
		this.add(wall);
		// rechts
		wall = new GLWallWithHole(GLVertex.addValues(center, 0.0f, height / 2.0f, 0.0f),
				dHelp, dHelp / 3.0f, dHelp / 3.0f, height, height / 3.0f, height / 3.0f, wallDepth);
		wall.rotate(0.0f, 90.0f, 0.0f);
		wall.translate(width / 2.0f - wallDepth / 2.0f, 0.0f, 0.0f);
		wall.setReferencePoint(position);
		this.add(wall);
		// hinten
		wall = new GLWallWithHole(GLVertex.addValues(center, 0.0f, height / 2.0f, 0.0f),
				width, width / 3.0f, width / 3.0f, height, height / 3.0f, height / 3.0f, wallDepth);
		wall.rotate(0.0f, 180.0f, 0.0f);
		wall.translate(0.0f, 0.0f, - depth / 2.0f + wallDepth / 2.0f);
		wall.setReferencePoint(position);
		this.add(wall);
		// vorne
		wall = new GLWallWithHole(GLVertex.addValues(center, 0.0f, height / 2.0f, 0.0f),
				width, width / 4.0f, width / 2.0f, height, height / 3.0f, 0.0f, wallDepth);
		wall.translate(0.0f, 0.0f, depth / 2.0f - wallDepth / 2.0f);
		wall.setReferencePoint(position);
		this.add(wall);
		// Farbe (des Untergeschosses
		this.setColor(Color.GRAY);


		// Dach
		float roofOver = wallDepth; // Dach�berstand

		// Dach links
		GLVertex rLO = GLVertex.addValues(new GLVertex(), 0.0f, 0.0f, - depth / 2.0f - roofOver);
		GLVertex rRO = GLVertex.addValues(new GLVertex(), 0.0f, 0.0f, depth / 2.0f + roofOver);
		GLVertex rLU = new GLVertex(- width / 2.0f, - roofHeight, 0.0f);
		rLU.setNorm(rLU.getNorm() + roofOver);
		rLU.add(rLO);
		GLVertex rRU = new GLVertex(- width / 2.0f, - roofHeight, 0.0f);
		rRU.setNorm(rRU.getNorm() + roofOver);
		rRU.add(rRO);

		GLRectangle roofRect = new GLRectangle(rLU, rRU, rRO, rLO, true); // Referenzpunkt (0,0,0) ist mittig auf dem First
		roofRect.translate(0.0f, center.getY() + height + roofHeight, 0.0f);
		roofRect.setReferencePoint(position);

		GLCuboid roof = new GLCuboid(roofRect.clone(), wallDepth, true);
		roof.setReferencePoint(position);
		roof.setColor(Color.RED);
		this.add(roof);

		// Dach rechts
		rRO = GLVertex.addValues(new GLVertex(), 0.0f, 0.0f, - depth / 2.0f - roofOver);
		rLO = GLVertex.addValues(new GLVertex(), 0.0f, 0.0f, depth / 2.0f + roofOver);
		rLU = new GLVertex(width / 2.0f, - roofHeight, 0.0f);
		rLU.setNorm(rLU.getNorm() + roofOver);
		rLU.add(rLO);
		rRU = new GLVertex(width / 2.0f, - roofHeight, 0.0f);
		rRU.setNorm(rRU.getNorm() + roofOver);
		rRU.add(rRO);

		roofRect = new GLRectangle(rLU, rRU, rRO, rLO, true); // Referenzpunkt (0,0,0) ist mittig auf dem First
		roofRect.translate(0.0f, center.getY() + height + roofHeight, 0.0f);
		roofRect.setReferencePoint(position);

		roof = new GLCuboid(roofRect.clone(), wallDepth, true);
		roof.setReferencePoint(position);
		roof.setColor(Color.RED);
		this.add(roof);

		// Dach-First
		GLVertex frontVertex = GLStraightLine.getIntersection(
				new GLStraightLine(this.getRoofLeft().getRectFront().getNodes()[2], // point
				GLVertex.sub(this.getRoofLeft().getRectFront().getNodes()[2], // direction
						this.getRoofLeft().getRectFront().getNodes()[1])),
				new GLStraightLine(this.getRoofRight().getRectFront().getNodes()[3], // point
				GLVertex.sub(this.getRoofRight().getRectFront().getNodes()[3], // direction
						this.getRoofRight().getRectFront().getNodes()[0]))
				); // Punkt vorne, Spitze des Giebels, h�chster Punkt des Hauses (Bezugspunkt ist position bzw. center)

		roofRect = new GLRectangle(new GLVertex(), // aktuell: Bezugspunkt des Rechtecks ist (0,0,0)
				GLVertex.sub(this.getRoofRight().getRectTop().getNodes()[0], this.getRoofRight().getRectTop().getNodes()[3]),
				GLVertex.sub(frontVertex, new GLVertex(0.0f, height + roofHeight, depth / 2.0f + roofOver)),
				GLVertex.sub(this.getRoofLeft().getRectTop().getNodes()[1], this.getRoofLeft().getRectTop().getNodes()[2]),
				true);
		roofRect.translate(position);
		roofRect.translate(0.0f, height + roofHeight, depth / 2.0f + roofOver);
		roofRect.setReferencePoint(position); // jetzt: Bezugspunkt ist position

		roof = new GLCuboid(roofRect, depth + 2.0f * roofOver, false);
		roof.setReferencePoint(position);
		roof.setColor(Color.RED);
		this.add(roof);

		// Obergeschoss
		// vorne
		GLTriangle tri = new GLTriangle(
				this.getFront().getLeft().getRectFront().getAboveLeft().clone(),
				this.getFront().getRight().getRectFront().getAboveRight().clone(),
				this.getRoofLeft().getRectRear().getAboveLeft().clone(),
				true);
		tri.getThird().setZ(tri.getFirst().getZ()); // Dach�berstand ber�cksichtigen
		tri.translate(position);
		tri.setReferencePoint(position);

		GLTriangleWall triWall = new GLTriangleWall(tri, wallDepth, false);
		triWall.setReferencePoint(position);
		triWall.setColor(Color.GRAY);
		this.add(triWall);
		
		// hinten
		tri = new GLTriangle(
				this.getBack().getLeft().getRectFront().getAboveLeft().clone(),
				this.getBack().getRight().getRectFront().getAboveRight().clone(),
				this.getRoofLeft().getRectRear().getAboveRight().clone(),
				true);
		tri.getThird().setZ(tri.getFirst().getZ()); // Dach�berstand ber�cksichtigen
		tri.translate(position);
		tri.setReferencePoint(position);

		triWall = new GLTriangleWall(tri, wallDepth, false);
		triWall.setReferencePoint(position);
		triWall.setColor(Color.GRAY);
		this.add(triWall);

		// Fu�boden
		GLRectangle floor = new GLRectangle(new GLVertex(), width, depth);
		floor.rotate(270.0f, 0.0f, 0.0f);
		floor.setPosition(position);
		floor.setReferencePoint(position);
		this.add(floor);

		this.setReferencePoint(position);
	}

	public GLWallWithHole getLeft() {
		return (GLWallWithHole) objects.get(0);
	}
	public GLWallWithHole getRight() {
		return (GLWallWithHole) objects.get(1);
	}
	public GLWallWithHole getBack() {
		return (GLWallWithHole) objects.get(2);
	}
	public GLWallWithHole getFront() {
		return (GLWallWithHole) objects.get(3);
	}

	public GLCuboid getRoofLeft() {
		return (GLCuboid) objects.get(4);
	}
	public GLCuboid getRoofRight() {
		return (GLCuboid) objects.get(5);
	}

	public GLCuboid getRoofRidge() { // Dachfirst
		return (GLCuboid) objects.get(6);
	}

	/**
	 * Obergeschoss (Dreick-Wand) vorne
	 * @return
	 */
	public GLTriangleWall getAboveFront() {
		return (GLTriangleWall) objects.get(7);
	}
	/**
	 * Obergeschoss (Dreick-Wand) hinten
	 * @return
	 */
	public GLTriangleWall getAboveBack() {
		return (GLTriangleWall) objects.get(8);
	}

	/**
	 * Fu�boden des Hauses
	 * @return
	 */
	public GLRectangle getFloor() {
		return (GLRectangle) objects.get(9);
	}

	public void setTextureOuterWall(GLTexture2D texture, int textureUnit) {
		// Au�enw�nde
		this.getFront().setTexture(texture, textureUnit);
		this.getLeft().setTexture(texture, textureUnit);
		this.getRight().setTexture(texture, textureUnit);
		this.getBack().setTexture(texture, textureUnit);
		// Dreieck vorne
		GLTriangle tri = this.getAboveFront().getTriangleFront();
		tri.setTexture(texture, textureUnit);
		tri.getFirst().setTexCoord(new GLTexCoord(0.0f, 1.0f, textureUnit));
		tri.getSecond().setTexCoord(new GLTexCoord(1.0f, 1.0f, textureUnit));
		tri.getThird().setTexCoord(new GLTexCoord(0.5f, 0.0f, textureUnit));
		// Dreieck hinten
		tri = this.getAboveBack().getTriangleFront();
		tri.setTexture(texture, textureUnit);
		tri.getFirst().setTexCoord(new GLTexCoord(0.0f, 1.0f, textureUnit));
		tri.getSecond().setTexCoord(new GLTexCoord(1.0f, 1.0f, textureUnit));
		tri.getThird().setTexCoord(new GLTexCoord(0.5f, 0.0f, textureUnit));
	}

	public void setTextureInnerWall(GLTexture2D texture, int textureUnit) {
		// Innenw�nde
		this.getLeft().setTextureBack(texture, textureUnit);
		this.getRight().setTextureBack(texture, textureUnit);
		this.getFront().setTextureBack(texture, textureUnit);
		this.getBack().setTextureBack(texture, textureUnit);
		// Dreieck vorne
		GLTriangle tri = this.getAboveFront().getTriangleBack();
		tri.setTexture(texture, textureUnit);
		// Blick aus Hausmittelpunkt: unten rechts, unten links, oben
		tri.getFirst().setTexCoord(new GLTexCoord(0.0f, 1.0f, textureUnit));
		tri.getSecond().setTexCoord(new GLTexCoord(1.0f, 1.0f, textureUnit));
		tri.getThird().setTexCoord(new GLTexCoord(0.5f, 0.0f, textureUnit));
		// Dreieck hinten
		tri = this.getAboveBack().getTriangleBack();
		tri.setTexture(texture, textureUnit);
		tri.getFirst().setTexCoord(new GLTexCoord(0.0f, 1.0f, textureUnit));
		tri.getSecond().setTexCoord(new GLTexCoord(1.0f, 1.0f, textureUnit));
		tri.getThird().setTexCoord(new GLTexCoord(0.5f, 0.0f, textureUnit));
	}

	public void setTextureOuterRoof(GLTexture2D texture, int textureUnit) {
		float little = this.getRoofRidge().getRectTop().getWidth();
		float big = this.getRoofLeft().getRectFront().getWidth();

		GLTexRect rect;
		// links
		rect = new GLTexRect(0.0f, little / (little + big), 1.0f, big / (little + big));
		this.getRoofLeft().getRectFront().setTexture(texture, textureUnit);
		this.getRoofLeft().getRectFront().setTexRect(rect, 0, false);

		this.getRoofRidge().getRectTop().setTexture(texture, textureUnit);
		this.getRoofRidge().getRectTop().getNodes()[0].setTexCoord(new GLTexCoord(1.0f, little / (little + big), textureUnit));
		this.getRoofRidge().getRectTop().getNodes()[1].setTexCoord(new GLTexCoord(1.0f, 0.0f, textureUnit));
		this.getRoofRidge().getRectTop().getNodes()[2].setTexCoord(new GLTexCoord(0.0f, 0.0f, textureUnit));
		this.getRoofRidge().getRectTop().getNodes()[3].setTexCoord(new GLTexCoord(0.0f, little / (little + big), textureUnit));

		// rechts
		rect = new GLTexRect(0.0f, little / (little + big), 1.0f, big / (little + big));
		this.getRoofRight().getRectFront().setTexture(texture, textureUnit);
		this.getRoofRight().getRectFront().setTexRect(rect, 0, false);

		this.getRoofRidge().getRectRight().setTexture(texture, textureUnit);
		this.getRoofRidge().getRectRight().getNodes()[0].setTexCoord(new GLTexCoord(0.0f, little / (little + big), textureUnit));
		this.getRoofRidge().getRectRight().getNodes()[1].setTexCoord(new GLTexCoord(1.0f, little / (little + big), textureUnit));
		this.getRoofRidge().getRectRight().getNodes()[2].setTexCoord(new GLTexCoord(1.0f, 0.0f, textureUnit));
		this.getRoofRidge().getRectRight().getNodes()[3].setTexCoord(new GLTexCoord(0.0f, 0.0f, textureUnit));
	}

	public void setTextureInnerRoof(GLTexture2D texture, int textureUnit) {
		this.getRoofLeft().getRectRear().setTexture(texture, textureUnit);
		this.getRoofRight().getRectRear().setTexture(texture, textureUnit);
	}

	public void setTextureFloor(GLTexture2D texture, int textureUnit) {
		this.getFloor().setTexture(texture, textureUnit);
	}

	/**
	 * Liefert die von au�en sichtbaren Fl�chen des Hauses, die mit einer Textur belegt sind / werden k�nnen.
	 * @return
	 */
	public List<GLObject> getOuterPlanesWithTexture() {
		List<GLObject> result = new LinkedList<GLObject>();

		// Au�enfl�chen
		result.addAll(this.getLeft().getOuterPlanes());
		result.addAll(this.getRight().getOuterPlanes());
		result.addAll(this.getFront().getOuterPlanes());
		result.addAll(this.getBack().getOuterPlanes());

		result.add(this.getAboveFront().getTriangleFront());
		result.add(this.getAboveBack().getTriangleFront());

		// "Kanten"
		result.addAll(this.getLeft().getBetweenPlanes());
		result.addAll(this.getRight().getBetweenPlanes());
		result.addAll(this.getFront().getBetweenPlanes());
		result.addAll(this.getBack().getBetweenPlanes());

		// Dach
		result.add(this.getRoofLeft().getRectFront());
		result.add(this.getRoofRight().getRectFront());
		result.add(this.getRoofRidge().getRectTop());
		result.add(this.getRoofRidge().getRectRight());

		return result;
	}

	/**
	 * Liefert die von au�en sichtbaren Fl�chen des Hauses, die nicht mit einer Textur belegt sind / werden k�nnen.
	 * @return
	 */
	public List<GLObject> getOuterPlanesWithOutTexture() {
		List<GLObject> result = new LinkedList<GLObject>();
		// Dach komplett
		GLCuboid c = this.getRoofLeft();
		result.add(c.getRectLeft());
		result.add(c.getRectRight());
//		result.add(c.getRectTop());
		result.add(c.getRectBottom());
		c = this.getRoofRight();
		result.add(c.getRectLeft());
		result.add(c.getRectRight());
//		result.add(c.getRectTop());
		result.add(c.getRectBottom());

		c = this.getRoofRidge();
		result.add(c.getRectFront());
		result.add(c.getRectRear());

		return result;
	}

	/**
	 * Liefert die von innen sichtbaren Fl�chen des Hauses.
	 * @return
	 */
	public List<GLObject> getInnerPlanes() {
		List<GLObject> result = new LinkedList<GLObject>();

		// Innenw�nde
		result.addAll(this.getLeft().getInnerPlanes());
		result.addAll(this.getRight().getInnerPlanes());
		result.addAll(this.getFront().getInnerPlanes());
		result.addAll(this.getBack().getInnerPlanes());

		result.add(this.getRoofLeft().getRectRear());
		result.add(this.getRoofRight().getRectRear());
		result.add(this.getAboveFront().getTriangleBack());
		result.add(this.getAboveBack().getTriangleBack());
		result.add(this.getFloor());

		return result;
	}
}
