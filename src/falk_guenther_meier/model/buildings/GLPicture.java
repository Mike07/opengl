package falk_guenther_meier.model.buildings;

import java.awt.Color;
import java.util.LinkedList;
import java.util.List;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.textures.GLTexture2D;

/**
 * Gem�lde
 * @author Johannes
 */
public class GLPicture extends GLComplexObject {
	/**
	 * Erstellt ein Bild / Gem�lde.
	 * @param center Mittelpunkt des Gem�ldes (auf der R�ckseite an der Wand)
	 * @param width Breite
	 * @param height H�he
	 * @param thickness St�rke des Rahmens
	 * @param texture Bild
	 * @param textureUnit
	 */
	public GLPicture(GLVertex center, float width, float height, float thickness, GLTexture2D texture, int textureUnit) {
		super();
		position = center;

		// TODO: entsprechende Materialien heraussuchen!
		GLRectangle r = new GLRectangle(position.clone(), width, height);
		r.setColor(Color.WHITE);
		r.setMaterial(GLMaterial.createColor(new GLVertex(Color.WHITE)));
		this.add(r);

		r = new GLRectangle(position.clone(), 0.8f * width, 0.8f * height);
		r.translate(0.0f, 0.0f, 0.01f);
		r.setColor(Color.WHITE);
		r.setMaterial(GLMaterial.createTextureMaterial());
		r.setTexture(texture, textureUnit);
		this.add(r);

		float w = width / 2.0f;
		float h = height / 2.0f;
		float d = thickness / 2.0f;

		GLVertex color = new GLVertex(0.54f, 0.27f, 0.07f);
		GLCuboid c = new GLCuboid(GLVertex.addValues(position, - w - d, 0.0f, d), thickness, height, thickness);
		c.setColor(color.clone());
		c.setMaterial(GLMaterial.createColor(color));
		this.add(c);
		c = new GLCuboid(GLVertex.addValues(position, w + d, 0.0f, d), thickness, height, thickness);
		c.setColor(color.clone());
		c.setMaterial(GLMaterial.createColor(color));
		this.add(c);
		c = new GLCuboid(GLVertex.addValues(position, 0.0f, h + d, d), width + 2 * thickness, thickness, thickness);
		c.setColor(color.clone());
		c.setMaterial(GLMaterial.createColor(color));
		this.add(c);
		c = new GLCuboid(GLVertex.addValues(position, 0.0f, - h - d, d), width + 2 * thickness, thickness, thickness);
		c.setColor(color.clone());
		c.setMaterial(GLMaterial.createColor(color));
		this.add(c);

		this.setReferencePoint(position);
	}

	public GLRectangle getWhitePlane() {
		return (GLRectangle) objects.get(0);
	}
	public GLRectangle getPicturePlane() {
		return (GLRectangle) objects.get(1);
	}
	public GLCuboid getLeft() {
		return (GLCuboid) objects.get(2);
	}
	public GLCuboid getRight() {
		return (GLCuboid) objects.get(3);
	}
	public GLCuboid getAbove() {
		return (GLCuboid) objects.get(4);
	}
	public GLCuboid getBelow() {
		return (GLCuboid) objects.get(5);
	}

	public List<GLObject> getObjectsWithOutTexture() {
		List<GLObject> result = new LinkedList<GLObject>();
		result.add(this.getLeft());
		result.add(this.getRight());
		result.add(this.getAbove());
		result.add(this.getBelow());
		result.add(this.getWhitePlane());
		return result;
	}
}
