package falk_guenther_meier.model.buildings;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLComplexObject;

public class GLOpenRoom extends GLComplexObject {
	public GLOpenRoom(GLVertex center, float xSize, float ySize, float height, float wallThickness) {
		super();
		position = center;
		// position ist im Mittelpunkt des Bodens auf der Oberseite des Bodens
		// W�nde umschlie�en den Boden, xSize und ySize sind absolute Werte (Boden abh�ngig von Wandst�rke)
		// vorne und hinten umschlie�en links und rechts

		// Boden
		GLCuboid help = new GLCuboid(new GLVertex(position.getX(), position.getY() - wallThickness / 2.0f,
				position.getZ()),
				xSize - 2.0f * wallThickness, wallThickness, ySize - 2.0f * wallThickness);
		help.setReferencePoint(position);
		this.add(help);
		
		// links
		help = new GLCuboid(new GLVertex(position.getX() - xSize / 2.0f + wallThickness / 2.0f,
				position.getY() - wallThickness + height / 2.0f, position.getZ()),
				ySize - 2.0f * wallThickness, height, wallThickness);
		help.rotate(0.0f, 90.0f, 0.0f);
		help.setReferencePoint(position);
		this.add(help);

		// rechts
		help = new GLCuboid(new GLVertex(position.getX() + xSize / 2.0f - wallThickness / 2.0f,
				position.getY() - wallThickness + height / 2.0f, position.getZ()),
				ySize - 2.0f * wallThickness, height, wallThickness);
		help.rotate(0.0f, 270.0f, 0.0f);
		help.setReferencePoint(position);
		this.add(help);

		// hinten
		help = new GLCuboid(new GLVertex(position.getX(),	position.getY() - wallThickness + height / 2.0f,
				position.getZ() - ySize / 2.0f + wallThickness / 2.0f),
				xSize, height, wallThickness);
		help.setReferencePoint(position);
		this.add(help);

		// vorne
		help = new GLCuboid(new GLVertex(position.getX(),	position.getY() - wallThickness + height / 2.0f,
				position.getZ() + ySize / 2.0f - wallThickness / 2.0f),
				xSize, height, wallThickness);
		help.rotate(0.0f, 180.0f, 0.0f);
		help.setReferencePoint(position);
		this.add(help);

		this.setReferencePoint(position);
	}

	public GLCuboid getFloor() {
		return (GLCuboid) this.getObjects().get(0);
	}
	public GLCuboid getLeft() {
		return (GLCuboid) this.getObjects().get(1);
	}
	public GLCuboid getRight() {
		return (GLCuboid) this.getObjects().get(2);
	}
	public GLCuboid getFront() {
		return (GLCuboid) this.getObjects().get(4);
	}
	public GLCuboid getBack() {
		return (GLCuboid) this.getObjects().get(3);
	}
}
