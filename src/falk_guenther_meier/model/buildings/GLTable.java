package falk_guenther_meier.model.buildings;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLComplexObject;

/**
 * Tisch.
 * @author Michael
 */
public class GLTable extends GLComplexObject {

	/**
	 * Erstellt einen Tisch mit vier Beinen.
	 * @param center Mittelpunkt des Tisches auf dem Boden.
	 * @param xSize Breite
	 * @param ySize Tiefe
	 * @param height H�he der Beine
	 * @param offset Verschiebung der Beine Richtung Mitte, absolute Verschiebung: offset * Dicke der Beine
	 */
	public GLTable(GLVertex center, float xSize, float ySize, float height, float offset) {
		this(center, xSize, ySize, height, 0.08f, 0.10f, offset);
	}
	
	/**
	 * Erstellt einen Tisch mit vier Beinen.
	 * @param center Mittelpunkt des Tisches auf dem Boden.
	 * @param xSize Breite
	 * @param ySize Tiefe
	 * @param height H�he der Beine
	 * @param feetThickness Faktor f�r Dicke der Beine (rel.): feetThickness * (xSize + ySize) / 2.0f
	 * @param topThickness rel. Dicke der Platte: topThickness * height
	 * @param offset Verschiebung der Beine Richtung Mitte, absolute Verschiebung: offset * Dicke der Beine
	 */
	public GLTable(GLVertex center, float xSize, float ySize, float height, float feetThickness, float topThickness, float offset) {
		super();
		position = center;
		// position ist im Mittelpunkt des Tisch vom Boden aus
		// xSize und ySize und height sind absolute Werte
		
		// Tischplatte
		topThickness = topThickness * height; // abs. Dicke der Platte
		GLCuboid help = new GLCuboid(new GLVertex(position.getX(), position.getY() + height + topThickness / 2.0f,
				position.getZ()), xSize, topThickness, ySize);
		help.setReferencePoint(position);
		this.add(help);
		
		// F��e
		feetThickness = feetThickness * (xSize + ySize) / 2.0f; // absolute Dicke der Beine
		// zu gro�er offset -> offset wird nicht ber�cksichtigt
		if (offset > ySize / 2.0f - feetThickness || offset > xSize / 2.0f - feetThickness) offset = 0f;
			
		// links hinten
		help = new GLCuboid(new GLVertex(position.getX() - (xSize - feetThickness) / 2.0f + offset * feetThickness, position.getY() + height / 2.0f,
				position.getZ() - (ySize - feetThickness) / 2.0f + offset * feetThickness), feetThickness, height, feetThickness);
		help.setReferencePoint(position);
		this.add(help);
		// links vorne
		help = new GLCuboid(new GLVertex(position.getX() - (xSize - feetThickness) / 2.0f + offset * feetThickness, position.getY() + height / 2.0f,
				position.getZ() + (ySize - feetThickness) / 2.0f - offset * feetThickness), feetThickness, height, feetThickness);
		help.setReferencePoint(position);
		this.add(help);
		// rechts hinten
		help = new GLCuboid(new GLVertex(position.getX() + (xSize - feetThickness) / 2.0f - offset * feetThickness, position.getY() + height / 2.0f,
				position.getZ() - (ySize - feetThickness) / 2.0f + offset * feetThickness), feetThickness, height, feetThickness);
		help.setReferencePoint(position);
		this.add(help);
		// rechts vorne
		help = new GLCuboid(new GLVertex(position.getX() + (xSize - feetThickness) / 2.0f - offset * feetThickness, position.getY() + height / 2.0f,
				position.getZ() + (ySize - feetThickness) / 2.0f - offset * feetThickness), feetThickness, height, feetThickness);
		help.setReferencePoint(position);
		this.add(help);

		this.setReferencePoint(position);
	}

	public GLCuboid getTableDesk() {
		return (GLCuboid) this.getObjects().get(0);
	}
	public GLCuboid getLeftBackLeg() {
		return (GLCuboid) this.getObjects().get(1);
	}
	public GLCuboid getLeftFrontLeg() {
		return (GLCuboid) this.getObjects().get(2);
	}
	public GLCuboid getRightBackLeg() {
		return (GLCuboid) this.getObjects().get(3);
	}
	public GLCuboid getRightFrontLeg() {
		return (GLCuboid) this.getObjects().get(4);
	}
	
	/**
	 * Erzeugt einen Standard-Tisch.
	 * @param center
	 * @return
	 */
	public static GLTable createStandardTable(GLVertex center) {
		return new GLTable(center, 7, 5, 4, 1);
	}
}
