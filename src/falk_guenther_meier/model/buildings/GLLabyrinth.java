package falk_guenther_meier.model.buildings;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLComplexObject;

/**
 * Gr��erer Innenraum (nicht generisch)
 * @author Michael
 */
public class GLLabyrinth extends GLComplexObject {
	/**
	 * @param center Mittelpunkt der Grundfl�che
	 * @param width
	 * @param height
	 * @param depth
	 */
	public GLLabyrinth(GLVertex center, float width, float height, float depth) {
		super();
		position = center;
		// Au�enw�nde
		GLCuboid room = new GLCuboid(center, width, height, depth);
		// TODO: Textur f�r Innenseite setzen
		this.add(room);
		this.setReferencePoint(position);
	}

	public GLWallWithHole getLeft() {
		return (GLWallWithHole) objects.get(0);
	}
	public GLWallWithHole getRight() {
		return (GLWallWithHole) objects.get(1);
	}
	public GLWallWithHole getBack() {
		return (GLWallWithHole) objects.get(2);
	}
	public GLWallWithHole getFront() {
		return (GLWallWithHole) objects.get(3);
	}

	public GLCuboid getRoofLeft() {
		return (GLCuboid) objects.get(4);
	}
	public GLCuboid getRoofRight() {
		return (GLCuboid) objects.get(5);
	}

	public GLCuboid getRoofRidge() { // Dachfirst
		return (GLCuboid) objects.get(6);
	}
}
