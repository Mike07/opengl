package falk_guenther_meier.model.buildings;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLComplexObject;

/**
 * Demo-Landschaft
 * @author Michael
 */
public class DemoLandscape extends GLComplexObject {	
	public DemoLandscape(GLVertex position) {
		super();
		this.position = position;

		{ GLVertex cuboidPos = new GLVertex(-1.3f, -0.1f, -1);
		GLCuboid cuboid = new GLCuboid(cuboidPos, 4, 2, 4);
		
		GLVertex color = new GLVertex(0.25f, 0.25f, 0.35f);
		cuboid.setColor(color);
		
		cuboid.getObjects().get(2).setColor(new GLVertex(0.2f, 0.7f, 0.2f));
		
		cuboid.rotate(-90, 0, 0);
		this.add(cuboid); }
		
		{ GLVertex cuboidPos = new GLVertex(-1.5f, -1f, 1.6f);
		GLCuboid cuboid = new GLCuboid(cuboidPos, 2.5f, 1, 2);
		
		GLVertex color = new GLVertex(0.9f, 0.2f, 0.3f);
		cuboid.setColor(color);
		
		cuboid.rotate(0, -90, 0);
		this.add(cuboid); }
		
		{ GLVertex cuboidPos = new GLVertex(-1.4f, -0.1f, 0);
		GLCuboid cuboid = new GLCuboid(cuboidPos, 1.8f, 1.8f, 6);
		
		GLVertex color = new GLVertex(0.2f, 0.7f, 0.2f);
		cuboid.setColor(color);
		
		cuboid.getObjects().get(5).setColor(new GLVertex(0.25f, 0.25f, 0.35f));
		
		cuboid.rotate(0, -90, 0);
		cuboid.translate(0, 0.5f, 0);
		this.add(cuboid); }
		
		GLVertex cuboidPos = new GLVertex(-0.5f, 0.2f, -2.5f);
		GLCuboid cuboid = new GLCuboid(cuboidPos, 3, 3, 3);
		
		GLVertex color = new GLVertex(0.5f, 0.5f, 0.2f);
		cuboid.setColor(color);
		
		cuboid.getObjects().get(2).setColor(new GLVertex(0.9f, 0.2f, 0.3f));
		
		cuboid.rotate(0, 90, 0);
		this.add(cuboid);
		this.setReferencePoint(position);
	}
}
