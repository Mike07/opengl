package falk_guenther_meier.model.buildings;

import java.util.LinkedList;
import java.util.List;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.textures.GLTexRect;
import falk_guenther_meier.model.textures.GLTexture2D;

/**
 * "Mauer mit Loch": zusammengebaut aus 2 bis 4 Quadern
 * @author Johannes
 */
public class GLWallWithHole extends GLComplexObject {
	// gibt jeweils an, ob die einzelnen Bereiche gesetzt sind
	private boolean left;
	private boolean right;
	private boolean above;
	private boolean below;

	/**
	 * Erstellt eine Wand mit einem zentrierten Rechteck-Fenster /-Loch.
	 * Es d�rfen weder die linke und rechte Seite noch die obere und untere Seite gleichzeitig 0 sein.
	 * @param center Mittelpunkt vom Rechteck, zentriert, mittig in Bezug auf die Mauerdicke
	 * @param width Breite der Wand
	 * @param left Abstand von der Au�enkante des Rechtecks bis zum linken Fensterrand
	 * @param right Abstand von der Au�enkante des Rechtecks bis zum rechten Fensterrand
	 * @param height H�he der Wand
	 * @param above Abstand von der Au�enkante des Rechtecks bis zum oberen Fensterrand
	 * @param below Abstand von der Au�enkante des Rechtecks bis zum unteren Fensterrand
	 * @param depth Dicke der Mauer
	 */
	public GLWallWithHole(GLVertex center, float width, float left, float right,
			float height, float above, float below, float depth) {
		super();
		this.position = center;

		if (left <= 0.0f && right <= 0.0f) {
			throw new IllegalArgumentException();
		}
		if (above <= 0.0f && below <= 0.0f) {
			throw new IllegalArgumentException();			
		}

		float w = (width - left - right) / 2.0f; // halbe Breite des Fensters
		GLCuboid help;

		// linker Teil (auf der ganzen H�he)
		if (left > 0.0f) {
			help = new GLCuboid(GLVertex.addValues(center, - width / 2.0f + left / 2.0f, 0.0f, 0.0f), left, height, depth);
			help.setReferencePoint(center);
//			GLTexRect texRect = new GLTexRect(0, 0, left / width, 1);
//			help.setTexRect(texRect, 0, false);
			this.add(help);
			this.left = true;
		} else {
			this.left = false;			
		}
		// rechter Teil (auf der ganzen H�he)
		if (right > 0.0f) {
			help = new GLCuboid(GLVertex.addValues(center, width / 2.0f - right / 2.0f, 0.0f, 0.0f), right, height, depth);
			help.setReferencePoint(center);
//			GLTexRect texRect = new GLTexRect((width - right) / width, 0, 1, 1);
//			help.setTexRect(texRect, 0, false);
			this.add(help);
			this.right = true;
		} else {
			this.right = false;			
		}
		// oberer Teil (nur oberhalb des Fensters)
		if (above > 0.0f) {
			help = new GLCuboid(GLVertex.addValues(center, - width / 2.0f + left + w, height / 2.0f - above / 2.0f, 0.0f), 2.0f * w, above, depth);
			help.setReferencePoint(center);
//			GLTexRect texRect = new GLTexRect(left / width, 0, (width - right) / width , 0.1f);
//			help.setTexRect(texRect, 0, false);
			this.add(help);
			this.above = true;
		} else {
			this.above = false;			
		}
		// unterer Teil (nur unterhalb des Fensters)
		if (below > 0.0f) {
			help = new GLCuboid(GLVertex.addValues(center, - width / 2.0f + left + w, - height / 2.0f + below / 2.0f, 0.0f), 2.0f * w, below, depth);
			help.setReferencePoint(center);
			this.add(help);
			this.below = true;
		} else {
			this.below = false;			
		}
		this.setReferencePoint(position);
	}

	public GLCuboid getLeft() {
		if (left) {
			return (GLCuboid) objects.get(0);
		}
		return null;
	}
	public GLCuboid getRight() {
		if (right) {
			return (GLCuboid) objects.get(1);
		}
		return null;
	}
	public GLCuboid getAbove() {
		if (above) {
			return (GLCuboid) objects.get(2);
		}
		return null;
	}
	public GLCuboid getBelow() {
		if (below) {
			return (GLCuboid) objects.get(3);
		}
		return null;
	}

	/**
	 * Liefert die Rechtecke der Vorderseite.
	 * @return
	 */
	public List<GLObject> getOuterPlanes() {
		List<GLObject> result = new LinkedList<GLObject>();
		if (this.getLeft() != null) {
			result.add(this.getLeft().getRectFront());
		}
		if (this.getRight() != null) {
			result.add(this.getRight().getRectFront());
		}
		if (this.getAbove() != null) {
			result.add(this.getAbove().getRectFront());
		}
		if (this.getBelow() != null) {
			result.add(this.getBelow().getRectFront());
		}
		return result;
	}

	/**
	 * Liefert die Rechtecke der R�ckseite.
	 * @return
	 */
	public List<GLObject> getInnerPlanes() {
		List<GLObject> result = new LinkedList<GLObject>();
		if (this.getLeft() != null) {
			result.add(this.getLeft().getRectRear());
		}
		if (this.getRight() != null) {
			result.add(this.getRight().getRectRear());
		}
		if (this.getAbove() != null) {
			result.add(this.getAbove().getRectRear());
		}
		if (this.getBelow() != null) {
			result.add(this.getBelow().getRectRear());
		}
		return result;
	}

	/**
	 * Liefert die Rechtecke der Kanten (oben, unten, links, rechts).
	 * @return
	 */
	public List<GLObject> getBetweenPlanes() {
		List<GLObject> result = new LinkedList<GLObject>();
		GLCuboid c;
		c = this.getLeft();
		if (c != null) {
			result.add(c.getRectLeft());
			result.add(c.getRectRight());
			result.add(c.getRectTop());
			result.add(c.getRectBottom());
		}
		c = this.getRight();
		if (c != null) {
			result.add(c.getRectLeft());
			result.add(c.getRectRight());
			result.add(c.getRectTop());
			result.add(c.getRectBottom());
		}
		c = this.getAbove();
		if (c != null) {
			result.add(c.getRectLeft());
			result.add(c.getRectRight());
			result.add(c.getRectTop());
			result.add(c.getRectBottom());
		}
		c = this.getBelow();
		if (c != null) {
			result.add(c.getRectLeft());
			result.add(c.getRectRight());
			result.add(c.getRectTop());
			result.add(c.getRectBottom());
		}
		return result;
	}

	@Override
	public void setTexture(GLTexture2D texture, int textureUnit) {
		super.setTexture(texture, textureUnit);
		// (0,0) ist oben links !!
		float l = 0.0f; // Breite der linken Seite
		float d = 0.0f; // Tiefe der Mauer
		if (this.left) {
			l = this.getLeft().getRectFront().getWidth();
			d = this.getLeft().getRectLeft().getWidth();
		}
		float r = 0.0f; // Breite der rechten Seite
		if (this.right) {
			r = this.getRight().getRectFront().getWidth();
		}
		float a = 0.0f; // H�he des oberen St�cks 
		if (this.above) {
			a = this.getAbove().getRectFront().getHeight();
		}
		float b = 0.0f; // H�he des unteren St�cks
		if (this.below) {
			b = this.getBelow().getRectFront().getHeight();
		}

		float cw = 0.0f; // Breite des "Fensters"
		if (this.getAbove() != null) {
			cw = this.getAbove().getRectFront().getWidth();
		} else {
			cw = this.getBelow().getRectBottom().getWidth();
		}
		float w = l + r + cw; // Gesamt-Breite der Mauer

		float h = 0.0f; // Gesamt-H�he der Mauer
		if (this.left) {
			h = this.getLeft().getRectFront().getHeight();
		} else {
			h = this.getRight().getRectFront().getHeight();
		}
		float ch = h - a - b; // H�he des "Fensters"

		GLTexRect rect;
		if (this.left) { // links
			rect = new GLTexRect(0, 0, l / w, 1.0f);
			this.getLeft().setTexRect(rect, 0, false);
			rect = new GLTexRect(0, 0, l / w, d / h);
			this.getLeft().getRectTop().setTexRect(rect, 0, false);
			this.getLeft().getRectBottom().setTexRect(rect, 0, false);
		}
		if (this.right) { // rechts
			rect = new GLTexRect((l + cw) / w, 0, r / w, 1.0f);
			this.getRight().setTexRect(rect, 0, false);
			rect = new GLTexRect((l + cw) / w, 0, r / w, d / h);
			this.getRight().getRectTop().setTexRect(rect, 0, false);
			this.getRight().getRectBottom().setTexRect(rect, 0, false);
		}
		if (this.above) { // oben
			rect = new GLTexRect(l / w, 0, cw / w, a / h);
			this.getAbove().setTexRect(rect, 0, false);
			rect = new GLTexRect(l / w, 0, cw / w, d / h);
			this.getAbove().getRectTop().setTexRect(rect, 0, false);
		}
		if (this.below) { // unten
			rect = new GLTexRect(l / w, (a + ch) / h, cw / w, b / h);
			this.getBelow().setTexRect(rect, 0, false);
			rect = new GLTexRect(l / w, 0, cw / w, d / h);
			this.getBelow().getRectBottom().setTexRect(rect, 0, false);
		}
	}

	public void setTextureBack(GLTexture2D texture, int textureUnit) {
		// (0,0) ist oben links !!
		float l = 0.0f; // Breite der linken Seite
		if (this.left) {
			l = this.getLeft().getRectFront().getWidth();
		}
		float r = 0.0f; // Breite der rechten Seite
		if (this.right) {
			r = this.getRight().getRectFront().getWidth();
		}
		float a = 0.0f; // H�he des oberen St�cks 
		if (this.above) {
			a = this.getAbove().getRectFront().getHeight();
		}
		float b = 0.0f; // H�he des unteren St�cks
		if (this.below) {
			b = this.getBelow().getRectFront().getHeight();
		}

		float cw = 0.0f; // Breite des "Fensters"
		if (this.getAbove() != null) {
			cw = this.getAbove().getRectFront().getWidth();
		} else {
			cw = this.getBelow().getRectBottom().getWidth();
		}
		float w = l + r + cw; // Gesamt-Breite der Mauer

		float h = 0.0f; // Gesamt-H�he der Mauer
		if (this.left) {
			h = this.getLeft().getRectFront().getHeight();
		} else {
			h = this.getRight().getRectFront().getHeight();
		}
		float ch = h - a - b; // H�he des "Fensters"

		// Textur-Koordinaten setzen
		GLTexRect rect;
		if (this.left) { // links
//			rect = new GLTexRect((l + cw) / w, 0.0f, r / w, 1.0f);
			rect = new GLTexRect((r + cw) / w, 0.0f, l / w, 1.0f);
			this.getLeft().getRectRear().setTexture(texture, textureUnit);
			this.getLeft().getRectRear().setTexRect(rect, 0, false);
		}
		if (this.right) { // rechts
//			rect = new GLTexRect(0.0f, 0.0f, l / w, 1.0f);
			rect = new GLTexRect(0.0f, 0.0f, r / w, 1.0f);
			this.getRight().getRectRear().setTexture(texture, textureUnit);
			this.getRight().getRectRear().setTexRect(rect, 0, false);
		}
		if (this.above) { // oben
//			rect = new GLTexRect(l / w, 0.0f, cw / w, a / h);
			rect = new GLTexRect(r / w, 0.0f, cw / w, a / h);
			this.getAbove().getRectRear().setTexture(texture, textureUnit);
			this.getAbove().getRectRear().setTexRect(rect, 0, false);
		}
		if (this.below) { // unten
//			rect = new GLTexRect(l / w, (a + ch) / h, cw / w, b / h);
			rect = new GLTexRect(r / w, (a + ch) / h, cw / w, b / h);
			this.getBelow().getRectRear().setTexture(texture, textureUnit);
			this.getBelow().getRectRear().setTexRect(rect, 0, false);
		}		
	}
}
