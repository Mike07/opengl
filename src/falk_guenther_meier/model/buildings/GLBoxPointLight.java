package falk_guenther_meier.model.buildings;

import java.awt.Color;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.observers.RotateObservable;
import falk_guenther_meier.model.observers.ScaleObservable;
import falk_guenther_meier.model.observers.TranslateObservable;
import falk_guenther_meier.model.platform.GLComplexObject;

/**
 * "Stands�ule" f�r ein Punktlicht.
 * Anforderungen: das Punktlicht darf nicht bewegt werden! stattdessen muss die S�ule bewegt werden!
 * @author Johannes
 */
public class GLBoxPointLight extends GLComplexObject {
	private GLPointLight light;

	/**
	 * 
	 * @param light Punkt-Licht-Quelle
	 * @param bottom Punkt auf dem Fu�boden, auf dem die S�ule steht
	 * @param width Breite der quadratischen S�ule
	 * @param height H�he der S�ule
	 */
	public GLBoxPointLight(GLPointLight light, GLVertex bottom, float width, float height) {
		super();
		position = bottom;

		GLCuboid s = new GLCuboid(GLVertex.addValues(position.clone(), 0.0f, height / 2.0f, 0.0f),
				width, height, width);
		s.setColor(Color.GRAY);
		s.setMaterial(GLMaterial.createColor(new GLVertex(Color.GRAY)));
		this.add(s);

		s = new GLCuboid(GLVertex.addValues(position.clone(), 0.0f, height + width, 0.0f), 2 * width);
		s.setColor(Color.WHITE);
		s.setMaterial(GLMaterial.createColor(new GLVertex(Color.WHITE)));
		this.add(s);

		this.light = light;
		this.light.setPosition(this.getTop().getAbsoluteCenter().clone());

		this.setReferencePoint(position);
	}

	public GLCuboid getBottom() {
		return (GLCuboid) objects.get(0);
	}
	public GLCuboid getTop() {
		return (GLCuboid) objects.get(1);
	}

	@Override
	public void updateRotate(RotateObservable changed, float x, float y, float z) {
		super.updateRotate(changed, x, y, z);
		this.light.setPosition(this.getTop().getAbsoluteCenter());
	}

	@Override
	public void updateRotate(RotateObservable changed, GLVertex point, float x, float y, float z) {
		super.updateRotate(changed, point, x, y, z);
		this.light.setPosition(this.getTop().getAbsoluteCenter());
	}

	@Override
	public void updateRotate(RotateObservable changed, GLVertex first, GLVertex second, float angle) {
		super.updateRotate(changed, first, second, angle);
		this.light.setPosition(this.getTop().getAbsoluteCenter());
	}

	@Override
	public void updateScale(ScaleObservable changed, float x, float y, float z) {
		super.updateScale(changed, x, y, z);
		this.light.setPosition(this.getTop().getAbsoluteCenter());
	}

	@Override
	public void updateTranslate(TranslateObservable changed, float x, float y, float z) {
		super.updateTranslate(changed, x, y, z);
		this.light.setPosition(this.getTop().getAbsoluteCenter());
	}

	@Override
	public void updateTranslate(TranslateObservable changed, GLVertex v) {
		super.updateTranslate(changed, v);
		this.light.setPosition(this.getTop().getAbsoluteCenter());
}

	@Override
	public void updateAbsolutePosition(TranslateObservable changed, GLVertex position) {
		super.updateAbsolutePosition(changed, position);
		this.light.setPosition(this.getTop().getAbsoluteCenter());
	}
}
