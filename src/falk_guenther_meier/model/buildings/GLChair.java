package falk_guenther_meier.model.buildings;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;

/**
 * Stuhl.
 * @author Michael
 */
public class GLChair extends GLTable {
	
	/**
	 * Erstellt einen Stuhl mit Lehne und vier Beinen.
	 * @param center Mittelpunkt des Stuhles auf dem Boden.
	 * @param xSize Breite
	 * @param ySize Tiefe
	 * @param tableHeight H�he des Stuhls ohne Lehne (Tisch)
	 * @param backRestHeight H�he der Lehne
	 * @param feetThickness rel. Dicke der Beine
	 * @param topThickness rel. Dicke der Sitzfl�che
	 * @param backRestThickness rel. Dicke der Lehne
	 */
	public GLChair(GLVertex center, float xSize, float ySize, float tableHeight, float backRestHeight, float feetThickness, float topThickness, float backRestThickness) {
		// Stuhl ist erweiterter Tisch
		super(center, xSize, ySize, tableHeight, feetThickness, topThickness, 0);
		// Lehne
		backRestThickness = backRestThickness * ySize; // abs. dicke der lehne
		GLCuboid help = new GLCuboid(new GLVertex(position.getX(), position.getY() + tableHeight + backRestHeight / 2.0f,
				position.getZ() - (ySize - backRestThickness) / 2.0f), xSize, backRestHeight, backRestThickness);
		help.setReferencePoint(position);
		this.add(help);
	}
	
	/**
	 * Erstellt einen Stuhl mit Lehne und vier Beinen.
	 * @param center Mittelpunkt des Stuhles auf dem Boden.
	 * @param xSize Breite
	 * @param ySize Tiefe
	 * @param tableHeight H�he des Stuhls ohne Lehne (Tisch)
	 * @param backRestHeight H�he der Lehne
	 */
	public GLChair(GLVertex center, float xSize, float ySize, float tableHeight, float backRestHeight) {
		this(center, xSize, ySize, tableHeight, backRestHeight, 0.15f, 0.15f, 0.15f);
	}

	public GLCuboid getBackRest() {
		return (GLCuboid) this.getObjects().get(5);
	}
	
	/**
	 * Erzeugt einen Standardstuhl
	 * @param center.
	 * @return
	 */
	public static GLChair createStandardChair(GLVertex center) {
		return new GLChair(center, 3, 3, 3, 3.5f);
	}

}
