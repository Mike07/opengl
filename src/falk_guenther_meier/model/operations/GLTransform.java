package falk_guenther_meier.model.operations;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLMatrix;

public class GLTransform implements GLOperation {	
	private GLMatrix matrix;
	
	public GLTransform(GLMatrix matrix) {
		this.matrix = matrix;
	}
	
	@Override
	public void toGL(GL2 gl) {
		if (matrix != null) {
			gl.glMultTransposeMatrixf(matrix.get(), 0);
		}
	}
}
