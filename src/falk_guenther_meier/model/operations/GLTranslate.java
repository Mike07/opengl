package falk_guenther_meier.model.operations;

import javax.media.opengl.GL2;


public class GLTranslate implements GLOperation {
	private float x;
	private float y;
	private float z;
	
	public GLTranslate(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	@Override
	public void toGL(GL2 gl) {
		gl.glTranslatef(x, y, z);
	}
}
