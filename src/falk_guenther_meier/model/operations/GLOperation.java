package falk_guenther_meier.model.operations;

import javax.media.opengl.GL2;

public interface GLOperation {	
	public void toGL(GL2 gl);
}
