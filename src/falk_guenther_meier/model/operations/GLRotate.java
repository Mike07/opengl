package falk_guenther_meier.model.operations;

import javax.media.opengl.GL2;


public class GLRotate implements GLOperation {	
	private float x;
	private float y;
	private float z;
	
	public GLRotate(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	@Override
	public void toGL(GL2 gl) {
		if (x != 0)
			gl.glRotatef(x, 1, 0, 0);
		if (y != 0)
			gl.glRotatef(y, 0, 1, 0);
		if (z != 0)
			gl.glRotatef(z, 0, 0, 1);
	}
}
