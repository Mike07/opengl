package falk_guenther_meier.model.math;

/**
 * Gerade f�r mathematische Berechnungen.
 * @author Johannes
 */
public class GLStraightLine {
	private GLVertex point;
	private GLVertex direction;

	/**
	 * 
	 * @param point St�tzvektor
	 * @param direction Richtungsvektor
	 */
	public GLStraightLine(GLVertex point, GLVertex direction) {
		super();
		this.point = point;
		this.direction = direction;
	}

	/**
	 * Liefert den St�tzvektor der Geraden.
	 * @return
	 */
	public GLVertex getPoint() {
		return point;
	}

	/**
	 * Liefert den Richtungsvektor der Geraden.
	 * @return
	 */
	public GLVertex getDirection() {
		return direction;
	}

	/**
	 * Berechnet einen Punkt auf der Geraden.
	 * @param value Faktor des Richtungsvektors
	 * @return
	 */
	public GLVertex getPointOnLine(float value) {
		return GLVertex.add(point, GLVertex.mult(direction, value));
	}

	/**
	 * Berechnet den Schnittpunkt zweier Geraden
	 * @param a
	 * @param b
	 * @return Schnittpunkt (falls keiner vorhanden ist: null)
	 */
	public static GLVertex getIntersection(GLStraightLine a, GLStraightLine b) {
		// (a.point) + f * (a.direction) = (b.point) + g * (b.direction)
		float f = (b.point.getY() - a.point.getY() + b.direction.getY() / b.direction.getX() *
				(a.point.getX() - b.point.getX())) /
				(a.direction.getY() - a.direction.getX() * b.direction.getY() / b.direction.getX());
		float g = (a.point.getX() + f * a.direction.getX() - b.point.getX()) / b.direction.getX();
		if (Float.isNaN(f) || Float.isNaN(g)) { // TODO: Ursachen herausfinden!
			throw new IllegalArgumentException();
		}
		if (Math.abs(a.point.getZ() + f * a.direction.getZ()
				- b.point.getZ() - g * b.direction.getZ()) < 0.0001) { // TODO: Genauigkeit einstellen !!
			return a.getPointOnLine(f);
		} else {
			return null;
		}
	}
}
