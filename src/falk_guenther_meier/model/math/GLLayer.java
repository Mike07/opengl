package falk_guenther_meier.model.math;

import falk_guenther_meier.model.objects.GLRectangle;

/**
 * Ebene f�r mathematische Berechnungen.
 * @author Johannes
 */
public class GLLayer {
	// TODO: Kann jemand programmieren, wenn er will.

	/**
	 * Berechnet den Abstand zwischen einer Ebene und einem Punkt.
	 * @param layer Ebene, die durch ein Rechteck definiert wird
	 * @param point Ortsvektor
	 * @return
	 */
	public static float getDistanceRectanglePoint(GLRectangle layer, GLVertex point) {
		GLVertex normal = layer.getNormalOfPlane();
		return Math.abs(GLVertex.scalarProduct(normal, point) -
				GLVertex.scalarProduct(normal, layer.getAbsoluteCenter()));
	}
}
