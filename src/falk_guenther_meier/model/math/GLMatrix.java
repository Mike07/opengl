package falk_guenther_meier.model.math;


/*
 * Quellen (f�r Matrizen):
 * - http://prof.beuth-hochschule.de/fileadmin/user/linnemann/PDF-Dateien/Robotertechnik/Roboter_Technik_Vorlesung_Teil_03.pdf
 * - http://olli.informatik.uni-oldenburg.de/Grafiti3/grafiti/flow7/page1.html
 */
public final class GLMatrix {
	private float[][] matrix;

	public GLMatrix() {
		matrix = new float[4][4];
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				matrix[i][j] = 0.0f;
			}
		}
	}

	public GLMatrix(GLMatrix copyMatrix) {
		matrix = new float[4][4];
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				matrix[i][j] = copyMatrix.matrix[i][j];
			}
		}
	}
	
	public GLMatrix(float[] values) {
		matrix = new float[4][4];
		this.set(values);
	}

	@Override
	public GLMatrix clone() {
		return new GLMatrix(this);
	}

	public void set(int x, int y, float value) {
		matrix[x][y] = value;
	}
	
	/**
	 * spaltenweises Setzen der Matrix: xx, xy, xz, xw, yx, yy...
	 */
	public void set(float[] values) {
		int i = 0;
		for (int x = 0; x < 4; x++)
			for (int y = 0; y < 4; y++) {
				matrix[x][y] = values[i];
				i++;
			}
	}
	
	/**
	 * spaltenweises Setzen der Matrix: xx, xy, xz, xw, yx, yy...
	 * Richtige Darstellung der Matrix:
	 *  / xx yx zx wx \
	 * |  xy yy zy wy  |
	 * |  xz yz zz wz  |
	 *  \ xw yw zw ww /
	 */
	public void set(float xx, float xy, float xz, float xw,   // 1. Spalte
					float yx, float yy, float yz, float yw,   // 2. Spalte
					float zx, float zy, float zz, float zw,   // 3. Spalte
					float wx, float wy, float wz, float ww) { // 4. Spalte
		set(new float[]{xx, xy, xz, xw,
						yx, yy, yz, yw,
						zx, zy, zz, zw,
						wx, wy, wz, ww});
		

	}

	public void setValues(GLMatrix copyMatrix) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				matrix[i][j] = copyMatrix.get(i, j);
			}
		}
	}

	public float get(int x, int y) {
		return matrix[x][y];
	}
	
	/**
	 * @return spaltenweise Ausgabe der Matrix: xx, xy, xz, xw, yx, yy...
	 */
	public float[] get() {
		float [] values = new float[16];
		int i = 0;
		for (int x = 0; x < 4; x++)
			for (int y = 0; y < 4; y++) {
				values[i] = matrix[x][y];
				i++;
			}
		return values;
	}

	/**
	 * Wendet das Kreuzprdukt einer anderen Matrix auf diese Matrix an: M' = left x M
	 */
	public void mult(GLMatrix left) {
		this.matrix = GLMatrix.mult(left, this).matrix;
	}
	
	/**
	 * Wendet eine Multiplikation mit einem Skalar scalar auf diese Matrix an
	 */
	public void mult(float scalar) {
		this.matrix = GLMatrix.mult(this, scalar).matrix;	
	}
	
	public void add(GLMatrix m) {
		this.matrix = GLMatrix.add(this, m).matrix;	
	}
	
	public void sub(GLMatrix m) {
		this.matrix = GLMatrix.sub(this, m).matrix;
	}
	
	public void translate(float x, float y, float z) {
		GLMatrix translateMatrix = GLMatrix.createTranslateMatrix(x, y, z);
		this.mult(translateMatrix); // M' = translateMatrix x M
	}
	
	public void rotate(float x, float y, float z) {
		GLMatrix rotateMatrix = GLMatrix.createRotateMatrix(x, y, z);
		this.mult(rotateMatrix); // M' = rotateMatrix x M
	}
	
	
	//------------------------------ static methods --------------------------------
	

	public static GLMatrix createScaleMatrix(float x, float y, float z) {
		GLMatrix result = new GLMatrix();
		result.set(0, 0, x);
		result.set(1, 1, y);
		result.set(2, 2, z);
		result.set(3, 3, 1);
		return result;
	}

	public static GLMatrix createRotateXMatrix(float x) {
		GLMatrix result = new GLMatrix();
		float g = (float) (x * Math.PI / 180.0f);
		float cos = (float) (Math.cos(g));
		float sin = (float) (Math.sin(g));
		result.set(0, 0, 1);
		result.set(1, 1, cos);
		result.set(2, 2, cos);
		result.set(2, 1, -sin);
		result.set(1, 2, sin);
		result.set(3, 3, 1);		
		return result;		
	}
	public static GLMatrix createRotateYMatrix(float y) {
		GLMatrix result = new GLMatrix();
		float g = (float) (y * Math.PI / 180.0f);
		float cos = (float) (Math.cos(g));
		float sin = (float) (Math.sin(g));
		result.set(0, 0, cos);
		result.set(2, 0, sin);
		result.set(1, 1, 1);
		result.set(0, 2, -sin);
		result.set(2, 2, cos);
		result.set(3, 3, 1);		
		return result;		
	}
	public static GLMatrix createRotateZMatrix(float z) {
		GLMatrix result = new GLMatrix();
		float g = (float) (z * Math.PI / 180.0f);
		float cos = (float) (Math.cos(g));
		float sin = (float) (Math.sin(g));
		result.set(0, 0, cos);
		result.set(1, 0, -sin);
		result.set(0, 1, sin);
		result.set(1, 1, cos);
		result.set(2, 2, 1);
		result.set(3, 3, 1);		
		return result;		
	}

	public static GLMatrix createRotateMatrix(float x, float y, float z) {
		GLMatrix result = GLMatrix.mult(GLMatrix.createRotateXMatrix(x), GLMatrix.createRotateYMatrix(y));
		result = GLMatrix.mult(result, GLMatrix.createRotateZMatrix(z));
		return result;
	}
	
	/**
	 * Drehung, um eine Ursprungsgerade, die durch einen Vektor beschrieben wird.
	 * @param vector Richtungsvektor
	 * @param alpha Winkel, um den gedreht wird
	 * @return
	 */
	public static GLMatrix createRotateVMatrix(GLVertex vector, float alpha) {
		GLMatrix result = new GLMatrix();
		vector.normalize(); // TODO: Hanno! das hat Seiteneffekte!!
		float g = (float) (alpha * Math.PI / 180.0f);
		float cos = (float) (Math.cos(g));
		float sin = (float) (Math.sin(g));
		result.set(0, 0, vector.getX() * vector.getX() * (1 - cos) + cos);
		result.set(1, 0, vector.getX() * vector.getY() * (1 - cos) - vector.getZ() * sin);
		result.set(2, 0, vector.getX() * vector.getZ() * (1 - cos) + vector.getY() * sin);
		result.set(0, 1, vector.getX() * vector.getY() * (1 - cos) + vector.getZ() * sin);
		result.set(1, 1, vector.getY() * vector.getY() * (1 - cos) + cos);
		result.set(2, 1, vector.getY() * vector.getZ() * (1 - cos) - vector.getX() * sin);
		result.set(0, 2, vector.getX() * vector.getZ() * (1 - cos) - vector.getY() * sin);
		result.set(1, 2, vector.getY() * vector.getZ() * (1 - cos) + vector.getX() * sin);
		result.set(2, 2, vector.getZ() * vector.getZ() * (1 - cos) + cos);
		result.set(3, 3, 1);
		return result;
	}

	/**
	 * Anwendung: ViewModelMatrix' = TranslationsMatrix x ViewModelMatrix
	 * @return die TranslationsMatrix um einem Vektor (x, y, z)
	 */
	public static GLMatrix createTranslateMatrix(float x, float y, float z) {
		GLMatrix result = new GLMatrix();
		result.set(3, 0, x);
		result.set(3, 1, y);
		result.set(3, 2, z);
		result.set(0, 0, 1);
		result.set(1, 1, 1);
		result.set(2, 2, 1);
		result.set(3, 3, 1);
		return result;
	}
	
	/**
	 * Einheitsmatrix:
	 * 1 0 0 0
	 * 0 1 0 0
	 * 0 0 1 0
	 * 0 0 0 1
	 * 
	 * @return die Einheitsmatrix
	 */
	public static GLMatrix createIdentityMatrix() {
		GLMatrix result = new GLMatrix();
		result.set(0, 0, 1);
		result.set(1, 1, 1);
		result.set(2, 2, 1);
		result.set(3, 3, 1);
		return result;
	}
	
	/**
	 * @return die Multiplikation der Matrix m mit dem Skalar scalar
	 */
	public static GLMatrix mult(GLMatrix m, float scalar) {
		GLMatrix result = new GLMatrix();
		for (int x = 0; x < 4; x++) {
			for (int y = 0; y < 4; y++) {
				result.set(x, y, m.get(x, y) * scalar);
			}
		}
		return result;
	}

	/**
	 * @return das Kreuzprodukt left x right
	 */
	public static GLMatrix mult(GLMatrix left, GLMatrix right) {
		GLMatrix result = new GLMatrix();
		float value;
		for (int x = 0; x < 4; x++) {
			for (int y = 0; y < 4; y++) {
				value = 0.0f;
				for (int i = 0; i < 4; i++) {
					value += left.get(i, y) * right.get(x, i);
				}
				result.set(x, y, value);
			}
		}
		return result;
	}

	public static GLVertex mult(GLMatrix left, GLVertex right) {
		GLVertex result = right.clone();
		result.mult(left);
		return result;
	}
	
	/**
	 * @return Komponentenweise Addition der Matrizen m1 und m2
	 */
	public static GLMatrix add(GLMatrix m1, GLMatrix m2) {
		GLMatrix result = new GLMatrix();
		for (int x = 0; x < 4; x++) {
			for (int y = 0; y < 4; y++) {
				result.set(x, y, m1.get(x, y) + m2.get(x, y));
			}
		}
		return result;		
	}
	
	/**
	 * @return Komponentenweise Subtraktion der Matrizen m1 und m2
	 */
	public static GLMatrix sub(GLMatrix m1, GLMatrix m2) {
		GLMatrix result = new GLMatrix();
		for (int x = 0; x < 4; x++) {
			for (int y = 0; y < 4; y++) {
				result.set(x, y, m1.get(x, y) - m2.get(x, y));
			}
		}
		return result;		
	}

	@Override
	public String toString() {
		String str = "GLMatrix \n";
		for (int y = 0; y < 4; y++) {
			for (int x = 0; x < 4; x++) {
				str += matrix[x][y];
				if (x != 4) str += " ";
			}
			str += "\n";
		}
		return str;	
	}	
}
