package falk_guenther_meier.model.math;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.textures.GLTexCoord;

// TODO: sp�ter NaN-Tests entfernen (dienen bislang zum Testen und Fehler finden)!

public class GLVertex {
	/**
	 * Die 4 Vektor-Werte: x, y, z, w
	 */
	private float[] values;
	
	/**
	 * Die Normale des Vektors
	 */
	private GLVertex normal;
	private List<GLTexCoord> texCoords;

	public GLVertex() {
		texCoords = new ArrayList<GLTexCoord>();
		values = new float[4];
		values[0] = 0;
		values[1] = 0;
		values[2] = 0;
		values[3] = 1;
	}
	
	public GLVertex(float x, float y, float z, float w) {
		texCoords = new ArrayList<GLTexCoord>();
		values = new float[4];
		values[0] = x;
		values[1] = y;
		values[2] = z;
		values[3] = w;
	}

	public GLVertex(float x, float y, float z) {
		texCoords = new ArrayList<GLTexCoord>();
		values = new float[4];
		values[0] = x;
		values[1] = y;
		values[2] = z;
		values[3] = 1.0f;
	}

	public GLVertex(float[] initialValues) {
		texCoords = new ArrayList<GLTexCoord>();
		values = new float[4];
		this.setValues(initialValues);
	}

	public GLVertex(GLVertex copyVertex) {
		texCoords = new ArrayList<GLTexCoord>();
		values = new float[4];
		if (copyVertex.getNormal() != null)
			normal = copyVertex.getNormal().clone(); // deep clone
		for (GLTexCoord texCoord: copyVertex.getTexCoords()) {
			texCoords.add(texCoord.clone());
		}
		this.setValues(copyVertex.values);
	}
	
	public GLVertex(Color color) {
		texCoords = new ArrayList<GLTexCoord>();
		values = new float[4];
		values[0] = color.getRed() / 255f;
		values[1] = color.getBlue() / 255f;
		values[2] = color.getGreen() / 255f;
		values[3] = 1.0f;
	}

	public void checkNaN() {
		if (Double.isNaN(getX()) || Double.isNaN(getY()) || Double.isNaN(getZ())) {
			System.out.println(this);
			throw new IllegalArgumentException("NaN");
		}
	}

	public float[] getValues() {
		return values;
	}

	public float getX() {
		return values[0];
	}

	public float getY() {
		return values[1];
	}

	public float getZ() {
		return values[2];
	}

	public float getW() {
		return values[3];
	}

	public void setValues(GLVertex copyValues) {
		values[0] = copyValues.getX();
		values[1] = copyValues.getY();
		values[2] = copyValues.getZ();
		values[3] = copyValues.getW();
	}

	public void setValues(float[] newValues) {
		values[0] = newValues[0];
		values[1] = newValues[1];
		values[2] = newValues[2];
		values[3] = newValues[3];
	}
	
	public void setValues(Color color) {
		color.getRGBComponents(values);
	}

	public void setX(float newX) {
		values[0] = newX;
	}

	public void setY(float newY) {
		values[1] = newY;
	}

	public void setZ(float newZ) {
		values[2] = newZ;
	}

	public void setW(float newW) {
		values[3] = newW;
	}

	@Override
	public GLVertex clone() {
		return new GLVertex(this);
	}

	@Override
	public String toString() {
		return "[" + values[0] + "," + values[1] + "," + values[2] + "," + values[3] + "]";
	}

	/**
	 * Liefert die L�nge des Vektors
	 * @return L�nge des Vektors
	 */
	public float getNorm() {
		return (float) Math.sqrt(values[0] * values[0] + values[1] * values[1] + values[2] * values[2]);
	}

	/**
	 * Skaliert den Vektor so, dass er die angegebene L�nge hat.
	 * Die Richtigung des Vektors wird dabei nicht ver�ndert.
	 * @param length
	 */
	public void setNorm(float length) {
		if (length == 0.0f) {
			throw new IllegalArgumentException();
		}
		if (new Double(length / this.getNorm()).isNaN()) {
			throw new IllegalArgumentException("NaN");
		}
		this.mult(length / this.getNorm());		
	}

	public void addValues(float x, float y, float z) {
		values[0] += x;
		values[1] += y;
		values[2] += z;
	}

	public void add(GLVertex v) {
		values[0] += v.getX();
		values[1] += v.getY();
		values[2] += v.getZ();
	}

	public void sub(GLVertex v) {
		values[0] -= v.getX();
		values[1] -= v.getY();
		values[2] -= v.getZ();
	}

	/**
	 * Dreht die Richtung des Vektors genau herum, indem die Werte aller Komponeten negiert werden.
	 */
	public void invert() {
		values[0] = -values[0];
		values[1] = -values[1];
		values[2] = -values[2];
	}

	public void mult(GLMatrix left) {
		GLVertex right = this;
		float one = left.get(0, 0) * right.getX() + left.get(1, 0) * right.getY() + left.get(2, 0) * right.getZ() + left.get(3, 0) * right.getW();
		float two = left.get(0, 1) * right.getX() + left.get(1, 1) * right.getY() + left.get(2, 1) * right.getZ() + left.get(3, 1) * right.getW();
		float three = left.get(0, 2) * right.getX() + left.get(1, 2) * right.getY() + left.get(2, 2) * right.getZ() + left.get(3, 2) * right.getW();
		float four = left.get(0, 3) * right.getX() + left.get(1, 3) * right.getY() + left.get(2, 3) * right.getZ() + left.get(3, 3) * right.getW();
		right.setX(one);
		right.setY(two);
		right.setZ(three);
		right.setW(four);
		if (normal != null) {
			float normalNorm = normal.getNorm();
			if (normalNorm > 0) {
				normal.mult(left);
				normal.normalize();
			}
		}
	}

	/**
	 * Vektor wird zum Kreuzprodukt von sich und dem �bergebenen Vertex.
	 * @param right
	 */
	public void mult(GLVertex right) {
		this.setValues(GLVertex.mult(this, right));
	}
	
	public void mult(float value) {
		values[0] *= value;
		values[1] *= value;
		values[2] *= value;		
	}

	public float scalarProduct(GLVertex v) {
		return GLVertex.scalarProduct(this, v);
	}

	/**
	 * Ver�ndert den Vektor so, dass er die L�nge 1 bei gleicher Richtung hat.
	 */
	public void normalize() {
		this.setNorm(1.0f);
	}
	
	public GLVertex getNormal() {
		return normal;
	}
	
	public void setNormal(GLVertex normal) {
		this.normal = normal;
	}
	
	public List<GLTexCoord> getTexCoords() {
		return texCoords;
	}
	
	public void setTexCoord(GLTexCoord texCoord) {
		for (GLTexCoord tempTexCoord : texCoords.toArray(new GLTexCoord[0])) {
			if (tempTexCoord.getTextureUnit() == texCoord.getTextureUnit()) {
				texCoords.remove(tempTexCoord);
			}
		}
		texCoords.add(texCoord);
	}
	
	public void toGL(GL2 gl) {
		for (GLTexCoord texCoord: texCoords) {
			texCoord.toGL(gl);
		}
		if (normal != null) {
			gl.glNormal3f(normal.getX(), normal.getY(), normal.getZ());
		}
		gl.glVertex3fv(this.getValues(), 0);
	}
	
	public void toGL(GL2 gl, GLVertex position) {
		GLVertex.add(this, position).toGL(gl);
	}
	
	//------------------------------ static methods ------------------------------
	
	public static GLVertex addValues(GLVertex v, float x, float y, float z) {
		GLVertex result = v.clone();
		result.addValues(x, y, z);
		return result;
	}

	public static GLVertex add(GLVertex a, GLVertex b) {
		GLVertex result = a.clone();
		result.add(b);
		return result;
	}

	public static GLVertex sub(GLVertex left, GLVertex right) {
		GLVertex result = left.clone();
		result.sub(right);
		return result;
	}

	public static GLVertex invert(GLVertex v) {
		GLVertex result = v.clone();
		result.invert();
		return result;
	}

	public static GLVertex mult(GLMatrix left, GLVertex right) {
		return GLMatrix.mult(left, right);
	}
	
	/**
	 * Kreuzprodukt zweier Vektoren.
	 * @param left
	 * @param right
	 * @return
	 */
	public static GLVertex mult(GLVertex left, GLVertex right) {
		GLVertex result = new GLVertex();
		result.setX(left.getY() * right.getZ() - left.getZ() * right.getY());
		result.setY(left.getZ() * right.getX() - left.getX() * right.getZ());
		result.setZ(left.getX() * right.getY() - left.getY() * right.getX());
		return result;
	}

	public static GLVertex mult(GLVertex v, float value) {
		GLVertex result = v.clone();
		result.mult(value);
		return result;
	}

	public static float scalarProduct(GLVertex first, GLVertex second) {
		return first.getX() * second.getX() + first.getY() * second.getY() + first.getZ() * second.getZ();
	}

	public static GLVertex normalize(GLVertex v) {
		GLVertex result = v.clone();
		result.normalize();
		return result;
	}

	/**
	 * Berechnet die L�nge eines Vektors zwischen den �bergebenen Ortsvektoren.
	 * @param a
	 * @param b
	 * @return
	 */
	public static float getDistance(GLVertex a, GLVertex b) {
		return GLVertex.sub(a, b).getNorm();
	}

	/**
	 * Berechnet den Winkel zwischen den beiden Vektoren
	 * @return Winkel im Gradma� (0 <= Winkel <= 180)
	 */
	public static float getAngleBetween(GLVertex a, GLVertex b) {
		if (a.getNorm() == 0.0f) {
			throw new IllegalArgumentException("a hat L�nge 0");
		}
		if (b.getNorm() == 0.0f) {
			throw new IllegalArgumentException("b hat L�nge 0");
		}
		float cos = GLVertex.scalarProduct(a, b) / (a.getNorm() * b.getNorm());
		cos = Math.min(cos, 1.0f);
		cos = Math.max(cos, -1.0f);
		if (new Double(cos).isNaN()) {
			throw new IllegalArgumentException("NaN");
		}
		if (new Double(Math.toDegrees(Math.acos(cos))).isNaN()) {
			throw new IllegalArgumentException("NaN");
		}
		return (float) Math.toDegrees(Math.acos(cos));
	}

	/**
	 * Ordnet die Ortsvektoren so an, dass in der zur�ckgegebenen Liste benachbarte Vektoren
	 * k�rzeste Wege zwischen ihnen beschreiben.
	 * Vorbedingung: Keine doppelten Vertexes !! Mind. 2 Elemente in der Liste!
	 * @param list
	 * @return
	 */
	public static List<GLVertex> sortVertexesForShortestWays(List<GLVertex> list) {
		LinkedList<GLVertex> copy = new LinkedList<GLVertex>(list);
		LinkedList<GLVertex> result = new LinkedList<GLVertex>();
		// zwei Elemente initial einf�gen
		result.add(copy.removeFirst());
		GLVertex helpRight = GLVertex.lookForNearestVertex(result.getFirst(), copy);
		copy.remove(helpRight);
		result.addLast(helpRight);
		// restliche Elemente einf�gen
		GLVertex helpLeft = null;
		while (!copy.isEmpty()) {
			helpRight = GLVertex.lookForNearestVertex(result.getLast(), copy);
			if (GLVertex.getDistance(helpRight, result.getLast()) <= GLVertex.getDistance(helpRight, result.getFirst())) {
				copy.remove(helpRight);
				result.addLast(helpRight);
			} else {
				helpLeft = GLVertex.lookForNearestVertex(result.getFirst(), copy);
				if (helpLeft.equals(helpRight)) {
					copy.remove(helpRight);
					result.addLast(helpRight);					
				} else {
					copy.remove(helpLeft);
					result.addFirst(helpLeft);
				}
			}
		}
		return result;
	}

	/**
	 * Sucht aus der Liste den Vektor, der den k�rzesten Weg zum �bergebenen Vektor beschreibt.
	 * Vorbedingung: v ist in list nicht enthalten!
	 * @param v
	 * @param list
	 * @return
	 */
	private static GLVertex lookForNearestVertex(GLVertex v, List<GLVertex> list) {
		GLVertex result = list.get(0);
		float dist = GLVertex.getDistance(result, v);
		for (GLVertex akt : list) {
			if (GLVertex.getDistance(akt, v) < dist) {
				result = akt;
				dist = GLVertex.getDistance(result, v);
			}
		}
		return result;
	}

	/**
	 * Legt eine neue Liste an, die keine doppelten Vertexes enth�lt.
	 * @param list
	 * @return
	 */
	public static List<GLVertex> removeDoubledVertexes(List<GLVertex> list) {
		List<GLVertex> result = new ArrayList<GLVertex>(list.size());
		for (GLVertex v : list) {
			if (!result.contains(v)) {
				result.add(v);
			}
		}
		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((normal == null) ? 0 : normal.hashCode());
		result = prime * result
				+ ((texCoords == null) ? 0 : texCoords.hashCode());
		result = prime * result + Arrays.hashCode(values);
		return result;
	}
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (!(o instanceof GLVertex)) {
			return false;
		}
		GLVertex n = (GLVertex) o;
		return n.values[0] == this.values[0] && n.values[1] == this.values[1] && n.values[2] == this.values[2]
				&& n.values[3] == this.values[3];
	}
	
}
