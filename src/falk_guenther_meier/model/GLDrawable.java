package falk_guenther_meier.model;

import javax.media.opengl.GL2;

public interface GLDrawable extends GLable {
	public void draw(GL2 gl);
}
