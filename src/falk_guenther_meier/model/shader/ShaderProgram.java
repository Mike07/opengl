package falk_guenther_meier.model.shader;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;

import javax.media.opengl.GL2;

import com.jogamp.opengl.util.glsl.ShaderUtil;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.exceptions.NotCompiledException;
import falk_guenther_meier.util.exceptions.UniformVariableNotExistsException;

public class ShaderProgram {	
	private int shaderprogram = 0;
	private boolean compiled = false;
	private String vertexShader;
	private String fragmentShader;
	private int locTangente;

	public final static String TANGENTE = "tangente";

	public static void clearShaderProgram(GL2 gl) {
		gl.glUseProgram(0);
	}
	
	public ShaderProgram(String vertexShader, String fragmentShader) {
		this.fragmentShader = fragmentShader;
		this.vertexShader = vertexShader;
	}

	/**
	 * Shader kompilieren und linken
	 * @param root Ursprung, von dem readSFile ausgef�hrt wird
	 * @param gl
	 * @return
	 */
	public void compileShaders(Class<?> root, GL2 gl) {
		if (!compiled) {
			int v = gl.glCreateShader(GL2.GL_VERTEX_SHADER);
			int f = gl.glCreateShader(GL2.GL_FRAGMENT_SHADER);
			int[]success = new int[1];
			String vsrc = readShader(root, vertexShader);
			gl.glShaderSource(v, 1, new String[] { vsrc }, (int[]) null, 0);
			gl.glCompileShader(v);
			
			gl.glGetShaderiv(v, GL2.GL_COMPILE_STATUS, success, 0);
			if (success[0]==GL2.GL_FALSE) {System.out.println("Fehler im VS");}
			
			String fsrc = readShader(root, fragmentShader);
			gl.glShaderSource(f, 1, new String[] { fsrc }, (int[]) null, 0);
			gl.glCompileShader(f);
			gl.glGetShaderiv(v, GL2.GL_COMPILE_STATUS, success, 0);
			if(success[0]==GL2.GL_FALSE) {System.out.println("Fehler im FS");}
			shaderprogram = gl.glCreateProgram();
			gl.glAttachShader(shaderprogram, v);
			gl.glAttachShader(shaderprogram, f);
			gl.glLinkProgram(shaderprogram);
			gl.glGetProgramiv(shaderprogram, GL2.GL_LINK_STATUS, success, 0);
			if(success[0]==GL2.GL_FALSE) {System.out.println("Linkage Error!");}
			else{System.out.println("shader: "+shaderprogram+" - Linkage was successful.");}
			gl.glValidateProgram(shaderprogram);
			
			System.out.println(ShaderUtil.getShaderInfoLog(gl,shaderprogram));
			this.compiled = true;

			this.locTangente = gl.glGetAttribLocation(this.getShaderprogram(), ShaderProgram.TANGENTE);
		}
	}
	
	/**
	 * Shader kompilieren, linken und verwenden
	 * @param root Ursprung, von dem readSFile ausgef�hrt wird (normalerweise die JoglApp): davon die Class-Datei!
	 * @param gl
	 * @return
	 */
	public void compileShadersAndUseProgram(Class<?> root, GL2 gl) {
		this.compileShaders(root, gl);
		this.useProgram(gl);
	}

	public void useProgram(GL2 gl) {
		try {
			if (!compiled) throw new NotCompiledException();
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		gl.glUseProgram(shaderprogram);
	}
	
	/**
	 * Shader-Quellcode aus Datei lesen
	 * @param root Ursprung, von dem readSFile ausgef�hrt wird (normalerweise die JoglApp): davon die Class-Datei!
	 * @param src
	 * @return
	 */
	private String readShader(Class<?> root, String src) {
		try {
			InputStream is = root.getResourceAsStream(src);
//			InputStream is = ShaderProgram.class.getResourceAsStream(src);
			if (is == null) {
				System.err.println("Could not read "+src);
				System.exit(1);
			}
			@SuppressWarnings("resource")
			Scanner s = new Scanner(is).useDelimiter("\\A");
		    return s.hasNext() ? s.next() : "";
		} catch(Exception e) {
			System.err.println("Es ist ein Fehler beim Laden der Shaderprogramme aufgetreten!");
		}
		return null;
	}

	public int getShaderprogram() {
		return shaderprogram;
	}

	public boolean isCompiled() {
		return compiled;
	}

	public void setCompiled(boolean compiled) {
		this.compiled = compiled;
	}

	public String getVertexShader() {
		return vertexShader;
	}

	public String getFragmentShader() {
		return fragmentShader;
	}
	
	public void setTextureUnit(GL2 gl, String texUnitVar, int unit) {
		try {
			if (!compiled) throw new NotCompiledException();
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		int uniformLoc = gl.glGetUniformLocationARB(shaderprogram, texUnitVar);
		if (uniformLoc != -1) {
			gl.glUniform1iARB(uniformLoc, unit); // unit hier setzen
		} else {
			try {
				throw new UniformVariableNotExistsException(texUnitVar);
			} catch (UniformVariableNotExistsException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setUniformVec4(GL2 gl, String name, float[] values) {
		try {
			if (!compiled) throw new NotCompiledException();
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		int uniformLoc = gl.glGetUniformLocation(shaderprogram, name);
		if (uniformLoc != -1) {
			gl.glUniform4fv(uniformLoc, 4, values, 0);
		} else {
			try {
				throw new UniformVariableNotExistsException(name);
			} catch (UniformVariableNotExistsException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setUniformFloat(GL2 gl, String name, float value) {
		try {
			if (!compiled) throw new NotCompiledException();
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		int uniformLoc = gl.glGetUniformLocation(shaderprogram, name);
		if (uniformLoc != -1) {
			gl.glUniform1f(uniformLoc, value);
		} else {
			try {
				throw new UniformVariableNotExistsException(name);
			} catch (UniformVariableNotExistsException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setUniformMatrix(GL2 gl, String name, float[] values) {
		try {
			if (!compiled) throw new NotCompiledException();
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		int uniformLoc = gl.glGetUniformLocation(shaderprogram, name);
		if (uniformLoc != -1) {
			gl.glUniformMatrix4fv(uniformLoc, 16, false, values, 0);
		} else {
			try {
				throw new UniformVariableNotExistsException(name);
			} catch (UniformVariableNotExistsException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * �bergibt dem Shader die aktuelle Tangente, die intern noch normalisiert wird
	 * und die der Fixed Pipeline �bergeben wird.
	 * @param gl
	 * @param tangente
	 */
	public void setTangente(GL2 gl, GLVertex tangente) {
		tangente.normalize();
		gl.glVertexAttrib3f(this.locTangente, tangente.getX(), tangente.getY(), tangente.getZ());
	}
}
