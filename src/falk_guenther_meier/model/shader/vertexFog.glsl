// Aufgabe 31: Nebel
varying vec3 lightDirection;
varying vec3 normal;
varying float dist;

void main()
{
normal = gl_NormalMatrix * gl_Normal;

vec4 position4 = gl_ModelViewMatrix * gl_Vertex;

lightDirection = normalize(gl_LightSource[7].position.xyz - position4);

gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

dist = length(gl_Position);
gl_FrontColor = gl_Color;
}