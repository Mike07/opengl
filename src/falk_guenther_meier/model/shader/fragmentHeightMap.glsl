// Aufgabe 30
uniform sampler2D texHeightMap;
uniform sampler2D texMountain;
uniform sampler2D texLand;
uniform sampler2D texWater;

vec3 getNormal()
{
	vec2 texCoord = gl_TexCoord[0].st;
	float offset = 0.005;
    
    float texelLeft = texture2D(texHeightMap, texCoord + vec2(-offset, 0)).r;
    float texelRight = texture2D(texHeightMap, texCoord + vec2(offset, 0)).r;
    float texelUp = texture2D(texHeightMap, texCoord + vec2(0, -offset)).r;
    float texelBottom = texture2D(texHeightMap, texCoord + vec2(0, offset)).r;
    vec3 vRight = vec3(2.0*offset, texelRight - texelLeft, 0);
    vec3 vUp = vec3(0, texelUp - texelBottom, -2.0*offset);
    
    return normalize(cross(vRight,vUp));
}

float getHeight()
{
	return texture2D(texHeightMap, gl_TexCoord[0].st).r;
}

float getDiffuseIntensity() {
	vec3 normal = getNormal();
	vec3 lightPos = vec3(1.0, 1.0, 0.0);
	
	return max(0.3, dot(normal, normalize(lightPos))); // und min. 0.3 ambientes Licht
}

void main()
{
	vec2 texCoord = gl_TexCoord[0].st;

	// Texel-Farbe f�r die verschiedenen Texturen ermitteln
	vec4 texMountainColor = texture2D(texMountain, texCoord);
	vec4 texLandColor = texture2D(texLand, 2.0 * texCoord);
	vec4 beachColor = vec4(1.0, 0.90, 0.65, 1.0); 
	vec4 texWaterColor = texture2D(texWater, texCoord);
	texWaterColor = texWaterColor * vec4(0.2, 0.4, 1.0, 1.0) + 0.1; // Wasser blau einf�rben
	
	float texHeight = getHeight();
	
	// Texturen auf Ebenenen entsprechend der texHeight ausw�hlen und mischen
	vec4 texColor = mix(texWaterColor, beachColor, smoothstep(0.1,0.3, texHeight)); // �bergang Water -> Beach
	texColor = mix(texColor, texLandColor, smoothstep(0.3,0.35, texHeight));        // �bergang Beach -> Land
	texColor = mix(texColor, texMountainColor, smoothstep(0.5,0.8, texHeight));     // �bergang Land -> Mountain
	
	//gl_FragColor = texColor;
	gl_FragColor = vec4(getDiffuseIntensity() * texColor.rgb, 1.0); // mit einem direktionalen Licht
	//gl_FragColor = vec4(getNormal()*0.5 + 0.5, 1.0) + 0.01 * texColor; // zeigt die NormalMap
	//gl_FragColor = gl_Color;
}
