// Aufgabe 31: Nebel
varying vec3 lightDirection;
varying vec3 normal;
varying float dist;

void main()
{
float diffuseIntensity = max(0.0, dot(normalize(normal), normalize(lightDirection)));

gl_FragColor = gl_FrontMaterial.diffuse * diffuseIntensity * gl_LightSource[7].diffuse; // diffuse 

gl_FragColor += gl_FrontMaterial.ambient * gl_LightSource[7].ambient; // ambient

vec3 reflection = normalize(reflect( - normalize(lightDirection), normalize(normal)));
float specAngle = max(0.0, dot(normalize(normal), reflection));

float shininess = 128.0;
if (diffuseIntensity != 0.0) {
	float specularValue = pow(specAngle, shininess);
	gl_FragColor += gl_LightSource[7].specular * gl_FrontMaterial.specular * specularValue; // specular
}

float factor = 1.5 / (1.0 + dist * 0.08);
if (factor > 1.4) factor = 1.4;
gl_FragColor *= factor;
}