package falk_guenther_meier.model.world;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;

import com.jogamp.opengl.util.FPSAnimator;

import falk_guenther_meier.model.GLDrawable;
import falk_guenther_meier.model.animation.AbstractAnimation;
import falk_guenther_meier.model.animation.AnimationManagement;
import falk_guenther_meier.model.light.GLAbstractLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.observers.KeyboardObservable;
import falk_guenther_meier.model.observers.KeyboardObserver;
import falk_guenther_meier.model.platform.DirectionPositionable;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.util.Utility;

/**
 * Zentrales Welt-/Szene-Objekt basierend auf Justins Shadow-Mapping und der erweiterten JoglApp.
 * @author Johannes
 */
public abstract class JoglAppShadowMapping implements GLEventListener, KeyboardObservable, KeyboardObserver,
		GLDrawable {
	private Frame frame;
	private GLCanvas canvas;
	protected GLU glu = new GLU();

	private DirectionPositionable light;
	private int width = 640;
	private int height = 480;
	private String titleName;

	private int framebuffer = 0;
	private int depthtexture = 0;
	private int DEPTH_MAP_SIZE = 1024; // 256
	private float[] cameraProjectionMatrix = new float[16];
	private float[] cameraModelviewMatrix = new float[16];
	private float[] lightProjectionMatrix = new float[16];
	private float[] lightModelviewMatrix = new float[16];
	private float[] cameraInverse = new float[16];

    // Winkel und Blickweite f�r Kamera
    private float angle;
    private float distance;

    // ------------ neu (begin) ----------------
    private float frameRate;
	private volatile int frameCount;
    protected Konfiguration konfiguration;
    private KeyboardAdapter keyboard;
    protected AnimationManagement animations;
    protected List<GLAbstractLight> lights;

	private boolean isFirstFrame;
    private boolean pauseAnimation;
    private boolean startAnimation;

    private boolean drawShadow;
    // ------------ neu (end) ------------------

    public JoglAppShadowMapping(String titleName, int breite, int hoehe, boolean start, Konfiguration konfiguration) {
    	this.titleName = titleName;

    	GLProfile.initSingleton();

		GLProfile glp = GLProfile.get(GLProfile.GL2);
		GLCapabilities caps = new GLCapabilities(glp);
		canvas = new GLCanvas(caps);

		this.initConstructor();

		this.start();

    	this.angle = 60;
    	this.distance = 150; 	
    	this.width = breite;
    	this.height = hoehe;
    	
    	this.frame = new Frame(titleName);
		this.frame.add(canvas);
		this.frame.setSize(width, height);

		// -------------- neu (begin) --------------------
    	this.konfiguration = konfiguration;
    	this.keyboard = new KeyboardAdapter();

    	this.lights = new ArrayList<GLAbstractLight>();
    	this.addLight(this.konfiguration.getLight());


    	canvas.addKeyListener(this.keyboard);

    	this.addKeyboardObserver(this.konfiguration.getCamera());
    	this.addKeyboardObserver(this);
    	this.keyboard.printAllKeys();

    	this.animations = new AnimationManagement();
    	this.pauseAnimation = false;
    	this.startAnimation = false;
    	this.drawShadow = true;
    	this.isFirstFrame = true;
    	// -------------- neu (end) ----------------------

    	frame.setVisible(true);
		final FPSAnimator animator = new FPSAnimator(canvas, 60);

		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				// Run this on another thread than the AWT event queue to
				// make sure the call to Animator.stop() completes before
				// exiting
				new Thread(new Runnable() {
					public void run() {
						animator.stop();
						System.exit(0);
					}
				}).start();
			}
		});

		canvas.requestFocus();

		// Berechnet die FPS
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				frameRate = frameCount;
				frameCount = 0;
			}
		}, new Date(1), 1000);

		animator.start();    	

		this.start();
    }
    
    public JoglAppShadowMapping(String titleName, int breite, int hoehe, boolean start) {
    	this(titleName, breite, hoehe, start, new Konfiguration());
    }

    protected final void start() {
    	canvas.addGLEventListener(this);
    }

    /**
     * Enth�lt alle Initialisierungen aus dem inidividuellen Konstruktor (der muss leer sein!).
     */
    protected void initConstructor() {
    	// bleibt erstmal leer!
    }

    @Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		System.err.println("GLBasecode Init: " + drawable);
		System.err.println("Chosen GLCapabilities: " + drawable.getChosenGLCapabilities());
		System.err.println("INIT GL IS: " + gl.getClass().getName());
		System.err.println("GL_VENDOR: " + gl.glGetString(GL2.GL_VENDOR));
		System.err.println("GL_RENDERER: " + gl.glGetString(GL2.GL_RENDERER));
		System.err.println("GL_VERSION: " + gl.glGetString(GL2.GL_VERSION));

		gl.setSwapInterval(1);

		gl.glClearColor(0.25f, 0.25f, 0.3f, 1.0f);
		gl.glDisable(GL2.GL_CULL_FACE);
		gl.glShadeModel(GL2.GL_SMOOTH);
		gl.glClearDepth(1.0f);
		gl.glEnable(GL2.GL_DEPTH_TEST);
		gl.glDepthFunc(GL2.GL_LEQUAL);
		gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST);

		// Initialize shadow map
		this.createDepthTexture(gl);

		// use normal framebuffer (screen)
		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, 0);
    }

	@Override
	public final void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		GL2 gl = drawable.getGL().getGL2();
		GLU glu = new GLU();

		if (height == 0) {
			height = 1;
		}

//		float aspect = (float) width / (float) height;

		this.width = width;
		this.height = height;
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();

		glu.gluPerspective(angle, (float) this.width / (float) this.height, 1, distance); // TODO: passt das?!
//		glu.gluPerspective(angle, (float) window_x / window_y, 1, distance);
//		glu.gluPerspective(45.0f, aspect, 0.1f, 100.0f);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	/**
	 * Enth�lt den Code f�r die Camera, aus deren Sicht die Szene gerendert werden soll.
	 * TODO: sinnvoll ?!
	 * @param gl
	 */
	protected void useCamera(GL2 gl) {
		this.konfiguration.getCamera().refreshCamera();
	}

	private final void makeShadowMap(GL2 gl) {
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		gl.glLoadIdentity();

		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glLoadMatrixf(this.lightProjectionMatrix, 0);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
		gl.glLoadMatrixf(this.lightModelviewMatrix, 0);

		this.startDepthMapRendering(gl);
		gl.glUseProgram(0); // No shader necessary
		this.drawShadowRendering(gl);
		this.endDepthMapRendering(gl);

		this.calculateTextureMatrix(gl);
	}

	@Override
	public final void display(GLAutoDrawable drawable) {
		this.initFrame(); // initiale Anweisungen (wie z.B. Animationen durchf�hren)

		GL2 gl = drawable.getGL().getGL2();

		// calculate all Projection and View Matrices
		this.saveMatrices(gl, width, height, light);

		if (this.drawShadow) { // TODO: steuert nur das Aktualisieren der Schatten!!
			// render the shadow map
			this.makeShadowMap(gl);
		}

		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();

		// now the scene will be rendered to the screen
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glLoadMatrixf(this.cameraProjectionMatrix, 0);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadMatrixf(this.cameraModelviewMatrix, 0);

		// individual scene rendering
		this.draw(gl);
		ShaderProgram.clearShaderProgram(gl);
	}

	/**
	 * Enth�lt alle Anweisungen, die ausgef�hrt werden, wenn die Szene ganz normal gerendert werden soll.
	 * @param gl Render-Fl�che
	 */
	@Override
	public final void draw(GL2 gl) {
		gl.glPushMatrix();

		// zeichne die Szene
		this.beginGL(gl);
		this.endGL(gl);

		ShaderProgram.clearShaderProgram(gl);
    	// Skybox zeichnen
    	if (konfiguration.isShowSkybox()) {
    		konfiguration.getSkybox().draw(gl);
    	}
		// Zeichne ein Koordinatensystem
		if (this.konfiguration.getCoordinates()) {
			this.printKoordinates(gl, -10 , 10);
		}

		gl.glPopMatrix();
	}

	/**
	 * Enth�lt alle Anweisungen, die ausgef�hrt werden, wenn der Schatten f�r die Szene gerendert werden soll.
	 * @param gl Render-Fl�che
	 */
	protected void drawShadowRendering(GL2 gl) {
		// bleibt erstmal leer
	}

	@Override
	public void beginGL(GL2 gl) {
    	// Lichtquellen ber�cksichtigen
    	for (GLAbstractLight l : this.lights) {
    		l.beginGL(gl);
    	}
	}

	@Override
	public void endGL(GL2 gl) {
    	// Lichtquellen ber�cksichtigen
    	for (GLAbstractLight l : this.lights) {
    		l.endGL(gl);
    	}
    	// TODO: sollen die Lichter wieder geschlossen werden ??
	}

	/**
	 * Enth�lt die Anweisungen, die direkt zu Beginn eines Frames ausgef�hrt werden.
	 * Enth�lt in der Regel Gesch�ftslogik wie beispielsweise Animationen.
	 */
	protected void initFrame() {
    	this.konfiguration.getTime().updateTime();

    	// aktualisiere die Framerate
		this.frameCount++;
		this.frame.setTitle(this.titleName + " - " + Float.toString(this.frameRate) + " FPS");		

		this.keyboard.updateKeys();

		// Anweisungen f�r den ersten Frame
		if (isFirstFrame) {
			isFirstFrame = false;
			this.animations.startAnimation();
			this.startAnimation = true;
		}

		// an dieser Stelle sollten alle eigenen Animationen aktualisiert / durchgef�hrt werden (?)
    	this.animations.animate();
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		// bleibt erstmal leer
	}

	private final void createDepthTexture(GL2 gl) {
		// FramebufferObjekt erstellen
		int[] tmp = new int[1];
		gl.glGenFramebuffers(1, tmp, 0);
		framebuffer = tmp[0];

		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, framebuffer);

		// Tiefentexture
		gl.glGenTextures(1, tmp, 0);
		depthtexture = tmp[0];

		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glActiveTexture(GL.GL_TEXTURE2);
		gl.glBindTexture(GL.GL_TEXTURE_2D, depthtexture);
		gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL2.GL_DEPTH_COMPONENT32,
				DEPTH_MAP_SIZE, DEPTH_MAP_SIZE, 0, GL2.GL_DEPTH_COMPONENT,
				GL.GL_UNSIGNED_BYTE, null);

		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER,
				GL.GL_NEAREST);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER,
				GL.GL_NEAREST);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S,
				GL.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T,
				GL.GL_CLAMP_TO_EDGE);

		checkFrameBuffer(gl);
		gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL.GL_DEPTH_ATTACHMENT,
				GL.GL_TEXTURE_2D, depthtexture, 0);
		gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
		checkFrameBuffer(gl);
		gl.glDrawBuffer(0);
		gl.glReadBuffer(0);

	}

	// prepare render to texture using fbo
	private final void startDepthMapRendering(GL2 gl) {
		gl.glShadeModel(GL2.GL_FLAT);
		gl.glColorMask(false, false, false, false);
		gl.glEnable(GL.GL_POLYGON_OFFSET_FILL);
		gl.glPolygonOffset(1.0F, 4.0F);

		gl.glEnable(GL.GL_CULL_FACE);
		gl.glCullFace(GL.GL_FRONT);
		gl.glFrontFace(GL.GL_CCW);

		gl.glClearDepth(1.0f);
		gl.glUseProgram(0);
		gl.glViewport(0, 0, DEPTH_MAP_SIZE, DEPTH_MAP_SIZE);
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, framebuffer);
		gl.glClear(GL.GL_DEPTH_BUFFER_BIT);
		gl.glDrawBuffer(0);
		gl.glReadBuffer(0);

	}

	// end render to fbo, prepare normal render
	private final void endDepthMapRendering(GL2 gl) {
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0);
		gl.glViewport(0, 0, width, height);
		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glActiveTexture(GL.GL_TEXTURE2);
		gl.glBindTexture(GL.GL_TEXTURE_2D, depthtexture);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S,
				GL.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T,
				GL.GL_CLAMP_TO_EDGE);

		gl.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER,
				GL.GL_NEAREST);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER,
				GL.GL_NEAREST);

		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_FUNC,
				GL2.GL_LESS);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_MODE,
				GL2.GL_NONE);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_DEPTH_TEXTURE_MODE,
				GL2.GL_INTENSITY);

		gl.glCullFace(GL.GL_BACK);

		gl.glDisable(GL.GL_CULL_FACE);
		gl.glShadeModel(GL2.GL_SMOOTH);
		gl.glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
		gl.glClearDepth(1.0f);
		gl.glColorMask(true, true, true, true);

	}

	// save all the matrices that will be used for both camera and lights view
	private final void saveMatrices(GL2 gl, int width, int height, DirectionPositionable lightPos) {
		GLU glu = new GLU();

		// Setze die Einstellungen fuer die PROJECTION_MATRIX.
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluPerspective(45.0f, ((float) width / (float) height), 0.1f, 200.0f);

		// aktuelle Projection-Matrix speichern
		gl.glGetFloatv(GL2.GL_PROJECTION_MATRIX, this.cameraProjectionMatrix, 0);

		gl.glLoadIdentity();
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
		this.useCamera(gl);

		gl.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, this.cameraModelviewMatrix, 0);
		gl.glLoadIdentity();

		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		// TODO try Ortho here as well
		glu.gluPerspective(42.0f, 1.0f, 0.01f, 28.0f);

		gl.glGetFloatv(GL2.GL_PROJECTION_MATRIX, this.lightProjectionMatrix, 0);

		gl.glLoadIdentity();
		glu.gluPerspective(45.0f, ((float) width / (float) height), 0.1f, 200.0f);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
		GLVertex pos = lightPos.getPosition();
		GLVertex center = GLVertex.add(pos, lightPos.getDirection());
		glu.gluLookAt(pos.getX(), pos.getY(), pos.getZ(), center.getX(), center.getY(), center.getZ(),
				1.0f, 0.0f, 0.0f); // TODO: oder (0,1,0) ??
		gl.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, this.lightModelviewMatrix, 0);
		gl.glLoadIdentity();

	}

	private final void calculateTextureMatrix(GL2 gl) {
		// instead of using texture matrix you can also use a uniform!
		gl.glMatrixMode(GL.GL_TEXTURE);
		gl.glActiveTexture(GL.GL_TEXTURE5);

		float texMat[] = { 0.5F, 0F, 0F, 0.0F, 0.0F, 0.5F, 0F, 0.0F, 0.0F, 0F,
				0.5F, 0.0F, 0.5F, 0.5F, 0.5F, 1.0F };
		gl.glLoadMatrixf(texMat, 0);
		gl.glMultMatrixf(lightProjectionMatrix, 0);
		gl.glMultMatrixf(lightModelviewMatrix, 0);

		// So you ask, why do i have to invert the Cameras View Matrix?
		// This is only neccesary if you use the Modelviewmatrix for the
		// translation and rotaiton of objects: if you do so,
		// you cannot distinguish between Model and View! (look at the vertex
		// shader)
		// So in order to compare the correct coordinates
		// you have to separate the Model and the view matrix.
		// You can also do this in a different manner,
		// but doing it this way is straight forward if
		// you use glRotate and glTranslate.
		Utility.fastinvert(cameraModelviewMatrix, cameraInverse);
		gl.glMultMatrixf(cameraInverse, 0);

		gl.glMatrixMode(GL2.GL_MODELVIEW_MATRIX);
		// Ende Texture-Matrix berechnen
	}

	private final void checkFrameBuffer(GL gl) {
		int status = gl.glCheckFramebufferStatus(GL.GL_FRAMEBUFFER);
		System.out.println(status);
		switch (status) {
		case GL.GL_FRAMEBUFFER_COMPLETE:
			System.out.println("FRAMEBUFFER OK");
			break;
		case GL.GL_FRAMEBUFFER_UNSUPPORTED:
			System.out.println("<!> Unsupported framebuffer format\n");
			break;
		case GL.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			System.out.println("<!> Framebuffer incomplete, missing attachment\n");
			break;
		case GL.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			System.out.println("<!> Framebuffer incomplete attachement\n");
			break;
		case GL.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
			System.out.println("<!> Framebuffer incomplete, attached images must have same dimensions\n");
			break;
		case GL.GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
			System.out.println("<!> Framebuffer incomplete, attached images must have same format\n");
			break;
		default:
			System.out.println("<!> Framebuffer incomplete, other reason\n");
			return;
		}
	}

	// ------------- neue Methoden (begin) --------------------

	/**
     * Diese Methode zeichnet ein Koordinatensystem von min bis max
     * @param gl,
     * @param min
     * @param max 
     */
	private void printKoordinates(GL2 gl, float min, float max) {
		gl.glBegin(GL.GL_LINES);
		
		// x Achse 
		gl.glColor3f(1, 0, 0);
		gl.glVertex3f(min,0f,0f);
		gl.glVertex3f(max,0f,0f);
		
		// y Achse
		gl.glColor3f(0, 1, 0);
		gl.glVertex3f(0f,min,0f);
		gl.glVertex3f(0f,max,0f);
		
		// z Achse
		gl.glColor3f(0, 0, 1);
		gl.glVertex3f(0f,0f,min);
		gl.glVertex3f(0f,0f,max);
		
		gl.glEnd();
		
		// 1er Teilstriche
		for( float i = min; i < max; i++ ){
			gl.glBegin(GL.GL_LINES);		
			// Striche auf der X - Achse
			gl.glColor3f(1, 0, 0);
			gl.glVertex3f(i,	0f,		0.2f);
			gl.glVertex3f(i,	0f,		-0.2f);	
			gl.glVertex3f(i,	0.2f,	0f);
			gl.glVertex3f(i,	-0.2f,	0f);
			
			// Striche auf der Y - Achse
			gl.glColor3f(0, 1, 0);
			gl.glVertex3f(0.2f	,i	,	0f);
			gl.glVertex3f(-0.2f	,i	,	0f);	
			gl.glVertex3f(0f	,i	,	0.2f);
			gl.glVertex3f(0f	,i	,	-0.2f);

			// Striche auf der Z - Achse
			gl.glColor3f(0, 0, 1);
			gl.glVertex3f(0.2f	,0	,	i);
			gl.glVertex3f(-0.2f	,0	,	i);	
			gl.glVertex3f(0f	,0.2f	,	i);
			gl.glVertex3f(0f	,-0.2f	,	i);
			
			gl.glEnd();
		}
		
		// 0.1er Teilstriche
		for( float i = min; i < max; i+=0.1f ){
			gl.glBegin(GL.GL_LINES);		
			// Striche auf der X - Achse
			gl.glColor3f(1, 0, 0);
			gl.glVertex3f(i,	0f,		0.02f);
			gl.glVertex3f(i,	0f,		-0.02f);	
			gl.glVertex3f(i,	0.02f,	0f);
			gl.glVertex3f(i,	-0.02f,	0f);
			
			// Striche auf der Y - Achse
			gl.glColor3f(0, 1, 0);
			gl.glVertex3f(0.02f	,i	,	0f);
			gl.glVertex3f(-0.02f,i	,	0f);	
			gl.glVertex3f(0f	,i	,	0.02f);
			gl.glVertex3f(0f	,i	,	-0.02f);
			
			// Striche auf der Z - Achse
			gl.glColor3f(0, 0, 1);
			gl.glVertex3f(0.02f	,0	,	i);
			gl.glVertex3f(-0.02f,0	,	i);	
			gl.glVertex3f(0f	,0.02f	,	i);
			gl.glVertex3f(0f	,-0.02f	,	i);
			gl.glEnd();
		}
	}

	public final Konfiguration getKonfiguration() {
		return konfiguration;
	}

	/**
	 * Liefert den Blickwinkel f�r glPerspective. 
	 * @param angle
	 */
	public final float getAngle() {
		return angle;
	}
	
	/**
	 * Setzt den Blickwinkel f�r glPerspective. 
	 * @param angle
	 */
	public final void setAngle(float angle) {
		this.angle = angle;
	}
	
	/**
	 * Liefert die Blickweite f�r glPerspective.
	 * @param distance
	 */
	public final float getDistance() {
		return distance;
	}
	
	/**
	 * Setzt die Blickweite f�r glPerspective.
	 * @param distance
	 */
	public final void setDistance(float distance) {
		this.distance = distance;
	}

	/**
	 * Setzt das Licht, das f�r das Shadow-Mapping verwendet werden soll.
	 * @param light
	 */
	protected final void setLightForShadowMapping(DirectionPositionable light) {
		this.light = light;
	}

	/**
	 * Liefert das Licht, das aktuell f�r das Shadow-Mapping verwendet wird.
	 * @return
	 */
	protected final DirectionPositionable getLightForShadowMapping() {
		return this.light;
	}

	protected final boolean isDrawShadow() {
		return drawShadow;
	}

	protected final void setDrawShadow(boolean drawShadow) {
		this.drawShadow = drawShadow;
	}

	/**
	 * F�gt der Szene eine Animation hinzu, die zu Beginn eines jeden Frames "automatisch" durchgef�hrt wird.
	 * @param animation
	 */
	protected final void addAnimation(AbstractAnimation animation) {
		this.animations.addAnimation(animation);
	}

	protected final void addLight(GLAbstractLight light) {
		this.lights.add(light);
	}

	@Override
	public void forward() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void backward() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void left() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void right() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void up() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void down() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turnUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turnDown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turnLeft() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turnRight() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void swapWireMode() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cameraMode1() {
		// Kamera austauschen -> hat wieder die initiale Position
		this.konfiguration.getCamera().initiateCamera();
		this.konfiguration.getCamera().refreshCamera();
	}

	@Override
	public void cameraMode2() {
		this.drawShadow = ! this.drawShadow;
		if (this.drawShadow) {
			System.out.println("Schatten an!");
		} else {
			System.out.println("Schatten aus!");			
		}
	}

	@Override
	public void cameraMode3() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cameraMode4() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void swapLightMode() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void swapAnimationMode() {
		if (!this.pauseAnimation) {
			this.animations.pauseAnimation();
		} else {
			this.animations.resumeAnimation();
		}
		this.pauseAnimation = ! this.pauseAnimation;
	}

	@Override
	public void swapAnimationStartMode() {
		if (!this.startAnimation) {
			this.animations.stopAnimation();
		} else {
			this.animations.startAnimation();
		}
		this.startAnimation = ! this.startAnimation;
	}

	@Override
	public void keyZ() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyU() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyI() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyO() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyP() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void keyH() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void printMyKeys() {
//		System.out.println("Gitter (Wireframe) an/aus: G");
		System.out.println("Default-Kameraposition: 1");
		System.out.println("Schatten ein/aus: 2");
		System.out.println("Animation pausieren / fortf�hren: SPACE");
		System.out.println("Animation abbrechen / neu starten: ENTER");
	}

	@Override
	public void addKeyboardObserver(KeyboardObserver o) {
		this.keyboard.addKeyboardObserver(o);
	}

	@Override
	public void removeKeyboardObserver(KeyboardObserver o) {
		this.keyboard.removeKeyboardObserver(o);
	}

	@Override
	public List<KeyboardObserver> getKeyboardObservers() {
		return this.keyboard.getKeyboardObservers();
	}
}
