package falk_guenther_meier.model.world;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;

import com.jogamp.opengl.util.FPSAnimator;

import falk_guenther_meier.model.GLDrawable;
import falk_guenther_meier.model.animation.AbstractAnimation;
import falk_guenther_meier.model.animation.AnimationManagement;
import falk_guenther_meier.model.light.GLAbstractLight;
import falk_guenther_meier.model.observers.KeyboardObservable;
import falk_guenther_meier.model.observers.KeyboardObserver;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.util.Utility;

/**
 * Diese Klasse soll eine Hilfe f�r meine Tutorials sein, so dass man sich erst einmal
 * nicht mit der Initialisierung o.�. rum�rgern muss ;)
 * 
 *  Viel Spa� damit!
 *  
 * @author Joerg Amelunxen - japr0.wordpress.com
 * @modified Michael Falk, Hanno G�nther, Johannes Meier
 */
public class JoglApp implements GLEventListener, KeyboardObservable, KeyboardObserver, GLDrawable {
	private volatile int frameCount;

	protected GLU glu;
	private final static int RESHAPE_COUNT_LIMIT = 2;
	private int reshapeCounter;
	private boolean isInitialized;
	private boolean isFirstFrame;
	private String name;
    private int window_x;
    private int window_y;
    private GLCanvas canvas;

    final private FPSAnimator animator;

    private Konfiguration konfiguration;

    private float frameRate;
    private boolean swapWiresMode;
    private boolean pauseAnimation;
    private boolean startAnimation;

    private KeyboardAdapter keyboard;
    protected AnimationManagement animations;
    private List<GLAbstractLight> lights;

    private float[] viewMatrix;
    
    // Winkel und Blickweite f�r Kamera
    private float angle;
    private float distance;

	/**
	 * Konstruktor.
	 * @param name_value
	 * @param width
	 * @param height
	 */
    
    public JoglApp(String name_value, int width, int height, boolean start, Konfiguration konfiguration) {
    	// Setze interne Variablen
    	glu = new GLU();
    	reshapeCounter = 0;
    	isInitialized = false;
    	isFirstFrame = true;
    	name = name_value;
    	window_x = width;
    	window_y = height;

    	this.konfiguration = konfiguration;
    	
    	angle = 60;
    	distance = 150;    	

    	this.keyboard = new KeyboardAdapter();
    	this.addKeyboardObserver(this.konfiguration.getCamera());
    	this.addKeyboardObserver(this);

    	this.animations = new AnimationManagement();
    	this.pauseAnimation = false;
    	this.startAnimation = false;

    	this.lights = new ArrayList<GLAbstractLight>();
    	this.lights.add(this.getKonfiguration().getLight());

    	this.swapWiresMode = false;
    	
    	this.viewMatrix = new float[16];

    	// Starte App
    	// Suche passendens Profil und setze es
    	GLProfile glp = GLProfile.getDefault(); // GLProfile.get(GLProfile.GL2)
        GLCapabilities caps = new GLCapabilities(glp);
        canvas = new GLCanvas(caps);

        // Erstelle neuen Frame und bette die Zeichenfl�che ein
        Frame frame = new Frame(name);
        frame.setSize(window_x, window_y);
        frame.add(canvas);
        frame.setVisible(true);
        
        animator = new FPSAnimator(canvas, 60);
        
        //animator.add(canvas);
        
        // Erstelle einen Window Listener und sorge f�r korrektes
        // schlie�en des Programmes
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                new Thread(new Runnable() {
					@Override
					public void run() {
						animator.stop();
	                	System.exit(0);				
					}
                }).start();
            }
        });

        // Setze den Eventlistener
        if (start) {
        	canvas.addGLEventListener(this);
        }
        canvas.requestFocus();

        // Berechnet die FPS
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				frameRate = frameCount;
				frameCount = 0;
			}
		}, new Date(1), 1000);

        animator.start();
    }
    
    public JoglApp(String name_value, int width, int height, Konfiguration konfiguration) {
    	this(name_value, width, height, true, konfiguration);
    }
    
    public JoglApp(String name_value, int width, int height, boolean start) {
    	this(name_value, width, height, start, new Konfiguration());
    }
    
    public JoglApp(String name_value, int width, int height) {
    	this(name_value, width, height, true);
    }

    protected final void start() {
    	canvas.addGLEventListener(this);
    }

    public float getFrameRate() {
		return frameRate;
	}

	public Konfiguration getKonfiguration() {
		return konfiguration;
	}

	public void setKonfiguration(Konfiguration konfiguration) {
		this.konfiguration = konfiguration;
    	this.addKeyboardObserver(this.konfiguration.getCamera());
	}
	

	public int getWindowWidth() {
		return window_x;
	}

	public int getWindowHeight() {
		return window_y;
	}

	public float[] getViewMatrix() {
		return viewMatrix;
	}
	
	
	/**
	 * Liefert den Blickwinkel f�r glPerspective. 
	 * @param angle
	 */
	public float getAngle() {
		return angle;
	}
	
	/**
	 * Setzt den Blickwinkel f�r glPerspective. 
	 * @param angle
	 */
	public void setAngle(float angle) {
		this.angle = angle;
	}
	
	/**
	 * Liefert die Blickweite f�r glPerspective.
	 * @param distance
	 */
	public float getDistance() {
		return distance;
	}
	
	/**
	 * Setzt die Blickweite f�r glPerspective.
	 * @param distance
	 */
	public void setDistance(float distance) {
		this.distance = distance;
	}

	/**
	 * F�gt der Szene eine Animation hinzu, die zu Beginn eines jeden Frames "automatisch" durchgef�hrt wird.
	 * @param animation
	 */
	protected final void addAnimation(AbstractAnimation animation) {
		this.animations.addAnimation(animation);
	}

	protected final void addLight(GLAbstractLight light) {
		this.lights.add(light);
	}

	/**
     * Diese Methode wird einmal pro Frame aufgerufen und k�mmert sich um 
     * die Berechnungen und das Zeichnen
     * 
     * @param GLAutoDrawable drawable
     */
    @Override
    public void display(GLAutoDrawable drawable) {
    	GL2 gl = drawable.getGL().getGL2();
    	gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

		gl.glViewport(0, 0, this.getWindowWidth(), this.getWindowHeight());
		gl.glLoadIdentity();

		if (isFirstFrame) {
			isFirstFrame = false;
			this.animations.startAnimation();
			this.startAnimation = true;
		}

		this.keyboard.updateKeys();
		this.konfiguration.getCamera().refreshCamera();
		
		// ViewMatrix speichern
		gl.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, viewMatrix, 0);

		// Gitter ein oder ausblenden
		if (this.swapWiresMode) {
			IntBuffer initPolygonMode= IntBuffer.allocate(1);
			gl.glGetIntegerv(GL2.GL_POLYGON_MODE, initPolygonMode);
			if (initPolygonMode.get(0) == GL2.GL_LINE) {
				gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);
			} else {
				gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_LINE);
			}
			this.swapWiresMode = false;
		}

    	// Frames z�hlen
    	frameCount++;
    	
    	draw(gl);

    	ShaderProgram.clearShaderProgram(gl); // zur Sicherheit? (ben�tigt ?!)
    }
    
    /**
     * Ab hier wird die Welt gezeichnet
     */
    @Override
    public final void draw(GL2 gl) {
    	// Skybox zeichnen
    	if (konfiguration.isShowSkybox()) {
    		konfiguration.getSkybox().draw(gl);
    	}
    	// Zeichne ein Koordinatensystem
    	if (konfiguration.getCoordinates()) {
            print_koordinates(gl, -10 , 10);
    	}

    	this.getKonfiguration().getTime().updateTime();

    	beginGL(gl);
    	endGL(gl);
    }

    /**
     * Zeichnen der Welt
     */
    @Override
    public void beginGL(GL2 gl) {
    	// Animationen ausf�hren
    	this.animations.animate();

    	// Lichtquellen ber�cksichtigen
    	for (GLAbstractLight l : this.lights) {
    		l.beginGL(gl);
    	}
    }
    
    /**
     * hier werden OpenGL-Befehle wieder geschlossen 
     */
    @Override
    public void endGL(GL2 gl) {
    	// Lichtquellen "schlie�en"
    	for (GLAbstractLight l : this.lights) {
    		l.endGL(gl);
    	}

    	// Framerate anzeigen
    	if (konfiguration.isShowFrameRate()) {
    		Utility.drawText(gl, konfiguration.getTextFont(), konfiguration.getTextColor(),
    				Float.toString(frameRate) + " FPS", 15, gl.getContext().getGLDrawable().getHeight() - 70);
    	}    	
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
    }

    @Override
    public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		System.out.println("##############< Info >#################");
		System.out.println("JoglApp from japr0.wordpress.com");
		System.out.println("GL_VENDOR: " + gl.glGetString(GL2.GL_VENDOR));
		System.out.println("GL_RENDERER: " + gl.glGetString(GL2.GL_RENDERER));
		System.out.println("GL_VERSION: " + gl.glGetString(GL2.GL_VERSION));
		System.out.println("##############</Info >#################");

		// Listener nur einmal initialisieren !
		if (!isInitialized) {
			isInitialized = true;
			canvas.addKeyListener(this.keyboard);
		}
		keyboard.printAllKeys();
		gl.glEnable(GL2.GL_DEPTH_TEST);
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int w, int h) {
    	window_x = w;
    	window_y = h;
    	
    	if (reshapeCounter < JoglApp.RESHAPE_COUNT_LIMIT) {
    		reshapeCounter++;
    	} else {
    		return;
    	}
		GL2 gl = drawable.getGL().getGL2();
		// Setze einen passenden Viewport
		gl.glViewport(0, 0, window_x, window_y);
		//Projektionsmatrix 'leeren'
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		//glu.gluPerspective(30, (float) window_x / window_y, 1, 100);
		glu.gluPerspective(angle, (float) window_x / window_y, 1, distance);
		// Modelview 'leeren'
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
    }

    /**
     * Diese Methode zeichnet ein Koordinatensystem von min bis max
     * 
     * @param GL2 gl, float min, float max 
     */
	protected void print_koordinates(GL2 gl, float min, float max) {
		gl.glBegin(GL.GL_LINES);
		
		// x Achse 
		gl.glColor3f(1, 0, 0);
		gl.glVertex3f(min,0f,0f);
		gl.glVertex3f(max,0f,0f);
		
		// y Achse
		gl.glColor3f(0, 1, 0);
		gl.glVertex3f(0f,min,0f);
		gl.glVertex3f(0f,max,0f);
		
		// z Achse
		gl.glColor3f(0, 0, 1);
		gl.glVertex3f(0f,0f,min);
		gl.glVertex3f(0f,0f,max);
		
		gl.glEnd();
		
		// 1er Teilstriche
		for( float i = min; i < max; i++ ){
			gl.glBegin(GL.GL_LINES);		
			// Striche auf der X - Achse
			gl.glColor3f(1, 0, 0);
			gl.glVertex3f(i,	0f,		0.2f);
			gl.glVertex3f(i,	0f,		-0.2f);	
			gl.glVertex3f(i,	0.2f,	0f);
			gl.glVertex3f(i,	-0.2f,	0f);
			
			// Striche auf der Y - Achse
			gl.glColor3f(0, 1, 0);
			gl.glVertex3f(0.2f	,i	,	0f);
			gl.glVertex3f(-0.2f	,i	,	0f);	
			gl.glVertex3f(0f	,i	,	0.2f);
			gl.glVertex3f(0f	,i	,	-0.2f);

			// Striche auf der Z - Achse
			gl.glColor3f(0, 0, 1);
			gl.glVertex3f(0.2f	,0	,	i);
			gl.glVertex3f(-0.2f	,0	,	i);	
			gl.glVertex3f(0f	,0.2f	,	i);
			gl.glVertex3f(0f	,-0.2f	,	i);
			
			gl.glEnd();
		}
		
		// 0.1er Teilstriche
		for( float i = min; i < max; i+=0.1f ){
			gl.glBegin(GL.GL_LINES);		
			// Striche auf der X - Achse
			gl.glColor3f(1, 0, 0);
			gl.glVertex3f(i,	0f,		0.02f);
			gl.glVertex3f(i,	0f,		-0.02f);	
			gl.glVertex3f(i,	0.02f,	0f);
			gl.glVertex3f(i,	-0.02f,	0f);
			
			// Striche auf der Y - Achse
			gl.glColor3f(0, 1, 0);
			gl.glVertex3f(0.02f	,i	,	0f);
			gl.glVertex3f(-0.02f,i	,	0f);	
			gl.glVertex3f(0f	,i	,	0.02f);
			gl.glVertex3f(0f	,i	,	-0.02f);
			
			// Striche auf der Z - Achse
			gl.glColor3f(0, 0, 1);
			gl.glVertex3f(0.02f	,0	,	i);
			gl.glVertex3f(-0.02f,0	,	i);	
			gl.glVertex3f(0f	,0.02f	,	i);
			gl.glVertex3f(0f	,-0.02f	,	i);
			gl.glEnd();
		}
	}
	
	@Override
	public void addKeyboardObserver(KeyboardObserver o) {
//		this.keyboardObservers.add(o);
		this.keyboard.addKeyboardObserver(o);
	}

	@Override
	public void removeKeyboardObserver(KeyboardObserver o) {
//		this.keyboardObservers.remove(o);
		this.keyboard.removeKeyboardObserver(o);
	}

	@Override
	public List<KeyboardObserver> getKeyboardObservers() {
//		return this.keyboardObservers;
		return this.keyboard.getKeyboardObservers();
	}

	@Override
	public void forward() {
		// bleibt leer
	}

	@Override
	public void backward() {
		// bleibt leer
	}

	@Override
	public void left() {
		// bleibt leer
	}

	@Override
	public void right() {
		// bleibt leer
	}

	@Override
	public void up() {
		// bleibt leer
	}

	@Override
	public void down() {
		// bleibt leer
	}

	@Override
	public void turnUp() {
		// bleibt leer
	}

	@Override
	public void turnDown() {
		// bleibt leer
	}

	@Override
	public void turnLeft() {
		// bleibt leer
	}

	@Override
	public void turnRight() {
		// bleibt leer
	}

	@Override
	public void swapWireMode() {
		this.swapWiresMode = true;
	}

	@Override
	public void cameraMode1() {
		// Kamera austauschen -> hat wieder die initiale Position
		this.konfiguration.getCamera().initiateCamera();
		this.konfiguration.getCamera().refreshCamera();
	}
	
	@Override
	public void cameraMode2() {
		// bleibt leer
		
	}

	@Override
	public void cameraMode3() {
		// bleibt leer
		
	}

	@Override
	public void cameraMode4() {
		// bleibt leer
		
	}

	@Override
	public void swapLightMode() {
		// bleibt leer
	}

	@Override
	public void keyZ() {
		// bleibt leer
	}

	@Override
	public void keyU() {
		// bleibt leer
	}

	@Override
	public void swapAnimationMode() {
		if (!this.pauseAnimation) {
			this.animations.pauseAnimation();
		} else {
			this.animations.resumeAnimation();
		}
		this.pauseAnimation = ! this.pauseAnimation;
	}

	@Override
	public void swapAnimationStartMode() {
		if (!this.startAnimation) {
			this.animations.stopAnimation();
		} else {
			this.animations.startAnimation();
		}
		this.startAnimation = ! this.startAnimation;
	}

	@Override
	public void keyI() {
		// bleibt leer
	}

	@Override
	public void keyO() {
		// bleibt leer
	}

	@Override
	public void keyP() {
		// bleibt leer
	}
	
	@Override
	public void keyH() {
		// bleibt leer
	}

	@Override
	public void printMyKeys() {
		System.out.println("Gitter (Wireframe) an/aus: G");
		System.out.println("Default-Kameraposition: 1");
		System.out.println("Animation pausieren / fortf�hren: SPACE");
		System.out.println("Animation abbrechen / neu starten: ENTER");
	}	
}
