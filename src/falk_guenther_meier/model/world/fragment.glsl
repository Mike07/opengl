varying vec3 eyeSpaceNormal;
varying vec4 texCoord2;
uniform sampler2D shadowMap2;

void main() {
	// lighting
	vec3 normal = normalize(eyeSpaceNormal).xyz;
	float intensity = max(0.0, dot(normalize(gl_LightSource[0].position.xyz).xyz, normal)) + 0.25;

	// shadow mapping
	float tmp1 = texture2DProj(shadowMap2, texCoord2).r;
	float tmp = 0.0;
	tmp = tmp1 >= texCoord2.z ? 1.0 : 0.5;

	// final color
	tmp *= intensity;
	gl_FragColor = vec4(tmp, tmp, tmp, 1.0);
}
