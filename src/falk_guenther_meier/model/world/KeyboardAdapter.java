package falk_guenther_meier.model.world;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.observers.KeyboardObservable;
import falk_guenther_meier.model.observers.KeyboardObserver;

public class KeyboardAdapter implements KeyboardObservable, KeyListener {
    private List<KeyboardObserver> keyboardObservers;

    private boolean keyW = false;
	private boolean keyS = false;
	private boolean keyA = false;
	private boolean keyD = false;
	private boolean keyUp = false;
	private boolean keyDown = false;
	private boolean keyLeft = false;
	private boolean keyRight = false;
	private boolean keyR = false; // Bewegung nach unten
	private boolean keyF = false; // Bewegung nach oben

	private boolean keyG = false; // Gitter an/aus
	private boolean keyL = false; // Licht an/aus
	private boolean keySpace = false; // Animation pausieren
	private boolean keyEnter = false; // Animation abbrechen / neu starten
	private boolean keyZ = false; // z.B. Shader wechseln
	private boolean keyU = false; // z.B. Shading Mode
	private boolean keyI = false; // z.B. Backface Culling
	private boolean keyO = false; // z.B. Depth test
	private boolean keyP = false; // z.B. Counter clockwise winding
	private boolean keyH = false; // z.B. Hilfe anzeigen

	private boolean key1 = false; // initiale Kamera-Position
	private boolean key2;
	private boolean key3;
	private boolean key4;

	public KeyboardAdapter() {
    	this.keyboardObservers = new ArrayList<KeyboardObserver>();
    }

    @Override
	public void keyTyped(KeyEvent e) {
		// bleibt leer!
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_W) {
			keyW = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_S) {
			keyS = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_A) {
			keyA = true;
		} 
		if (e.getKeyCode() == KeyEvent.VK_D) {
			keyD = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			keyUp = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			keyDown = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			keyLeft = true;
		} 
		if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			keyRight = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_R) {
			keyR = true;
		} 
		if (e.getKeyCode() == KeyEvent.VK_F) {
			keyF = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_G) {
			keyG = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_1) {
			key1 = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_2) {
			key2 = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_3) {
			key3 = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_4) {
			key4 = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_Z) {
			keyZ = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_L) {
			keyL = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_SPACE) {
			keySpace = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			keyEnter = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_U) {
			keyU = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_I) {
			keyI = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_O) {
			keyO = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_P) {
			keyP = true;
		}
		if (e.getKeyCode() == KeyEvent.VK_H) {
			keyH = true;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_W) {
			keyW = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_S) {
			keyS = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_A) {
			keyA = false;
		} 
		if (e.getKeyCode() == KeyEvent.VK_D) {
			keyD = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			keyUp = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			keyDown = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			keyLeft = false;
		} 
		if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			keyRight = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_R) {
			keyR = false;
		} 
		if (e.getKeyCode() == KeyEvent.VK_F) {
			keyF = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_G) {
			keyG = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_1) {
			key1 = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_2) {
			key2 = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_3) {
			key3 = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_4) {
			key4 = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_Z) {
			keyZ = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_L) {
			keyL = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_SPACE) {
			keySpace = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			keyEnter = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_U) {
			keyU = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_I) {
			keyI = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_O) {
			keyO = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_P) {
			keyP = false;
		}
		if (e.getKeyCode() == KeyEvent.VK_H) {
			keyH = false;
		}
	}

	public void updateKeys() {
		if (keyW) {
			// Vorw�rtsbewegung
			for (KeyboardObserver o : keyboardObservers) {
				o.forward();
			}
		}
		if (keyS) {
			// R�ckw�rtsbewegung
			for (KeyboardObserver o : keyboardObservers) {
				o.backward();
			}
		}
		if (keyA) {
			// Linksbewegung
			for (KeyboardObserver o : keyboardObservers) {
				o.left();
			}
		}
		if (keyD) {
			// Rechtsbewegung
			for (KeyboardObserver o : keyboardObservers) {
				o.right();
			}
		}
		if (keyUp) {
			// Rotation oben
			for (KeyboardObserver o : keyboardObservers) {
				o.turnUp();
			}
		}
		if (keyDown) {
			// Rotation unten
			for (KeyboardObserver o : keyboardObservers) {
				o.turnDown();
			}
		}
		if (keyLeft) {
			// Rotation links
			for (KeyboardObserver o : keyboardObservers) {
				o.turnLeft();
			}
		}
		if (keyRight) {
			// Rotation rechts
			for (KeyboardObserver o : keyboardObservers) {
				o.turnRight();
			}
		}
		if (keyR) {
			// Bewegung nach oben
			for (KeyboardObserver o : keyboardObservers) {
				o.up();
			}
		}
		if (keyF) {
			// Bewegung nach unten
			for (KeyboardObserver o : keyboardObservers) {
				o.down();
			}
		}
		if (keyG) {
			// Gitter ein oder ausblenden
			for (KeyboardObserver o : keyboardObservers) {
				o.swapWireMode();
			}			
			keyG = false;
		}
		if (key1) {
			// Kamera austauschen -> hat wieder die initiale Position
			for (KeyboardObserver o : keyboardObservers) {
				o.cameraMode1();
			}
			key1 = false;
		}
		if (key2) {
			for (KeyboardObserver o : keyboardObservers) {
				o.cameraMode2();
			}
			key2 = false;
		}
		if (key3) {
			for (KeyboardObserver o : keyboardObservers) {
				o.cameraMode3();
			}
			key3 = false;
		}
		if (key4) {
			for (KeyboardObserver o : keyboardObservers) {
				o.cameraMode4();
			}
			key4 = false;
		}
		if (keyZ) {
			// Shader austauschen
			for (KeyboardObserver o : keyboardObservers) {
				o.keyZ();
			}			
			keyZ = false;
		}
		if (keyL) {
			// Licht an / aus schalten
			for (KeyboardObserver o : keyboardObservers) {
				o.swapLightMode();
			}			
			keyL = false;
		}
		if (keySpace) {
			// Animation pausieren
			for (KeyboardObserver o : keyboardObservers) {
				o.swapAnimationMode();
			}			
			keySpace = false;
		}
		if (keyEnter) {
			// Animation abbrechen / neu starten
			for (KeyboardObserver o : keyboardObservers) {
				o.swapAnimationStartMode();
			}			
			keySpace = false;
		}
		if (keyU) {
			// Shading Mode umschalten
			for (KeyboardObserver o : keyboardObservers) {
				o.keyU();
			}			
			keyU = false;
		}
		if (keyI) {
			// backface culling
			for (KeyboardObserver o : keyboardObservers) {
				o.keyI();
			}			
			keyI = false;
		}
		if (keyO) {
			// Depth test
			for (KeyboardObserver o : keyboardObservers) {
				o.keyO();
			}			
			keyO = false;
		}
		if (keyP) {
			// Counter clockwise winding
			for (KeyboardObserver o : keyboardObservers) {
				o.keyP();
			}			
			keyP = false;
		}
		if (keyH) {
			for (KeyboardObserver o : keyboardObservers) {
				o.keyH();
			}			
			keyH = false;
		}
	}

	@Override
	public void addKeyboardObserver(KeyboardObserver o) {
		this.keyboardObservers.add(o);
	}

	@Override
	public void removeKeyboardObserver(KeyboardObserver o) {
		this.keyboardObservers.remove(o);
	}

	@Override
	public List<KeyboardObserver> getKeyboardObservers() {
		return this.keyboardObservers;
	}
	
	/**
	 * Gibt alle Tastenbelegungen aus.
	 */
	public void printAllKeys() {
		System.out.println();
		for (KeyboardObserver o : keyboardObservers) {
			o.printMyKeys();
		}
		System.out.println();
	}	
}
