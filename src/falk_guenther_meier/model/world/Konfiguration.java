package falk_guenther_meier.model.world;

import java.awt.Color;
import java.awt.Font;
import java.io.File;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLSkyBox;
import falk_guenther_meier.model.observers.TranslateObserver;
import falk_guenther_meier.model.textures.GLTexGrid;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.time.TimeComponent;
import falk_guenther_meier.util.FileLoader;

/**
 * Startkonfiguration f�r JoglApp.
 * 
 * @author Michael
 */
public class Konfiguration {
	private GLCamera camera;
	private GLPointLight light;
	private GLSkyBox skybox;
	private TimeComponent time;
	private Font textFont;
	private GLVertex textColor;
	private boolean coordinates;
	private boolean showFrameRate;
	private boolean showSkybox;

	// Default-Konstruktor mit Defaulteinstellungen
	public Konfiguration() {
		this.camera = new GLCamera();
		this.time = new TimeComponent();

		GLVertex position = new GLVertex(-5f, 8f, -5f, 1.0f);
		GLVertex ambient = new GLVertex(0.45f, 0.45f, 0.45f, 1.0f);
		GLVertex diffuse = new GLVertex(0.7f, 0.7f, 0.7f, 1.0f);
		GLVertex specular = new GLVertex(0.2f, 0.2f, 0.2f, 1.0f);
		// Light7 ist hierf�r reserviert
		this.light = new GLPointLight(GL2.GL_LIGHT7, position, ambient,
				diffuse, specular, true);

		this.skybox = new GLSkyBox(this.getCamera(), 100.0f, 0.008f);
		File textureFile = FileLoader.getFileFromFilesFolder(
				"skybox/skybox_texture.jpg", true);
		GLTexture2D skyBoxTexture = new GLTexture2D(textureFile);
		skyBoxTexture.setTexGrid(new GLTexGrid(4,3));
		
		this.skybox.setTexture(skyBoxTexture);
		// jetzt Texturen aus Zeilen und Spalten richtig zuordnen
		this.skybox.getRectFront().setTexRect(3, 1);
		this.skybox.getRectBack().setTexRect(1, 1);
		this.skybox.getRectLeft().setTexRect(0, 1);
		this.skybox.getRectRight().setTexRect(2, 1);
		this.skybox.getRectTop().setTexRect(1, 0);
		this.skybox.getRectDown().setTexRect(1, 2);

		this.textFont = new Font("SansSerif", Font.BOLD, 20);
		this.textColor = new GLVertex(Color.ORANGE);
		this.coordinates = true;
		this.showFrameRate = true;
		this.showSkybox = true;
	}

	public Konfiguration(GLCamera camera, GLPointLight light,
			GLSkyBox skybox, Font textFont,
			boolean coordinates, boolean showFrameRate, boolean showSkybox) {
		this.time = new TimeComponent();
		this.camera = camera;
		this.light = light;
		this.skybox = skybox;
		this.textFont = textFont;
		this.textColor = new GLVertex(Color.ORANGE);
		this.coordinates = coordinates;
		this.showFrameRate = showFrameRate;
		this.showSkybox = showSkybox;
	}

	public GLCamera getCamera() {
		return camera;
	}

	public void changeCamera(GLCamera camera) {
		if (this.camera != null) {
			for (TranslateObserver o : this.camera.getTranslateObservers().toArray(new TranslateObserver[0])) {
				this.camera.removeTranslateObserver(o);
				camera.addTranslateObserver(o);
//				System.out.println("zus�tzliches Wechsel-Update");
				o.updateAbsolutePosition(camera, camera.getPosition());
			}
		}
		this.camera = camera;
	}

	public void setCameraWithoutObservers(GLCamera camera) {
		this.camera = camera;
	}

	public GLPointLight getLight() {
		return light;
	}

	public void setLight(GLPointLight light) {
		this.light = light;
	}

	public GLSkyBox getSkybox() {
		return skybox;
	}

	public void setSkybox(GLSkyBox skybox) {
		this.skybox = skybox;
	}

	public TimeComponent getTime() {
		return time;
	}

	public Font getTextFont() {
		return this.textFont;
	}

	public void setTextFont(Font textFont) {
		this.textFont = textFont;
	}

	/**
	 * Liefert, ob das Koordinatensystem gezeichnet wird oder nicht.
	 * 
	 * @return
	 */
	public boolean getCoordinates() {
		return coordinates;
	}

	/**
	 * Setzt, ob das Koordinatensystem gezeichnet wird oder nicht.
	 * 
	 * @param controlMode
	 */
	public void setCoordinates(boolean coordinates) {
		this.coordinates = coordinates;
	}

	public boolean isShowFrameRate() {
		return showFrameRate;
	}

	public void setShowFrameRate(boolean showFrameRate) {
		this.showFrameRate = showFrameRate;
	}

	public GLVertex getTextColor() {
		return textColor;
	}

	public void setTextColor(GLVertex textColor) {
		this.textColor = textColor;
	}

	public boolean isShowSkybox() {
		return showSkybox;
	}

	public void setShowSkybox(boolean showSkybox) {
		this.showSkybox = showSkybox;
	}
}