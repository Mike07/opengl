package falk_guenther_meier.model;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;

/**
 * Material f�r Objekte.
 */
public class GLMaterial {
	public static GLMaterial DEFAULT = new GLMaterial();
	public static GLMaterial DEFAULT_OWN = new GLMaterial(
			new GLVertex(0.2f, 0.2f, 0.2f),
			new GLVertex(0.2f, 0.2f, 0.2f),
			new GLVertex(0.2f, 0.2f, 0.2f),
			new GLVertex(0.0f, 0.0f, 0.0f), 0.2f);
	public static GLMaterial EMERALD = new GLMaterial(
			new GLVertex(0.0215f, 0.1745f, 0.0215f),
			new GLVertex(0.07568f, 0.61424f, 0.07568f),
			new GLVertex(0.633f, 0.727811f, 0.633f),
			new GLVertex(), 0.6f);
	public static GLMaterial JADE = new GLMaterial(
			new GLVertex(0.135f, 0.2225f, 0.1575f),
			new GLVertex(0.54f, 0.89f, 0.63f),
			new GLVertex(0.316228f, 0.316228f, 0.316228f),
			new GLVertex(), 0.1f);
	public static GLMaterial OBSIDIAN = new GLMaterial(
			new GLVertex(0.05375f, 0.05f, 0.06625f),
			new GLVertex(0.18275f, 0.17f, 0.22525f),
			new GLVertex(0.332741f, 0.328634f, 0.346435f),
			new GLVertex(), 0.3f);
	public static GLMaterial PEARL = new GLMaterial(
			new GLVertex(0.25f, 0.20725f, 0.20725f),
			new GLVertex(1f, 0.829f, 0.829f),
			new GLVertex(0.296648f, 0.296648f, 0.296648f),
			new GLVertex(),0.088f);
	public static GLMaterial RUBY = new GLMaterial(
			new GLVertex(0.1745f, 0.01175f, 0.01175f),
			new GLVertex(0.61424f, 0.04136f, 0.04136f),
			new GLVertex(0.727811f, 0.626959f, 0.626959f),
			new GLVertex(),0.6f);
	public static GLMaterial TURQUOISE = new GLMaterial(
			new GLVertex(0.1f, 0.18725f, 0.1745f),
			new GLVertex(0.396f, 0.74151f, 0.69102f),
			new GLVertex(0.297254f, 0.30829f, 0.306678f),
			new GLVertex(),0.1f);

	private GLVertex matAmbient;
	private GLVertex matDiffuse;
	private GLVertex matSpecular;
	private GLVertex matEmission;
	private float matShininess;

	public GLMaterial() {
		matAmbient = new GLVertex(0.4f, 0.4f, 0.4f, 1.0f);
		matDiffuse = new GLVertex(0.8f, 0.8f, 0.8f, 1.0f);
		matSpecular = new GLVertex(0.4f, 0.4f, 0.4f, 1.0f);
		matEmission = new GLVertex(0.0f, 0.0f, 0.0f, 1.0f);
		matShininess = 0;
	}
	
	public GLMaterial(GLVertex matAmbient, GLVertex matDiffuse,
			GLVertex matSpecular, GLVertex matEmission, float matShininess) {
		this.matAmbient = matAmbient;
		this.matDiffuse = matDiffuse;
		this.matSpecular = matSpecular;
		this.matEmission = matEmission;
		this.matShininess = matShininess;
	}
	
	public GLMaterial(GLMaterial mat) {
		this.matAmbient = mat.matAmbient;
		this.matDiffuse = mat.matDiffuse;
		this.matSpecular = mat.matSpecular;
		this.matEmission = mat.matEmission;
		this.matShininess = mat.matShininess;
	}

	public GLVertex getMatAmbient() {
		return matAmbient;
	}

	public void setMatAmbient(GLVertex matAmbient) {
		this.matAmbient = matAmbient;
	}

	public GLVertex getMatDiffuse() {
		return matDiffuse;
	}

	public void setMatDiffuse(GLVertex matDiffuse) {
		this.matDiffuse = matDiffuse;
	}

	public GLVertex getMatSpecular() {
		return matSpecular;
	}

	public void setMatSpecular(GLVertex matSpecular) {
		this.matSpecular = matSpecular;
	}

	public GLVertex getMatEmission() {
		return matEmission;
	}

	public void setMatEmission(GLVertex matEmission) {
		this.matEmission = matEmission;
	}

	public float getMatShininess() {
		return matShininess;
	}

	public void setMatShininess(float matShininess) {
		this.matShininess = matShininess;
	}

	public void toGL(GL2 gl) {
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, matAmbient.getValues(), 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, matDiffuse.getValues(), 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, matSpecular.getValues(), 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_EMISSION, matEmission.getValues(), 0);
		gl.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, matShininess);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((matAmbient == null) ? 0 : matAmbient.hashCode());
		result = prime * result
				+ ((matDiffuse == null) ? 0 : matDiffuse.hashCode());
		result = prime * result
				+ ((matEmission == null) ? 0 : matEmission.hashCode());
		result = prime * result + Float.floatToIntBits(matShininess);
		result = prime * result
				+ ((matSpecular == null) ? 0 : matSpecular.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GLMaterial other = (GLMaterial) obj;
		if (matAmbient == null) {
			if (other.matAmbient != null)
				return false;
		} else if (!matAmbient.equals(other.matAmbient))
			return false;
		if (matDiffuse == null) {
			if (other.matDiffuse != null)
				return false;
		} else if (!matDiffuse.equals(other.matDiffuse))
			return false;
		if (matEmission == null) {
			if (other.matEmission != null)
				return false;
		} else if (!matEmission.equals(other.matEmission))
			return false;
		if (Float.floatToIntBits(matShininess) != Float
				.floatToIntBits(other.matShininess))
			return false;
		if (matSpecular == null) {
			if (other.matSpecular != null)
				return false;
		} else if (!matSpecular.equals(other.matSpecular))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GLMaterial [matAmbient=" + matAmbient + ", matDiffuse="
				+ matDiffuse + ", matSpecular=" + matSpecular
				+ ", matEmission=" + matEmission + ", matShininess="
				+ matShininess + "]";
	}

	@Override
	protected GLMaterial clone() {
		return new GLMaterial(this);
	}

	/**
	 * Liefert das Material entsprechend so, dass die angegebene Farbe berechnet wird!
	 * @param colorVertex
	 * @return
	 */
	public static GLMaterial createColor(GLVertex colorVertex) {
		// TODO: so korrekt ?!
		return new GLMaterial(new GLVertex(colorVertex.clone()), new GLVertex(colorVertex.clone()),
				new GLVertex(colorVertex.clone()), new GLVertex(), 0.2f);
	}

	/**
	 * Liefert ein Material, um damit Textur-Fl�chen zu belegen.
	 * @return
	 */
	public static GLMaterial createTextureMaterial() {
		return new GLMaterial(new GLVertex(1.0f, 1.0f, 1.0f), new GLVertex(1.0f, 1.0f, 1.0f),
				new GLVertex(1.0f, 1.0f, 1.0f), new GLVertex(), 0.0f);
	}
}
