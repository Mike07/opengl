package falk_guenther_meier.model.animation;

import falk_guenther_meier.model.interpolation.AbstractMathInterpolation;
import falk_guenther_meier.model.platform.Positionable;
import falk_guenther_meier.model.time.TimeComponent;

/**
 * Animation der Position von beliebigen animationsfähigen Objekten.
 * @author Johannes, Michael
 */
public class PositionAnimation extends InterpolationAnimation {
	/**
	 * Animationsobjekt, das animiert werden soll
	 */
	private Positionable animationObject;

	public PositionAnimation(TimeComponent time, Positionable animationObject) {
		super(time);
		this.animationObject = animationObject;
	}

	@Override
	protected void doUpdate() {
		this.animationObject.setPosition(this.positionInterpolations.get(index).getInterpolatedPoint(
				(this.getTime() - this.startTimeActuallInterpolation) / this.interpolationTimes.get(index)));		
	}

	@Override
	protected void doEnd() {
		this.animationObject.setPosition(this.getActuallEndPoint());
	}

	/**
	 * Fügt der Animation eine beliebige Interpolation hinzu.
	 * @param interpolation
	 * @param time Dauer der Interpolation
	 */
	public void addInterpolation(AbstractMathInterpolation interpolation, float time) {
		// falsche Eingaben abfangen (Sprünge bei Übergängen sind erlaubt: Design-Entscheidung!)
		if (time <= 0.0f || interpolation == null) {
			throw new IllegalArgumentException();
		}
		this.wholeTime += time;
		this.positionInterpolations.add(interpolation);
		this.interpolationTimes.add(time);
	}

	@Override
	public String toString() {
		return "[" + positionInterpolations.toString() + "]";
	}

	@Override
	public void startAnimation() {
		super.startAnimation();
		this.animationObject.setPosition(this.positionInterpolations.get(0).getStartPoint().clone());
	}
}
