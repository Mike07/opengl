package falk_guenther_meier.model.animation;

import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.observers.AnimationStatusObservable;
import falk_guenther_meier.model.observers.AnimationStatusObserver;

/**
 * Verwaltet alle Animationen einer Szene.
 * Sendet eine Benachrichtigung, wenn alle Animationen gestartet, gestoppt, pausiert oder fortgesetzt werden.
 * (Ob das sinnvoll ist oder gar ben�tigt wird, ist aktuell nebens�chlich. Ergibt sich aus Analogie)
 * @author Johannes
 */
public final class AnimationManagement implements AnimationFunctions, AnimationStatusObserver {
    private List<AnimationFunctions> animations;
	private List<AnimationStatusObserver> observer;

    public AnimationManagement() {
    	this.animations = new ArrayList<AnimationFunctions>();
    	this.observer = new ArrayList<AnimationStatusObserver>();
    }

    public void addAnimation(AnimationFunctions animation) {
    	animation.addAnimationStatusObserver(this);
    	this.animations.add(animation);
    }

    @Override
	public void pauseAnimation() {
    	for (AnimationFunctions ani : this.animations.toArray(new AnimationFunctions[0])) {
    		ani.pauseAnimation();
    	}
    	for (AnimationStatusObserver o : this.observer) {
    		o.updatePause(this);
    	}
	}

	@Override
	public void resumeAnimation() {
    	for (AnimationFunctions ani : this.animations.toArray(new AnimationFunctions[0])) {
    		ani.resumeAnimation();
    	}
    	for (AnimationStatusObserver o : this.observer) {
    		o.updateResume(this);
    	}
	}

	@Override
	public void startAnimation() {
    	for (AnimationFunctions ani : this.animations.toArray(new AnimationFunctions[0])) {
    		ani.startAnimation();
    	}
    	for (AnimationStatusObserver o : this.observer) {
    		o.updateStartOfAnimation(this);
    	}
	}

	@Override
	public void stopAnimation() {
    	for (AnimationFunctions ani : this.animations.toArray(new AnimationFunctions[0])) {
    		ani.stopAnimation();
    	}
    	for (AnimationStatusObserver o : this.observer) {
    		o.updateEndOfAnimation(this);
    	}
	}

	@Override
	public void animate() {
    	for (AnimationFunctions ani : this.animations.toArray(new AnimationFunctions[0])) {
    		ani.animate();
    	}
	}

	@Override
	public void updateStartOfAnimation(AnimationStatusObservable changed) {
		System.out.println(changed + " wurde gestartet!");		
	}

	@Override
	public void updateEndOfAnimation(AnimationStatusObservable changed) {
		System.out.println(changed + " wurde beendet!");		
	}

	@Override
	public void updatePause(AnimationStatusObservable changed) {
		System.out.println(changed + " wurde pausiert!");		
	}

	@Override
	public void updateResume(AnimationStatusObservable changed) {
		System.out.println(changed + " wurde fortgesetzt!");		
	}

	@Override
	public void addAnimationStatusObserver(AnimationStatusObserver o) {
		this.observer.add(o);
	}

	@Override
	public void removeAnimationStatusObserver(AnimationStatusObserver o) {
		this.observer.remove(o);
	}

	@Override
	public List<AnimationStatusObserver> getAnimationStatusObservers() {
		return this.observer;
	}
}
