package falk_guenther_meier.model.animation;

import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.interpolation.AbstractMathInterpolation;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.time.TimeComponent;

/**
 * Abstrakte Klasse f�r die Animation von beliebigen animationsf�higen Objekten.
 * @author Johannes, Michael
 */
public abstract class InterpolationAnimation extends AbstractAnimation {
	/**
	 * Enth�lt die verschiedenen Interpolationen f�r die Position, die nacheinander durchgef�hrt werden sollen.
	 */
	protected List<AbstractMathInterpolation> positionInterpolations;
	/**
	 * Enth�lt die Dauer aller Interpolationen.
	 */
	protected List<Float> interpolationTimes;
	/**
	 * Nr. der Position in der Liste, von der sich das Animationsobjekt gerade wegbewegt hin zur n�chsten Position
	 */
	protected int index;
	/**
	 * Gibt an, ob nach Ende der eigentlichen Animation das Animationsobjekt schon genau auf die Zielposition bewegt wurde,
	 * was normalerweise bei Ende der Animation wegen schlechter Zeit-Aufl�sung nicht der Fall ist.
	 */
	private boolean endCorrectionDone;
	/**
	 * Speichert die Start-Zeit der Animation (bzw. des aktuellen Animation-Durchgangs bei "Endlos-Animationen").
	 */
	private float startTime;
	/**
	 * Speichert die Start-Zeit der aktuellen Interpolation.
	 */
	protected float startTimeActuallInterpolation;
	/**
	 * Gesamt-Dauer der Animation (Summe der Dauer aller Interpolationen)
	 */
	protected float wholeTime;

	public InterpolationAnimation(TimeComponent time) {
		super(time);
		this.positionInterpolations = new ArrayList<AbstractMathInterpolation>();
		this.interpolationTimes = new ArrayList<Float>();
		this.index = 0;
		this.endCorrectionDone = false;
		this.startTime = 0.0f; // diese Angabe wird beim Starten �berschrieben!
		this.startTimeActuallInterpolation = this.startTime;
		this.wholeTime = 0.0f;
	}

	@Override
	protected final void doAnimation() {
		float time = this.getTime();
		// keine Animation bei zu wenig Positionen oder aktuelle Zeit liegt nicht im Zeitintervall f�r diese Animation
		if (this.positionInterpolations.size() < 1 || this.startTime > time) {
			return;
		}
		// Ende der Animation behandeln
		if (this.startTime + this.wholeTime <= time) {
			if (!this.endCorrectionDone) {
				this.endCorrectionDone = true;
				this.doEnd();
				this.stopAnimation();
			}
			return;
		}
		// Index aktualisieren (theoretisch k�nnen einzelne Positionen / Interpolationen �bersprungen werden!)
		while (index + 1 < this.positionInterpolations.size() &&
				this.interpolationTimes.get(index) + this.startTimeActuallInterpolation <= time) {
			this.startTimeActuallInterpolation += this.interpolationTimes.get(index);
			index++;
		}
		// aktuelle Position des Objekts setzen
		this.doUpdate();
	}

	/**
	 * Enth�lt die Anweisungen, die als letzter Animationsschritt durchgef�hrt werden.
	 * Enth�lt typischerweise die Anweisungen, um dem Objekt die letztendliche Position zu geben.
	 */
	protected abstract void doEnd();

	/**
	 * Enth�lt die Anweisungen, mit denen das Objekt bei jedem Animationsschritt aktualisiert wird.
	 */
	protected abstract void doUpdate();

	@Override
	public void startAnimation() {
		if (this.positionInterpolations.size() <= 0) {
			throw new IllegalArgumentException();
		}
		super.startAnimation();
		this.index = 0;
		this.endCorrectionDone = false;
		this.startTime = this.getTime();
		this.startTimeActuallInterpolation = this.startTime;
	}

	/**
	 * Liefert den aktuellen Endpunkt der Animation zur�ck.
	 * @return Endpunkt (bzw. null, wenn noch keine Interpolation hinzugef�gt wurde) in einer neuen Instanz!
	 */
	public final GLVertex getActuallEndPoint() {
		if (this.positionInterpolations.size() <= 0) {
			return null;
		}
		return this.positionInterpolations.get(this.positionInterpolations.size() - 1).getEndPoint().clone();
	}

	/**
	 * Liefert die aktuelle End-Richtung der Animation zur�ck.
	 * @return End-Richtung (bzw. null, wenn noch keine Interpolation hinzugef�gt wurde) in einer neuen Instanz!
	 */
	public GLVertex getActuallEndDirection() {
		if (this.positionInterpolations.size() <= 0) {
			return null;
		}
		return this.positionInterpolations.get(this.positionInterpolations.size() - 1).getEndDirection().clone();
	}
}
