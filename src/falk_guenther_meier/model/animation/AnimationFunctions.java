package falk_guenther_meier.model.animation;

import falk_guenther_meier.model.observers.AnimationStatusObservable;

/**
 * Beschreibt die Funktionalitšten einer Animation.
 * @author Johannes
 */
public interface AnimationFunctions extends AnimationStatusObservable {
	public void pauseAnimation();
	public void resumeAnimation();
	public void startAnimation();
	public void stopAnimation();
	public void animate();
}
