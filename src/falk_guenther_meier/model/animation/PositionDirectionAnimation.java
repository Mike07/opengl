package falk_guenther_meier.model.animation;

import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.interpolation.AbstractMathInterpolation;
import falk_guenther_meier.model.interpolation.ConstantInterpolation;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.DirectionPositionable;
import falk_guenther_meier.model.time.TimeComponent;

/**
 * Animation der Position und Richtung von entsprechenden Objekten (Kamera, Spotlight).
 * @author Johannes
 */
public class PositionDirectionAnimation extends InterpolationAnimation {
	/**
	 * Animationsobjekt, das animiert werden soll
	 */
	private DirectionPositionable animationObject;
	/**
	 * Enth�lt die verschiedenen Interpolationen f�r die Richtung, die nacheinander durchgef�hrt werden sollen.
	 */
	protected List<AbstractMathInterpolation> directionInterpolations;
	/**
	 * Speichert die Art der Kombination von Positions- und Richtungs-Animation:
	 * 0: interpolation, 1: fester Blickpunkt, 2: entlang Bewegungsrichtung
	 */
	protected List<Integer> directionInterpolationCases;

	public PositionDirectionAnimation(TimeComponent time, DirectionPositionable animationObject) {
		super(time);
		this.directionInterpolations = new ArrayList<AbstractMathInterpolation>();
		this.directionInterpolationCases = new ArrayList<Integer>();
		this.animationObject = animationObject;
	}

	@Override
	protected void doUpdate() {
		int c = this.directionInterpolationCases.get(this.index);
		if (c == 0) { // beides variabel
			this.animationObject.setPosition(this.positionInterpolations.get(index).getInterpolatedPoint(
					(this.getTime() - this.startTimeActuallInterpolation) / this.interpolationTimes.get(index)));
			GLVertex d = this.directionInterpolations.get(index).getInterpolatedPoint(
					(this.getTime() - this.startTimeActuallInterpolation) / this.interpolationTimes.get(index));
			this.animationObject.setDirection(d);
		} else if (c == 1) { // Blickpunkt ist fest
			this.animationObject.setPosition(this.positionInterpolations.get(index).getInterpolatedPoint(
					(this.getTime() - this.startTimeActuallInterpolation) / this.interpolationTimes.get(index)), true);			
		} else if (c == 2) { // entlang Bewegungsrichtung
			this.animationObject.setPosition(this.positionInterpolations.get(index).getInterpolatedPoint(
					(this.getTime() - this.startTimeActuallInterpolation) / this.interpolationTimes.get(index)));
			this.animationObject.setDirection(this.positionInterpolations.get(index).getInterpolatedDirection(
					(this.getTime() - this.startTimeActuallInterpolation) / this.interpolationTimes.get(index)));			
		}
	}

	@Override
	protected void doEnd() {
		this.animationObject.setPosition(this.getActuallEndPoint());
		int c = this.directionInterpolationCases.get(this.directionInterpolationCases.size() - 1);
		if (c == 0) {
			this.animationObject.setDirection(this.getActuallEndDirection());
		} else if (c == 2) {
			this.animationObject.setDirection(
					this.positionInterpolations.get(this.positionInterpolations.size() - 1).getEndDirection());
		}
		// Fall 1: nichts tun!
	}

	/**
	 * F�gt der Animation je eine Interpolation f�r die Position und die Richtung hinzu.
	 * Position dun Richtung k�nnen so unabh�ngig voneinander animiert werden.
	 * Fall 0.
	 * @param position
	 * @param direction
	 * @param time Dauer der Interpolation
	 */
	public void addInterpolationBothVariable(AbstractMathInterpolation position, AbstractMathInterpolation direction, float time) {
		// falsche Eingaben abfangen (Spr�nge bei �berg�ngen sind erlaubt: Design-Entscheidung!)
		if (time <= 0.0f || position == null || direction == null) {
			throw new IllegalArgumentException();
		}
		this.wholeTime += time;
		this.positionInterpolations.add(position);
		this.directionInterpolations.add(direction);
		this.directionInterpolationCases.add(0);
		this.interpolationTimes.add(time);
	}

	/**
	 * F�gt der Animation eine Interpolation f�r die Position hinzu,
	 * w�hrend die Richtung immer auf den gleichen Punkt "schaut"
	 * (dieser Punkt ist der "End-Blickpunkt" der vorherigen Interpolation).
	 * Eine solche Animation darf nicht als erste ausgef�hrt / hinzugef�gt werden!
	 * Fall 1.
	 * @param position
	 * @param time Dauer der Interpolation
	 */
	public void addInterpolationVariablePosition(AbstractMathInterpolation position, float time) {
		// falsche Eingaben abfangen (Spr�nge bei �berg�ngen sind erlaubt: Design-Entscheidung!)
		if (time <= 0.0f || position == null || this.positionInterpolations.size() < 1) {
			throw new IllegalArgumentException();
		}
		this.wholeTime += time;
		this.positionInterpolations.add(position);
		this.directionInterpolations.add(null);
		this.directionInterpolationCases.add(1);
		this.interpolationTimes.add(time);
	}

	/**
	 * F�gt der Animation eine Interpolation nur f�r die Richtung hinzu,
	 * w�hrend die Position fest ist.
	 * Interne L�sung ist ein Sonderfall von Fall 0.
	 * @param direction
	 * @param time
	 */
	public void addInterpolationVariableDirection(AbstractMathInterpolation direction, float time) {
		this.addInterpolationBothVariable(new ConstantInterpolation(this.getActuallEndPoint(),
				this.getActuallEndDirection()), direction, time);
	}

	/**
	 * F�gt der Animation eine Interpolation f�r die Position hinzu.
	 * Die Blickrichtung bestimmt sich anhand des aktuellen "Richtungs-Vektors" der Bewegungsrichtung
	 * Fall 2.
	 * @param position
	 * @param direction
	 * @param time Dauer der Interpolation
	 */
	public void addInterpolationAlong(AbstractMathInterpolation position, float time) {
		// falsche Eingaben abfangen (Spr�nge bei �berg�ngen sind erlaubt: Design-Entscheidung!)
		if (time <= 0.0f || position == null) {
			throw new IllegalArgumentException();
		}
		this.wholeTime += time;
		this.positionInterpolations.add(position);
		this.directionInterpolations.add(null);
		this.directionInterpolationCases.add(2);
		this.interpolationTimes.add(time);
	}

	/**
	 * F�gt der Animation eine konstante Pause hinzu, also einen Zeitraum, w�hrend dem sich Position und Richtung
	 * nicht �ndern.
	 * @param pauseTime
	 */
	public void addInterpolationPause(float pauseTime) {
		this.addInterpolationBothVariable(new ConstantInterpolation(this.getActuallEndPoint(),
				this.getActuallEndDirection()),
				new ConstantInterpolation(this.getActuallEndDirection()), pauseTime);
	}

	@Override
	public String toString() {
		return "[" + positionInterpolations.toString() + "]";
	}

	@Override
	public void startAnimation() {
		super.startAnimation();
		this.animationObject.setPosition(this.positionInterpolations.get(0).getStartPoint().clone());
	}

	@Override
	public GLVertex getActuallEndDirection() {
		if (this.directionInterpolationCases.size() < 1) {
			throw new IllegalStateException("Es wurde noch keine Interpolation hinzugef�gt!");
		}
		int c = this.directionInterpolationCases.get(this.directionInterpolationCases.size() - 1);
		if (c == 0) { // 2 unabh�ngige Interpolationen
			return this.directionInterpolations.get(this.directionInterpolations.size() - 1).getEndPoint();
		} else if (c == 1) { // fester Blickpunkt
			throw new IllegalStateException("geht gerade nicht: ist Ani mit const. Blick!"); // TODO: beseitigen!!
		} else if (c == 2) { // Blick entlang der Bewegungsrichtung!
			return super.getActuallEndDirection();
		}
		throw new IllegalStateException("Dieser Fehler darf nie auftreten: Programmierfehler!");
	}
}
