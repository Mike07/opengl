package falk_guenther_meier.model.animation;

import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.observers.AnimationStatusObservable;
import falk_guenther_meier.model.observers.AnimationStatusObserver;
import falk_guenther_meier.model.time.Timable;
import falk_guenther_meier.model.time.TimeComponent;

/**
 * Abstrakte Oberklasse aller Animationen.
 * Verwaltet Beobachter von Animationen sowie die Zust�nde einer Animation.
 * @author Johannes
 */
public abstract class AbstractAnimation implements AnimationStatusObservable, Timable, AnimationFunctions,
		AnimationStatusObserver {
	/**
	 * Zeitgeber
	 */
	private TimeComponent time;
	/**
	 * Liste aller Beobachter dieser Animation
	 */
	private List<AnimationStatusObserver> observer;
	/**
	 * Speichert, ob eine gestartete(!) Animationen gerade ausgef�hrt (==true) oder pausiert (==false) wird.
	 */
	private boolean active;
	/**
	 * Speichert, ob eine Animation gestartet (==true) ist oder gestoppt (==false) ist.
	 */
	private boolean on;
	/**
	 * Speichert die Zeit, die diese Animation durch Pausen oder einen sp�teren Start der absoluten "Weltzeit"
	 * hinter h�ngt.
	 */
	private float resumeSeconds;
	/**
	 * Speichert den absoluten Zeitpunkt, an dem die Animation pausiert wurde.
	 */
	private float helpTime;
	/**
	 * Speichert, ob die Animation schleifenartig immer von vorne durchgef�hrt werden soll.
	 */
	private boolean loop;

	public AbstractAnimation(TimeComponent time) {
		this.time = time;
		this.observer = new ArrayList<AnimationStatusObserver>();
		this.on = false;
		this.active = false;
		this.resumeSeconds = 0.0f;
		this.loop = false;
		this.addAnimationStatusObserver(this);
	}

	@Override
	public final void animate() {
		if (this.active && this.on) {
			this.doAnimation();
		}
	}

	protected abstract void doAnimation();

	/**
	 * Liefert die Zeit in Bezug auf den aktuellen Status der Animation.
	 * Ber�cksichtigt also Startzeit und Pausezeiten der Animation.
	 * @return
	 */
	@Override
	public final float getTime() {
		if (!this.on) { // Animation wurde nicht gestartet
			return 0.0f;
		}
		if (this.active) { // Animation l�uft gerade
			return this.time.getTime() - this.resumeSeconds;
		}
		return this.helpTime - this.resumeSeconds; // Animation pausiert gerade
	}

	public final float getDiffToLastTime() { // TODO: wird diese Methode irgendwann mal gebraucht ?!
		if (!this.on) { // Animation wurde nicht gestartet
			return 0.0f;
		}
		if (this.active) { // Animation l�uft gerade
			return this.time.getDiffToLastTime();
		}
		return 0.0f; // Animation pausiert gerade
	}

	public final boolean isLoop() {
		return loop;
	}

	public final void setLoop(boolean loop) {
		this.loop = loop;
	}

	@Override
	public final void addAnimationStatusObserver(AnimationStatusObserver o) {
		this.observer.add(o);
	}

	@Override
	public final void removeAnimationStatusObserver(AnimationStatusObserver o) {
		this.observer.remove(o);
	}

	@Override
	public final List<AnimationStatusObserver> getAnimationStatusObservers() {
		return this.observer;
	}

	@Override
	public void updateStartOfAnimation(AnimationStatusObservable changed) {
		// bleibt leer
	}

	@Override
	public void updateEndOfAnimation(AnimationStatusObservable changed) {
		// TODO: ist evtl. zeitlich ein kleines bischen ungenau: eigentlich m�sste nur die Gesamtzeit der Animation hinzu addiert werden!
		if (this.loop) {
//			this.resumeSeconds = this.resumeSeconds - this.time.getTime() + this.DAUER (existiert aber nicht!);
			this.startAnimation();
		}
	}

	@Override
	public void updatePause(AnimationStatusObservable changed) {
		// bleibt leer
	}

	@Override
	public void updateResume(AnimationStatusObservable changed) {
		// bleibt leer
	}

	@Override
	public final void pauseAnimation() {
		if (!this.on || !this.active) {
			return;
		}
		this.active = false;
		this.helpTime = this.time.getTime();
		for (AnimationStatusObserver observer : this.getAnimationStatusObservers()) {
			observer.updatePause(this);
		}
	}

	@Override
	public final void resumeAnimation() {
		if (!this.on || this.active) {
			return;
		}
		this.active = true;
		this.resumeSeconds += this.time.getTime() - this.helpTime;
		this.helpTime = 0.0f;
		for (AnimationStatusObserver observer : this.getAnimationStatusObservers()) {
			observer.updateResume(this);
		}
	}

	@Override
	public void startAnimation() {
		if (this.on) {
			return;
		}
		this.on = true;
		this.active = true;
		this.helpTime = 0.0f;
		this.resumeSeconds = this.time.getTime();
		for (AnimationStatusObserver observer : this.getAnimationStatusObservers()) {
			observer.updateStartOfAnimation(this);
		}
	}

	@Override
	public final void stopAnimation() {
		if (!this.on) {
			return;
		}
		this.on = false;
		this.active = false;
		this.helpTime = 0.0f;
		this.resumeSeconds = this.time.getTime();
		for (AnimationStatusObserver observer : this.getAnimationStatusObservers()) {
			observer.updateEndOfAnimation(this);
		}
	}
}
