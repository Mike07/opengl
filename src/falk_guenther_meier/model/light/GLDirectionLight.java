package falk_guenther_meier.model.light;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;

/**
 * Direktionale Lichtquelle, die aber f�r die ganze Szene gilt (Sonne).
 * @author Michael, Johannes
 */
public class GLDirectionLight extends GLAbstractLight {
	private GLVertex direction;

	public GLDirectionLight(int lightNumber, GLVertex direction) {
		super(lightNumber);
		this.direction = direction;
	}

	public GLDirectionLight(int lightNumber, GLVertex direction, GLVertex ambient, GLVertex diffuse,
			GLVertex specular) {
		this(lightNumber, direction, ambient, diffuse, specular, false);
	}

	public GLDirectionLight(int lightNumber, GLVertex direction, GLVertex ambient, GLVertex diffuse,
			GLVertex specular, boolean colorMaterial) {
		super(lightNumber, ambient, diffuse, specular, colorMaterial);
		this.direction = direction;
		this.direction.setW(0.0f);
	}

	public GLVertex getDirection() {
		return direction;
	}

	public void setDirection(GLVertex position) {
		this.direction = position;
		this.direction.setW(0.0f);
	}

	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		gl.glLightfv(this.getLightNumber(), GL2.GL_POSITION, direction.getValues(), 0);
	}

	public void endGL(GL2 gl) {
		super.endGL(gl);
	}
}
