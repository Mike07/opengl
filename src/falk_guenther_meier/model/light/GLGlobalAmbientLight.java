package falk_guenther_meier.model.light;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;

/**
 * Lichtquelle f�r globales ambientes Licht, die f�r die ganze Szene gilt.
 * @author Johannes
 */
public class GLGlobalAmbientLight extends GLAbstractLight {
	public GLGlobalAmbientLight(GLVertex ambient) {
		super(0, ambient, new GLVertex(), new GLVertex());
	}

	public void beginGL(GL2 gl) {
		gl.glEnable(GL2.GL_LIGHTING);
		gl.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, this.getAmbient().getValues(), 0);
	}

	public void endGL(GL2 gl) {
		gl.glDisable(GL2.GL_LIGHTING);
	}
}
