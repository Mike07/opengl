package falk_guenther_meier.model.light;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.observers.TranslateObservable;
import falk_guenther_meier.model.observers.TranslateObserver;
import falk_guenther_meier.model.platform.DirectionPositionable;

/**
 * Spotlight-Lichtquelle, die aber f�r die ganze Szene gilt.
 * @author Johannes, Michael
 */
public class GLSpotLight extends GLAbstractLight implements TranslateObservable, DirectionPositionable {
	private GLVertex position;
	private GLVertex direction;
	private float spotCutoff;
	private float spotExponent;
	private float constantAttenuation;
	private float linearAttenuation;
	private float quadraticAttenuation;

	private List<TranslateObserver> translateObserver;

	public GLSpotLight(int lightNumber, GLVertex position, GLVertex direction) {
		super(lightNumber);
		this.position = position;
		this.direction = direction;
		this.init();
	}

	public GLSpotLight(int lightNumber, GLVertex position, GLVertex direction, float cutoff, float exponent) {
		this(lightNumber, position, direction);
		this.spotCutoff = cutoff;
		this.spotExponent = exponent;
	}

	public GLSpotLight(int lightNumber, GLVertex position, GLVertex direction, GLVertex ambient, GLVertex diffuse,
			GLVertex specular, boolean colorMaterial) {
		super(lightNumber, ambient, diffuse, specular, colorMaterial);
		this.position = position;
		this.direction = direction;
		this.init();
	}

	private void init() {
		translateObserver = new ArrayList<TranslateObserver>();
		constantAttenuation = 1.0f; // angegeben sind die Standard-Werte von OGL
		linearAttenuation = 0.0f;
		quadraticAttenuation = 0.0f;		
		this.spotCutoff = 45.0f; // OGL-Standard-Wert ist 180.0
		this.spotExponent = 0.0f; // Standard ist 0.0 ?!
	}

	@Override
	public GLVertex getPosition() {
		return position;
	}

	@Override
	public void setPosition(GLVertex position) {
		this.setPosition(position, false);
	}

	@Override
	public void setPosition(GLVertex newPosition, boolean changeDirection) {
		if (changeDirection) {
			this.direction = GLVertex.sub(GLVertex.add(this.position, this.direction), newPosition);
		}
		this.position = newPosition;
		for (TranslateObserver o : translateObserver) {
			o.updateAbsolutePosition(this, newPosition);
		}
	}

	public void translate(float x, float y, float z) {
		this.position.mult(GLMatrix.createTranslateMatrix(x, y, z));
		for (TranslateObserver o : translateObserver) {
			o.updateTranslate(this, x, y, z);
		}
	}
	
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		gl.glLightfv(this.getLightNumber(), GL2.GL_POSITION, this.position.getValues(), 0);

		gl.glLightf(this.getLightNumber(), GL2.GL_SPOT_CUTOFF, this.spotCutoff);
		gl.glLightf(this.getLightNumber(), GL2.GL_SPOT_EXPONENT, this.spotExponent);
		gl.glLightfv(this.getLightNumber(), GL2.GL_SPOT_DIRECTION, this.direction.getValues(), 0);

		/*
		 * Attenuation = Abschw�chung des Lichtes mit der Entfernung
		 * Faktor: 1 / (k_c + k_l * d + k_q * d^2)
		 * d: Distanz zwischen der Position der Lichtquelle und dem Vertex
		 * k_c: GL_CONSTANT_ATTENUATION
		 * k_l: GL_LINEAR_ATTENUATION
		 * k_q: GL_QUADRATIC_ATTENUATION
		 * je gr��er k_c, k_l, k_q gew�hlt werden, desto gr��er ist die Abschw�chung des Lichts
		 * Quelle: http://www.jogl.info/tutorial/lektion3.htm (03.01.2013)
		 */
		gl.glLightf(this.getLightNumber(), GL2.GL_CONSTANT_ATTENUATION, this.constantAttenuation);
		gl.glLightf(this.getLightNumber(), GL2.GL_LINEAR_ATTENUATION, this.linearAttenuation);
		gl.glLightf(this.getLightNumber(), GL2.GL_QUADRATIC_ATTENUATION, this.quadraticAttenuation);
	}

	public void endGL(GL2 gl) {
		super.endGL(gl);
	}

	@Override
	public GLVertex getDirection() {
		return direction;
	}

	@Override
	public void setDirection(GLVertex direction) {
		this.direction = direction;
	}

	public float getSpotCutoff() {
		return spotCutoff;
	}

	public void setSpotCutoff(float spotCutoff) {
		this.spotCutoff = spotCutoff;
	}

	public float getSpotExponent() {
		return spotExponent;
	}

	public void setSpotExponent(float spotExponent) {
		this.spotExponent = spotExponent;
	}

	public float getConstantAttenuation() {
		return constantAttenuation;
	}

	public void setConstantAttenuation(float constantAttenuation) {
		this.constantAttenuation = constantAttenuation;
	}

	public float getLinearAttenuation() {
		return linearAttenuation;
	}

	public void setLinearAttenuation(float linearAttenuation) {
		this.linearAttenuation = linearAttenuation;
	}

	public float getQuadraticAttenuation() {
		return quadraticAttenuation;
	}

	public void setQuadraticAttenuation(float quadraticAttenuation) {
		this.quadraticAttenuation = quadraticAttenuation;
	}

	public void setAttenuation(float constant, float linear, float quadratic) {
		this.constantAttenuation = constant;
		this.linearAttenuation = linear;
		this.quadraticAttenuation = quadratic;
	}

	@Override
	public final void addTranslateObserver(TranslateObserver o) {
		translateObserver.add(o);
	}

	@Override
	public final void removeTranslateObserver(TranslateObserver o) {
		translateObserver.remove(o);
	}

	@Override
	public List<TranslateObserver> getTranslateObservers() {
		return translateObserver;
	}
}
