package falk_guenther_meier.model.light;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.GLable;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.observers.RotateObservable;
import falk_guenther_meier.model.observers.RotateObserver;
import falk_guenther_meier.model.observers.ScaleObservable;
import falk_guenther_meier.model.observers.ScaleObserver;
import falk_guenther_meier.model.observers.TranslateObservable;
import falk_guenther_meier.model.observers.TranslateObserver;

public abstract class GLAbstractLight implements RotateObserver, ScaleObserver, TranslateObserver, GLable {
	private int lightNumber;
	private GLVertex ambient;
	private GLVertex diffuse;
	private GLVertex specular;
	// Bei true, wird zusätzlich die Farbe der Objekte verwendet, bei false nur Beleuchtung und Texture
	private boolean useColorMaterial;
	private boolean lightOn;

	public GLAbstractLight(int lightNumber) {
		this(lightNumber, new GLVertex(0.5f, 0.5f, 0.5f), new GLVertex(0.5f, 0.5f, 0.5f),
				new GLVertex(0.5f, 0.5f, 0.5f), true); // Default-Werte
	}

	public GLAbstractLight(int lightNumber, GLVertex ambient, GLVertex diffuse, GLVertex specular) {
		this(lightNumber, ambient, diffuse, specular, true);
	}

	public GLAbstractLight(int lightNumber, GLVertex ambient, GLVertex diffuse, GLVertex specular,
			boolean colorMaterial) {
		this.lightOn = true;
		this.lightNumber = lightNumber;
		this.ambient = ambient;
		this.diffuse = diffuse;
		this.specular = specular;
		this.useColorMaterial = colorMaterial;
	}

	@Override
	public void beginGL(GL2 gl) {
		gl.glEnable(GL2.GL_LIGHTING);
		if (this.lightOn) {
			gl.glEnable(lightNumber);
		} else {
			gl.glDisable(lightNumber);
		}
		if (useColorMaterial) {
			gl.glEnable(GL2.GL_COLOR_MATERIAL);
		} else {
			gl.glDisable(GL2.GL_COLOR_MATERIAL);
		}
		gl.glLightfv(lightNumber, GL2.GL_AMBIENT, ambient.getValues(), 0);
		gl.glLightfv(lightNumber, GL2.GL_DIFFUSE, diffuse.getValues(), 0);
		gl.glLightfv(lightNumber, GL2.GL_SPECULAR, specular.getValues(), 0);
	}

	@Override
	public void endGL(GL2 gl) {
		gl.glDisable(lightNumber);
		gl.glDisable(GL2.GL_LIGHTING);
	}

	public boolean isLightOn() {
		return lightOn;
	}

	public void setLightOn(boolean lightOn) {
		this.lightOn = lightOn;
	}

	public int getLightNumber() {
		return lightNumber;
	}

	public void setLightNumber(int lightNumber) {
		this.lightNumber = lightNumber;
	}

	public boolean isSetColorMaterial() {
		return useColorMaterial;
	}

	public void setUseColorMaterial(boolean useColorMaterial) {
		this.useColorMaterial = useColorMaterial;
	}

	public GLVertex getAmbient() {
		return ambient;
	}

	public void setAmbient(GLVertex ambient) {
		this.ambient = ambient;
	}

	public GLVertex getDiffuse() {
		return diffuse;
	}

	public void setDiffuse(GLVertex diffuse) {
		this.diffuse = diffuse;
	}

	public GLVertex getSpecular() {
		return specular;
	}

	public void setSpecular(GLVertex specular) {
		this.specular = specular;
	}

	@Override
	public void updateTranslate(TranslateObservable changed, float x, float y, float z) {
		// bleibt leer
	}

	@Override
	public void updateTranslate(TranslateObservable changed, GLVertex v) {
		// bleibt leer
	}

	@Override
	public void updateAbsolutePosition(TranslateObservable changed, GLVertex position) {
		// bleibt leer
	}

	@Override
	public void updateScale(ScaleObservable changed, float x, float y, float z) {
		// bleibt leer
	}

	@Override
	public void updateRotate(RotateObservable changed, float x, float y, float z) {
		// bleibt leer
	}

	@Override
	public void updateRotate(RotateObservable changed, GLVertex point, float x, float y, float z) {
		// bleibt leer
	}

	@Override
	public void updateRotate(RotateObservable changed, GLVertex first, GLVertex second, float angle) {
		// bleibt leer
	}	
}
