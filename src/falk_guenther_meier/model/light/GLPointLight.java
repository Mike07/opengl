package falk_guenther_meier.model.light;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.observers.TranslateObservable;
import falk_guenther_meier.model.observers.TranslateObserver;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.platform.Positionable;

/**
 * Punkt-Lichtquelle, die aber f�r die ganze Szene gilt.
 * @author Michael, Johannes
 */
public class GLPointLight extends GLAbstractLight implements TranslateObservable, Positionable {
	private GLObject lightObject;
	private GLVertex position;
	private float constantAttenuation;
	private float linearAttenuation;
	private float quadraticAttenuation;
	private boolean useLightObject;

	private List<TranslateObserver> translateObserver;

	public GLPointLight(int lightNumber, GLVertex position) {
		super(lightNumber);
		this.position = position;
		this.useLightObject = true;
		this.init();
	}

	public GLPointLight(int lightNumber, GLVertex position, GLVertex ambient, GLVertex diffuse,
			GLVertex specular) {
		this(lightNumber, position, ambient, diffuse, specular, false);
	}
	
	public GLPointLight(int lightNumber, GLVertex position, GLVertex ambient, GLVertex diffuse,
			GLVertex specular, boolean colorMaterial) {
		super(lightNumber, ambient, diffuse, specular, colorMaterial);
		this.position = position;
		this.position.setW(1.0f);
		this.init();
	}

	private void init() {
		translateObserver = new ArrayList<TranslateObserver>();
		constantAttenuation = 1.0f; // angegeben sind die Standard-Werte von OGL
		linearAttenuation = 0.0f;
		quadraticAttenuation = 0.0f;		
	}

	public GLPointLight(int lightNumber, GLObject lightObject, GLVertex position, GLVertex ambient,
			GLVertex diffuse, GLVertex specular) {
		this(lightNumber, position, ambient, diffuse, specular);
		this.lightObject = lightObject;
		// Objekt wird an die Position des Lichtes verschoben
		// geht nur bei Positionslichtern
		if (this.lightObject != null) {
			this.lightObject.setPosition(position);
		}
	}

	public GLObject getLightObject() {
		return lightObject;
	}

	public void setLightObject(GLObject lightObject) {
		if (lightObject == null) {
			throw new IllegalArgumentException();
		}
		this.lightObject = lightObject;
		this.lightObject.setPosition(position);
	}

	@Override
	public GLVertex getPosition() {
		return position;
	}

	@Override
	public void setPosition(GLVertex position) {
		this.position = position;
		this.position.setW(1.0f);
		for (TranslateObserver o : translateObserver) {
			o.updateAbsolutePosition(this, position);
		}
		if (lightObject != null) {
			lightObject.setPosition(position);
		}
	}

	public void translate(float x, float y, float z) {
		this.position.mult(GLMatrix.createTranslateMatrix(x, y, z));
		this.position.setW(1.0f);
		for (TranslateObserver o : translateObserver) {
			o.updateTranslate(this, x, y, z);
		}
		if (lightObject != null) {
			lightObject.translate(x, y, z);
		}
	}
	
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		gl.glLightfv(this.getLightNumber(), GL2.GL_POSITION, position.getValues(), 0);

		// Attenuation = Abschw�chung des Lichtes mit der Entfernung
		// Faktor: 1 / (k_c + k_l * d + k_q * d^2)
		gl.glLightf(this.getLightNumber(), GL2.GL_CONSTANT_ATTENUATION, this.constantAttenuation);
		gl.glLightf(this.getLightNumber(), GL2.GL_LINEAR_ATTENUATION, this.linearAttenuation);
		gl.glLightf(this.getLightNumber(), GL2.GL_QUADRATIC_ATTENUATION, this.quadraticAttenuation);

		if (lightObject != null && position.getW() == 1.0f && useLightObject) {
			lightObject.beginGL(gl);
		}
	}

	public void endGL(GL2 gl) {
		if (lightObject != null && position.getW() == 1.0f) {
			lightObject.endGL(gl);
		}
		super.endGL(gl);
	}
	
	public boolean isUseLightObject() {
		return useLightObject;
	}

	public void setUseLightObject(boolean useLightObject) {
		this.useLightObject = useLightObject;
	}

	public float getConstantAttenuation() {
		return constantAttenuation;
	}

	public void setConstantAttenuation(float constantAttenuation) {
		this.constantAttenuation = constantAttenuation;
	}

	public float getLinearAttenuation() {
		return linearAttenuation;
	}

	public void setLinearAttenuation(float linearAttenuation) {
		this.linearAttenuation = linearAttenuation;
	}

	public float getQuadraticAttenuation() {
		return quadraticAttenuation;
	}

	public void setQuadraticAttenuation(float quadraticAttenuation) {
		this.quadraticAttenuation = quadraticAttenuation;
	}

	public void setAttenuation(float constant, float linear, float quadratic) {
		this.constantAttenuation = constant;
		this.linearAttenuation = linear;
		this.quadraticAttenuation = quadratic;
	}

	@Override
	public final void addTranslateObserver(TranslateObserver o) {
		translateObserver.add(o);
	}

	@Override
	public final void removeTranslateObserver(TranslateObserver o) {
		translateObserver.remove(o);
	}

	@Override
	public List<TranslateObserver> getTranslateObservers() {
		return translateObserver;
	}
}
