package falk_guenther_meier.model.particle;

import falk_guenther_meier.model.math.GLVertex;

/**
 * Repräsentiert ein einziges Partikel.
 * @author Johannes
 */
public final class Particle {
	private float liveTime;
	private float decay;
	private GLVertex position;
	private GLVertex speed;

	public Particle(float liveTime, float decay, GLVertex position, GLVertex speed) {
		super();
		this.liveTime = liveTime;
		this.decay = decay;
		this.position = position;
		this.speed = speed;
	}

	public Particle(float liveTime, float decay) {
		this(liveTime, decay, new GLVertex(), new GLVertex());
	}

	public boolean isAlive() {
		return this.liveTime > 0.0f;
	}

	public void kill() {
		this.liveTime = 0.0f;
	}

	public void evolve() {
		this.liveTime = - this.decay;
		this.position.add(this.speed);
	}

	public float getDecay() {
		return decay;
	}

	public void setDecay(float decay) {
		this.decay = decay;
	}

	public GLVertex getSpeed() {
		return speed;
	}

	public void setSpeed(GLVertex speed) {
		this.speed = speed;
	}

	public float getLiveTime() {
		return liveTime;
	}

	public GLVertex getPosition() {
		return position;
	}
}
