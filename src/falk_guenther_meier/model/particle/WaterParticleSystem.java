package falk_guenther_meier.model.particle;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.math.GLVertex;

/**
 * einfache Wasser-Font�ne
 * Quelle: Martin Hering, Partikelsysteme mit Java und OpenGL - Ein Tutorial, 2005
 * @author Johannes
 */
public class WaterParticleSystem {
	private List<Particle> particles;
	private float colorFaktor;
	
	public WaterParticleSystem() {
		this(500);
	}
	
	public WaterParticleSystem(int count) {
		this.particles = new ArrayList<Particle>(count);
		for (int i = 0; i < count; i++) {
			this.particles.add(this.createParticle());
		}
		colorFaktor = 4.0f;
	}

	private Particle createParticle() {
		return new Particle(1.0f, 0.001f,
				new GLVertex(0.0f, 0.0f, 0.0f),
				new GLVertex(0.001f - (float) Math.random() / 500.0f,
						0.01f - (float) Math.random() / 1000.0f, 0.001f - (float) Math.random() / 500.0f));
	}

	public void setColorFaktor(float faktor) {
		this.colorFaktor = faktor;
	}
	
	public void beginGL(GL2 gl) {
		GLMaterial m;
		for (int i = 0; i < particles.size(); i++) {
			// alte Partikel durch neue ersetzen
			if (this.particles.get(i).getPosition().getY() < 0.0f) {
				this.particles.set(i, this.createParticle());
			}
			// Partikel fortbewegen
			Particle p = this.particles.get(i);
			m = GLMaterial.createColor(new GLVertex(p.getPosition().getY() * colorFaktor, 0.0f, 0.0f));
			p.setSpeed(new GLVertex(p.getSpeed().getX(), p.getSpeed().getY() - 0.0004f, p.getSpeed().getZ()));
			p.evolve();
			// Partikel zeichnen
			gl.glColor3f(p.getPosition().getY() * colorFaktor, 0.0f, 0.0f);
			m.toGL(gl);
			gl.glBegin(GL2.GL_QUADS);
			gl.glVertex3f(p.getPosition().getX() - 0.002f, p.getPosition().getY() + 0.002f, p.getPosition().getZ());
			gl.glVertex3f(p.getPosition().getX() - 0.002f, p.getPosition().getY() - 0.002f, p.getPosition().getZ());
			gl.glVertex3f(p.getPosition().getX() + 0.002f, p.getPosition().getY() - 0.002f, p.getPosition().getZ());
			gl.glVertex3f(p.getPosition().getX() + 0.002f, p.getPosition().getY() + 0.002f, p.getPosition().getZ());
			gl.glEnd();
		}
	}
}
