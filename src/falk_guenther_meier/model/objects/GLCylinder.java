package falk_guenther_meier.model.objects;

import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.util.approximation.ObjectApproximation;
import falk_guenther_meier.util.approximation.structures.CylinderStructure;

/**
 * Zylinder, dessen Mittelpunkt (0,0,0) ist und sich parallel zur y-Achse erstreckt ("er steht aufrecht").
 * @author Johannes
 */
public class GLCylinder extends GLComplexObject {
	public GLCylinder(GLVertex position, float radius, float height, int iterations) {
		this(radius, height, iterations);
		this.translate(position.getX(), position.getY(), position.getZ());
		this.setReferencePoint(position);
	}

	public GLCylinder(float radius, float height, int iterations) {
		super();
		this.init(radius, height, iterations);		
	}

	private void init(float radius, float height, int iterations) {
		this.position = new GLVertex(0.0f, 0.0f, 0.0f);
		List<GLTriangle> list = new ArrayList<GLTriangle>();

		float length = (float) (2.0f * Math.sqrt(radius * radius / 2.0f));
		GLCuboid cube = new GLCuboid(this.position, length, height, length);
		list.addAll(((GLRectangle) cube.getRectFront()).splitToTriangles());
		list.addAll(((GLRectangle) cube.getRectRear()).splitToTriangles());
		list.addAll(((GLRectangle) cube.getRectLeft()).splitToTriangles());
		list.addAll(((GLRectangle) cube.getRectRight()).splitToTriangles());

		List<GLTriangle> newList = ObjectApproximation.approximateIterations(list, new CylinderStructure(radius), iterations);
		GLVertex normal;
		for (GLTriangle tri : newList) {
			normal = tri.getNodes()[0].clone();
			normal.setY(0.0f);
			normal.normalize();
			tri.getNodes()[0].setNormal(normal);

			normal = tri.getNodes()[1].clone();
			normal.setY(0.0f);
			normal.normalize();
			tri.getNodes()[1].setNormal(normal);

			normal = tri.getNodes()[2].clone();
			normal.setY(0.0f);
			normal.normalize();
			tri.getNodes()[2].setNormal(normal);
		}
		this.addAll(newList);

		// oberen Kreis
		List<GLVertex> vertexes = new ArrayList<GLVertex>();
		for (GLTriangle t : newList) {
			if (t.getNodes()[0].getY() == height / 2.0f) {
				vertexes.add(t.getNodes()[0].clone());
			}
			if (t.getNodes()[1].getY() == height / 2.0f) {
				vertexes.add(t.getNodes()[1].clone());
			}
			if (t.getNodes()[2].getY() == height / 2.0f) {
				vertexes.add(t.getNodes()[2].clone());
			}
		}
		for (GLVertex v : vertexes) {
			v.setY(0.0f);
		}
		GLCircle circle = new GLCircle(this.position.clone(), vertexes, new GLVertex(0.0f, 2 * height + 1.0f, 0.0f),
				iterations - 2);
		circle.translate(0.0f, height / 2.0f, 0.0f);
		this.add(circle);

		// unteren Kreis
		vertexes = new ArrayList<GLVertex>();
		for (GLTriangle t : newList) {
			if (t.getNodes()[0].getY() == -height / 2.0f) {
				vertexes.add(t.getNodes()[0].clone());
			}
			if (t.getNodes()[1].getY() == -height / 2.0f) {
				vertexes.add(t.getNodes()[1].clone());
			}
			if (t.getNodes()[2].getY() == -height / 2.0f) {
				vertexes.add(t.getNodes()[2].clone());
			}
		}
		for (GLVertex v : vertexes) {
			v.setY(0.0f);
		}
		circle = new GLCircle(this.position.clone(), vertexes, new GLVertex(0.0f, -2 * height -1.0f, 0.0f),
				iterations - 2);
		circle.translate(0.0f, -height / 2.0f, 0.0f);
		this.add(circle);

		this.setReferencePoint(position);
	}
}
