package falk_guenther_meier.model.objects;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLPrimitiveObject;

public class GLTriangleStrip extends GLPrimitiveObject {
	public GLTriangleStrip(GLVertex... vertices) {
		super();
		nodes = vertices;
		position = new GLVertex(0.0f, 0.0f, 0.0f);
	}
	
	/**
	 * Teilt das Rechteck in ein Dreiecks-Gitter auf. (Tessellation)
	 * Der Bedarf an Vertices wird möglichst gering gehalten.
	 * Ergebnis: Das Rechteck als TriangleStrip mit count*count*2 Dreiecken
	 * @param count die Anzahl, wie oft vertikal und horizontal geteilt werden soll
	 */
	public GLTriangleStrip(GLRectangle rect, int count) {
		super();
		int colCount = count;
		int rowCount = count;
		
		GLVertex[] rectNodes = rect.getNodes();
		
		// see http://www.learnopengles.com/wordpress/wp-content/uploads/2012/05/ibo_with_degenerate_triangles.png
		GLVertex down = GLVertex.sub(rectNodes[0], rectNodes[3]);
		down.mult(1.0f/((float)rowCount));                 // Vektor nach unten im Mini-Quadrat (rectWidth/colCount)*(rectHeight/rowCount)
		
		GLVertex right = GLVertex.sub(rectNodes[2], rectNodes[3]); 
		right.mult(1.0f/((float)colCount));                // Vektor nach rechts im Mini-Quadrat
		
		GLVertex upRight = GLVertex.sub(right, down);      // Vektor nach rechts-oben im Mini-Quadrat
		
		GLVertex pos = rectNodes[3].clone();
		GLVertex firstInRow = pos.clone();
		List<GLVertex> vertices = new ArrayList<GLVertex>();
		for (int y = 0; y < rowCount; y++) {
			vertices.add(pos.clone());
			for (int x = 0; x < colCount; x++) {
				pos.add(down);
				vertices.add(pos.clone());
				pos.add(upRight);
				vertices.add(pos.clone());
			}
			
			pos.add(down);
			vertices.add(pos.clone());
			boolean doDegenerateForNewRow = y < rowCount - 1;
			if (doDegenerateForNewRow) {
				vertices.add(pos.clone()); // Zeile abschließen -> 2x auf den letzten Punkt setzen
			}
			firstInRow.add(down);
			pos = firstInRow.clone();
			if (doDegenerateForNewRow) {
				vertices.add(pos.clone()); // neue Zeile beginnen -> 2x auf den ersten Punkt setzen
			}
		}
		//                 Punkte einer Zeile  + Abschluss- und Beginnpunkte (Degenerate)
		int n = rowCount * (2 * colCount + 2)  +         2 * (rowCount - 1);
		nodes = vertices.toArray(new GLVertex[n]);
		position = new GLVertex(0.0f, 0.0f, 0.0f);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		beginDraw2(gl);
	}
	
	protected void beginDraw2(GL2 gl) {
		gl.glBegin(GL2.GL_TRIANGLE_STRIP);
		for (GLVertex v : nodes) {
			v.toGL(gl, position); 
		}
		gl.glEnd();
	}
}
