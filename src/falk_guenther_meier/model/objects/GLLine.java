package falk_guenther_meier.model.objects;

import java.nio.FloatBuffer;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLPrimitiveObject;

public class GLLine extends GLPrimitiveObject {
	
	private float lineWidth = 1;

	public GLLine(GLVertex a, GLVertex b) {
		super();
		nodes = new GLVertex[2];
		nodes[0] = a;
		nodes[1] = b;
		position = new GLVertex(0.0f, 0.0f, 0.0f);
	}
	
	public GLLine(GLVertex a, GLVertex b, float lineWidth) {
		this(a, b);
		this.lineWidth = lineWidth;
	}
	
	public final float getLineWidth() {
		return lineWidth;
	}

	public final void setLineWidth(float lineWidth) {
		this.lineWidth = lineWidth;
	}

	@Override
	public final void beginGL(GL2 gl) {
		super.beginGL(gl);
		FloatBuffer initLineWidth = FloatBuffer.allocate(1);
		gl.glGetFloatv(GL2.GL_LINE_WIDTH, initLineWidth);
		gl.glLineWidth(lineWidth);
		gl.glBegin(GL2.GL_LINES);
			nodes[0].toGL(gl, position);
			nodes[1].toGL(gl, position);
		gl.glEnd();
		gl.glLineWidth(initLineWidth.get()); // wieder auf Standard setzen
	}
}
