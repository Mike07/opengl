package falk_guenther_meier.model.objects;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.textures.GLTexGrid;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.approximation.ObjectApproximation;

public class GLHeightMapRectangle extends GLComplexObject {
	/**
	 * Initiale Position: Mittelpunkt ist Ursprung, "Ebene" in x-z-Ebene, sichtbar von "oben"
	 * @param pathOfHighMap Pfad der H�hen-Map
	 * @param xLength Breite des Rechtecks in X-Richtung
	 * @param yLength Breite des Rectecks in Z-Ebene
	 * @param height H�he / gesamte Ausdehnung in Y-Ebene
	 * @param pathOfTexture Pfad der Textur
	 * @param countTextureX so h�ufig soll die Textur in X-Richtung in das Rechteck eingepasst werden
	 */
	public GLHeightMapRectangle(String pathOfHighMap, float xLength, float yLength, float height, String pathOfTexture,
			int countTextureX) {
		super();
		this.position = new GLVertex();
		int[][] map = FileLoader.loadBlackWhitePixelsFromFile(pathOfHighMap);
		float x;
		float y;
		float z;
		GLVertex a = null;
		GLVertex b = null;
		GLVertex c = null;
		GLTriangle tri = null;
		List<GLTriangle> list = new ArrayList<GLTriangle>();

		GLTexture2D rectTexture = null;
		if (pathOfTexture != null && pathOfTexture.length() > 0) {
			File textureFile = FileLoader.getFileFromFilesFolder(pathOfTexture, true);	
			rectTexture = new GLTexture2D(textureFile);
			rectTexture.setTexGrid(new GLTexGrid(map.length - 1, map.length - 1));
		}

		for (int i = 0; i < map.length - 1; i++) {
			for (int j = 0; j < map[i].length - 1; j++) {
				x = (i * xLength / (float) map.length);
				y = map[i][j] * height / 256.0f;
				z = (float) (j * yLength / (float) map[i].length);
				a = new GLVertex(x, y, z);

				x = ((i+1) * xLength / (float) map.length);
				y = map[i+1][j] * height / 256.0f;
				z = (float) (j * yLength / (float) map[i+1].length);
				b = new GLVertex(x, y, z);

				x = ((i+1) * xLength / (float) map.length);
				y = map[i+1][j+1] * height / 256.0f;
				z = (float) ((j+1) * yLength / (float) map[i+1].length);
				c = new GLVertex(x, y, z);

				tri = new GLTriangle(c, b, a, true); // Bilddaten: 0,0 ist oben links!
				tri.setTexture(rectTexture);
//				tri.setTexRect(i, j, 0, false);
				tri.setTexRect(i, j, 1, 2, 3); // 012
				list.add(tri);

				a = a.clone();

				x = (i * xLength / (float) map.length);
				y = map[i][j+1] * height / 256.0f;
				z = (float) ((j+1) * yLength / (float) map[i].length);
				b = new GLVertex(x, y, z);

				c = c.clone();

				tri = new GLTriangle(a, b, c, true);
				tri.setTexture(rectTexture);
//				tri.setTexRect(i, j, 0, true);
				tri.setTexRect(i, j, 3, 0, 1); // 230
				list.add(tri);
			}
		}
		ObjectApproximation.calculateAndSetNormals(list);
		this.addAll(list);
		this.setReferencePoint(position);
		this.translate(- xLength / 2.0f, 0.0f, - yLength / 2.0f);
		this.setReferencePoint(position);
	}
}
