package falk_guenther_meier.model.objects;

import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.util.approximation.ObjectApproximation;
import falk_guenther_meier.util.approximation.structures.SphereStructure;

/**
 * Kugel mit Mittelpunkt in (0,0,0).
 * @author Johannes
 */
public class GLSphere extends GLComplexObject {
	public GLSphere(float radius, int iterations) {
		this(new GLVertex(), radius, iterations);
	}

	public GLSphere(GLVertex position, float radius, int iterations) {
		super();
		this.init(radius, iterations);
		this.translate(position.getX(), position.getY(), position.getZ());
		this.setReferencePoint(position);		
	}

	private void init(float radius, int iterations) {
		this.position = new GLVertex(0.0f, 0.0f, 0.0f);
		List<GLTriangle> list = new ArrayList<GLTriangle>();

		float length = (float) (2.0f * Math.sqrt(radius * radius / 3.0f));
		GLCuboid cube = new GLCuboid(this.position, length);
		for (GLObject rectangle : cube.getObjects()) {
			list.addAll(((GLRectangle) rectangle).splitToTriangles());
		}

		List<GLTriangle> newList = ObjectApproximation.approximateIterations(list, new SphereStructure(radius),
				iterations);
		for (GLTriangle tri : newList) {
			tri.getNodes()[0].setNormal(GLVertex.normalize(tri.getNodes()[0]));
			tri.getNodes()[1].setNormal(GLVertex.normalize(tri.getNodes()[1]));
			tri.getNodes()[2].setNormal(GLVertex.normalize(tri.getNodes()[2]));
		}
		for (GLTriangle t : newList) {
			t.setReferencePoint(position);
			this.add(t);
		}
//		this.addAll(newList);

		this.setReferencePoint(position);		
	}
}