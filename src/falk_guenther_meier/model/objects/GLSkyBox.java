package falk_guenther_meier.model.objects;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.observers.TranslateObservable;
import falk_guenther_meier.model.platform.GLComplexObject;

/**
 * SkyBox: sehr gro�er W�rfel mit innen liegender Textur, dessen Mitte sich immer nach der Kameraposition ausrichtet
 * @author Hanno, Johannes
 */
public class GLSkyBox extends GLComplexObject {
	
	public GLSkyBox(GLCamera camera, float size) {
		init(camera.getPosition(), size, size, size, 0.0f);
		camera.addTranslateObserver(this);
	}
	
	/**
	 * @param overlap z.B. 0.008f damit bei SkyBox-Bildern in einem Bild keine schwarzen R�nder angezeigt werden.
	 * 	Dies w�rde dann die Breite und H�he der Rechtecke um den Faktor 1.008f strecken.
	 */
	public GLSkyBox(GLCamera camera, float size, float overlap) {
		init(camera.getPosition(), size, size, size, overlap);
		camera.addTranslateObserver(this);
	}
	
	protected void init(GLVertex center, float width, float height, float depth, float overlap) {
		position = center;

		// sonst werden schwarze und wei�e Kanten angezeigt -> ist mit GL_CLAMP_TO_EDGE leider auch nicht wegzubekommen.
		//float overlap = 0.008f;
		
		GLRectangle rectBase = new GLRectangle( new GLVertex(), width*(1+overlap), height*(1+overlap));
		rectBase.translate(GLVertex.addValues(center, 0, 0, -depth / 2.0f));
		// GLVertex.addValues(center, 0, 0, d)
		rectBase.setReferencePoint(center);
		
		GLRectangle rect = rectBase.clone(); // front
		rect.rotate(0, 180, 0);
		this.add(rect);
		
		rect = rectBase.clone(); // back
		this.add(rect);
		
		rect = rectBase.clone(); // left
		rect.rotate(0, 90, 0);
		this.add(rect);
		
		rect = rectBase.clone(); // right
		rect.rotate(0, -90, 0);
		this.add(rect);
		
		rect = rectBase.clone(); // top
		rect.rotate(90, 0, 0);
		this.add(rect);
		
		rect = rectBase.clone(); // bottom
		rect.rotate(-90, 0, 0);
		this.add(rect);
		
		this.setReferencePoint(position);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		// Belichtung und Farbe ausstellen, Texture2D anstellen
		gl.glPushAttrib(GL2.GL_ALL_ATTRIB_BITS); // gl.glIsEnabled(GL2.GL_TEXTURE_2D)
		gl.glEnable(GL2.GL_TEXTURE_2D);
		gl.glColor4f(1.0f, 1.0f, 1.0f, 0.0f);
		gl.glDisable(GL2.GL_LIGHTING);
		gl.glDisable(GL2.GL_COLOR_MATERIAL);
		super.beginGL(gl);
	}
	
	@Override
	public void endGL(GL2 gl) {	
		super.endGL(gl);
		gl.glPopAttrib();
	}
	
	public GLRectangle getRectFront() {
		return (GLRectangle) objects.get(0);
	}

	public GLRectangle getRectBack() {
		return (GLRectangle) objects.get(1);
	}

	public GLRectangle getRectLeft() {
		return (GLRectangle) objects.get(2);
	}

	public GLRectangle getRectRight() {
		return (GLRectangle) objects.get(3);
	}

	public GLRectangle getRectTop() {
		return (GLRectangle) objects.get(4);
	}

	public GLRectangle getRectDown() {
		return (GLRectangle) objects.get(5);
	}
	

	@Override
	public void updateTranslate(TranslateObservable changed, float x, float y, float z) {
		super.updateTranslate(changed, x, y, z);
		this.translate(x, y, z);
	}

	@Override
	public void updateTranslate(TranslateObservable changed, GLVertex v) {
		super.updateTranslate(changed, v);
		this.translate(v);
	}

	@Override
	public void updateAbsolutePosition(TranslateObservable changed, GLVertex position) {
		super.updateAbsolutePosition(changed, position);
		this.setPosition(position);
	}
}
