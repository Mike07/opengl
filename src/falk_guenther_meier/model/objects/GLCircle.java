package falk_guenther_meier.model.objects;

import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.util.approximation.ObjectApproximation;
import falk_guenther_meier.util.approximation.structures.CircleStructure;
import falk_guenther_meier.util.approximation.structures.SameStructure;

public class GLCircle extends GLComplexObject {
	/**
	 * N�hert einen Kreis in der x-z-Ebene durch Kreisabschnitte an, die durch die �bergebenen Kreispunkte
	 * definiert werden.
	 * @param position Ortsvektor des Mittelspunkts des Kreises
	 * @param liste Liste von Vektoren, die vom Mittelpunkt aus betrachtet Punkte auf der Kreislinie sind,
	 * die eine N�herung des Kreises darstellen (muss mindestens 3 Vertices enthalten!)
	 */
	public GLCircle(GLVertex position, List<GLVertex> list, GLVertex eye, int iterations) {
		super();
		List<GLVertex> newList = GLVertex.removeDoubledVertexes(list);
		newList = GLVertex.sortVertexesForShortestWays(newList);

		this.position = new GLVertex(0.0f, 0.0f, 0.0f);

		GLTriangle tri;
		List<GLTriangle> triangles;
		triangles = new ArrayList<GLTriangle>(newList.size());
		for (int i = 0; i < newList.size() - 1; i++) {
			tri = new GLTriangle(newList.get(i).clone(), newList.get(i+1).clone(), this.position.clone(), eye);
			// Normalen werden intern im Dreieck gesetzt ??
			triangles.add(tri);
		}
		tri = new GLTriangle(newList.get(0).clone(), newList.get(newList.size() - 1).clone(),
				this.position.clone(), eye);
		triangles.add(tri);
		triangles = ObjectApproximation.approximateIterations(triangles, new SameStructure(), iterations);

		// Normalen setzen
		GLVertex crossT = GLVertex.mult(GLVertex.sub(newList.get(1), newList.get(0)),
				GLVertex.sub(newList.get(2), newList.get(0))); // in Ecke a
		crossT.normalize();
		crossT = GLVertex.add(newList.get(0), crossT);
		GLVertex crossF = GLVertex.mult(GLVertex.sub(newList.get(2), newList.get(0)),
				GLVertex.sub(newList.get(1), newList.get(0))); // in Ecke a
		crossF.normalize();
		crossF = GLVertex.add(newList.get(0), crossF);
		if (GLVertex.getDistance(eye, crossT) <= GLVertex.getDistance(eye, crossF)) { // Normalen nach oben
			for (GLTriangle t : triangles) {
				t.getNodes()[0].setNormal(new GLVertex(0.0f, 1.0f, 0.0f));
				t.getNodes()[1].setNormal(new GLVertex(0.0f, 1.0f, 0.0f));
				t.getNodes()[2].setNormal(new GLVertex(0.0f, 1.0f, 0.0f));
			}			
		} else { // Normalen nach unten (abh�ngig von eye!)
			for (GLTriangle t : triangles) {
				t.getNodes()[0].setNormal(new GLVertex(0.0f, - 1.0f, 0.0f));
				t.getNodes()[1].setNormal(new GLVertex(0.0f, - 1.0f, 0.0f));
				t.getNodes()[2].setNormal(new GLVertex(0.0f, - 1.0f, 0.0f));
			}			
		}

		this.addAll(triangles);
		this.translate(position.getX(), position.getY(), position.getZ());
		this.setReferencePoint(position);
	}

	public GLCircle(GLVertex position, float radius, int iterations) {
		super();
		this.init(radius, iterations);
		this.translate(position.getX(), position.getY(), position.getZ());
		this.setReferencePoint(position);
	}

	public GLCircle(float radius, int iterations) {
		super();
		this.init(radius, iterations);
	}

	private void init(float radius, int iterations) {
		this.position = new GLVertex(0.0f, 0.0f, 0.0f);
		List<GLTriangle> list = new ArrayList<GLTriangle>();

		float length = (float) (2.0f * Math.sqrt(radius * radius / 2.0f));
		GLRectangle rectangle = new GLRectangle(this.position, length, length);
		list.addAll(rectangle.splitToTriangles());

		List<GLTriangle> newList = ObjectApproximation.approximateIterations(list, new CircleStructure(radius), iterations);
		ObjectApproximation.calculateAndSetNormals(newList);
		this.addAll(newList);

		this.setReferencePoint(position);
	}
}
