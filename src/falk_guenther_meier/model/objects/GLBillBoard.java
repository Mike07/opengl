package falk_guenther_meier.model.objects;

import java.io.File;

import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.observers.ScaleObservable;
import falk_guenther_meier.model.observers.TranslateObservable;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.util.FileLoader;

/**
 * BillBoard: Ein Rechteck mit einer (teilweise transparenten) Textur, das sich immer senkrecht zur Kameraposition
 * dreht.
 *
 * Mindest-Anforderungen an die Benutzung:
 * - die Farbe darf nicht gesetzt werden
 * - die *Now(...)-Befehle d�rfen nicht verwendet werden
 * - nur translate(...), scale(...) und setPosition(...) d�rfen verwendet werden
 * - rotate(...) ist ebenfalls verboten
 * - den Referenz-Punkt verschieben ist (anscheinend) m�glich
 *
 * @author Johannes
 */
public class GLBillBoard extends GLRectangle implements Comparable<GLBillBoard> {
	/**
	 * Enth�lt den aktuellen Bezugspunkt f�r die Ausrichtung des BillBoards.
	 */
	private TranslateObservable actual;

	public GLBillBoard(GLCamera camera, GLTexture2D texture, GLVertex position, float width, float height) {
		super(position, width, height);
		this.init(camera, texture);
	}

	public GLBillBoard(GLCamera camera, String textureFile, GLVertex position, float width, float height) {
		super(position, width, height);
		File file = FileLoader.getFileFromFilesFolder(textureFile, true);
		this.init(camera, new GLTexture2D(file));
	}

	private void init(GLCamera camera, GLTexture2D texture) {
		this.setTexture(texture);
		this.setColor(new GLVertex(1.0f, 1.0f, 1.0f));
		this.setMaterial(GLMaterial.createTextureMaterial());
		this.actual = camera;

		camera.addTranslateObserver(this);
		this.addScaleObserver(this);
		this.addTranslateObserver(this);
		this.updatePosition(actual);
	}

	@Override
	public void updateTranslate(TranslateObservable changed, float x, float y, float z) {
		super.updateTranslate(changed, x, y, z);
		this.updatePosition(changed);
	}

	@Override
	public void updateTranslate(TranslateObservable changed, GLVertex v) {
		super.updateTranslate(changed, v);
		this.updatePosition(changed);
	}

	@Override
	public void updateAbsolutePosition(TranslateObservable changed, GLVertex position) {
		super.updateAbsolutePosition(changed, position);
		this.updatePosition(changed);
	}

	@Override
	public void updateScale(ScaleObservable changed, float x, float y, float z) {
		super.updateScale(changed, x, y, z);
		this.updatePosition(this.actual);
	}

	private void updatePosition(TranslateObservable changed) {
		this.actual = changed;

		GLVertex center = this.getAbsoluteCenter();
		GLVertex normal = this.getNormalOfPlane(); // Normale des Rechtecks
		GLVertex between = GLVertex.sub(changed.getPosition(), center); // Vektor zwischen Kamera und Rechteck
		between.setY(0.0f);
		normal.setY(0.0f);

		if (between.getNorm() == 0.0f) {
			System.out.println("Die x- und z-Komponente der Positionen von Kamera und BillBoard stimmen genau �berein: " +
					"Diese Situation ist logisch f�r ein BillBoard nicht definiert! Deshalb wird " +
					"nichts getan (au�er diese Warnung auszugeben).");
			return;
		}
		if (normal.getNorm() == 0.0f) { // dieser Fehler-Fall sollte eigentlich nie auftreten!
			System.out.println("normal hat L�nge 0: Grund ist vermutlich, dass das BillBoard eine komische Form hat (insbesondere kein Rechteck ist)!");
			return;
		}

		float standardDistance = GLVertex.getDistance(changed.getPosition(), GLVertex.add(normal, center));
		float invertDistance = GLVertex.getDistance(changed.getPosition(), GLVertex.add(GLVertex.invert(normal), center));
		if (standardDistance > invertDistance) {
			this.rotate(center, 0.0f, 180.0f, 0.0f);
			this.updatePosition(changed); // JM: ist ein bischen gehackt, aber danach ist die Situation anscheinend stabilisiert!
			this.updatePosition(changed);
			return;
		}

		float angle = GLVertex.getAngleBetween(between, normal);

		if (angle == 0.0f) {
			return;
		}
		if (GLVertex.mult(normal, between).getY() < 0) { // ist richtig herum!!
			angle = 360.0f - 2 * angle;
		}
		this.rotate(center, 0.0f, angle, 0.0f);
	}
	
	/**
	 * Liefert den Abstand zum Bezugspunkt (Richtung: Bezugspunkt -> BillBoard).
	 * die y-Komponente wird ignoriert.
	 * @return
	 */
	public float getLenghtToFixPoint() {
		GLVertex diffVektor = GLVertex.sub(this.getPosition(),  actual.getPosition());
		diffVektor.setY(0f);
		return diffVektor.getNorm();
	}

	@Override
	public int compareTo(GLBillBoard o) {
		float length = this.getLenghtToFixPoint();
		float oLength = o.getLenghtToFixPoint(); 
		if (length < oLength) {
			return 1;
		} else if (length == oLength) {
			return 0;
		} else return -1;
	}
}
