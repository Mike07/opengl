package falk_guenther_meier.model.objects;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.platform.GLPrimitiveObject;
import falk_guenther_meier.model.textures.GLTexCoord;

public class GLVertexBufferObject extends GLPrimitiveObject {
	
	ByteBuffer verticesBB;
	ByteBuffer normalsBB;
	List<GLTexCoordsBuffer> texCoordsBList;
	
	int glObjMode; // z.B. GL2.GL_TRIANGLES
	
	public GLVertexBufferObject(int glObjMode, GLVertex... vertices) {
		super();
		init(glObjMode, vertices);
	}
	
	public GLVertexBufferObject(GLPrimitiveObject primitiveObject){
		super();
		if (primitiveObject instanceof GLTriangle)
			glObjMode = GL2.GL_TRIANGLES;
		else if (primitiveObject instanceof GLRectangle)
			glObjMode = GL2.GL_QUADS;
		else if (primitiveObject instanceof GLTriangleStrip)
			glObjMode = GL2.GL_TRIANGLE_STRIP;
		init(glObjMode, primitiveObject.getNodes());
		this.textureBindings = primitiveObject.getTextureBindings();
		this.color = primitiveObject.getColor();
		this.material = primitiveObject.getMaterial();
	}
	
	public GLVertexBufferObject(int glObjMode, GLComplexObject complexObject){
		super();
		List<GLVertex> vertexList = new ArrayList<GLVertex>();
		for (GLObject object: complexObject.getObjects()) {
			if (glObjMode == GL2.GL_TRIANGLES && object instanceof GLTriangle
					|| glObjMode == GL2.GL_QUADS && object instanceof GLRectangle
					|| glObjMode == GL2.GL_TRIANGLE_STRIP && object instanceof GLTriangleStrip)
				Collections.addAll(vertexList, object.getNodes());
		}
		init(glObjMode, vertexList.toArray(new GLVertex[0]));
		this.textureBindings = complexObject.getTextureBindings();
		this.color = complexObject.getColor();
		this.material = complexObject.getMaterial();
	}
	
	/**
	 * Konvertiert ein GLComplexObject aus Rechtecken und Dreiecken zu einem GLCustomComplexObject
	 * aus je einem GLVertexBufferObject f�r GLTriangle und GLTriangle
	 * @param complexObject
	 * @return
	 */
	public static GLCustomComplexObject convert2VBOs(GLComplexObject complexObject) {
		GLCustomComplexObject vbos = new GLCustomComplexObject();
		GLVertexBufferObject vbo;
		
		vbo= new GLVertexBufferObject(GL2.GL_TRIANGLES, complexObject);
		if (vbo.getNodes().length > 0)
			vbos.add(vbo);
		
		vbo = new GLVertexBufferObject(GL2.GL_QUADS, complexObject);
		if (vbo.getNodes().length > 0)
			vbos.add(vbo);
		
		for(GLObject object: complexObject.getObjects()) {
			if (object instanceof GLComplexObject) {
				GLCustomComplexObject subObjectVBOs = convert2VBOs((GLComplexObject)object);
				vbos.add(subObjectVBOs);
			}
		}
		
		return vbos;
	}
	
	
	class GLTexCoordsBuffer {
		int texUnit;
		ByteBuffer byteBuffer;
		FloatBuffer floatBuffer;

		GLTexCoordsBuffer(int texUnit, int verticesCount) {
			this.texUnit = texUnit;
			this.byteBuffer = ByteBuffer.allocateDirect(verticesCount*2*4);
			this.byteBuffer.order(ByteOrder.nativeOrder());
			this.floatBuffer = byteBuffer.asFloatBuffer();
		}
	}
	
	private GLTexCoordsBuffer getOrCreateTexCoordsBuffer(int texUnit) {
		for(GLTexCoordsBuffer buffer: texCoordsBList) {
			if (buffer.texUnit == texUnit)
				return buffer;
		}
		GLTexCoordsBuffer buffer = new GLTexCoordsBuffer(texUnit, nodes.length);
		texCoordsBList.add(buffer);
		return buffer;
	}
	
	private void init(int glObjMode, GLVertex... vertices) {
		this.glObjMode = glObjMode;
		this.nodes = vertices;
		
		verticesBB = ByteBuffer.allocateDirect(nodes.length*3*4); // nVertices * floatsPerVertex * floatSize
		verticesBB.order(ByteOrder.nativeOrder());
		FloatBuffer verticesFB = verticesBB.asFloatBuffer();
		
		normalsBB = ByteBuffer.allocateDirect(nodes.length*3*4);
		normalsBB.order(ByteOrder.nativeOrder());
		FloatBuffer normalsFB = normalsBB.asFloatBuffer();
		
		texCoordsBList = new ArrayList<GLTexCoordsBuffer>();
		
		for(GLVertex vertex: nodes) {
			verticesFB.put(vertex.getValues(), 0, 3);
			GLVertex normal = vertex.getNormal();
			if (normal != null) {
				normalsFB.put(normal.getValues(), 0, 3);
			}
			for(GLTexCoord texCoord: vertex.getTexCoords()) {
				GLTexCoordsBuffer texCoordsB = getOrCreateTexCoordsBuffer(texCoord.getTextureUnit());
				texCoordsB.floatBuffer.put(texCoord.getU());
				texCoordsB.floatBuffer.put(texCoord.getV());
			}
		}
		position = new GLVertex(0.0f, 0.0f, 0.0f);
	}
	

	@Override
	public final void beginGL(GL2 gl) {
		super.beginGL(gl);
		gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL2.GL_NORMAL_ARRAY);
		
		gl.glVertexPointer(3,GL2.GL_FLOAT,0,verticesBB);
		
		for (GLTexCoordsBuffer texCoordsB: texCoordsBList) {
			if (texCoordsB.texUnit>0) break;
			gl.glClientActiveTexture(GL2.GL_TEXTURE0+texCoordsB.texUnit);
			gl.glEnableClientState(GL2.GL_TEXTURE_COORD_ARRAY);
			ByteBuffer texCoordsBB = texCoordsB.byteBuffer;
			gl.glTexCoordPointer(2, GL2.GL_FLOAT, 0, texCoordsBB);
		}
		
		gl.glNormalPointer(GL2.GL_FLOAT,0,normalsBB);
				
		gl.glDrawArrays(glObjMode,0,nodes.length);
		
		for (GLTexCoordsBuffer texCoordsB: texCoordsBList) {
			if (texCoordsB.texUnit>0) break;
			gl.glClientActiveTexture(GL2.GL_TEXTURE0+texCoordsB.texUnit);
			gl.glDisableClientState(GL2.GL_TEXTURE_COORD_ARRAY);
		}
		gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL2.GL_NORMAL_ARRAY);
	}
}
