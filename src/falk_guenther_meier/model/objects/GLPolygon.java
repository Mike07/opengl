package falk_guenther_meier.model.objects;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLPrimitiveObject;

public class GLPolygon extends GLPrimitiveObject {

	public GLPolygon(boolean isCCW, GLVertex... aCCW) {
		super();
		if (isCCW) {
			nodes = aCCW;
		} else {
			nodes = new GLVertex[aCCW.length];
			nodes[0] = aCCW[0];
			for (int i = 1; i < aCCW.length; i++) {
				nodes[i] = aCCW[aCCW.length - i];
			}
		}
		position = new GLVertex(0.0f, 0.0f, 0.0f);
	}
	
	@Override
	public final void beginGL(GL2 gl) {
		super.beginGL(gl);
		gl.glBegin(GL2.GL_POLYGON);   // TODO: CalculateNormal muss evlt. anders berechnet werden
		for (GLVertex v : nodes) {
			v.toGL(gl, position); 
		}
		gl.glEnd();
	}
}
