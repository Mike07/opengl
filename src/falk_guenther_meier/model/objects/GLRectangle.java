package falk_guenther_meier.model.objects;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLPrimitiveObject;
import falk_guenther_meier.model.textures.GLObjectTextureBinding;
import falk_guenther_meier.model.textures.GLTexGrid;
import falk_guenther_meier.model.textures.GLTexRect;
import falk_guenther_meier.model.textures.GLTexture2D;

// TODO: umbenennen! bezeichnet eigentlich nur ein Viereck!

public class GLRectangle extends GLPrimitiveObject {
	/**
	 * Konstruktor f�r ein Viereck (alle angegebenen Vektoren sind Ortsvektoren)
	 * @param aCCW unten links (Start-Ecke)
	 * @param bCCW unten rechts
	 * @param cCCW oben rechts
	 * @param dCCW oben links
	 * @param isCCW
	 */
	public GLRectangle(GLVertex aCCW, GLVertex bCCW, GLVertex cCCW, GLVertex dCCW, boolean isCCW) {
		super();
		position = new GLVertex(0.0f, 0.0f, 0.0f);
		nodes = new GLVertex[4];
		if (isCCW) {
			/* 
			 * d c
			 * a b
			 */
			nodes[0] = aCCW;
			nodes[1] = bCCW;
			nodes[2] = cCCW;
			nodes[3] = dCCW;
		} else {
			/* 
			 * if isCCW then / 180� um (a-d)-Achse drehen
			 * c d
			 * b a
			 */
			nodes[0] = bCCW;
			nodes[1] = aCCW;
			nodes[2] = dCCW;
			nodes[3] = cCCW; // 180� um (a-d)-Achse drehen
		}
		color = null;
	}

	/**
	 * Konstruktor f�r ein Viereck
	 * @param vCCW
	 * @param isCCW
	 */
	public GLRectangle(GLVertex[] vCCW, boolean isCCW) {
		this(vCCW[0], vCCW[1], vCCW[2], vCCW[3], isCCW);
	}

	/**
	 * Konstruktor f�r ein Rechteck
	 * @param position
	 * @param width
	 * @param height
	 */
	public GLRectangle(GLVertex position, float width, float height) {
		super();
		this.position = position;
		float w = width / 2.0f;
		float h = height / 2.0f;
		nodes = new GLVertex[4];
		nodes[0] = new GLVertex(-w, -h, 0.0f);
		nodes[1] = new GLVertex(w, -h, 0.0f);
		nodes[2] = new GLVertex(w, h, 0.0f);
		nodes[3] = new GLVertex(-w, h, 0.0f);
	}
	
	/**
	 * Konstruktor f�r ein Quadrat
	 * @param position
	 * @param width
	 * @param height
	 */
	public GLRectangle(GLVertex position, float size) {
		this(position, size, size);
	}

	/**
	 * Copy-Konstruktor
	 * @param copyRect
	 */
	public GLRectangle(GLRectangle copyRect) {
		super();
		this.parent = copyRect.parent;
		this.position = copyRect.position.clone();
		this.nodes = new GLVertex[4];
		for (int i = 0; i < 4; i++) {
			this.nodes[i] = copyRect.nodes[i].clone();
		}
		if (copyRect.color != null)
			this.color = copyRect.color.clone();
		this.alwaysSetNormals = copyRect.alwaysSetNormals;
		this.drawModeNormals = copyRect.drawModeNormals;
		this.material = copyRect.material;
		this.textureBindings.addAll(copyRect.textureBindings);
	}

	/**
	 * Konstruktor, der ein Parallelogramm erstellt
	 * @param start Bezugspunkt von a und b, Ortsvektor der ersten Ecke des Parallelogramms ("Start-Ecke", "unten links")
	 * @param a Vektor der ersten Seite, ausgehend von start
	 * @param b Vektor der zweiten Seite, ausgehend von start
	 */
	public GLRectangle(GLVertex start, GLVertex a, GLVertex b) {
		super();
		this.position = start;
		nodes = new GLVertex[4];
		nodes[0] = new GLVertex();
		nodes[1] = a.clone();
		nodes[2] = GLVertex.add(a, b);
		nodes[3] = b.clone();
	}

	/**
	 * Liefert den relativen Vektor der angegebenen Ecke.
	 * @return
	 */
	public GLVertex getBelowLeft() {
		return nodes[0];
	}
	/**
	 * Liefert den relativen Vektor der angegebenen Ecke.
	 * @return
	 */
	public GLVertex getBelowRight() {
		return nodes[1];
	}
	/**
	 * Liefert den relativen Vektor der angegebenen Ecke.
	 * @return
	 */
	public GLVertex getAboveRight() {
		return nodes[2];
	}
	/**
	 * Liefert den relativen Vektor der angegebenen Ecke.
	 * @return
	 */
	public GLVertex getAboveLeft() {
		return nodes[3];
	}

	/**
	 * Liefert den Ortsvektor des Mittelpunkts des Rechtecks.
	 * @return geliefert wird eine neue Instanz
	 */
	public final GLVertex getAbsoluteCenter() {
		GLVertex a = GLVertex.add(this.getNodes()[0], this.getPosition()); // ein Eckpunkt des Rechtecks
		GLVertex c = GLVertex.add(this.getNodes()[2], this.getPosition()); // gegen�ber liegender Eckpunkt
		return GLVertex.add(a, GLVertex.mult(GLVertex.sub(c, a), 0.5f)); // Mittelpunkt des Rechtecks		
	}

	/**
	 * Liefert die Normale auf der Ebene, die durch die ersten drei Vektoren des Vierecks aufgespannt wird.
	 * @return geliefert wird eine neue Instanz
	 */
	public final GLVertex getNormalOfPlane() {
		GLVertex a = GLVertex.add(this.getNodes()[0], this.getPosition()); // ein Eckpunkt des Rechtecks
		GLVertex b = GLVertex.add(this.getNodes()[1], this.getPosition()); // ein weiterer Eckpunkt des Rechtecks
		GLVertex center = this.getAbsoluteCenter();
		GLVertex normal = GLVertex.mult(GLVertex.sub(a, center), GLVertex.sub(b, center)); // Normale des Rechtecks
		normal.normalize();
		return normal;
	}

	/**
	 * Liefert die Breite des Rechtecks.
	 * @return
	 */
	public float getWidth() {
		return GLVertex.getDistance(this.getAboveLeft(), this.getAboveRight());
	}

	/**
	 * Liefert die H�he des Rechtecks.
	 * @return
	 */
	public float getHeight() {
		return GLVertex.getDistance(this.getAboveLeft(), this.getBelowLeft());
	}

	@Override
	public GLRectangle clone() {
		return new GLRectangle(this);
	}

	/**
	 * Textur setzen und wenn m�glich auch Textur-Koordinaten setzen
	 */
	@Override
	public void setTexture(GLTexture2D texture, int textureUnit) {
		super.setTexture(texture, textureUnit);
		// Textur setzen, falls kein Gitter vorhanden ist.
		GLTexGrid textureTexGrid = texture.getTexGrid();
		if (textureTexGrid.getColCount() == 1 && textureTexGrid.getRowCount() == 1) {
			GLTexRect texRect = ((GLTexRect)textureTexGrid).clone();
			texRect.setTextureUnit(textureUnit);
			setTexRect(texRect, 0, false); // entspricht setTexRect(0, 0, 0, false);
		}
	}

	/**
	 * Setzt die Textur-Koordinaten aus einem Bereich der Textur
	 * Gilt f�r alle Texture-Units!
	 * @param col die Spalte aus der Textur
	 * @param row die Zeile aus der Textur
	 */
	public void setTexRect(int col, int row) {
		setTexRect(col, row, 0, false);
	}

	/**
	 * Setzt die Textur-Koordinaten aus einem Bereich der Textur
	 * Gilt f�r alle Texture-Units!
	 * @param col die Spalte aus der Textur
	 * @param row die Zeile aus der Textur
	 * @param timesRotate90 gibt an, wie h�ufig die Textur gegen den Uhrzeigersinn um 90� gedreht werden soll
	 */
	public void setTexRect(int col, int row, int timesRotate90) {
		setTexRect(col, row, timesRotate90, false);
	}

	/**
	 * Setzt die Textur-Koordinaten aus einem Bereich der Textur
	 * Gilt f�r alle Texture-Units!
	 * @param col die Spalte aus der Textur
	 * @param row die Zeile aus der Textur
	 * @param timesRotate90 gibt an, wie h�ufig die Textur gegen den Uhrzeigersinn um 90� gedreht werden soll
	 * @param mirror gibt an, ob die Textur horizontal gespiegelt werden soll (wie 180�-Drehung um die Vertikal-Achse)
	 */
	public void setTexRect(int col, int row, int timesRotate90, boolean mirror) {
		for (GLObjectTextureBinding textureBinding: this.getTextureBindings()) {
			GLTexRect texRect = textureBinding.getTexture().getTexGrid().getTexRect(col, row).clone(); 
			texRect.setTextureUnit(textureBinding.getTextureUnit());
			setTexRect(texRect, timesRotate90, mirror);
		}		
	}
	
	/**
	 * Setzt die Textur-Koordinaten mithilfe des GLTexRect.
	 * @param timesRotate90 gibt an, wie h�ufig die Textur gegen den Uhrzeigersinn um 90� gedreht werden soll
	 * @param mirror gibt an, ob die Textur horizontal gespiegelt werden soll (wie 180�-Drehung um die Vertikal-Achse)
	 */
	public void setTexRect(GLTexRect texRect, int timesRotate90, boolean mirror) {
		int vertIndex = timesRotate90 % 4;
		if (mirror)
			vertIndex = (vertIndex+1) % 4;
		for (int i = 0; i < 4; i++) {
			nodes[vertIndex].setTexCoord(texRect.getTexCoord(i));
			if (mirror) // TODO JM: wirken sich diese �nderungen �berhaupt aus ??!!
				vertIndex = (vertIndex + 4 - 1) % 4;
			else
				vertIndex = (vertIndex + 1) % 4;
		}		
	}

	/**
	 * Liefert zwei Dreiecke zur�ck, die die Fl�che dieses Rechtecks abdecken.
	 * Das Rechteck wird an den vorhandenen Ecken gespalten.
	 * Zur�ckgeliefert werden neu erstellte (kopierte) GLVertexes.
	 * @return Liste mit den zwei neuen Dreicken
	 */
	public List<GLTriangle> splitToTriangles() {
		// TODO JM: wird hier wirklich alles kopiert ?!
		List<GLTriangle> result = new ArrayList<GLTriangle>(2);
		result.add(new GLTriangle(nodes[0].clone(), nodes[1].clone(), nodes[2].clone(), true));
		result.add(new GLTriangle(nodes[2].clone(), nodes[3].clone(), nodes[0].clone(), true));
		return result;
	}
	
	/**
	 * Teilt das Rechteck in ein Dreiecks-Gitter auf. (Tessellation)
	 * Der Bedarf an Vertices wird m�glichst gering gehalten.
	 * @param count die Anzahl, wie oft vertikal und horizontal geteilt werden soll
	 * @return Das Rechteck als TriangleStrip mit count*count*2 Dreiecken
	 */
	public GLTriangleStrip splitToTriangleGrid(int count) {
		return new GLTriangleStrip(this, count);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		if (this.getShader() != null) {
			this.getShader().setTangente(gl, GLVertex.sub(this.getBelowRight(), this.getBelowLeft()));
		}
		gl.glBegin(GL2.GL_QUADS);
			nodes[0].toGL(gl, position);
			nodes[1].toGL(gl, position);
			nodes[2].toGL(gl, position);
			nodes[3].toGL(gl, position);
		gl.glEnd();
	}
}
