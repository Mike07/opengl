package falk_guenther_meier.model.objects;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.textures.GLObjectTextureBinding;
import falk_guenther_meier.model.textures.GLTexCoord;
import falk_guenther_meier.model.textures.GLTexture2D;

public class GLHeightMapTriangleStrip extends GLTriangleStrip {
	
	private boolean useDisplayList;
	private int displayListID;
	
	public GLHeightMapTriangleStrip(int count) {
		super(getBasisRectangle(), count);
		useDisplayList = true;
		displayListID = -1;
	}
	
	private static GLRectangle getBasisRectangle() {
		return new GLRectangle(new GLVertex(-1, 0, 1), new GLVertex(1, 0, 1), new GLVertex(1, 0, -1), new GLVertex(-1, 0, -1), true);
	}
	
	@Override
	public void setTexture(GLTexture2D texture, int textureUnit) {
		super.setTexture(texture, textureUnit);
		for (GLVertex node: this.getNodes()) {
			float s = (node.getX() + 1f) / 2f;
			float t = (node.getZ() + 1f) / 2f;
			node.setTexCoord(new GLTexCoord(s, t, textureUnit));
		}
	}
	
	public void setUseDisplayList(boolean useDisplayList) {
		this.useDisplayList = useDisplayList;
	}
	
	protected void beginDraw2(GL2 gl) {
		if (useDisplayList) {
			if (displayListID == -1) {
				displayListID = gl.glGenLists(1);
				gl.glNewList(displayListID, GL2.GL_COMPILE);
				super.beginDraw2(gl);
				gl.glEndList();
			} else {
				gl.glCallList(displayListID);
			}
		} else {
			super.beginDraw2(gl);
		}
	}
	
	
	/**
	 * in @Override beginDraw2 umbenennen, um TexCoord-Generation zu benutzen - hat f�r Shader leider bis jetzt nicht funktioniert.
	 * @param gl
	 */
	protected void beginDrawWithTexGen(GL2 gl) {
		
		// Textur-Koordinaten linear setzen
		for(GLObjectTextureBinding textureBinding: this.getTextureBindings()) {
			int texUnit = GL2.GL_TEXTURE0 + textureBinding.getTextureUnit();
			gl.glActiveTexture(texUnit);
			
			// s = (x + 1)/2, t = (z + 1) / 2     // s = 0.5x + 0.5w, t = 0.5z + 0.5w
			float linearS[] = {0.5f, 0.0f, 0.0f, 0.5f};
			gl.glTexGeni(GL2.GL_S, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_OBJECT_LINEAR);
			gl.glTexGenfv(GL2.GL_S, GL2.GL_OBJECT_PLANE, linearS, 0);
			//gl.glMultiTexGeniEXT(texUnit, GL2.GL_S, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_OBJECT_LINEAR);
			//gl.glMultiTexGenfvEXT(texUnit, GL2.GL_S, GL2.GL_OBJECT_PLANE, linearS, 0);
			gl.glEnable(GL2.GL_TEXTURE_GEN_S);
			
			float linearT[] = {0.0f, 0.0f, 0.5f, 0.5f};
			//gl.glMultiTexGeniEXT(texUnit, GL2.GL_T, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_OBJECT_LINEAR);
			//gl.glMultiTexGenfvEXT(texUnit, GL2.GL_T, GL2.GL_OBJECT_PLANE, linearT, 0);
			gl.glTexGeni(GL2.GL_T, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_OBJECT_LINEAR);
			gl.glTexGenfv(GL2.GL_T, GL2.GL_OBJECT_PLANE, linearT, 0);
			gl.glEnable(GL2.GL_TEXTURE_GEN_T);
		}
		
		super.beginDraw2(gl);
	}

}
