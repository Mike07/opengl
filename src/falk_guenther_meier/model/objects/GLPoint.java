package falk_guenther_meier.model.objects;

import java.nio.FloatBuffer;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLPrimitiveObject;

public class GLPoint extends GLPrimitiveObject {
	
	private float size = 4;

	public GLPoint(GLVertex point) {
		super();
		nodes = new GLVertex[1];
		nodes[0] = point;
		position = new GLVertex(0.0f, 0.0f, 0.0f);
	}
	
	public GLPoint(GLVertex point, float size) {
		this(point);
		this.size = size;
	}
	
	public final float getSize() {
		return size;
	}

	public final void setSize(float size) {
		this.size = size;
	}

	@Override
	public final void beginGL(GL2 gl) {
		super.beginGL(gl);
		FloatBuffer initSize= FloatBuffer.allocate(1);
		gl.glGetFloatv(GL2.GL_POINT_SIZE, initSize);
		gl.glPointSize(size);
		gl.glBegin(GL2.GL_POINTS);
			nodes[0].toGL(gl, position);
		gl.glEnd();
		gl.glPointSize(initSize.get()); // wieder auf Standard setzen
	}
}
