package falk_guenther_meier.model.objects;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.textures.GLTexRect;

public class GLCuboid extends GLComplexObject {
	public GLCuboid(GLVertex center, float width, float height, float depth) {
		super();
		this.init(center, width, height, depth);
	}

	public GLCuboid(GLVertex center, float cubeWidth) {
		this(center, cubeWidth, cubeWidth, cubeWidth);
	}

	/**
	 * Erstellt aus einem Rechteck ein Rechteck / "Mauerst�ck" mit der angegebenen Dicke
	 * @param rect die Ecken des Rechtecks sind in folgender Reihenfolge angegeben: links unten, rechts unten, rechts oben, links oben
	 * @param depth "St�rke der Mauer"
	 * @param cCW true: die "Mauerst�rke" / depth wird senkrecht (Kreuzprodukt) auf die Ebene aufgetragen,
	 * aufgespannt von den Vektoren des Rechtecks in CCW-Richtung
	 * (das "neue" Rechteck ist vorne, das "alte" / vorhandene / �bergebene Rechteck ist hinten);
	 * false: entgegen CCW (das "alte" / vorhandene / �bergebene Rechteck ist vorne, das "neue" Rechteck ist hinten)
	 */
	public GLCuboid(GLRectangle rect, float depth, boolean cCW) {
		super();
		position = rect.getReferencePoint().clone(); // Designentscheidung: clone() !
		GLVertex normal = GLVertex.mult(GLVertex.sub(rect.getNodes()[1], rect.getNodes()[0]),
				GLVertex.sub(rect.getNodes()[2], rect.getNodes()[0]));
		normal.setNorm(depth);
		GLVertex start = null; // Vektor vorne links unten
		if (cCW) {
			start = GLVertex.add(rect.getNodes()[0], normal);
		} else {
			start = rect.getNodes()[0].clone();
		}
		normal.invert();

		GLVertex a = start.clone(); // vorne links unten
		GLVertex b = GLVertex.add(a, GLVertex.sub(rect.getNodes()[1], rect.getNodes()[0])); // vorne rechts unten
		GLVertex c = GLVertex.add(a, GLVertex.sub(rect.getNodes()[2], rect.getNodes()[0])); // vorne rechts oben
		GLVertex dV = GLVertex.add(a, GLVertex.sub(rect.getNodes()[3], rect.getNodes()[0])); // vorne links oben
		GLVertex e = GLVertex.add(a, normal); // hinten links unten
		GLVertex f = GLVertex.add(b, normal); // hinten rechts unten
		GLVertex g = GLVertex.add(c, normal); // hinten rechts oben
		GLVertex hV = GLVertex.add(dV, normal); // hinten links oben

		this.initRectangles(a, b, c, dV, e, f, g, hV);

		this.setReferencePoint(position);
	}

	protected void init(GLVertex center, float width, float height, float depth) {
		position = center;

		float w = width / 2.0f;
		float h = height / 2.0f;
		float d = depth / 2.0f;

		GLVertex a = GLVertex.addValues(center, -w, -h, d); // vorne links unten
		GLVertex b = GLVertex.addValues(center, w, -h, d); // vorne rechts unten
		GLVertex c = GLVertex.addValues(center, w, h, d); // vorne rechts oben
		GLVertex dV = GLVertex.addValues(center, -w, h, d); // vorne links oben
		GLVertex e = GLVertex.addValues(center, -w, -h, -d); // hinten links unten
		GLVertex f = GLVertex.addValues(center, w, -h, -d); // hinten rechts unten
		GLVertex g = GLVertex.addValues(center, w, h, -d); // hinten rechts oben
		GLVertex hV = GLVertex.addValues(center, -w, h, -d); // hinten links oben

		// Rechtecke haben Ortsvektoren als Punkte		
		this.initRectangles(a, b, c, dV, e, f, g, hV);
		
		this.setReferencePoint(position);
	}

	/**
	 * 
	 * @param a vorne links unten
	 * @param b vorne rechts unten
	 * @param c vorne rechts oben
	 * @param dV vorne links oben
	 * @param e hinten links unten
	 * @param f hinten rechts unten
	 * @param g hinten rechts oben
	 * @param hV hinten links oben
	 */
	protected void initRectangles(GLVertex a, GLVertex b, GLVertex c, GLVertex dV, GLVertex e, GLVertex f,
			GLVertex g, GLVertex hV) {
		// Reihenfolge ist wichtig f�r Setzen der Texturen => erster Vektor wird als links unten definiert.
		this.add(new GLRectangle(a.clone(), b.clone(), c.clone(), dV.clone(), true));	
		this.add(new GLRectangle(f.clone(), e.clone(), hV.clone(), g.clone(), true));
		this.add(new GLRectangle(e.clone(), a.clone(), dV.clone(), hV.clone(), true));
		this.add(new GLRectangle(b.clone(), f.clone(), g.clone(), c.clone(), true));
		this.add(new GLRectangle(dV.clone(), c.clone(), g.clone(), hV.clone(), true));
		this.add(new GLRectangle(e.clone(), f.clone(), b.clone(), a.clone(), true));
	}
	
	/**
	 * Setzt die Textur-Koordinaten mithilfe des GLTexRect.
	 * @param timesRotate90 gibt an, wie h�ufig die Textur gegen den Uhrzeigersinn um 90� gedreht werden soll
	 * @param mirror gibt an, ob die Textur horizontal gespiegelt werden soll (wie 180�-Drehung um die Vertikal-Achse)
	 */
	public void setTexRect(GLTexRect texRect, int timesRotate90, boolean mirror) {
		for (GLObject obj : this.objects) {
			((GLRectangle) obj).setTexRect(texRect, timesRotate90, mirror);
		}
	}
	
	public GLRectangle getRectFront() {
		return (GLRectangle) objects.get(0);
	}

	public GLRectangle getRectRear() {
		return (GLRectangle) objects.get(1);
	}

	public GLRectangle getRectLeft() {
		return (GLRectangle) objects.get(2);
	}

	public GLRectangle getRectRight() {
		return (GLRectangle) objects.get(3);
	}

	public GLRectangle getRectTop() {
		return (GLRectangle) objects.get(4);
	}

	public GLRectangle getRectBottom() {
		return (GLRectangle) objects.get(5);
	}

	/**
	 * Liefert den Ortsvektor des Mittelpunkts des Quaders.
	 * @return in einer neuen Instanz
	 */
	public GLVertex getAbsoluteCenter() {
		GLVertex a = this.getRectLeft().getAboveRight();
		GLVertex b = this.getRectRight().getBelowRight();
		return GLVertex.add(GLVertex.add(a, GLVertex.mult(GLVertex.sub(b, a), 0.5f)), this.position);
	}
}
