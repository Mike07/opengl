package falk_guenther_meier.model.objects;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLComplexObject;

/**
 * Ein Dreiecks-Prisma: Dreieck mit Dicke / Mauerstärke
 * @author Johannes
 */
public class GLTriangleWall extends GLComplexObject {
	public GLTriangleWall(GLTriangle triangle, float depth, boolean cCW) {
		super();
		position = triangle.getReferencePoint().clone(); // Designentscheidung: clone() !
		GLVertex normal = GLVertex.mult(GLVertex.sub(triangle.getNodes()[1], triangle.getNodes()[0]),
				GLVertex.sub(triangle.getNodes()[2], triangle.getNodes()[0]));
		normal.setNorm(depth);
		GLVertex start = null; // Vektor vorne links unten
		if (cCW) {
			start = GLVertex.add(triangle.getNodes()[0], normal);
		} else {
			start = triangle.getNodes()[0].clone();
		}
		normal.invert();

		GLVertex a = start.clone(); // vorne links unten
		GLVertex b = GLVertex.add(a, GLVertex.sub(triangle.getNodes()[1], triangle.getNodes()[0])); // vorne rechts unten
		GLVertex c = GLVertex.add(a, GLVertex.sub(triangle.getNodes()[2], triangle.getNodes()[0])); // vorne rechts oben
		GLVertex e = GLVertex.add(a, normal); // hinten links unten
		GLVertex f = GLVertex.add(b, normal); // hinten rechts unten
		GLVertex g = GLVertex.add(c, normal); // hinten rechts oben

		this.add(new GLTriangle(a.clone(), b.clone(), c.clone(), true));
		this.add(new GLTriangle(f.clone(), e.clone(), g.clone(), true));
		this.add(new GLRectangle(a.clone(), b.clone(), f.clone(), e.clone(), false));
		this.add(new GLRectangle(b.clone(), c.clone(), f.clone(), g.clone(), false));
		this.add(new GLRectangle(c.clone(), a.clone(), g.clone(), e.clone(), false));

		this.setReferencePoint(position);		
	}

	public GLTriangle getTriangleFront() {
		return (GLTriangle) objects.get(0);
	}
	public GLTriangle getTriangleBack() {
		return (GLTriangle) objects.get(1);
	}

	public GLRectangle getRectFirst() {
		return (GLRectangle) objects.get(2);
	}
	public GLRectangle getRectSecond() {
		return (GLRectangle) objects.get(3);
	}
	public GLRectangle getRectThird() {
		return (GLRectangle) objects.get(4);
	}
}
