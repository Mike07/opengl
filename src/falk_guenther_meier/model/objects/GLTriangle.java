package falk_guenther_meier.model.objects;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLPrimitiveObject;
import falk_guenther_meier.model.textures.GLObjectTextureBinding;
import falk_guenther_meier.model.textures.GLTexRect;

// TODO (evtl.): Exceptions werfen, wenn Vertexes gleich sind!!
public class GLTriangle extends GLPrimitiveObject {
	public GLTriangle(GLVertex aCCW, GLVertex bCCW, GLVertex cCCW, boolean isCCW) {
		super();
		nodes = new GLVertex[3];
		if (isCCW) {
			nodes[0] = aCCW;
			nodes[1] = bCCW;
			nodes[2] = cCCW;
		} else {
			nodes[0] = aCCW;
			nodes[1] = cCCW;
			nodes[2] = bCCW;
		}
		position = new GLVertex(0.0f, 0.0f, 0.0f);
	}

	/**
	 * Alle Vektoren sind Ortsvektoren.
	 * @param aCCW Vektoren f�r die drei Dreieck-Punkte
	 * @param bCCW
	 * @param cCCW
	 * @param eye Vektor, von dem aus das Dreieck seine Vorderseite zeigen soll (darf nicht in der Ebene des Dreiecks liegen!)
	 */
	public GLTriangle(GLVertex aCCW, GLVertex bCCW, GLVertex cCCW, GLVertex eye) {
		super();
		GLVertex crossT = GLVertex.mult(GLVertex.sub(bCCW, aCCW), GLVertex.sub(cCCW, aCCW)); // in Ecke a
		crossT.normalize();
		crossT = GLVertex.add(aCCW, crossT);
		GLVertex crossF = GLVertex.mult(GLVertex.sub(cCCW, aCCW), GLVertex.sub(bCCW, aCCW)); // in Ecke a
		crossF.normalize();
		crossF = GLVertex.add(aCCW, crossF);
		nodes = new GLVertex[3];
		if (GLVertex.getDistance(eye, crossT) <= GLVertex.getDistance(eye, crossF)) {
			nodes[0] = aCCW;
			nodes[1] = bCCW;
			nodes[2] = cCCW;
		} else {
			nodes[0] = aCCW;
			nodes[1] = cCCW;
			nodes[2] = bCCW;
		}
		position = new GLVertex(0.0f, 0.0f, 0.0f);
	}

	public GLVertex getFirst() {
		return nodes[0];
	}
	public GLVertex getSecond() {
		return nodes[1];
	}
	public GLVertex getThird() {
		return nodes[2];
	}

	public GLTriangle(GLVertex[] vCCW, boolean isCCW) {
		this(vCCW[0],vCCW[1], vCCW[2], isCCW);
	}

	@Override
	public String toString() {
		return "Triangle: " + nodes[0] + " " + nodes[1] + " " + nodes[2];
	}

	public void checkNaN() {
		try {
			nodes[0].checkNaN();
			nodes[1].checkNaN();
			nodes[2].checkNaN();
		} catch (IllegalArgumentException e) {
			System.out.println(this);
			throw e;
		}
	}

	/**
	 * Berechnet den Fl�cheninhalt des Dreiecks.
	 * @return
	 */
	public float getSurfaceArea() {
		return 0.5f * GLVertex.mult(GLVertex.sub(nodes[1], nodes[0]), GLVertex.sub(nodes[2], nodes[0])).getNorm();
	}

	/**
	 * Setzt die Textur-Koordinaten aus einem Bereich der Textur
	 * Gilt f�r alle Texture-Units!
	 * @param col die Spalte aus der Textur
	 * @param row die Zeile aus der Textur
	 * @param edge Ecke, der gegen�ber die Grundseite des Dreiecks liegt und die zum Dreieck geh�rt
	 * (0: unten links, 1: unten rechts, 2: oben rechts, 3: oben links); TODO: verbessern!, ist so sehr unklar!!
	 */
	public void setTexRect(int col, int row, int edge, boolean cCW) {
		for (GLObjectTextureBinding textureBinding: this.getTextureBindings()) {
			GLTexRect texRect = textureBinding.getTexture().getTexGrid().getTexRect(col, row).clone();
			texRect.setTextureUnit(textureBinding.getTextureUnit());
			for (int i = 0; i < 3; i++) {
				GLVertex vertex = cCW ? nodes[i] : nodes[2 - i];
				vertex.setTexCoord(texRect.getTexCoord((edge + i) % 4));
			}
		}
	}
	
	public void setTexRect(int col, int row, int a, int b, int c) {
		for (GLObjectTextureBinding textureBinding : this.getTextureBindings()) {
			GLTexRect texRect = textureBinding.getTexture().getTexGrid().getTexRect(col, row).clone();
			texRect.setTextureUnit(textureBinding.getTextureUnit());
			nodes[0].setTexCoord(texRect.getTexCoord(a));
			nodes[1].setTexCoord(texRect.getTexCoord(b));
			nodes[2].setTexCoord(texRect.getTexCoord(c));
		}
	}

	@Override
	public final void beginGL(GL2 gl) {
		super.beginGL(gl);
		gl.glBegin(GL2.GL_TRIANGLES);
			nodes[0].toGL(gl, position);
			nodes[1].toGL(gl, position);
			nodes[2].toGL(gl, position);
		gl.glEnd();	
	}
}
