package falk_guenther_meier.model.interpolation;

import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.math.GLVertex;

/**
 * Interpolation, die interpolierte Werte auf einer Bezierkurve zwischen Start- und Endpunkt der Interpolation bildet.
 * @author Johannes
 */
public class BezierInterpolation extends AbstractMathInterpolation {
	/**
	 * Enth�lt alle (n+1) Vektoren der Bezierkurve (Startpunkt, St�tzvektoren, Endpunkt).
	 */
	private List<GLVertex> vectors;

	/**
	 * 
	 * @param time
	 * @param beginTime
	 * @param interpolationTime
	 * @param startPoint
	 * @param directionEndPoint
	 * @param isAbsolute true: "directionEndPoint" ist der Ortsvektor des Endpunkts, false: "directionEndPoint" ist
	 * @param vectors die angegebenen St�tzvektoren m�ssen Ortsvektoren sein!
	 */
	public BezierInterpolation(GLVertex startPoint, GLVertex directionEndPoint, boolean isAbsolute,
			GLVertex... vectors) {
		super(startPoint, directionEndPoint, isAbsolute);
		this.vectors = new ArrayList<GLVertex>();
		this.vectors.add(startPoint.clone());
		for (GLVertex v : vectors) {
			this.vectors.add(v);
		}
		this.vectors.add(this.getEndPoint());
	}

	@Override
	public GLVertex calculateInterpolatedVertex(float factor) {
		int n = this.vectors.size() - 1;
		GLVertex result = new GLVertex();
		for (int i = 0; i < n + 1; i++) { // 0 <= i <= n
			int bin = binomial(n, i); // (n �ber i)
			result.add(GLVertex.mult(this.vectors.get(i),
					(float) (bin * Math.pow(1.0 - factor, n - i) * Math.pow(factor, i))));
		}
		return result;
	}

	@Override
	protected GLVertex calculateInterpolatedDirection(float factor) {
		// TODO h�ssliche und ungenaue L�sung!!!! (geht es besser??)
		float diff = 0.0001f;
		GLVertex first = this.getInterpolatedPoint(factor - diff);
		GLVertex second = this.getInterpolatedPoint(factor + diff);
		GLVertex result = GLVertex.sub(second, first);
		result.normalize();
		return result;
	}

	private int binomial(int n, int k) { // TODO: gibst daf�r auch bereits implementierte / bessere Funktionen ?!
		return fakultaet(n) / (fakultaet(k) * fakultaet(n - k));
	}

	private int fakultaet(int n) {
		int result = 1;
		for (int i = 1; i <= n; i++) {
			result *= i;
		}
		return result;
	}
}
