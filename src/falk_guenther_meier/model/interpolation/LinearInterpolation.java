package falk_guenther_meier.model.interpolation;

import falk_guenther_meier.model.math.GLVertex;

/**
 * Interpolation, die interpolierte Werte auf einer Geraden zwischen Start- und Endpunkt der Interpolation bildet.
 * @author Johannes
 */
public class LinearInterpolation extends AbstractMathInterpolation {
	/**
	 * Konstruktor
	 * @param startPoint
	 * @param directionEndPoint
	 * @param isAbsolute true: "directionEndPoint" ist der Ortsvektor des Endpunkts, false: "directionEndPoint" ist
	 */
	public LinearInterpolation(GLVertex startPoint, GLVertex directionEndPoint, boolean isAbsolute) {
		super(startPoint, directionEndPoint, isAbsolute);
	}

	@Override
	protected GLVertex calculateInterpolatedVertex(float factor) {
		return GLVertex.add(this.getStartPoint(), GLVertex.mult(this.getDirectionEndPoint(), factor));
	}

	@Override
	protected GLVertex calculateInterpolatedDirection(float factor) {
		return this.getDirectionEndPoint().clone();
	}
}
