package falk_guenther_meier.model.interpolation;

import falk_guenther_meier.model.math.GLVertex;

/**
 * Interpolation, die immer den gleichen Vektor berechnet.
 * Dient dazu, einen Stillstand des zu animierenden Objekts in eine Animation einzubauen.
 * @author Johannes
 */
public class ConstantInterpolation extends AbstractMathInterpolation {
	private GLVertex direction;

	public ConstantInterpolation(GLVertex constantPoint) {
		this(constantPoint, new GLVertex());
	}

	/**
	 * 
	 * @param constantPoint
	 * @param direction Richtung, die als aktuelle Richtung der Animation bei der entsprechenden Methode
	 * zur�ck gegeben werden soll (normalerweise: (0,0,0))
	 */
	public ConstantInterpolation(GLVertex constantPoint, GLVertex direction) {
		super(constantPoint, new GLVertex(), false);
		this.direction = direction;
	}

	@Override
	protected GLVertex calculateInterpolatedVertex(float factor) {
		return this.getStartPoint().clone();
	}

	@Override
	protected GLVertex calculateInterpolatedDirection(float factor) {
		return this.direction.clone();
	}
}
