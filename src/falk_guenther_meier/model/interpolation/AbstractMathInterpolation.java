package falk_guenther_meier.model.interpolation;

import falk_guenther_meier.model.math.GLVertex;

/**
 * Basisklasse, um Interpolationen zwischen Positionen / Vektoren durchzuf�hren.
 * @author Johannes
 */
public abstract class AbstractMathInterpolation {
	private GLVertex startPoint;
	private GLVertex directionEndPoint;

	/**
	 * Konstruktor f�r eine beliebige Interpolation
	 * @param startPoint Ortsvektor des Startpunkts
	 * @param directionEndPoint Vektor f�r den Endpunkt
	 * @param isAbsolute true: "directionEndPoint" ist der Ortsvektor des Endpunkts, false: "directionEndPoint" ist
	 * der (relative) Vektor zwischen den Ortsvektoren von Start- und Endpunkt
	 */
	public AbstractMathInterpolation(GLVertex startPoint, GLVertex directionEndPoint, boolean isAbsolute) {
		super();
		this.startPoint = startPoint;
		if (isAbsolute) {
			this.directionEndPoint = GLVertex.sub(directionEndPoint, startPoint);
		} else {
			this.directionEndPoint = directionEndPoint;
		}
	}

	/**
	 * Liefert einen interpolierten Vektor in Abh�ngigkeit eines Faktors zwischen 0 und 1.
	 * 0 bezeichnet den Startpunkt der Interpolation, 1 den Endpunkt. Werte zwischen 0 und 1 werden auf einen
	 * interpolierten Vektor zwischen Start- und Endpunkt abgebildet.
	 * @param factor
	 * @return
	 */
	public final GLVertex getInterpolatedPoint(float factor) {
		if (factor < 0.0f) {
			return this.getStartPoint().clone();
		}
		if (factor > 1.0f) {
			return this.getEndPoint().clone();
		}
		return this.calculateInterpolatedVertex(factor);
	}

	protected abstract GLVertex calculateInterpolatedVertex(float factor);

	/**
	 * Liefert die aktuelle (Bewegungs-)Richtung an der Stelle eines interpolierten Vektors
	 * in Abh�ngigkeit eines Faktors zwischen 0 und 1.
	 * 0 bezeichnet den Startpunkt der Interpolation, 1 den Endpunkt. Werte zwischen 0 und 1 werden auf einen
	 * interpolierten Vektor zwischen Start- und Endpunkt abgebildet.
	 * Die Richtung / Steigung / Bewegungsrichtung an dieser Stelle wird zur�ckgegeben.
	 * @param factor
	 * @return
	 */
	public final GLVertex getInterpolatedDirection(float factor) {
		if (factor < 0.0f) {
			return this.getStartDirection().clone();
//		 	return new GLVertex(); // TODO: ist Design-Entscheidung!
		}
		if (factor > 1.0f) {
			return this.getEndDirection().clone();
//			return new GLVertex();
		}
		return this.calculateInterpolatedDirection(factor);		
	}

	protected abstract GLVertex calculateInterpolatedDirection(float factor);

	/**
	 * Start-Vektor (Ortsvektor)
	 * @return
	 */
	public final GLVertex getStartPoint() {
		return startPoint;
	}

	/**
	 * Vektor des Endpunkts der Interpolation vom Startpunkt aus (Richtung).
	 * Dieser Vektor gibt somit sowohl die "Richtung" der Interpolation als auch den
	 * (vom Startpunkt aus relativ gesehenen) Endpunkt der Interpolation.
	 * @return
	 */
	protected final GLVertex getDirectionEndPoint() {
		return directionEndPoint;
	}

	/**
	 * Liefert den Endpunkt der Interpolation
	 * @return
	 */
	public final GLVertex getEndPoint() {
		return GLVertex.add(this.getStartPoint(), this.getDirectionEndPoint());
	}

	/**
	 * Liefert die (Bewegungs-)Richtung am Anfang der Interpolation.
	 * @return
	 */
	public final GLVertex getStartDirection() {
		return this.getInterpolatedDirection(0.0f);
	}

	/**
	 * Liefert die (Bewegungs-)Richtung am Ende der Interpolation.
	 * @return
	 */
	public final GLVertex getEndDirection() {
		return this.getInterpolatedDirection(1.0f);
	}

	@Override
	public String toString() {
		return "[" + startPoint + ", " + directionEndPoint + "]";
	}
}
