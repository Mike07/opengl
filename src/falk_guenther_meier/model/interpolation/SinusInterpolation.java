package falk_guenther_meier.model.interpolation;

import falk_guenther_meier.model.math.GLVertex;

/**
 * Interpolation, die interpolierte Werte auf einer Geraden mit Sinus-Ausschlägen
 * zwischen Start- und Endpunkt der Interpolation bildet.
 * @author Johannes
 */
public class SinusInterpolation extends AbstractMathInterpolation {
	private GLVertex halfAmplitude;
	private int countIterations;

	private LinearInterpolation line;

	/**
	 * 
	 * @param startPoint
	 * @param directionEndPoint
	 * @param isAbsolute true: "directionEndPoint" ist der Ortsvektor des Endpunkts, false: "directionEndPoint" ist
	 * @param halfAmplitude
	 * @param countIterations
	 */
	public SinusInterpolation(GLVertex startPoint, GLVertex directionEndPoint, boolean isAbsolute,
			GLVertex halfAmplitude, int countIterations) {
		super(startPoint, directionEndPoint, isAbsolute);
		this.halfAmplitude = halfAmplitude;
		this.countIterations = countIterations;

		this.line = new LinearInterpolation(startPoint, directionEndPoint, isAbsolute);
	}

	@Override
	protected GLVertex calculateInterpolatedVertex(float factor) {
		GLVertex pos = this.line.getInterpolatedPoint(factor);
		float angle = 360.0f * this.countIterations * factor;
		pos = GLVertex.add(pos,	GLVertex.mult(this.halfAmplitude, (float) Math.sin(Math.toRadians(angle))));
		return pos;
	}

	@Override
	protected GLVertex calculateInterpolatedDirection(float factor) {
//		GLVertex direction = this.line.getInterpolatedDirection(factor);
//		float angle = 360.0f * this.countIterations * factor;
//		float d = (float) Math.cos(Math.toRadians(angle)); // Steigung der normalen Sinus-Funktion
//		// TODO das Folgende ist nicht korrekt !!
//		return GLVertex.add(direction, new GLVertex(1.0f, d, 0.0f));
		float diff = 0.0001f;
		GLVertex first = this.getInterpolatedPoint(factor - diff);
		GLVertex second = this.getInterpolatedPoint(factor + diff);
		GLVertex result = GLVertex.sub(second, first);
		result.normalize();
		return result;
	}

	/**
	 * Vektor (relativ vom Startpunkt aus), der die halbe Amplitude des Sinus-Ausschlags angibt
	 * @return
	 */
	public GLVertex getHalfAmplitude() {
		return halfAmplitude;
	}

	/**
	 * Anzahl der vollständigen Sinus-360-Grad-Abschnitte während der Interpolation
	 * @return
	 */
	public int getCountIterations() {
		return countIterations;
	}
}
