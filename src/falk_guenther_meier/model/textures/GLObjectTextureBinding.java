package falk_guenther_meier.model.textures;

public class GLObjectTextureBinding {
	private GLTexture2D texture;
	private int textureUnit;
	
	public GLObjectTextureBinding(GLTexture2D texture, int textureUnit) {
		this.texture = texture;
		this.textureUnit = textureUnit;
	}
	
	public GLTexture2D getTexture() {
		return texture;
	}

	public void setTexture(GLTexture2D texture) {
		this.texture = texture;
	}

	public int getTextureUnit() {
		return textureUnit;
	}

	public void setTextureUnit(int textureUnit) {
		this.textureUnit = textureUnit;
	}
}
