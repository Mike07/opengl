package falk_guenther_meier.model.textures;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import javax.imageio.ImageIO;
import javax.media.opengl.GL2;

public abstract class GLTexture {	
	protected int textureID;
	protected int target;
	private boolean loaded;
	private int glTextureWrap; // GL2.GL_CLAMP_TO_EDGE, GL2.GL_REPEAT

	/**
	 * @param texture wenn null �bergeben wird, dann muss die Textur in load gesetzt werden
	 */
	public GLTexture() {
		loaded = false;
		this.glTextureWrap = GL2.GL_CLAMP_TO_EDGE;
	}

	public synchronized void setTexture(int textureID, int target) {
		this.textureID = textureID;
		this.target = target;
	}
	
	public int getTextureID() {
		return textureID;
	}
	
	
	public synchronized boolean isLoaded() {
		return loaded;
	}
	
	/**
	 * Setzt Parameter wie Clamp / Repeat, sowie MipMap-Verhalten
	 * f�r den Kontext der Textur (GL2.GL_TEXTURE_2D / GL2.GL_TEXTURE_CUBE_MAP)
	 * @param gl
	 */
	protected void setTexParameters(GL2 gl) {
		//gl.glTexParameteri(target, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_NEAREST);
		//gl.glTexParameteri(target, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_NEAREST);
		
		gl.glTexParameteri(target, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
		gl.glTexParameteri(target, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
		
		gl.glTexParameteri(target, GL2.GL_TEXTURE_WRAP_S, glTextureWrap);
		gl.glTexParameteri(target, GL2.GL_TEXTURE_WRAP_T, glTextureWrap);
		gl.glTexParameteri(target, GL2.GL_TEXTURE_WRAP_R, glTextureWrap);

		gl.glTexParameteri(target, GL2.GL_GENERATE_MIPMAP, GL2.GL_TRUE);
	}
	
	/**
	 * Wurde die Textur noch nicht initialisiert,
	 * bzw. wird beim Initialisieren der gl-Kontext ben�tigt,
	 * so muss die Textur an dieser Stelle initialisiert werden.
	 * 
	 * Gibt es mehrere M�glichkeiten, eine Textur zu initialisieren, dann muss 
	 * 1. klar sein, welche M�glichkeit genommen wird, 
	 * 2. alle entsprechenden Parameter m�ssen verf�gbar sein.
	 * @param gl
	 */
	
	public void load(GL2 gl) {
		
		loaded = true;
	}
	
	/**
	 * Setzt den Texture-Wrap-Mode (GL2.GL_CLAMP_TO_EDGE, GL2.GL_REPEAT, ...)
	 * Muss im init, vor dem binden bzw. vor load gesetzt werden
	 */
	public void setGLTextureWrap(int glTextureWrap) {
		this.glTextureWrap = glTextureWrap;
	}
	
	/**
	 * Bindet die Textur in gl ein. Ist die Textur noch nicht geladen, so geschieht dies automatisch.
	 * Wichtig: Sicherstellen, dass gl.glEnable(GL.GL_TEXTURE_2D) vorher aufgerufen wird!
	 * @param gl
	 */
	public synchronized void bind(GL2 gl) {
		if (!loaded) {
			load(gl);
		}
		setTexParameters(gl);
		gl.glBindTexture(target, textureID);
	}
	
	/**
	 * Gibt die Textur aus dem Speicher frei
	 * @param gl
	 */
	public void delete(GL2 gl) {
		int[] textureIDs = new int[]{textureID};
		gl.glDeleteTextures(1, textureIDs, 0);
	}
	
	static public int genTextureID(GL2 gl) {
	    int[] tmp = new int[1];
	    gl.glGenTextures(1, tmp, 0);
	    return tmp[0];
	  }
	
	static public void loadImage(GL2 gl, File file, int target){
		int width=0;
		int height=0;

		try {
			BufferedImage bufferedImage = ImageIO.read(file);
			
			int format = bufferedImage.getColorModel().hasAlpha() ? GL2.GL_RGBA : GL2.GL_RGB;
			width = bufferedImage.getWidth();
			height = bufferedImage.getHeight();
			ByteBuffer buffer = ByteBuffer.allocateDirect(width*height*4);
			IntBuffer buf = buffer.asIntBuffer();

			for(int i = 0;i < height; i++) {
				for(int l = 0;l < width; l++) {
					int color = bufferedImage.getRGB(l,i);

					
					int blue = color & 0x000000ff;
					int green = (color & 0x0000ff00) >> 8;
					int red = (color & 0x00ff0000) >> 16;
					
					int alpha;
					if (format == GL2.GL_RGBA) {
						alpha = (color & 0xff000000) >> 24;
						if (alpha < 0) {
							alpha = 0xFF;
						}
					} else {
						alpha = 0xFF;
					}
					
					
	
					color=red*0x1000000+green*0x10000+blue*0x100+alpha;

					buf.put(color);
				}	
			}
			gl.glTexParameteri(target, GL2.GL_GENERATE_MIPMAP, GL2.GL_TRUE);
			gl.glTexImage2D(target, 0, GL2.GL_RGBA, width, height, 0, GL2.GL_RGBA, GL2.GL_UNSIGNED_BYTE, buffer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
