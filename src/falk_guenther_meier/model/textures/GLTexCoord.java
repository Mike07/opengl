package falk_guenther_meier.model.textures;

import javax.media.opengl.GL2;

/**
 * Eine Textur-Koordinate
 * @author hanno
 */
public class GLTexCoord {	
	private float u;
	private float v;
	private int textureUnit;

	public GLTexCoord(float u, float v) {
		this.u = u;
		this.v = v;
		this.textureUnit = 0;
	}
	
	public GLTexCoord(float u, float v, int textureUnit) {
		this.u = u;
		this.v = v;
		this.textureUnit = textureUnit;
	}
	
	public GLTexCoord(GLTexCoord copyTexCoord) {
		this.u = copyTexCoord.u;
		this.v = copyTexCoord.v;
		this.textureUnit = copyTexCoord.textureUnit;
	}

	public void toGL(GL2 gl) {
		gl.glMultiTexCoord2f(GL2.GL_TEXTURE0 + textureUnit, u, v);
	}

	public float getU() {
		return this.u;
	}
	
	public void setU(float u) {
		this.u = u;
	}

	public float getV() {
		return this.v;
	}
	
	public void setV(float v) {
		this.v = v;
	}

	public int getTextureUnit() {
		return textureUnit;
	}

	public void setTextureUnit(int textureUnit) {
		this.textureUnit = textureUnit;
	}
	
	public GLTexCoord clone() {
		return new GLTexCoord(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + textureUnit;
		result = prime * result + Float.floatToIntBits(u);
		result = prime * result + Float.floatToIntBits(v);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GLTexCoord other = (GLTexCoord) obj;
		if (textureUnit != other.textureUnit)
			return false;
		if (Float.floatToIntBits(u) != Float.floatToIntBits(other.u))
			return false;
		if (Float.floatToIntBits(v) != Float.floatToIntBits(other.v))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GLTexCoord [u=" + u + ", v=" + v + ", textureUnit="
				+ textureUnit + "]";
	}	
}