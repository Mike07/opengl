package falk_guenther_meier.model.textures;

/**
 * Liste von Textur-Koordinaten f�r Rechtecks-Bereiche aus Texturen
 * @author hanno
 */
public class GLTexRect {
	private GLTexCoord[] texCoords;
	private int textureUnit;
	
	public GLTexRect() {
		texCoords = new GLTexCoord[4];
		texCoords[0] = new GLTexCoord(0,1); // links unten
		texCoords[1] = new GLTexCoord(1,1); // rechts unten
		texCoords[2] = new GLTexCoord(1,0); // rechts oben
		texCoords[3] = new GLTexCoord(0,0); // links oben
		textureUnit = 0;
	}
	
	/**
	 * 
	 * @param x die x-Koordinate des Punktes links unten
	 * @param y die y-Koordinate des Punktes links unten
	 * @param width die Breite des Rechtecks
	 * @param height die H�he des Rechtecks
	 */
	public GLTexRect(float x, float y, float width, float height) {
		texCoords = new GLTexCoord[4];
		texCoords[0] = new GLTexCoord(x, y + height); // links unten
		texCoords[1] = new GLTexCoord(x + width, y + height); // rechts unten
		texCoords[2] = new GLTexCoord(x + width, y); // rechts oben
		texCoords[3] = new GLTexCoord(x, y); // links oben
		textureUnit = 0;
	}
	
	/**
	 * Erstellt ein neues TexRect als eine Kopie aus einem vorhandenen.
	 * @param copyTexRect
	 */
	public GLTexRect(GLTexRect copyTexRect) {
		texCoords = new GLTexCoord[4];
		for (int i = 0; i < 4; i++) {
			texCoords[i] = copyTexRect.texCoords[i].clone();
		}
	}

	public GLTexCoord getBottomLeft() {
		return texCoords[0];
	}

	public GLTexCoord getBottomRight() {
		return texCoords[1];
	}

	public GLTexCoord getTopRight() {
		return texCoords[2];
	}

	public GLTexCoord getTopLeft() {
		return texCoords[3];
	}
	
	public float getX() {
		return getTopLeft().getU();
	}
	
	public float getY() {
		return getTopLeft().getV();
	}
	
	public float getWidth() {
		return getBottomRight().getU() - getTopLeft().getU();
	}
	
	public float getHeight() {
		return getBottomRight().getV() - getTopLeft().getV();
	}

	/**
	 * 
	 * @param i 0: links unten, 1: rechts unten, 2: rechts oben, 3: links oben
	 * @return
	 */
	public GLTexCoord getTexCoord(int i) {
		return texCoords[i];
	}
	
	public GLTexRect clone() {
		return new GLTexRect(this);
	}

	public void setTextureUnit(int textureUnit) {
		this.textureUnit = textureUnit;
		for (GLTexCoord texCoord: texCoords) {
			texCoord.setTextureUnit(textureUnit);
		}
	}
	
	public int getTextureUnit() {
		return this.textureUnit;
	}
}
