package falk_guenther_meier.model.textures;

import javax.media.opengl.GL2;

import falk_guenther_meier.model.GLable;

public class GLFramebuffer implements GLable {
	public enum DrawMode {
		DRAW_DEPTH, DRAW_COLOR
	}
	
	int width;
	int height;
	DrawMode drawMode;
	int framebufferID;
	GLTexture2D texture;
	
	boolean loaded;
	
	/**
	 * Initialisiert ein neues Framebuffer-Objekt
	 * @param width Breite in Pixel
	 * @param height H�he in Pixel
	 */
	public GLFramebuffer(int width, int height, DrawMode drawMode) {
		this.width = width;
		this.height = height;
		this.drawMode = drawMode;
		this.texture = new GLTexture2D();
		//texture.setGLTextureWrap(GL2.GL_CLAMP_TO_BORDER); // GL_CLAMP, GL_CLAMP_TO_EDGE
		this.loaded = false;
	}
	
	/**
	 * Quelle: http://fabiensanglard.net/shadowmapping/index.php
	 * depth Framebuffer
	 */
	private void load(GL2 gl) {
		texture.setTexture(GLTexture.genTextureID(gl), GL2.GL_TEXTURE_2D);
		texture.bind(gl);

		this.framebufferID = genFramebufferID(gl);
		gl.glBindFramebuffer(GL2.GL_DRAW_FRAMEBUFFER, framebufferID); // GL_DRAW_FRAMEBUFFER
		
		if (drawMode == DrawMode.DRAW_DEPTH) {
			// depth
			//gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_DEPTH_TEXTURE_MODE, GL2.GL_INTENSITY);
			gl.glTexImage2D(GL2.GL_TEXTURE_2D, 0, GL2.GL_DEPTH_COMPONENT, width, height, 0, GL2.GL_DEPTH_COMPONENT, GL2.GL_UNSIGNED_INT, null); // GL_UNSIGNED_BYTE
			gl.glFramebufferTexture2D(GL2.GL_FRAMEBUFFER, GL2.GL_DEPTH_ATTACHMENT, GL2.GL_TEXTURE_2D, texture.getTextureID(), 0);
			
			// Instruct openGL that we won't bind a color texture with the currently bound FBO
			gl.glDrawBuffer(GL2.GL_NONE);
			gl.glReadBuffer(GL2.GL_NONE);
		} else if  (drawMode == DrawMode.DRAW_COLOR) {
			// color
			gl.glTexImage2D(GL2.GL_TEXTURE_2D, 0, GL2.GL_RGBA, width, height, 0, GL2.GL_RGBA, GL2.GL_UNSIGNED_BYTE, null);
			gl.glFramebufferTexture2D(GL2.GL_FRAMEBUFFER, GL2.GL_COLOR_ATTACHMENT0, GL2.GL_TEXTURE_2D, texture.getTextureID(), 0);
		}
		
		checkHasErrors(gl);
		
		gl.glBindFramebuffer(GL2.GL_DRAW_FRAMEBUFFER, 0); // switch back to window-system-provided framebuffer // GL_DRAW_FRAMEBUFFER
		this.loaded = true;
	}
	
	public GLTexture2D getTexture() {
		return this.texture;
	}
	
	// TODO
//	public void refreshTexture(GL2 gl, GLDrawable world) {
//	}
	
	/**
	 * Hier wird die Aufnahme vor dem Zeichnen der Welt gestartet
	 * @param gl
	 */
	public void beginGL(GL2 gl) {
		if (!loaded) {
			load(gl);
		}
		gl.glPushAttrib(GL2.GL_VIEWPORT_BIT);
		gl.glViewport(0, 0, width, height);
		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, framebufferID);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
	}
	
	/**
	 * Hier wird die Aufnahme nach dem Zeichnen der Welt beendet
	 * @param gl
	 */
	public void endGL(GL2 gl) {
		gl.glPopAttrib();
		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, 0); // switch back to window-system-provided framebuffer
	}
	
	/**
	 * Entfernt den Framebuffer aus dem Speicher
	 * @param gl
	 */
	public void delete(GL2 gl) {
		int[] framebufferIDs = new int[]{framebufferID};
		gl.glDeleteFramebuffers(1, framebufferIDs, 0);
	}
	
	public boolean checkHasErrors(GL2 gl){
		int error = gl.glCheckFramebufferStatus(GL2.GL_FRAMEBUFFER);
		switch( error ){
		case GL2.GL_FRAMEBUFFER_COMPLETE:
			System.out.println( "FRAMEBUFFER COMPLETE"); return true;
		case GL2.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			System.out.println( "Incomplete attachment");break;
		case GL2.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			System.out.println("Missing attachment"); break;
		case  GL2.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
			System.out.println("Incomplete Dimensions"); break;
		case  GL2.GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
			System.out.println("Incomplete formats"); break;
		case  GL2.GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
			System.out.println("Incomplete draw buffer"); break;
		case GL2.GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
			System.out.println("Incomplete read buffer"); break;
		case GL2.GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
			System.out.println("Incomplete multisample"); break;
		case GL2.GL_FRAMEBUFFER_UNSUPPORTED:
			System.out.println("Framebufferobjects unsupported"); break;
		}
		return false;
	}
	
	static public int genFramebufferID(GL2 gl) {
	    int[] array = new int[1];
	    gl.glGenFramebuffers(1, array, 0);
	    return array[0];
	}
	
	

}
