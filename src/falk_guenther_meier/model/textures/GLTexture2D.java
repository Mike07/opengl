package falk_guenther_meier.model.textures;

import java.io.File;

import javax.media.opengl.GL2;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

public class GLTexture2D extends GLTexture {	
	public enum LoadMode {
		LOAD_JOGAMP, LOAD_IMAGE_BUFFER
	}
	
	private GLTexGrid texGrid;
	private File file;
	private boolean loadfromFileMode;
	private LoadMode loadMode;
	
	public GLTexture2D() {
		super();
		texGrid = new GLTexGrid(1, 1);
		loadfromFileMode = false;
		loadMode = LoadMode.LOAD_IMAGE_BUFFER;
	}
	
	public GLTexture2D(GL2 gl, int width, int height) {
		this();
		int textureID = GLTexture.genTextureID(gl);
		TextureIO.newTexture(textureID, GL2.GL_TEXTURE_2D, width, height, width, height, false);
		setTexture(textureID, GL2.GL_TEXTURE_2D);
	}
	
	/**
	 * erstellt eine neue Textur
	 * @param file Datei muss ab jetzt bereits vorliegen
	 */
	public GLTexture2D(File file) {
		this();
		this.file = file;
		loadfromFileMode = true;
	}

	public GLTexGrid getTexGrid() {
		return texGrid;
	}
	
	public void setTexGrid(GLTexGrid texGrid) {
		this.texGrid = texGrid;
	}
	
	@Override
	public void load(GL2 gl) {
		if (loadfromFileMode) {
			Texture texture = getTextureFromFile(gl, file, loadMode);
			this.setTexture(texture.getTextureObject(gl), GL2.GL_TEXTURE_2D);
		}
		super.load(gl);
	}
	
	private static Texture getTextureFromFile(GL2 gl, File file, LoadMode loadMode) {
		Texture texture = null;
		if (loadMode == LoadMode.LOAD_JOGAMP) {
			try {
				texture = TextureIO.newTexture(file, true);
			}
			catch(Exception e) {
				System.out.println("Error loading texture " + file.getPath());
				e.printStackTrace();
			}
		} else if (loadMode == LoadMode.LOAD_IMAGE_BUFFER) {
			texture = TextureIO.newTexture(GL2.GL_TEXTURE_2D);
			texture.bind(gl);
			loadImage(gl, file, GL2.GL_TEXTURE_2D);
		}
		
		return texture;
	}	
}