package falk_guenther_meier.model.textures;

import java.io.File;
import java.io.IOException;

import javax.media.opengl.GL2;
import javax.media.opengl.GLException;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

public class GLTextureCube extends GLTexture {
	
	public enum LoadMode {
		LOAD_TEXTUREDATA, LOAD_IMAGE_BUFFER
	}

	private LoadMode loadMode;
	private File[] cubeMapFiles;

	public GLTextureCube(File[] cubeMapFiles) {
		super();
		this.cubeMapFiles = cubeMapFiles;
		loadMode = LoadMode.LOAD_IMAGE_BUFFER;
	}
	
	
	@Override
	public void load(GL2 gl) {
		Texture texture = getTextureCubeFromFiles(gl, cubeMapFiles, loadMode);
		setTexture(texture.getTextureObject(gl), GL2.GL_TEXTURE_CUBE_MAP);
		super.load(gl);
	}
	

	/**
	 * Liefert eine neue TextureCube
	 * @param gl
	 * @param cubeMapFiles in der Reihenfolge right, left, top, down, front, back
	 * @param loadMode f�r die einzelnen Seiten JogAmp TextureIO oder einen ImageBuffer via loadImage verwenden?
	 * @return TextureCube als Texture
	 */
	private static Texture getTextureCubeFromFiles(GL2 gl, File[] cubeMapFiles, LoadMode loadMode) {
		int  cube[] = {  
				GL2.GL_TEXTURE_CUBE_MAP_POSITIVE_X,
				GL2.GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
				GL2.GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
				GL2.GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
				GL2.GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
				GL2.GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
			};
		
		Texture texture = TextureIO.newTexture(GL2.GL_TEXTURE_CUBE_MAP);
		texture.bind(gl);

		//gl.glTexParameteri(GL2.GL_TEXTURE_CUBE_MAP, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
		//gl.glTexParameteri(GL2.GL_TEXTURE_CUBE_MAP, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
		
		//Load Cube Map images
		for(int i = 0; i < 6; i++)
		{
			if (loadMode == LoadMode.LOAD_TEXTUREDATA) {
				try {
					texture.updateImage(gl, TextureIO.newTextureData(gl.getGLProfile(), cubeMapFiles[i], true, null), cube[i]);
				} catch (GLException | IOException e) {
					e.printStackTrace();
				}
			} else if (loadMode == LoadMode.LOAD_IMAGE_BUFFER) {
				loadImage(gl, cubeMapFiles[i], cube[i]);
			}
		}	
		
		return texture;
	}
}
