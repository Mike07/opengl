package falk_guenther_meier.model.textures;

/**
 * Teilt eine Textur in mehrere Zeilen und Spalten, dessen Rechtecke und zugeh�rige Textur-Koordinaten einzeln ausgelesen werden k�nnen
 * @author hanno
 */
public class GLTexGrid extends GLTexRect {
	private int colCount = 1;
	private int rowCount = 1;
	
	public GLTexGrid() {
		super();
	}
	
	public GLTexGrid(int colCount, int rowCount) {
		super();
		this.colCount = colCount;
		this.rowCount = rowCount;
	}
	
	/**
	 * Erstellt ein neues Textur-Grid innerhalb des eines Ausschnitts (inTexRect) der Textur.
	 */
	public GLTexGrid(GLTexRect inTexRect, int colCount, int rowCount) {
		super(inTexRect.getX(), inTexRect.getY(), inTexRect.getWidth(), inTexRect.getHeight());
		this.colCount = colCount;
		this.rowCount = rowCount;
	}
	
	/**
	 * Liefert die Anzahl der Spalten in einer Textur
	 */
	public int getColCount() {
		return colCount;
	}

	/**
	 * Setzt die Anzahl der Spalten einer Textur
	 */
	public void setColCount(int colCount) {
		this.colCount = colCount;
	}

	/**
	 * Liefert die Anzahl der Zeilen in einer Textur
	 */
	public int getRowCount() {
		return rowCount;
	}

	/**
	 * Setzt die Anzahl der Zeilen einer Textur
	 */
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}	
	
	/**
	 * Liefert ein Rechteck mit den Textur-Koordinaten der entsprechenden Spalte und Zeile
	 * @param col
	 * @param row
	 */
	public GLTexRect getTexRect(int col, int row) {
		float x = getX() + (float)col*(getWidth() / (float)colCount);
		float y = getY() + (float)row*(getHeight() / (float)rowCount);
		float width = getWidth()/(float)colCount;
		float height = getHeight()/(float)rowCount;
		return new GLTexRect(x, y, width, height);
	}
}
