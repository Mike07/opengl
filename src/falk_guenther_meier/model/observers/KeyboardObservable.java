package falk_guenther_meier.model.observers;

import java.util.List;

public interface KeyboardObservable {
	public void addKeyboardObserver(KeyboardObserver o);
	public void removeKeyboardObserver(KeyboardObserver o);
	public List<KeyboardObserver> getKeyboardObservers();
}
