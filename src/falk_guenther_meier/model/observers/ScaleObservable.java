package falk_guenther_meier.model.observers;

import java.util.List;

public interface ScaleObservable extends ObservableProperties {
	public void addScaleObserver(ScaleObserver o);
	public void removeScaleObserver(ScaleObserver o);
	public List<ScaleObserver> getScaleObservers();
}
