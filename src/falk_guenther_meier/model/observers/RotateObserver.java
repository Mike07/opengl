package falk_guenther_meier.model.observers;

import falk_guenther_meier.model.math.GLVertex;

public interface RotateObserver {
	public void updateRotate(RotateObservable changed, float x, float y, float z);
	public void updateRotate(RotateObservable changed, GLVertex point, float x, float y, float z);
	public void updateRotate(RotateObservable changed, GLVertex first, GLVertex second, float angle);
}
