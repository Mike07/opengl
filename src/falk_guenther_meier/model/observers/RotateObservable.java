package falk_guenther_meier.model.observers;

import java.util.List;

public interface RotateObservable extends ObservableProperties {
	public void addRotateObserver(RotateObserver o);
	public void removeRotateObserver(RotateObserver o);
	public List<RotateObserver> getRotateObservers();
}
