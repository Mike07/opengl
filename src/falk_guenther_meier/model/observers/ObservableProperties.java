package falk_guenther_meier.model.observers;

import falk_guenther_meier.model.math.GLVertex;

public interface ObservableProperties {
	public GLVertex getPosition();
}
