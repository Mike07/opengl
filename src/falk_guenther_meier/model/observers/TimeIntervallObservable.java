package falk_guenther_meier.model.observers;

import java.util.List;

import falk_guenther_meier.model.time.Timable;

public interface TimeIntervallObservable extends Timable {
	public void addTimeIntervallObserver(TimeIntervallObserver o);
	public void removeTimeIntervallObserver(TimeIntervallObserver o);
	public List<TimeIntervallObserver> getTimeIntervallObservers();
}
