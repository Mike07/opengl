package falk_guenther_meier.model.observers;


public interface ScaleObserver {
	public void updateScale(ScaleObservable changed, float x, float y, float z);
}
