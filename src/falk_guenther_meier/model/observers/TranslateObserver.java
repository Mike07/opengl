package falk_guenther_meier.model.observers;

import falk_guenther_meier.model.math.GLVertex;

public interface TranslateObserver {
	public void updateTranslate(TranslateObservable changed, float x, float y, float z);
	public void updateTranslate(TranslateObservable changed, GLVertex v);
	public void updateAbsolutePosition(TranslateObservable changed, GLVertex position);
}
