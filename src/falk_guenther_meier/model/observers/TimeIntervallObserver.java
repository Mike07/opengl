package falk_guenther_meier.model.observers;

public interface TimeIntervallObserver {
	public void updateTimeIntervall(TimeIntervallObservable changed);
}
