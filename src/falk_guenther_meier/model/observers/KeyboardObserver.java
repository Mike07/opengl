package falk_guenther_meier.model.observers;

public interface KeyboardObserver {
	public abstract void forward();
	public abstract void backward();
	public abstract void left();
	public abstract void right();
	public abstract void up();
	public abstract void down();
	public abstract void turnUp();
	public abstract void turnDown();
	public abstract void turnLeft();
	public abstract void turnRight();

	public abstract void swapWireMode();
	public abstract void cameraMode1();
	public abstract void cameraMode2();
	public abstract void cameraMode3();
	public abstract void cameraMode4();
	public abstract void swapLightMode();
	public abstract void swapAnimationMode();
	public abstract void swapAnimationStartMode();
	public abstract void keyZ();
	public abstract void keyU();
	public abstract void keyI();
	public abstract void keyO();
	public abstract void keyP();
	public abstract void keyH();
	
	public abstract void printMyKeys();
}
