package falk_guenther_meier.model.observers;

import java.util.List;

public interface AnimationStatusObservable {
	public void addAnimationStatusObserver(AnimationStatusObserver o);
	public void removeAnimationStatusObserver(AnimationStatusObserver o);
	public List<AnimationStatusObserver> getAnimationStatusObservers();
}
