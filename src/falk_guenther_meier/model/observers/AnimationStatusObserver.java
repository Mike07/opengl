package falk_guenther_meier.model.observers;

public interface AnimationStatusObserver {
	public void updateStartOfAnimation(AnimationStatusObservable changed);
	public void updateEndOfAnimation(AnimationStatusObservable changed);
	public void updatePause(AnimationStatusObservable changed);
	public void updateResume(AnimationStatusObservable changed);
}
