package falk_guenther_meier.model.observers;

import java.util.List;

public interface TranslateObservable extends ObservableProperties {
	public void addTranslateObserver(TranslateObserver o);
	public void removeTranslateObserver(TranslateObserver o);
	public List<TranslateObserver> getTranslateObservers();
}
