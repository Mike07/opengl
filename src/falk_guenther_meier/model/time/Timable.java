package falk_guenther_meier.model.time;

public interface Timable {
	public float getTime();
}
