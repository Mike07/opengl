package falk_guenther_meier.model.time;

import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.observers.TimeIntervallObservable;
import falk_guenther_meier.model.observers.TimeIntervallObserver;

/**
 * Sender, der Beobachter dar�ber informiert, wenn ein bestimmtes Zeitintervall abl�uft.
 * Die Zeit, die dieser Sender zur�ckliefert, bezieht sich nicht auf die aktuelle Zeit sondern auf die Zeit des
 * letzten verstrichen Intervalls.
 * @author Johannes
 */
public class TimeIntervallSender implements TimeIntervallObservable, TimeIntervallObserver {
	/**
	 * Zeitintervall, nach dem jeweils eine Benachrichtigung versendet wird
	 */
	private float intervall;
	/**
	 * Anzahl, wie h�ufig das Intervall bereits verstrichen ist
	 */
	private int intervallCount;
	/**
	 * Zeitpunkt, an dem dieser Zeitintervall-Sender initialisiert (d.h. gestartet) wurde
	 */
	private float startTime;
	/**
	 * Beobachter
	 */
	private List<TimeIntervallObserver> observer;

	/**
	 * Der Konstruktor meldet sich selbst�ndig am Zeitgeber an.
	 * @param time
	 * @param intervall
	 */
	public TimeIntervallSender(TimeIntervallObservable time, float intervall) {
		super();
		this.intervall = intervall;
		this.observer = new ArrayList<TimeIntervallObserver>();

		this.startTime = time.getTime();
		this.intervallCount = 0;
		time.addTimeIntervallObserver(this);
	}

	@Override
	public final void updateTimeIntervall(TimeIntervallObservable changed) {
		float diff = changed.getTime() - (this.intervallCount * this.intervall) - this.startTime;
		if (diff >= this.intervall) {
			this.intervallCount += Math.round((float) Math.floor(diff / this.intervall));
			for (TimeIntervallObserver o : this.observer) {
				o.updateTimeIntervall(this);
			}
		}
	}

	/**
	 * Liefert das Zeitintervall, nach dessen Ablauf alle Beoachter dar�ber informiert werden.
	 * @return
	 */
	public final float getIntervall() {
		return intervall;
	}

	/**
	 * Liefert den Zeitpunkt, an dem das Intervall das letzte Mal abgelaufen ist.
	 * Diese Methode liefert NICHT die aktuelle absolute Zeit!
	 */
	@Override
	public final float getTime() {
		return this.startTime + this.intervallCount * this.intervall;
	}

	@Override
	public final void addTimeIntervallObserver(TimeIntervallObserver o) {
		this.observer.add(o);
	}

	@Override
	public final void removeTimeIntervallObserver(TimeIntervallObserver o) {
		this.observer.remove(o);
	}

	@Override
	public final List<TimeIntervallObserver> getTimeIntervallObservers() {
		return this.observer;
	}
}
