package falk_guenther_meier.model.time;

import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.observers.TimeIntervallObservable;
import falk_guenther_meier.model.observers.TimeIntervallObserver;

/**
 * Zeitkomponente. updateTime muss zu Beginn eines Frames aufgerufen werden.
 * Zeiteinheit: Sekunden.
 * @author Michi, Johannes
 */
public final class TimeComponent implements Timable, TimeIntervallObservable {
	private long startTime;
	private long currentTime;
	private long lastTime;
	private boolean beforeFirstUpdate;
	private List<TimeIntervallObserver> observer;

	public TimeComponent() {
		this.restart();
		this.observer = new ArrayList<TimeIntervallObserver>();
	}

	/**
	 * Restart der Zeitkomponente.
	 * Auch der Konstruktor ruft am Ende "restart" auf.
	 */
	public void restart() {
		startTime = System.currentTimeMillis();
		lastTime = startTime;
		currentTime = startTime;
		beforeFirstUpdate = true;
	}

	/**
	 * Liefert die Zeit zwischen dem letzten Update und dem letzten Restart.
	 * @return
	 */
	@Override
	public float getTime() {
		if (beforeFirstUpdate) {
			return 0.0f;
		} else {
			return currentTime / 1000f;
		}
	}

	/**
	 * Liefert die Zeit, die zwischen den letzten beiden "update"-Aufrufen vergangen ist.
	 * @return
	 */
	public float getDiffToLastTime() {
		if (beforeFirstUpdate) {
			return 0.0f;
		} else {
			return (currentTime - lastTime) / 1000f;
		}
	}

	/**
	 * Aktualisiert die Zeitkomponente zu Beginn eines Frames.
	 */
	public void updateTime() {
		lastTime = currentTime;
		currentTime = System.currentTimeMillis() - startTime;
		if (beforeFirstUpdate) {
			beforeFirstUpdate = false;
		}
		for (TimeIntervallObserver o : this.observer) {
			o.updateTimeIntervall(this);
		}
	}

	@Override
	public void addTimeIntervallObserver(TimeIntervallObserver o) {
		this.observer.add(o);
	}

	@Override
	public void removeTimeIntervallObserver(TimeIntervallObserver o) {
		this.observer.remove(o);
	}

	@Override
	public List<TimeIntervallObserver> getTimeIntervallObservers() {
		return this.observer;
	}	
}
