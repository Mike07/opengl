package falk_guenther_meier.model;

import javax.media.opengl.GL2;

/**
 * Erzeugt eine DisplayList. GL-Code zwischen begin und end kann sp�ter durch execute ausgef�hrt werden.
 * @author Michael
 */
public class GLDisplayList {
	
	private int displayListID;
	private boolean begin;
	
	public GLDisplayList() {
		this.displayListID = -1;
		this.begin = false;
	}
	
	public void begin(GL2 gl) {
		this.displayListID = gl.glGenLists(1);
		gl.glNewList(displayListID, GL2.GL_COMPILE);
		this.begin = true;
	}
	
	public void end(GL2 gl) {
		if (begin) {
			gl.glEndList();
			this.begin = false;
		}
	}
	
	public void execute(GL2 gl) {
		if (this.displayListID != -1)
			gl.glCallList(displayListID);
	}
	
	public boolean isBegin() {
		return this.begin;
	}
	
}
