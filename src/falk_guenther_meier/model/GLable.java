package falk_guenther_meier.model;

import javax.media.opengl.GL2;

public interface GLable {
	public void beginGL(GL2 gl);
	public void endGL(GL2 gl);
}
