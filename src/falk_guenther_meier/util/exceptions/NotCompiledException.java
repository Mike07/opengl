package falk_guenther_meier.util.exceptions;

/**
 * Tritt auf, wenn useProgram aufgerufen wird, ohne das Programm vorher kompiliert zu haben.
 * @author Michi
 */
public class NotCompiledException extends Exception {
	public NotCompiledException() {
		System.err.println("useProgram wurde aufgerufen, ohne dass das Programm vorher kompiliert wurde.");
	}
}
