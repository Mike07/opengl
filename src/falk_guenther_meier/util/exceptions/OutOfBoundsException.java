package falk_guenther_meier.util.exceptions;

import falk_guenther_meier.model.math.GLVertex;

/**
 * Tritt auf, wenn ein Objekt (z.B. die Kamera) positionelle Grenzen überschreitet.
 * @author Michael
 */
public class OutOfBoundsException extends RuntimeException {
	public OutOfBoundsException(GLVertex pos) {
		System.err.println("Die Position " + pos + " befindet sich außerhalb des definierten Bereichs.");
	}
}
