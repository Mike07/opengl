package falk_guenther_meier.util.exceptions;

/**
 * Tritt auf, wenn eine Uniform-Variable als String �bergeben wurde, die aber im Shaderprogramm nicht existiert.
 * @author Hanno
 */
public class UniformVariableNotExistsException extends Exception {
	public UniformVariableNotExistsException(String uniformVar) {
		System.err.println("Die Uniform-Variable " + uniformVar + " existiert nicht im Shader.");
	}
}
