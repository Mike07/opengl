package falk_guenther_meier.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

public class FileLoader {
	
	/**
	 * Datei als Ressource unter falk_guenther_meier/files/ + name ermitteln
	 * @param name z.B. "dome256.jpg" f�r falk_guenther_meier/files/dome256.jpg
	 * @return die Datei
	 */
	public static File getFileFromFilesFolder(String name) {
		URL url = FileLoader.class.getResource("../files/"+name);
		//URL url = ClassLoader.getSystemResource("falk_guenther_meier/files/"+name);
		String path = "";
		if (url != null) {
			try {
				URI uri = url.toURI();
				path = uri.getPath();
			} catch (URISyntaxException e) {
			}
		}
		return new File(path);
	}
	
	/**
	 * Datei als Ressource unter falk_guenther_meier/files/ + name ermitteln
	 * @param name z.B. "dome256.jpg" f�r falk_guenther_meier/files/dome256.jpg
	 * @param fileChooserIfNotExists ruft bei true den Datei-�ffnen-Dialog auf, wenn die Datei nicht verf�gbar ist
	 * @return die Datei
	 */
	public static File getFileFromFilesFolder(String name, boolean fileChooserIfNotExists) {
		File file = getFileFromFilesFolder(name);
		if (!file.exists() && fileChooserIfNotExists) {
			String[] nameParts = name.split("\\.");
			String fileExt = "*";
			if (nameParts.length>0)
				fileExt = nameParts[nameParts.length-1];
			file = getFileFromFileChooser("Select File: " + name, fileExt);
		}
		return file;
	}
	
	
	/**
	 * Datei �ber Datei-�ffnen-Dialog ausw�hlen
	 * @param title der Titel f�r den Dialog
	 * @param fileEndings die akzeptierten Dateiendungen ohne Punkt, z.B. "jpg", "png", "gif", "bmp" f�r Bilddateien, oder "*" f�r alle Dateitypen
	 * @return die ausgew�hlte Datei, ansonsten eine nicht existierende Datei mit Pfad=""
	 */
	public static File getFileFromFileChooser(String title, String... fileEndings) {    
	    StringBuilder regexFileMatchBuilder = new StringBuilder("");
	    for (String fileEnding: fileEndings) {
	    	regexFileMatchBuilder.append( ".*\\." + fileEnding + "|"); // "|" am Ende st�rt nicht, funktioniert trotzdem
	    }
	    final String regexFileMatch = regexFileMatchBuilder.toString();
	    
	    StringBuilder descriptionBuilder = new StringBuilder("");
	    for (String fileEnding: fileEndings) {
	    	descriptionBuilder.append( "*." + fileEnding + " ");
	    }
	    final String description = descriptionBuilder.toString();
	    
		// Dateidialog
	    JFileChooser fileChooser = new JFileChooser("../files");
	    fileChooser.setDialogTitle(title);
	    fileChooser.setAcceptAllFileFilterUsed(false);
	    fileChooser.setFileFilter(new FileFilter() {
	        @Override
	        public boolean accept(File f) {
	            return f.isDirectory() || f.getName().matches(regexFileMatch);
	        }

	        @Override
	        public String getDescription() {
	            return description;
	        }
	    });
		
	    
	    if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
	    	return new File(fileChooser.getSelectedFile().getAbsolutePath());
	    }
	    return new File("");
	}
	
	/**
	 * 1. Komponente: x-Richtung, 2. Komponente: y-Richtung (0,0 : oben links), 3. Komponente: R,G,B (0,1,2)
	 * @param fileName
	 * @return
	 */
	public static int[][][] loadRGBPixelsFromFile(String fileName) {
		try {
			File file = FileLoader.getFileFromFilesFolder(fileName, true);
			FileInputStream stream = new FileInputStream(file);
			BufferedImage image = ImageIO.read(stream);
			int width = image.getWidth();
			int height = image.getHeight();
			int[] pixels = new int[width * height];
			image.getRGB(0, 0, width, height, pixels, 0, width);
			int[][][] result = new int[width][height][3];
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					result[i][j][0] = (pixels[j * width + i] >> 16) & 0xFF; // red
					result[i][j][1] = (pixels[j * width + i] >> 8) & 0xFF; // green
					result[i][j][2] = (pixels[j * width + i]) & 0xFF; // blue
				}
			}
			return result;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int[][] loadBlackWhitePixelsFromFile(String fileName) {
		int[][][] help = FileLoader.loadRGBPixelsFromFile(fileName);
		int[][] result = new int[help.length][help[0].length];
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[i].length; j++) {
				result[i][j] = Math.round((help[i][j][0] + help[i][j][1] + help[i][j][2]) / 3.0f);
			}
		}
		return result;
	}
}
