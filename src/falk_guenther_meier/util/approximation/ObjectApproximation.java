package falk_guenther_meier.util.approximation;

import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLTriangle;

public class ObjectApproximation {
	public static List<GLTriangle> approximateIterations(List<GLTriangle> list, ApproximationStructure structure, int iterations) {
		List<GLTriangle> result = new ArrayList<GLTriangle>(list);
		int iteration = iterations;
		int countElements = 0;
		TriangleSide side = null;
		while (iteration > 0) {
			countElements = result.size();
			System.out.println("iteration: " + iteration + ", elements: " + countElements);
			while (countElements > 0) {
				side = new TriangleSide(result.get(0), result, structure);
				result.addAll(side.splitSide());
				result.remove(0);
				countElements--;
				if (side.getSecond() != null) {
					result.remove(side.getSecond());
					countElements--;
				}
			}
			iteration--;
		}
		return result;
	}

	public static List<GLTriangle> approximateSize(List<GLTriangle> list, ApproximationStructure structure, float size) {
		ArrayList<GLTriangle> actList = new ArrayList<GLTriangle>(list);
		int countReady = 0;
		List<GLTriangle> splits = null;
		TriangleSide side = null;
		while (countReady < actList.size()) {
			if (actList.get(0).getSurfaceArea() <= size) {
				actList.add(actList.remove(0));
				continue;
			}
			side = new TriangleSide(actList.get(0), actList, structure);
			splits = side.splitSide();
			for (GLTriangle t : splits) {
				if (t.getSurfaceArea() <= size) {
					countReady++;
				}
			}
			actList.addAll(splits);
			actList.remove(0);
			if (side.getSecond() != null) {
				actList.remove(side.getSecond());
			}
		}
		return actList;
	}

	public static void calculateAndSetNormals(List<GLTriangle> list) {
		for (GLTriangle t : list) { // f�r alle Dreiecke
			for (GLVertex v : t.getNodes()) { // f�r alle Vertexes aller Dreiecke
				GLVertex normal = new GLVertex();				
				for (GLTriangle nT : list) { // benachbarte Dreiecke suchen
					if (nT != t) {
						if (v.equals(nT.getNodes()[0])) {
							normal.add(nT.calculateNormal());
						}
						if (v.equals(nT.getNodes()[1])) {
							normal.add(nT.calculateNormal());
						}
						if (v.equals(nT.getNodes()[2])) {
							normal.add(nT.calculateNormal());
						}
					}
				}
				// Mittelwert der Normalen aller benachbarten Dreiecke bilden
				normal.normalize();
				v.setNormal(normal);
			}
		}
	}
}
