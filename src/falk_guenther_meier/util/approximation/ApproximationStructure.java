package falk_guenther_meier.util.approximation;

import falk_guenther_meier.model.math.GLVertex;

public interface ApproximationStructure {
	public GLVertex getRealVertex(GLVertex v);
}
