package falk_guenther_meier.util.approximation;

import java.util.ArrayList;
import java.util.List;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLTriangle;

public class TriangleSide {
	private List<GLTriangle> list;
	private ApproximationStructure structure;

	private GLTriangle first;
	private GLTriangle second;
	private GLVertex firstStartLongestSide;
	private GLVertex firstEndLongestSide;
	private GLVertex firstOther;
	private GLVertex secondStartLongestSide;
	private GLVertex secondEndLongestSide;
	private GLVertex secondOther;

	public TriangleSide(GLTriangle t, List<GLTriangle> list, ApproximationStructure structure) {
		this.list = list;
		this.structure = structure;
		this.first = t;

		// bestimme die l�ngste Seite des Dreiecks
		float lf = GLVertex.sub(first.getNodes()[1], first.getNodes()[0]).getNorm();
		float ls = GLVertex.sub(first.getNodes()[2], first.getNodes()[1]).getNorm();
		float lt = GLVertex.sub(first.getNodes()[0], first.getNodes()[2]).getNorm();

		if (lf >= ls && lf >= lt) {
			this.firstStartLongestSide = first.getNodes()[0];
			this.firstEndLongestSide = first.getNodes()[1];
			this.firstOther = first.getNodes()[2];
		} else if (ls >= lf && ls >= lt) {
			this.firstStartLongestSide = first.getNodes()[1];
			this.firstEndLongestSide = first.getNodes()[2];
			this.firstOther = first.getNodes()[0];
		} else {
			this.firstStartLongestSide = first.getNodes()[2];
			this.firstEndLongestSide = first.getNodes()[0];
			this.firstOther = first.getNodes()[1];
		}

		// pr�ft, ob es ein "benachbartes" Dreieck an der l�ngsten Seite gibt
		for (GLTriangle e : this.list) {
			if (e == this.first) {
				continue;
			}
			if (e.getNodes()[0].equals(this.firstStartLongestSide) && e.getNodes()[2].equals(this.firstEndLongestSide)) {
				this.second = e;
				this.secondStartLongestSide = this.second.getNodes()[0];
				this.secondEndLongestSide = this.second.getNodes()[2];
				this.secondOther = this.second.getNodes()[1];
				break;
			} else if (e.getNodes()[1].equals(this.firstStartLongestSide) && e.getNodes()[0].equals(this.firstEndLongestSide)) {
				this.second = e;
				this.secondStartLongestSide = this.second.getNodes()[1];
				this.secondEndLongestSide = this.second.getNodes()[0];
				this.secondOther = this.second.getNodes()[2];
				break;
			} else if (e.getNodes()[2].equals(this.firstStartLongestSide) && e.getNodes()[1].equals(this.firstEndLongestSide)) {
				this.second = e;
				this.secondStartLongestSide = this.second.getNodes()[2];
				this.secondEndLongestSide = this.second.getNodes()[1];
				this.secondOther = this.second.getNodes()[0];
				break;
			}
		}
	}

	public List<GLTriangle> splitSide() {
		List<GLTriangle> result = new ArrayList<GLTriangle>(4);
		GLVertex longestSide = GLVertex.sub(this.firstEndLongestSide, this.firstStartLongestSide);
		GLVertex center = GLVertex.add(this.firstStartLongestSide, GLVertex.mult(longestSide, 0.5f));
		center = this.structure.getRealVertex(center);

		result.add(new GLTriangle(this.firstStartLongestSide.clone(), center.clone(), this.firstOther.clone(), true));
		result.add(new GLTriangle(center.clone(), this.firstEndLongestSide.clone(), this.firstOther.clone(), true));

		if (second != null) {
			result.add(new GLTriangle(this.secondStartLongestSide.clone(), this.secondOther.clone(), center.clone(), true));
			result.add(new GLTriangle(center.clone(), this.secondOther.clone(), this.secondEndLongestSide.clone(), true));
		}
		return result;
	}

	public GLTriangle getSecond() {
		return second;
	}
}
