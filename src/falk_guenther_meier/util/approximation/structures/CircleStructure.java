package falk_guenther_meier.util.approximation.structures;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.util.approximation.ApproximationStructure;

public class CircleStructure implements ApproximationStructure {
	private float radius;

	public CircleStructure(float radius) {
		this.radius = radius;
	}

	@Override
	public GLVertex getRealVertex(GLVertex v) {
		GLVertex result = v.clone();
		result.normalize();
		result.mult(radius);
		return result;
	}
}
