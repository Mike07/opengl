package falk_guenther_meier.util.approximation.structures;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.util.approximation.ApproximationStructure;

public class SameStructure implements ApproximationStructure {
	@Override
	public GLVertex getRealVertex(GLVertex v) {
		return v.clone();
	}
}
