package falk_guenther_meier.util.approximation.structures;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.util.approximation.ApproximationStructure;

public class CylinderStructure implements ApproximationStructure {
	private float radius;

	public CylinderStructure(float radius) {
		this.radius = radius;
	}

	@Override
	public GLVertex getRealVertex(GLVertex v) {
		GLVertex result = v.clone();
		result.setY(0.0f);
		result.normalize();
		result.mult(radius);
		result.setY(v.getY());
		return result;
	}
}
