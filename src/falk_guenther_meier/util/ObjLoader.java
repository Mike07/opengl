package falk_guenther_meier.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLPolygon;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.objects.GLTriangle;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.textures.GLTexCoord;
import falk_guenther_meier.model.textures.GLTexture2D;

public class ObjLoader {	
	/**
	 * obj-Datei �ber Datei-�ffnen-Dialog ausw�hlen und dann Objektliste erzeugen
	 */
	public static GLCustomComplexObject objFile2ComplexObj() {  
	    // Datei verf�gbar? -> dann parsen
	    File objFile = FileLoader.getFileFromFileChooser("OBJ-Datei �ffnen", "obj");
	    
	    if (objFile.exists()) {
	    	return objFile2ComplexObj(objFile);
	    }
	    return new GLCustomComplexObject();
	}
	
	/**
	 * Objektliste aus obj-Datei erzeugen
	 */
	public static GLCustomComplexObject objFile2ComplexObj(File objFile) {
		GLCustomComplexObject complexObj = new GLCustomComplexObject();
		try {
			GLCustomComplexObject subComplexObj = new GLCustomComplexObject();
			complexObj.add(subComplexObj);
			ArrayList<GLVertex> vertexList = new ArrayList<GLVertex>();
			ArrayList<GLTexCoord> texCoordList = new ArrayList<GLTexCoord>();
			ArrayList<GLVertex> normalList = new ArrayList<GLVertex>();
			
			FileReader fileReader = new FileReader(objFile.getAbsolutePath());
	    	BufferedReader bufferedReader = new BufferedReader(fileReader);
	    	
	    	String line = bufferedReader.readLine();
	    	while (line != null) {
	    		
	    		String[] lineParts = line.split(" ");
	    		if (lineParts.length > 0)
	    		{
		    		if (lineParts[0].equals("v")) {
		    			GLVertex vertex = obj2Vertex(lineParts);
		    			vertexList.add(vertex);
		    		} else if (lineParts[0].equals("vn")) {
		    			GLVertex normal = obj2Vertex(lineParts);
		    			normalList.add(normal);
		    		} else if (lineParts[0].equals("vt")) {
		    			GLTexCoord texCoord = obj2TexCoord(lineParts);
		    			texCoordList.add(texCoord);
		    		} else if (lineParts[0].equals("f")) {
		    			GLObject face = obj2Face(lineParts, vertexList, texCoordList, normalList);
		    			subComplexObj.add(face);
		    		} else if (lineParts[0].equals("usemtl")) {
		    			if (subComplexObj.getObjects().size() > 0) {
		    				subComplexObj = new GLCustomComplexObject();
		    				complexObj.add(subComplexObj);
		    			}
		    			
		    			File texFile = new File(objFile.getParent() + File.separatorChar + lineParts[1]+".png"); 
		    			if (!texFile.exists())
		    				texFile = new File(objFile.getParent() + File.separatorChar + lineParts[1].replace("_", " ")+".png"); 
		    			if (!texFile.exists())
		    				texFile = new File(objFile.getParent() + File.separatorChar + lineParts[1]+".jpg"); 
		    			if (!texFile.exists())
		    				texFile = new File(objFile.getParent() + File.separatorChar + lineParts[1].replace("_", " ")+".jpg"); 		
		    			if (texFile.exists()) {
		    				GLTexture2D texture = new GLTexture2D(texFile);
		    				subComplexObj.setTexture(texture);
		    			}
		    		}
		    		
	    		}
	    			
	    		line = bufferedReader.readLine();
	    	}
	    	
	    	bufferedReader.close();
	    	fileReader.close();	
	    	
	    	/*// jetzt alle Objekte der Liste zum Zeichnen hinzuf�gen
	    	synchronized(drawObjListLock) {
	    		drawObjList.addAll(newObjList);
	    	}*/
	    	
        } catch (Exception e) {
        	System.out.println("Fehler beim Lesen der Datei");
        	e.printStackTrace();
        }
		complexObj.setReferencePoint(new GLVertex(0, 0, 0));
		return complexObj;
	}

	private static float str2FloatDef(String s, float defValue) {
		try {
			return Float.valueOf(s);
		} catch (NumberFormatException e) {
			return defValue;
		}
	}
	
	// vn -0.882333 0.151855 0.445453 // Normalen
	// v 0.017041 -0.047797 -0.014696 // Vertexes
	private static GLVertex obj2Vertex(String[] lineParts) {
		float x = str2FloatDef(lineParts[1], 0);
		float y = str2FloatDef(lineParts[2], 0);
		float z = str2FloatDef(lineParts[3], 0);
		float w = 1;
		if (lineParts.length>4) {
			w = str2FloatDef(lineParts[4], 1);
		}
		return new GLVertex(x,y,z,w);
	}
	
	// vt 3.656950 2.327490 0.000000
	private static GLTexCoord obj2TexCoord(String[] lineParts) {
		float u = str2FloatDef(lineParts[1], 0);
		float v = str2FloatDef(lineParts[2], 0);
		return new GLTexCoord(u,v);
	}
	
	// f 612/1220/612 726/1127/726 750/1229/750 // f Vertex0/TextCoord0/Normal0 Vertex1/TextCoord1/Normal1 Vertex2/TextCoord2/Normal2
	private static GLObject obj2Face(String[] lineParts, ArrayList<GLVertex> vertexList, ArrayList<GLTexCoord> texCoordList, ArrayList<GLVertex> normalList) {	
		GLVertex[] faceNodes = new GLVertex[lineParts.length-1]; // ohne dem "f"
		for (int i = 0; i < faceNodes.length; i++) {
			String[] faceNode = lineParts[i+1].split("/"); // Vertexi, TextCoordi, Normali // enthalten die Indexwerte um den richtigen Vertex, TextCoord oder die Normale zu holen
			
			int vertexIndex = Integer.valueOf(faceNode[0])-1;
			GLVertex vertex = vertexList.get(vertexIndex).clone();
			
			if (faceNode.length>1 && faceNode[1]!="") {
				int texCoordIndex = Integer.valueOf(faceNode[1])-1;
				GLTexCoord texCoord = texCoordList.get(texCoordIndex);
				vertex.setTexCoord(texCoord);
			}
			
			if (faceNode.length>2 && faceNode[2]!="") {
				int normalIndex = Integer.valueOf(faceNode[2])-1; // normalerweise sollte gelten: normalIndex = vertexIndex
				GLVertex normal = normalList.get(normalIndex).clone();
				vertex.setNormal(normal);
			}
			
			faceNodes[i] = vertex;
		}
		
		if (faceNodes.length == 3)
			return new GLTriangle(faceNodes, true);
		else if (faceNodes.length == 4)
			return new GLRectangle(faceNodes, true);
		else
			return new GLPolygon(true, faceNodes);
	}
}
