package falk_guenther_meier.util;

import java.awt.Font;

import javax.media.opengl.GL2;

import com.jogamp.opengl.util.awt.TextRenderer;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.platform.GLObject;

/**
 * Enth�lt hilfreiche Methoden und Konstanten.
 */
public abstract class Utility {
	
	public static Font DEFAULT_FONT = new Font("SansSerif", Font.BOLD, 26);
	
	/**
	 * Zeichnet Text auf dem Bildschirm.
	 * @param drawable
	 * @param font
	 * @param color
	 * @param text
	 * @param posX
	 * @param posY
	 */
	public static void drawText(GL2 gl, Font font, GLVertex color, String text, int posX, int posY) {
		TextRenderer textRenderer = new TextRenderer(font);
		textRenderer.beginRendering(gl.getContext().getGLDrawable().getWidth(), gl.getContext().getGLDrawable().getHeight());
		textRenderer.setColor(color.getX(), color.getY(), color.getZ(), color.getW());
		textRenderer.draw(text, posX, posY);
		textRenderer.endRendering();
	}
	
	/**
	 * Setzt f�r ein Objekt f�r alle Unterobjekte zuf�llige Farben
	 * @param o Objekt
	 */
	public static void setRandomColor(GLObject o) {
		if (o instanceof GLComplexObject) {
			for (GLObject act : ((GLComplexObject) o).getObjects()) {
				Utility.setRandomColor(act);
			}
		} else {
			o.setColor(Utility.getRandomColor());
		}
	}
	
	/**
	 * Liefert eine zuf�llige Farbe.
	 * @return Farbe
	 */
	public static GLVertex getRandomColor() {
		return new GLVertex((float) Math.random(), (float) Math.random(), (float) Math.random(), 1.0f);
	}
	
	static public float[] invertMatrix(float[] in) {
		float[] to = new float[16];
		float d1 = in[2]* in[7]  - in[6]* in[3];
		float d2 = in[2]* in[11] - in[10]*in[3];
		float d3 = in[14]*in[3]  - in[2]* in[15];
		float d4 = in[6]* in[11] - in[10]*in[7];
		float d5 = in[14]*in[7]  - in[6]* in[15];
		float d6 = in[10]*in[15] - in[11]*in[14];
		to[0] =   in[5]* d6 + in[9]* d5 + in[13]*d4;
		to[1] = - in[1]* d6 - in[9]* d3 - in[13]*d2;
		to[2] = - in[1]* d5 + in[5]* d3 + in[13]*d1;
		to[3] = - in[1]* d4 + in[5]* d2 - in[9] *d1;
		to[4] = - in[4]* d6 - in[8]* d5 - in[12]*d4;
		to[5] =   in[0]* d6 + in[8]* d3 + in[12]*d2;
		to[6] =   in[0]* d5 - in[4]* d3 - in[12]*d1;
		to[7] =   in[0]* d4 - in[4]* d2 + in[8] *d1;
		
		d1 = in[0] * in[5] - in[1] * in[4];
		d2 = in[0] * in[9] - in[1] * in[8];
		d3 = in[1] * in[12] - in[0] * in[13];
		d4 = in[4] * in[9] - in[5] * in[8];
		d5 = in[4] * in[13] - in[5] * in[12];
		d6 = in[8] * in[13] - in[9] * in[12];
		
		to[8]  =   in[7] * d6 - in[11] * d5 + in[15] * d4;
		to[9]  = - in[3] * d6 - in[11] * d3 - in[15] * d2;
		to[10] =   in[3] * d5 + in[7]  * d3 + in[15] * d1;
		to[11] = - in[3] * d4 + in[7]  * d2 - in[11] * d1;
		to[12] = - in[6] * d6 + in[10] * d5 - in[14] * d4;
		to[13] =   in[2] * d6 + in[10] * d3 + in[14] * d2;
		to[14] = - in[2] * d5 - in[6]  * d3 - in[14] * d1;
		to[15] =   in[2] * d4 - in[6]  * d2 + in[10] * d1;
		
		float det = 1f/(to[0]*in[0] + to[4]*in[1] + to[8]*in[2] + to[12]*in[3]);
		for(int i=0;i<16;i++) to[i] *= det;
		return to;
	 }
	
	public static void fastinvert(float mat[], float ret[]) {
		ret[0 ] = mat[0];
		ret[1 ] = mat[4];
		ret[2 ] = mat[8];
		ret[3 ] = 0.0f;
		ret[4 ] = mat[1];
		ret[5 ] = mat[5];
		ret[6 ] = mat[9];
		ret[7 ] = 0.0f;
		ret[8 ] = mat[2];
		ret[9 ] = mat[6];
		ret[10] = mat[10];
		ret[11] = 0.0f;
		ret[12] = -(mat[12] * ret[0] + mat[13] * ret[4] + mat[14] * ret[8]);
		ret[13] = -(mat[12] * ret[1] + mat[13] * ret[5] + mat[14] * ret[9]);
		ret[14] = -(mat[12] * ret[2] + mat[13] * ret[6] + mat[14] * ret[10]);
		ret[15] = 1.0f;

	}
}
