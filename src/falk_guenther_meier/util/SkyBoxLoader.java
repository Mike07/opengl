package falk_guenther_meier.util;

import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.objects.GLSkyBox;
import falk_guenther_meier.model.textures.GLTexture2D;

public abstract class SkyBoxLoader {	
	public static GLSkyBox getAlienSkyBox(GLCamera camera, float size) {
		GLSkyBox skybox = new GLSkyBox(camera, size);
		GLTexture2D texture = new GLTexture2D(FileLoader.getFileFromFilesFolder(("skybox/alien/jajalien1_back.jpg")));
		GLTexture2D texture1 = new GLTexture2D(FileLoader.getFileFromFilesFolder(("skybox/alien/jajalien1_front.jpg")));
		GLTexture2D texture2 = new GLTexture2D(FileLoader.getFileFromFilesFolder(("skybox/alien/jajalien1_left.jpg")));
		GLTexture2D texture3 = new GLTexture2D(FileLoader.getFileFromFilesFolder(("skybox/alien/jajalien1_right.jpg")));
		GLTexture2D texture4 = new GLTexture2D(FileLoader.getFileFromFilesFolder(("skybox/alien/jajalien1_top.jpg")));
		
		skybox.getRectBack().setTexture(texture1);
		skybox.getRectFront().setTexture(texture);
		skybox.getRectTop().setTexture(texture4);
		skybox.getRectLeft().setTexture(texture2);
		skybox.getRectRight().setTexture(texture3);
		return skybox;
	}
	
	public static GLSkyBox getSundownSkyBox(GLCamera camera, float size) {
		GLSkyBox skybox = new GLSkyBox(camera, size);
		GLTexture2D texture = new GLTexture2D(FileLoader.getFileFromFilesFolder(("skybox/sundown/jajsundown1_back.jpg")));
		GLTexture2D texture1 = new GLTexture2D(FileLoader.getFileFromFilesFolder(("skybox/sundown/jajsundown1_front.jpg")));
		GLTexture2D texture2 = new GLTexture2D(FileLoader.getFileFromFilesFolder(("skybox/sundown/jajsundown1_left.jpg")));
		GLTexture2D texture3 = new GLTexture2D(FileLoader.getFileFromFilesFolder(("skybox/sundown/jajsundown1_right.jpg")));
		GLTexture2D texture4 = new GLTexture2D(FileLoader.getFileFromFilesFolder(("skybox/sundown/jajsundown1_top.jpg")));
		
		skybox.getRectBack().setTexture(texture1);
		skybox.getRectFront().setTexture(texture);
		skybox.getRectTop().setTexture(texture4);
		skybox.getRectLeft().setTexture(texture2);
		skybox.getRectRight().setTexture(texture3);
		return skybox;
	}
	
	public static GLSkyBox getOceanInletSkyBox(GLCamera camera, float size) {
		GLSkyBox skybox = new GLSkyBox(camera, size);
		
		GLTexture2D texture_right = new GLTexture2D(FileLoader.getFileFromFilesFolder("skybox/ocean_inlet/ocean_right.png", true));
		GLTexture2D texture_left = new GLTexture2D(FileLoader.getFileFromFilesFolder("skybox/ocean_inlet/ocean_left.png", true));
		GLTexture2D texture_top = new GLTexture2D(FileLoader.getFileFromFilesFolder("skybox/ocean_inlet/ocean_top.png", true));
		GLTexture2D texture_down = new GLTexture2D(FileLoader.getFileFromFilesFolder("skybox/ocean_inlet/ocean_down.png", true));
		GLTexture2D texture_front = new GLTexture2D(FileLoader.getFileFromFilesFolder("skybox/ocean_inlet/ocean_front.png", true));
		GLTexture2D texture_back = new GLTexture2D(FileLoader.getFileFromFilesFolder("skybox/ocean_inlet/ocean_back.png", true));

		
		skybox.getRectRight().setTexture(texture_right);
		skybox.getRectLeft().setTexture(texture_left);
		skybox.getRectTop().setTexture(texture_top);
		skybox.getRectDown().setTexture(texture_down);
		skybox.getRectFront().setTexture(texture_front);
		skybox.getRectBack().setTexture(texture_back);
		return skybox;
	}
	
	
	public static GLSkyBox getLostAtSeaSkyBox(GLCamera camera, float size) {
		GLSkyBox skybox = new GLSkyBox(camera, size);
		
		GLTexture2D texture_right = new GLTexture2D(FileLoader.getFileFromFilesFolder("skybox/lostatseaday/lostatseaday_right.jpg", true));
		GLTexture2D texture_left = new GLTexture2D(FileLoader.getFileFromFilesFolder("skybox/lostatseaday/lostatseaday_left.jpg", true));
		GLTexture2D texture_top = new GLTexture2D(FileLoader.getFileFromFilesFolder("skybox/lostatseaday/lostatseaday_top.jpg", true));
		GLTexture2D texture_down = new GLTexture2D(FileLoader.getFileFromFilesFolder("skybox/lostatseaday/lostatseaday_down.jpg", true));
		GLTexture2D texture_front = new GLTexture2D(FileLoader.getFileFromFilesFolder("skybox/lostatseaday/lostatseaday_front.jpg", true));
		GLTexture2D texture_back = new GLTexture2D(FileLoader.getFileFromFilesFolder("skybox/lostatseaday/lostatseaday_back.jpg", true));

		
		skybox.getRectRight().setTexture(texture_right);
		skybox.getRectLeft().setTexture(texture_left);
		skybox.getRectTop().setTexture(texture_top);
		skybox.getRectDown().setTexture(texture_down);
		skybox.getRectFront().setTexture(texture_front);
		skybox.getRectBack().setTexture(texture_back);
		return skybox;
	}
	
}
