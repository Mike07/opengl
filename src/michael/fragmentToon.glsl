varying vec3 normal;
varying vec3 position;

vec4 toonShading(int nr, vec3 normal)
{
	vec3 lightDirection = normalize(gl_LightSource[nr].position.xyz - position);
	float intensity = max(0.0, dot(normalize(normal), lightDirection));
	vec4 color = vec4(0.0, 0.0, 0.0, 1.0);
	if (intensity > 0.75) {
		color = vec4 (1.0, 0.56, 0.0, 1.0);
	}
	else if (intensity > 0.55) {
		color = vec4 (0.60, 0.30, 0.0, 1.0);
	}
	else if (intensity > 0.30) {
		color = vec4 (0.40, 0.20, 0.0, 1.0);
	}
	else if (intensity > 0.15) {
		color = vec4 (0.27, 0.10, 0.0, 1.0);
	}
	return color;
}

void main() {
	gl_FragColor = toonShading(7, normalize(normal));
}
