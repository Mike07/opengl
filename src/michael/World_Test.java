package michael;

import java.awt.Color;
import java.io.File;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import physic.GLCuboidPhysic;
import falk_guenther_meier.model.GLDisplayList;
import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.buildings.GLChair;
import falk_guenther_meier.model.buildings.GLClosedRoom;
import falk_guenther_meier.model.buildings.GLTable;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.light.GLSpotLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLBillBoard;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.model.world.JoglAppShadowMapping;
import falk_guenther_meier.model.world.Konfiguration;
import falk_guenther_meier.util.FileLoader;

public class World_Test extends JoglAppShadowMapping {
	
	private Konfiguration konfig;
	private GLPointLight pointLight;
	
	// Objekte
	private GLClosedRoom closedRoom;
	private GLCustomComplexObject tableAndChairs;
	private GLCuboidPhysic physicCube;
	private GLCuboidPhysic physicCube2;
	GLCustomComplexObject complexObject;
	private GLDisplayList displayList;
	private GLBillBoard billboard;
	private GLSpotLight spotLight;
	private ShaderProgram shaderShadow;
	private ShaderProgram shaderBumpShadow;
	private File texture_c;
	private File texture_n;
	private File texture_c2;
	private File texture_n2;
	
	public static void main(String[] args) {
		new World_Test("Einzelprojekt Michi", 800, 600);
	}
	
	public World_Test(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
		konfig = this.getKonfiguration();
//		konfig.setSkybox(SkyBoxLoader.getOceanInletSkyBox(this.getKonfiguration().getCamera(), 110)); // Skybox gr��er machen
//		defaultCamera = konfig.getCamera();

		// Billboard (Ritter)
		File texBillboard = FileLoader.getFileFromFilesFolder("billboard/Ritter.png");
		GLTexture2D rectTexture = new GLTexture2D(texBillboard);
		GLVertex bPosition = new GLVertex();
		billboard = new GLBillBoard(this.getKonfiguration().getCamera(), rectTexture, bPosition, 1f, 2.72f);
		billboard.translate(new GLVertex(-2, 2, -2));
//		billboard.setPosition(new GLVertex(-2, 2, -2));

		texture_c = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-c.jpg");
		texture_n = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-n.jpg");
		texture_c2 = FileLoader.getFileFromFilesFolder("texture/bumpmapping/texture_4.png");
		texture_n2 = FileLoader.getFileFromFilesFolder("texture/bumpmapping/normal_4.png");

		start();
	}
	
	@Override
	protected void drawShadowRendering(GL2 gl) {
		super.drawShadowRendering(gl);
		closedRoom.draw(gl);
		tableAndChairs.draw(gl);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();
		
//		defaultCamera.setPosition(new GLVertex(9, 9, 9));
//		defaultCamera.setDirection(new GLVertex(-1, -0.8f, -1));
		
		GLTexture2D renderTexture = new GLTexture2D(texture_c);
		renderTexture.setGLTextureWrap(GL2.GL_REPEAT);
		GLTexture2D normalTexture = new GLTexture2D(texture_n);
		normalTexture.setGLTextureWrap(GL2.GL_REPEAT);
		GLTexture2D renderTexture2 = new GLTexture2D(texture_c2);
		renderTexture2.setGLTextureWrap(GL2.GL_REPEAT);
		GLTexture2D normalTexture2 = new GLTexture2D(texture_n2);
		normalTexture2.setGLTextureWrap(GL2.GL_REPEAT);
		
		
		shaderShadow = new ShaderProgram("vertexShadow.glsl", "fragmentShadow.glsl");
		shaderShadow.compileShaders(this.getClass(), gl);

		shaderBumpShadow = new ShaderProgram("vertexBumpShadow.glsl", "fragmentBumpShadow.glsl");
		shaderBumpShadow.compileShaders(this.getClass(), gl);
		
		// Innenraum
		float width = 50; float depth = 30; float height = 30;
		GLVertex pos = new GLVertex();
		closedRoom = new GLClosedRoom(pos, width, depth, height, 0.005f);
		closedRoom.setTexture(renderTexture, 0);
		closedRoom.setTexture(normalTexture, 1);
		closedRoom.setShader(shaderBumpShadow);
		closedRoom.setMaterial(this.createTextureMaterial());
		
		// Tisch und St�hle
		tableAndChairs = new GLCustomComplexObject();
		GLTable table = new GLTable(new GLVertex(), 4.5f, 2.5f, 1.8f, 0.7f);
		tableAndChairs.add(table);
		GLChair chair = new GLChair(new GLVertex(-3.5f, 0, 0), 1.3f, 1.3f, 1.2f, 1.5f);
		chair.rotate(0, 90, 0);
		tableAndChairs.add(chair);
		chair = new GLChair(new GLVertex(3.5f, 0, 0), 1.3f, 1.3f, 1.2f, 1.5f);
		chair.rotate(0, -90, 0);
		tableAndChairs.add(chair);
		chair = new GLChair(new GLVertex(-1f, 0, 2.1f), 1.3f, 1.3f, 1.2f, 1.5f);
		chair.rotate(0, -180, 0);
		tableAndChairs.add(chair);
		chair = new GLChair(new GLVertex(-1f, 0, -2.1f), 1.3f, 1.3f, 1.2f, 1.5f);
		tableAndChairs.add(chair);
		chair = new GLChair(new GLVertex(1f, 0, 2.1f), 1.3f, 1.3f, 1.2f, 1.5f);
		chair.rotate(0, -180, 0);
		tableAndChairs.add(chair);
		chair = new GLChair(new GLVertex(1f, 0, -2.1f), 1.3f, 1.3f, 1.2f, 1.5f);
		tableAndChairs.add(chair);
		tableAndChairs.setColor(Color.GRAY);
		tableAndChairs.setMaterial(GLMaterial.DEFAULT_OWN);
		
		// Lichter setzen
		pointLight = new GLPointLight(GL2.GL_LIGHT7, new GLVertex(2f, 6f, -2f));
		pointLight.setUseColorMaterial(true);
		pointLight.setAmbient(new GLVertex(0.6f, 0.6f, 0.6f, 1.0f));
		pointLight.setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f, 1.0f));
		pointLight.setSpecular(new GLVertex(0.5f, 0.5f, 0.5f, 1.0f));
		pointLight.setAttenuation(0.9f, 0.01f, 0.008f);
		GLCuboid lightCube = new GLCuboid(new GLVertex(0f, 0f, 0f), 0.3f);
		lightCube.setColor(new GLVertex(1f, 1f, 1f));
		pointLight.setLightObject(lightCube);
		
		// Spotlight f�r Shadow Mapping
		spotLight = new GLSpotLight(GL2.GL_LIGHT5, 
				new GLVertex(8.0f, 4.0f, -8.0f), new GLVertex(-4.0f, 0f, 4.0f));
		spotLight.setConstantAttenuation(0.2f);
		spotLight.setLinearAttenuation(0.04f);
		spotLight.setQuadraticAttenuation(0.004f);
		spotLight.setAmbient(new GLVertex(0.4f, 0.4f, 0.3f));
		spotLight.setDiffuse(new GLVertex(0.3f, 0.3f, 0.3f));
		spotLight.setSpecular(new GLVertex(0.3f, 0.3f, 0.3f));
		spotLight.setSpotCutoff(30.0f);
		this.addLight(spotLight);
		this.setLightForShadowMapping(spotLight);
	}
	
	private GLMaterial createTextureMaterial() {
		GLMaterial mat = new GLMaterial();
		mat.setMatAmbient(new GLVertex(1, 1, 1));
		mat.setMatDiffuse(new GLVertex(1, 1, 1));
		mat.setMatSpecular(new GLVertex(1, 1, 1));
		return mat;
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		
		shaderBumpShadow.useProgram(gl);
		shaderBumpShadow.setTextureUnit(gl, "renderTexture", 0);
		shaderBumpShadow.setTextureUnit(gl, "normalTexture", 1);
		shaderBumpShadow.setTextureUnit(gl, "shadowMap", 2);
		closedRoom.draw(gl);
		
		shaderShadow.useProgram(gl);
		shaderShadow.setTextureUnit(gl, "shadowMap", 2);
		tableAndChairs.draw(gl);
		
		ShaderProgram.clearShaderProgram(gl);
	}

	@Override
	public void cameraMode1() {
		// inside
		konfig.getCamera().setPosition(new GLVertex(0, 5, 0));
		konfig.getCamera().setDirection(new GLVertex(-1, 0, -1));
	}

	@Override
	public void cameraMode2() {
		// outside
		konfig.getCamera().setPosition(new GLVertex(25, 5, 25));
		konfig.getCamera().setDirection(new GLVertex(1, 0, 1));
	}

	@Override
	public void cameraMode3() {

	}

	@Override
	public void cameraMode4() {

	}
}
