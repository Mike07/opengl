// Aufgabe 30
uniform sampler2D texHeightMap;
uniform sampler2D texMountain;
uniform sampler2D texLand;
uniform sampler2D texWater;
uniform sampler2D texBeach;
varying vec3 normal;
varying vec4 position;

vec3 getNormal()
{
	vec2 texCoord = gl_TexCoord[0].st;
	float offset = 0.005; // 0.005
    
    float texelLeft = texture2D(texHeightMap, texCoord + vec2(-offset, 0)).r;
    float texelRight = texture2D(texHeightMap, texCoord + vec2(offset, 0)).r;
    float texelUp = texture2D(texHeightMap, texCoord + vec2(0, -offset)).r;
    float texelBottom = texture2D(texHeightMap, texCoord + vec2(0, offset)).r;
    vec3 vRight = vec3(2.0*offset, texelRight - texelLeft, 0);
    vec3 vUp = vec3(0, texelUp - texelBottom, -2.0*offset);
    
    return normalize(cross(vRight,vUp));
}

float getHeight()
{
	return texture2D(texHeightMap, gl_TexCoord[0].st).r;
}

vec4 ambientColor(int nr, vec4 front) // ambient
{
	return front * gl_LightSource[nr].ambient + front * gl_LightModel.ambient;
}

vec4 diffuseColor(int nr, vec3 direction, float diffuseIntensity, vec4 front) // diffuse
{
	return front * diffuseIntensity * gl_LightSource[nr].diffuse;
}

vec4 specularColor(int nr, vec3 direction, float diffuseIntensity, vec3 normal, vec4 front, float shininess) // specular
{
	// "reflect"-Operator: Phong-Modell (ohne Blinn (Blinn-Phong==Phnong vereinfacht)!!)
	vec3 reflection = normalize(reflect( - normalize(direction), normal));
	float specAngle = max(0.0, dot(normal, reflection));

	vec4 sp = vec4(0.0);
	if (diffuseIntensity != 0.0) {
		float specularValue = pow(specAngle, shininess); // / (direction * direction)
		sp += gl_LightSource[nr].specular * front * specularValue; // specular
	}
	return sp;
}

vec4 phong(int nr, vec3 direction, vec3 normal) // Hilfsmethode: Licht und Material
{
	float inten = max(0.0, dot(normal, normalize(direction)));
	vec4 color = diffuseColor(nr, direction, inten, gl_FrontMaterial.diffuse);
	color += ambientColor(nr, gl_FrontMaterial.ambient);
	color += specularColor(nr, direction, inten, normal, gl_FrontMaterial.specular, gl_FrontMaterial.shininess);
	return color;
}

vec4 phongTexture(int nr, vec3 direction, vec3 normal, vec4 textureColor) // Hilfsmethode: Licht und Textur
{
	float inten = max(0.0, dot(normal, normalize(direction)));
	vec4 color = diffuseColor(nr, direction, inten, textureColor);
	color += ambientColor(nr, textureColor);
	color += specularColor(nr, direction, inten, normal, textureColor, 0.6); // TODO: shininess ?! vec4(1.0) ??
	return color;
}


vec4 directionalLight(int nr, vec3 normal) // ambient, diffuse, specular of a directional light
{
	return phong(nr, - gl_LightSource[nr].position.xyz, normal);
}

vec4 directionalLightTexture(int nr, vec3 normal, vec4 textureColor)
{
	return phongTexture(nr, - gl_LightSource[nr].position.xyz, normal, textureColor);
}

vec4 fog(vec4 color) {
	float l = length(position);
	l = min(l - 22.0, 8.0);
	l = max(l, 0.0);
	l = l / 8.0;
	vec4 fog = color;
	fog = fog + vec4(0.33, 0.33, 0.66, 1.0) * l;
	fog = vec4(fog.xyz, max(1.0 - l, 0.0));
	return fog;
}

void main()
{
	vec2 texCoord = gl_TexCoord[0].st;

	// Texel-Farbe f�r die verschiedenen Texturen ermitteln
	vec4 texMountainColor = texture2D(texMountain, 2.0 * texCoord);
	vec4 texLandColor = texture2D(texLand, 2.0 * texCoord);
	vec4 texBeachColor = texture2D(texBeach, 2.0 * texCoord);// * vec4(1.0, 0.90, 0.65, 1.0);
	vec4 texWaterColor = texture2D(texWater, texCoord);
	texWaterColor = texWaterColor * vec4(0.4, 0.6, 0.8, 1.0); // Wasser blau einf�rben
	
	float texHeight = getHeight();
	
	// Texturen auf Ebenenen entsprechend der texHeight ausw�hlen und mischen
	vec4 texColor = mix(texWaterColor, texBeachColor, smoothstep(0.05,0.18, texHeight)); // �bergang Water -> Beach
	texColor = mix(texColor, texLandColor, smoothstep(0.18,0.3, texHeight));      // �bergang Beach -> Land
	texColor = mix(texColor, texMountainColor, smoothstep(0.4,0.6, texHeight));     // �bergang Land -> Mountain
	
	//gl_FragColor = fog(texColor);
	gl_FragColor = fog(directionalLightTexture(6, normalize(normal), texColor)); // mit einem direktionalen Licht
	//gl_FragColor = vec4(getNormal()*0.5 + 0.5, 1.0) + 0.01 * texColor; // zeigt die NormalMap
}
