varying vec3 normal;
varying vec3 position;
void main() {
	gl_Position = ftransform(); // Standardtransformation
	vec4 position2 = gl_ModelViewMatrix * gl_Vertex;
	position = position2.xyz / position2.w;
	normal = gl_NormalMatrix * gl_Normal;
}
