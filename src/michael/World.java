package michael;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import physic.GLCuboidPhysic;
import physic.GLStaticPlanePhysic;
import physic.PhysicAnimation;

import com.jogamp.opengl.util.gl2.GLUT;

import falk_guenther_meier.model.BillBoardDrawManager;
import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.animation.PositionAnimation;
import falk_guenther_meier.model.animation.PositionDirectionAnimation;
import falk_guenther_meier.model.buildings.GLChair;
import falk_guenther_meier.model.buildings.GLClosedRoom;
import falk_guenther_meier.model.buildings.GLHouse;
import falk_guenther_meier.model.buildings.GLTable;
import falk_guenther_meier.model.buildings.GLWallWithHole;
import falk_guenther_meier.model.interpolation.BezierInterpolation;
import falk_guenther_meier.model.interpolation.LinearInterpolation;
import falk_guenther_meier.model.light.GLDirectionLight;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.light.GLSpotLight;
import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLBillBoard;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLHeightMapTriangleStrip;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.observers.TimeIntervallObservable;
import falk_guenther_meier.model.observers.TimeIntervallObserver;
import falk_guenther_meier.model.particle.WaterParticleSystem;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexGrid;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.textures.GLTextureCube;
import falk_guenther_meier.model.time.TimeIntervallSender;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.model.world.Konfiguration;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.SkyBoxLoader;
import falk_guenther_meier.util.Utility;

public class World extends JoglApp {
	
	private GL2 gl;
	private Konfiguration konfig;
	private GLHeightMapTriangleStrip heightMap;
	private boolean helpMode = false;
	
	// Lichter
	private GLPointLight pointLight;
	private GLDirectionLight dirLight;
	
	// Kameras
	private GLCamera camera;
	
	// Szenen
	private enum Szene {SZENE1, SZENE2, SZENE3};
	private Szene szene;
	
	// Shader
	private ShaderProgram shaderBumpMapping;
	private ShaderProgram shaderHeightmap;
	private ShaderProgram shaderHeightmap2;
	private ShaderProgram shaderPhong;
	private ShaderProgram shaderPhongTexture;
	private ShaderProgram shaderToon;
	private ShaderProgram shaderPhongGluSphere;
	private ShaderProgram shaderCubicReflection;
	
	// Dateien: Texturen etc.
	private File texture_c;
	private File texture_n;
	private File texture_c2;
	private File texture_n2;
	private File texFileHeightMap;
	private File texFileMountain;
	private File texFileLand;
	private File texFileWater;
	private File texFileStone;
	private File texFileHeightMap2;
	private File texFileMountain2;
	private File texFileLand2;
	private File texFileWater2;
	private File texFileBeach2;
	private GLTexture2D explosionTexture;
	
	// Objekte
	private GLClosedRoom closedRoom;
	private GLCustomComplexObject tableAndChairs;
	private GLCuboidPhysic physicCube;
//	private GLCustomComplexObject complexObject;
	private GLBillBoard bbRitter;
	private GLBillBoard bbAssasine;
	private BillBoardDrawManager bbDrawManager;
	private GLTextureCube textureCube;
	private GLHeightMapTriangleStrip heightMap2;
	private WaterParticleSystem particle;
	private GLWallWithHole arc;
	private GLRectangle explosionRect;
	private ArrayList<GLObject> physics;
	private GLSpotLight spotLight;
	private GLWallWithHole arc2;
	private GLHouse house;
	private GLTexture2D outerTexture;
	private GLTexture2D innerTexture;
	private GLTexture2D outerRoofTexture;
	private GLTexture2D innerRoofTexture;
	private GLTexture2D floorTexture;
//	private GLTexture2D pictureTexture;
	private GLBillBoard bbBaum;
	
	
	public static void main(String[] args) {
		new World("Einzelprojekt Michi", 800, 600);
	}
	
	public World(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
		konfig = this.getKonfiguration();
		konfig.setSkybox(SkyBoxLoader.getOceanInletSkyBox(this.getKonfiguration().getCamera(), 150)); // Skybox gr��er machen
		konfig.setCoordinates(false);
		camera = konfig.getCamera();
		this.setAngle(60);
		this.setDistance(150);
		
		File texSkyMap_right = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_right.png", true);
		File texSkyMap_left = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_left.png", true);
		File texSkyMap_top = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_top.png", true);
		File texSkyMap_down = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_down.png", true);
		File texSkyMap_front = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_front.png", true);
		File texSkyMap_back = FileLoader.getFileFromFilesFolder("cubemap/ocean_inlet/ocean_back.png", true);
		textureCube = new GLTextureCube(new File[]{texSkyMap_right, texSkyMap_left, texSkyMap_top, texSkyMap_down, texSkyMap_front, texSkyMap_back});
		
		texture_c = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-c.jpg");
		texture_n = FileLoader.getFileFromFilesFolder("texture/bumpmapping/fieldstone-n.jpg");
		texture_c2 = FileLoader.getFileFromFilesFolder("texture/bumpmapping/texture_4.png");
		texture_n2 = FileLoader.getFileFromFilesFolder("texture/bumpmapping/normal_4.png");
		
		texFileHeightMap = FileLoader.getFileFromFilesFolder("heightmap/Heightmap_Michi2.png");
		texFileMountain = FileLoader.getFileFromFilesFolder("texture/Layered-rock-texture-13.jpg");
		texFileLand = FileLoader.getFileFromFilesFolder("texture/Texture-6.jpg");
		texFileWater = FileLoader.getFileFromFilesFolder("texture/Water_Texture_by_SimplyBackgrounds.jpg");
		
		texFileHeightMap2 = FileLoader.getFileFromFilesFolder("heightmap/heightmap_isle.png");
		texFileMountain2 = FileLoader.getFileFromFilesFolder("texture/stone.jpg");
		texFileLand2 = FileLoader.getFileFromFilesFolder("texture/Texture-6.jpg");
		texFileWater2 = FileLoader.getFileFromFilesFolder("texture/Water_Texture_by_SimplyBackgrounds.jpg");
		texFileBeach2 = FileLoader.getFileFromFilesFolder("texture/img1348666324.jpg");
		
		texFileStone = FileLoader.getFileFromFilesFolder("texture/Granit17.jpg");
		
		File explosionTextureFile = FileLoader.getFileFromFilesFolder("texture/explosion2.png");
		explosionTexture = new GLTexture2D(explosionTextureFile);
		explosionTexture.setTexGrid(new GLTexGrid(4,4));
		
		File textureFile = FileLoader.getFileFromFilesFolder("texture/Mauertextur_001.jpg");
		outerTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Holztextur_1.jpg");
		innerTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Dachtextur_2.jpg");
		outerRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Holztextur_3.jpg");
		innerRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Granit17.jpg");
		floorTexture = new GLTexture2D(textureFile);
//		textureFile = FileLoader.getFileFromFilesFolder("dome256.jpg");
//		pictureTexture = new GLTexture2D(textureFile);
		
//		objFile = FileLoader.getFileFromFilesFolder("obj/bunny.obj");
		
		// Billboards
		bbDrawManager = new BillBoardDrawManager();
		GLTexture2D ritter = new GLTexture2D(FileLoader.getFileFromFilesFolder("billboard/Ritter.png"));
		bbRitter = new GLBillBoard(this.getKonfiguration().getCamera(), ritter, new GLVertex(), 1.8f, 4.898f);
		bbRitter.translate(new GLVertex(-4, 2.38f, -4));
		bbRitter.setColor(new GLVertex(1.0f, 1.0f, 1.0f));
		bbDrawManager.addBillBoard(bbRitter);
		bbRitter = new GLBillBoard(this.getKonfiguration().getCamera(), ritter, new GLVertex(), 1.8f, 4.898f);
		bbRitter.translate(4, 2.38f, -4);
		bbRitter.setColor(new GLVertex(1.0f, 1.0f, 1.0f));
		bbDrawManager.addBillBoard(bbRitter);
		GLTexture2D assasine = new GLTexture2D(FileLoader.getFileFromFilesFolder("billboard/AC-Ezio_Render.png"));
		bbAssasine = new GLBillBoard(this.getKonfiguration().getCamera(), assasine, new GLVertex(), 3.5f, 7.269f);
		bbAssasine.translate(-20, 3.28f, 12);
		bbAssasine.setColor(new GLVertex(1.0f, 1.0f, 1.0f));
		bbDrawManager.addBillBoard(bbAssasine);
		bbAssasine = new GLBillBoard(this.getKonfiguration().getCamera(), assasine, new GLVertex(), 3.5f, 7.269f);
		bbAssasine.translate(-23f, 3.28f, 0);
		bbAssasine.rotate(0, 180, 0);
		bbAssasine.setColor(new GLVertex(1.0f, 1.0f, 1.0f));
		bbDrawManager.addBillBoard(bbAssasine);
		PositionAnimation ani = new PositionAnimation(konfig.getTime(), bbAssasine);
		GLVertex start = bbAssasine.getPosition();
		GLVertex dest = new GLVertex(2, 0, 0);
		ani.addInterpolation(new LinearInterpolation(start.clone(), dest, false), 2f);
		ani.addInterpolation(new LinearInterpolation(ani.getActuallEndPoint(), new GLVertex(-2, 0, 0), false), 2f);
		ani.setLoop(true);
		this.addAnimation(ani);
		
		bbBaum = new GLBillBoard(this.getKonfiguration().getCamera(), "billboard/baum04.png", new GLVertex(), 3f, 5f);
		bbBaum.translate(7f, 8.2f, 5f);
		bbBaum.setColor(new GLVertex(1.0f, 1.0f, 1.0f));
		
		start();
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		gl = drawable.getGL().getGL2();
		
		// Lichter setzen
		pointLight = new GLPointLight(GL2.GL_LIGHT7, new GLVertex(3f, 12f, -3f));
		pointLight.setUseColorMaterial(true);
		pointLight.setAmbient(new GLVertex(0.6f, 0.6f, 0.6f, 1.0f));
		pointLight.setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f, 1.0f));
		pointLight.setSpecular(new GLVertex(0.5f, 0.5f, 0.5f, 1.0f));
		pointLight.setAttenuation(0.5f, 0.01f, 0.008f);
		GLCuboid lightCube = new GLCuboid(new GLVertex(0f, 0f, 0f), 0.6f);
		lightCube.setColor(new GLVertex(1f, 1f, 1f));
		pointLight.setLightObject(lightCube);
		
		dirLight = new GLDirectionLight(GL2.GL_LIGHT6, new GLVertex(-1f, -1f, -1f));
		dirLight.setUseColorMaterial(true);
		dirLight.setAmbient(new GLVertex(0.2f, 0.2f, 0.2f, 1.0f));
		dirLight.setDiffuse(new GLVertex(0.2f, 0.2f, 0.2f, 1.0f));
		dirLight.setSpecular(new GLVertex(0.3f, 0.3f, 0.3f, 1.0f));
		
		// Spotlight f�r Shadow Mapping
		spotLight = new GLSpotLight(GL2.GL_LIGHT5, 
				new GLVertex(8.0f, 10.0f, -8.0f), new GLVertex(4.0f, 0.8f, 6.0f));
		spotLight.setConstantAttenuation(0.1f);
		spotLight.setLinearAttenuation(0.02f);
		spotLight.setQuadraticAttenuation(0.002f);
		spotLight.setAmbient(new GLVertex(0.5f, 0.4f, 0.4f));
		spotLight.setDiffuse(new GLVertex(0.4f, 0.3f, 0.3f));
		spotLight.setSpecular(new GLVertex(0.3f, 0.3f, 0.4f));
		spotLight.setSpotCutoff(26.0f);
		
		this.shaderInit();
		
		GLTexture2D renderTexture = new GLTexture2D(texture_c);
		renderTexture.setGLTextureWrap(GL2.GL_REPEAT);
		GLTexture2D normalTexture = new GLTexture2D(texture_n);
		normalTexture.setGLTextureWrap(GL2.GL_REPEAT);
		GLTexture2D renderTexture2 = new GLTexture2D(texture_c2);
		renderTexture2.setGLTextureWrap(GL2.GL_REPEAT);
		GLTexture2D normalTexture2 = new GLTexture2D(texture_n2);
		normalTexture2.setGLTextureWrap(GL2.GL_REPEAT);
		
		// Heightmap und Texturen erstellen
		heightMap = new GLHeightMapTriangleStrip(50); // Position und Skalierung darf erst sp�ter gesetzt werden!!
		heightMap.setColor(new GLVertex(1,1,1));
		
		GLTexture2D texHeightMap = new GLTexture2D(texFileHeightMap);
		heightMap.setTexture(texHeightMap);
		
		GLTexture2D texMountain = new GLTexture2D(texFileMountain);
		texMountain.setGLTextureWrap(GL2.GL_REPEAT);
		heightMap.setTexture(texMountain, 2);
		
		GLTexture2D texLand = new GLTexture2D(texFileLand);
		texLand.setGLTextureWrap(GL2.GL_REPEAT);
		heightMap.setTexture(texLand, 3);
		
		GLTexture2D texWater = new GLTexture2D(texFileWater);
		texWater.setGLTextureWrap(GL2.GL_REPEAT);
		heightMap.setTexture(texWater, 4);
		
		// Heightmap und Texturen erstellen
		heightMap2 = new GLHeightMapTriangleStrip(50); // Position und Skalierung darf erst sp�ter gesetzt werden!!
		heightMap2.setColor(new GLVertex(1,1,1));
		
		texHeightMap = new GLTexture2D(texFileHeightMap2);
		heightMap2.setTexture(texHeightMap);
		
		GLTexture2D texBeach = new GLTexture2D(texFileBeach2);
		texBeach.setGLTextureWrap(GL2.GL_REPEAT);
		heightMap2.setTexture(texBeach, 1);
		
		texMountain = new GLTexture2D(texFileMountain2);
		texMountain.setGLTextureWrap(GL2.GL_REPEAT);
		heightMap2.setTexture(texMountain, 2);
		
		texLand = new GLTexture2D(texFileLand2);
		texLand.setGLTextureWrap(GL2.GL_REPEAT);
		heightMap2.setTexture(texLand, 3);
		
		texWater = new GLTexture2D(texFileWater2);
		texWater.setGLTextureWrap(GL2.GL_REPEAT);
		heightMap2.setTexture(texWater, 4);
		
		GLTexture2D texStone = new GLTexture2D(texFileStone);
		
		// Physikanimation
		PhysicAnimation ani = new PhysicAnimation(konfig.getTime());
		this.addAnimation(ani);
		
		// Innenraum
		float width = 50; float depth = 30; float height = 30;
		GLVertex pos = new GLVertex();
		closedRoom = new GLClosedRoom(pos, width, depth, height, 0.005f);
		closedRoom.setTexture(renderTexture, 0);
		closedRoom.setTexture(normalTexture, 1);
		closedRoom.setShader(shaderBumpMapping);
		// Innenraum definiert Kameraeinschr�nkung
		float offset = 1.5f; // verhindert ein Rausschauen aus dem Raum
		float xMin = offset - (width / 2f); float xMax = width / 2f - offset;
		float yMin = offset; float yMax = height - offset;
		float zMin = offset - (depth / 2f); float zMax = depth / 2f - offset;
		camera.setBounds(xMin, xMax, yMin, yMax, zMin, zMax);
		// Innenraum ist gleichzeitig Kollisionsobjekt f�r andere Physikobjekte
		offset = 0.1f; // Fehlerkorrektur
		GLStaticPlanePhysic staticPlane = new GLStaticPlanePhysic(new GLVertex(0, 1, 0), offset); // Boden
		ani.addPhysicable(staticPlane);
		staticPlane = new GLStaticPlanePhysic(new GLVertex(0, -1, 0), -height + offset); // Decke
		ani.addPhysicable(staticPlane);
		staticPlane = new GLStaticPlanePhysic(new GLVertex(0, 0, 1), -depth / 2.0f + offset); // hinten
		ani.addPhysicable(staticPlane);
		staticPlane = new GLStaticPlanePhysic(new GLVertex(0, 0, -1), -depth / 2.0f + offset); // vorne
		ani.addPhysicable(staticPlane);
		staticPlane = new GLStaticPlanePhysic(new GLVertex(1, 0, 0), -width / 2.0f + offset); // links
		ani.addPhysicable(staticPlane);
		staticPlane = new GLStaticPlanePhysic(new GLVertex(-1, 0, 0), -width / 2.0f + offset); // rechts
		ani.addPhysicable(staticPlane);
		
		this.createTableAndChairs();
		
		// Partikelsystem
		particle = new WaterParticleSystem(100);
		particle.setColorFaktor(20);
		
		// obj-Dateien
//		complexObject = ObjLoader.objFile2ComplexObj(objFile);
//		complexObject.translate(-4, 2, -1);
//		displayList = new GLDisplayList();
//		displayList.begin(gl);
//		complexObject.draw(gl);
//		displayList.end(gl);
		
		// phys. W�rfel
		physics = new ArrayList<GLObject>();
		physicCube = new GLCuboidPhysic(new GLVertex(2, 6, 2), 1.8f, 1.8f, 1.8f, 2f, 1.0f, 0.6f);
		physicCube.setAngularVelocity(new GLVertex(5,0,0));
		physicCube.setLinearVelocity(new GLVertex(25,15,0));
		physicCube.setTexture(texStone, 0);
		ani.addPhysicable(physicCube);
		physics.add(physicCube);
		physicCube = new GLCuboidPhysic(new GLVertex(1, 3, 1), 1.8f, 1.8f, 1.8f, 2f, 1.0f, 0.6f);
		physicCube.setAngularVelocity(new GLVertex(0,5,3));
		physicCube.setLinearVelocity(new GLVertex(-20,15,-10));
		physicCube.setTexture(texStone, 0);
		ani.addPhysicable(physicCube);
		physics.add(physicCube);
		ani.pauseAnimation();
		
//		// Kameraanimation
//		PositionDirectionAnimation camAni = new PositionDirectionAnimation(konfig.getTime(), konfig.getCamera());
//		camAni.addInterpolation(new BezierInterpolation(new GLVertex(10, 10, 10), new GLVertex(-8, 3, -6), new GLVertex(), new GLVertex(-2, 6, -2)), new LinearInterpolation(new GLVertex(-1, -0.8f, -1), new GLVertex(1, 0.5f, 1)), 6f);
//		this.addAnimation(camAni);
		
		// Torbogen
		arc = createSolidArc(gl, new GLVertex(-23.5f, 5, 0f), 8f, 10f, 3f);
		arc.rotate(0, 90, 0);
		arc.setTexture(renderTexture2, 0);
		arc.setTexture(normalTexture2, 1);
		arc.setShader(shaderBumpMapping);
		
		arc2 = createSolidArc(gl, new GLVertex(20f, 2, 10.5f), 5f, 4f, 6f);
		arc2.rotate(0, 55, 0);
		arc2.setTexture(renderTexture2, 0);
		arc2.setTexture(normalTexture2, 1);
		arc2.setShader(shaderBumpMapping);
		
		// Rechteck mit Explosion
		explosionRect = new GLRectangle(new GLVertex(-5, 14, -5), 4f);
		explosionRect.rotate(0, 45, 0);
		explosionRect.setColor(new GLVertex(1.0f, 1.0f, 1.0f, 1.0f));
		explosionRect.setTexture(explosionTexture);
		
		// Haus (Szene 2)
		house = new GLHouse(new GLVertex(20.0f, 7.9f, 15.0f), 14, 8, 14, 7);
		house.setColor(new GLVertex(1.0f, 1.0f, 1.0f));
		house.setMaterial(GLMaterial.DEFAULT_OWN);
		house.getRoofLeft().setColor(Color.WHITE);
		house.getRoofRight().setColor(Color.WHITE);
		house.getRoofRidge().setColor(Color.WHITE);
		house.setTextureOuterWall(outerTexture, 0);
		house.setTextureInnerWall(innerTexture, 0);
		house.setTextureOuterRoof(outerRoofTexture, 0);
		house.setTextureInnerRoof(innerRoofTexture, 0);
		house.setTextureFloor(floorTexture, 0);
		
		this.cameraMode1();
		gl.glShadeModel(GL2.GL_SMOOTH);
		
		// Vorberechnung
		loadSzene2();
		loadSzene3();
	}
	
	private void loadSzene1() {
		GLUT glut = new GLUT();
		float timeDiff = konfig.getTime().getDiffToLastTime();
		GLMatrix rotateMatrix = GLMatrix.createRotateMatrix(0, timeDiff * 30, 0);
		pointLight.setPosition(GLVertex.mult(rotateMatrix, pointLight.getPosition()));
				
		spotLight.beginGL(gl);
		dirLight.beginGL(gl);
		pointLight.beginGL(gl);
		// Innenraum
		shaderBumpMapping.useProgram(gl);
		shaderBumpMapping.setTextureUnit(gl, "renderTexture", 0);
		shaderBumpMapping.setTextureUnit(gl, "normalTexture", 1);
		closedRoom.draw(gl);
		
		// Bogen zeichnen lassen
		arc.draw(gl);
		arc2.draw(gl);
		
		float time = konfig.getTime().getTime() % 10;
		// GLU Kugel (Normalen werden im Shader gesetzt)
		shaderPhongGluSphere.useProgram(gl);
		gl.glPushMatrix();
		gl.glTranslatef(10f, time, -8f);
		gl.glRotatef(time*36, 1, 0, 0);
		gl.glColor3f(0.7f, 0.2f, 0.3f);
		this.createColorMaterial(new GLVertex(0.7f, 0.2f, 0.3f)).toGL(gl);
		glut.glutSolidSphere(1f, 20, 20);
		gl.glPopMatrix();
		
		shaderPhong.useProgram(gl);
		tableAndChairs.draw(gl);
		// Teekanne mit Partikelsystem
		gl.glPushMatrix();
		this.renderTeaPot(gl, new GLVertex(19f, 4f, 10.5f), 2f, 55);
		gl.glPushMatrix();
		GLMaterial.DEFAULT.toGL(gl);
		gl.glTranslatef(20.78f, 6.2f, 7.85f);
		gl.glScalef(60.0f, 60.0f, 60.0f);
		gl.glRotatef(55, 0, 1, 0);
		particle.beginGL(gl);
		gl.glPopMatrix();
		gl.glPopMatrix();
		
		// Toonshader: Teekanne auf dem Tisch
		shaderToon.useProgram(gl);
		this.renderTeaPot(gl, new GLVertex(0, 2, 0), 0.5f, 0);
		
		// W�rfel mit phys. Verhalten
		shaderPhongTexture.useProgram(gl);
		shaderPhongTexture.setTextureUnit(gl, "texture", 0);
		this.drawList(gl, physics);
		
		ShaderProgram.clearShaderProgram(gl);
	}
	
	private void loadSzene2() {
		pointLight.setUseLightObject(false);
		pointLight.beginGL(gl);
		dirLight.beginGL(gl);
		// Landschaft (nur hier verschieben [mit Now-Befehlen]!!)
		shaderHeightmap.useProgram(gl);
		shaderHeightmap.setTextureUnit(gl, "texHeightMap", 0);
		shaderHeightmap.setTextureUnit(gl, "texMountain", 2);
		shaderHeightmap.setTextureUnit(gl, "texLand", 3);
		shaderHeightmap.setTextureUnit(gl, "texWater", 4);
		
		heightMap.glScaleNow(50, 15, 50);
		heightMap.draw(gl);
		
		ShaderProgram.clearShaderProgram(gl);
		
		house.draw(gl);
		
		// Explosion
		float picsPerSecond = 20;
		float time = this.getKonfiguration().getTime().getTime();
		int texCols = explosionTexture.getTexGrid().getColCount();
		int texRows = explosionTexture.getTexGrid().getRowCount();
		int texNr = Math.round(time * picsPerSecond) % (texCols*texRows);
		int texCol = texNr % texCols;
		int texRow = texNr / texRows;
		explosionRect.setTexRect(texCol, texRow);
		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glDepthMask(false);
		explosionRect.draw(gl);
		gl.glDepthMask(true);
		gl.glDisable(GL2.GL_BLEND);
		
		dirLight.endGL(gl);
		pointLight.endGL(gl);
		pointLight.setUseLightObject(true);
	}
	
	private void loadSzene3() {
		GLUT glut = new GLUT();
		dirLight.beginGL(gl);
		gl.glEnable(GL2.GL_BLEND);
		gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
		// Landschaft (nur hier verschieben [mit Now-Befehlen]!!)
		shaderHeightmap2.useProgram(gl);
		shaderHeightmap2.setTextureUnit(gl, "texHeightMap", 0);
		shaderHeightmap2.setTextureUnit(gl, "texBeach", 1);
		shaderHeightmap2.setTextureUnit(gl, "texMountain", 2);
		shaderHeightmap2.setTextureUnit(gl, "texLand", 3);
		shaderHeightmap2.setTextureUnit(gl, "texWater", 4);
		
		heightMap2.glScaleNow(30, 10, 30);
		heightMap2.draw(gl);
		gl.glDisable(GL2.GL_BLEND);
		
		// Reflection Mapping
		shaderCubicReflection.useProgram(gl);
		shaderCubicReflection.setTextureUnit(gl, "textureCubeMap", 0);
		gl.glEnable(GL2.GL_TEXTURE_CUBE_MAP);
		textureCube.bind(gl);
		gl.glPushMatrix();
		gl.glTranslatef(2, 15, 2);
		glut.glutSolidSphere(1.5f, 100, 100);
		gl.glPopMatrix();
		gl.glDisable(GL2.GL_TEXTURE_CUBE_MAP);
		ShaderProgram.clearShaderProgram(gl);
		dirLight.endGL(gl);
	}
	
	private void createTableAndChairs() {
		// Tisch und St�hle
		tableAndChairs = new GLCustomComplexObject();
		GLTable table = new GLTable(new GLVertex(), 4.5f, 2.5f, 1.8f, 0.7f);
		tableAndChairs.add(table);
		GLChair chair = new GLChair(new GLVertex(-3.5f, 0, 0), 1.3f, 1.3f, 1.2f, 1.5f);
		chair.rotate(0, 90, 0);
		tableAndChairs.add(chair);
		chair = new GLChair(new GLVertex(3.5f, 0, 0), 1.3f, 1.3f, 1.2f, 1.5f);
		chair.rotate(0, -90, 0);
		tableAndChairs.add(chair);
		chair = new GLChair(new GLVertex(-1f, 0, 2.1f), 1.3f, 1.3f, 1.2f, 1.5f);
		chair.rotate(0, -180, 0);
		tableAndChairs.add(chair);
		chair = new GLChair(new GLVertex(-1f, 0, -2.1f), 1.3f, 1.3f, 1.2f, 1.5f);
		tableAndChairs.add(chair);
		chair = new GLChair(new GLVertex(1f, 0, 2.1f), 1.3f, 1.3f, 1.2f, 1.5f);
		chair.rotate(0, -180, 0);
		tableAndChairs.add(chair);
		chair = new GLChair(new GLVertex(1f, 0, -2.1f), 1.3f, 1.3f, 1.2f, 1.5f);
		tableAndChairs.add(chair);
		tableAndChairs.setColor(Color.GRAY);
		tableAndChairs.setMaterial(this.createColorMaterial(new GLVertex(Color.GRAY)));
	}

	private void shaderInit() {
		// Shader
		shaderBumpMapping = new ShaderProgram("vertexPhongBumpMapping.glsl", "fragmentPhongBumpMapping.glsl");
		shaderBumpMapping.compileShaders(this.getClass(), gl);
		shaderHeightmap = new ShaderProgram("vertexHeightMap.glsl", "fragmentHeightMap.glsl");
		shaderHeightmap.compileShaders(this.getClass(), gl);
		shaderHeightmap2 = new ShaderProgram("vertexHeightMap.glsl", "fragmentHeightMap2.glsl");
		shaderHeightmap2.compileShaders(this.getClass(), gl);
		shaderPhong = new ShaderProgram("vertexPhong.glsl", "fragmentPhong.glsl");
		shaderPhong.compileShaders(this.getClass(), gl);
		shaderPhongTexture = new ShaderProgram("vertexPhongTexture.glsl", "fragmentPhongTexture.glsl");
		shaderPhongTexture.compileShaders(this.getClass(), gl);
		shaderPhongGluSphere = new ShaderProgram("vertexPhongGluSphere.glsl", "fragmentPhongGluSphere.glsl");
		shaderPhongGluSphere.compileShaders(this.getClass(), gl);
		shaderCubicReflection = new ShaderProgram("vertexCubicReflection.glsl", "fragmentCubicReflection.glsl");
		shaderCubicReflection.compileShaders(this.getClass(), gl);
		shaderToon = new ShaderProgram("vertexToon.glsl", "fragmentToon.glsl");
		shaderToon.compileShaders(this.getClass(), gl);
	}
	
	private GLMaterial createColorMaterial(GLVertex color) {
		GLMaterial mat = new GLMaterial();
		mat.setMatAmbient(color);
		mat.setMatDiffuse(color);
		mat.setMatSpecular(color);
		return mat;
	}
	
	private GLMaterial createTextureMaterial() {
		GLMaterial mat = new GLMaterial();
		mat.setMatAmbient(new GLVertex(1, 1, 1));
		mat.setMatDiffuse(new GLVertex(1, 1, 1));
		mat.setMatSpecular(new GLVertex(1, 1, 1));
		return mat;
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		if (helpMode) showHelp(gl);
		
		if (szene == Szene.SZENE1) {
			loadSzene1();
		}
		if (szene == Szene.SZENE2) {
			this.loadSzene2();
		}
		if (szene == Szene.SZENE3) {
			this.loadSzene3();
		}
	}

	@Override
	public void endGL(GL2 gl) {
		if (szene == Szene.SZENE1) {
			shaderPhongTexture.useProgram(gl);
			// Billboard (muss als Letztes gezeichnet werden)
			bbDrawManager.drawBillBoards(gl);
			ShaderProgram.clearShaderProgram(gl);
			spotLight.endGL(gl);
			dirLight.endGL(gl);
			pointLight.endGL(gl);
		}
		if (szene == Szene.SZENE3) {
			shaderPhongTexture.useProgram(gl);
			// Billboard (muss als Letztes gezeichnet werden)
			gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
			gl.glEnable(GL2.GL_BLEND);
			gl.glDepthMask(false);
			bbBaum.draw(gl);
			gl.glDepthMask(true);
			gl.glDisable(GL2.GL_BLEND);
			ShaderProgram.clearShaderProgram(gl);
		}
		super.endGL(gl);
	}
	
	private void drawList(GL2 gl, ArrayList<GLObject> objects) {
		for (GLObject object : objects) {
			object.draw(gl);
		}
		
	}
	
	private void renderTeaPot(GL2 gl, GLVertex pos, float scale, float yRotate) {
		GLUT glut = new GLUT();
		gl.glPushMatrix();
			GLMaterial.DEFAULT.toGL(gl);
			gl.glTranslatef(pos.getX(), pos.getY(), pos.getZ());
			gl.glRotatef(-90.0f, 1, 0, 0);
			gl.glRotatef(yRotate, 0, 0, 1);
			glut.glutSolidTeapot(scale, false);
		gl.glPopMatrix();
	}
	
	private GLWallWithHole createSolidArc(GL2 gl, GLVertex pos, float width, float height, float depth) {
		return new GLWallWithHole(pos, width, 0.2f * width, 0.2f * width, height, 0.15f * height, 0f, depth);
	}
	
	@Override
	public void cameraMode1() {
		// Szene 1 (drinnen)
		szene = Szene.SZENE1;
		camera.setUseBounds(true);
		camera.setPosition(new GLVertex(10, 10, 10));
		camera.setDirection(new GLVertex(-1, -0.8f, -1));
	}

	@Override
	public void cameraMode2() {
		// Szene 2 (drau�en)
		szene = Szene.SZENE2;
		konfig.getCamera().setUseBounds(false);
		konfig.getCamera().setPosition(new GLVertex(10, 15, 10));
		konfig.getCamera().setDirection(new GLVertex(-1, 0, -1));
	}

	@Override
	public void cameraMode3() {
		// Szene 3 (drau�en)
		szene = Szene.SZENE3;
		konfig.getCamera().setUseBounds(false);
		konfig.getCamera().setPosition(new GLVertex(10, 12, 10));
		konfig.getCamera().setDirection(new GLVertex(-1, 0, -1));
	}

	@Override
	public void cameraMode4() {

	}
	
	@Override
	public void keyH() {
		// Hilfe an/ausschalten
		helpMode = !helpMode;
	}

	private void showHelp(GL2 gl) {
		// funktioniert irgendwie nicht
//		Utility.drawText(gl, konfig.getTextFont(), konfig.getTextColor(), "Hilfe", 30, 100);
	}

}
