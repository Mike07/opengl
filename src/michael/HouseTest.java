package michael;

import java.awt.Color;
import java.io.File;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import johannes.J_Utility;

import physic.GLCuboidPhysic;
import physic.GLStaticPlanePhysic;
import physic.PhysicAnimation;

import com.jogamp.opengl.util.gl2.GLUT;

import falk_guenther_meier.model.BillBoardDrawManager;
import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.GLDisplayList;
import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.buildings.GLChair;
import falk_guenther_meier.model.buildings.GLClosedRoom;
import falk_guenther_meier.model.buildings.GLHouse;
import falk_guenther_meier.model.buildings.GLTable;
import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLBillBoard;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLHeightMapTriangleStrip;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.textures.GLTextureCube;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.model.world.Konfiguration;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.SkyBoxLoader;
import falk_guenther_meier.util.Utility;

public class HouseTest extends JoglApp {
	
	private GL2 gl;
	private Konfiguration konfig;
	private GLHeightMapTriangleStrip heightMap;
	private boolean helpMode = false;
	
	// Lichter
	private GLPointLight pointLight;
	
	// Kameras
	private GLCamera camera;
	
	// Szenen
	private enum Szene {SZENE1, SZENE2, SZENE3};
	private Szene szene;
	
	// Shader
	private ShaderProgram shaderBumpMapping;
	private ShaderProgram shaderHeightmap;
	private ShaderProgram shaderHeightmap2;
	private ShaderProgram shaderPhong;
//	private ShaderProgram shaderPhongTexture;
	private ShaderProgram shaderPhongGluSphere;
	private GLTexture2D outerTexture;
	private GLTexture2D innerTexture;
	private GLTexture2D outerRoofTexture;
	private GLTexture2D innerRoofTexture;
	private GLTexture2D floorTexture;
	private GLTexture2D pictureTexture;
	private GLHouse house;
	
	// Dateien: Texturen etc.

	
	// Objekte

	
	public static void main(String[] args) {
		new HouseTest("Einzelprojekt Michi", 800, 600);
	}
	
	public HouseTest(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
		konfig = this.getKonfiguration();
		konfig.setSkybox(SkyBoxLoader.getOceanInletSkyBox(this.getKonfiguration().getCamera(), 150)); // Skybox gr��er machen
		camera = konfig.getCamera();
		this.setAngle(70);
		this.setDistance(150);
		this.cameraMode3();
		
		File textureFile = FileLoader.getFileFromFilesFolder("texture/Mauertextur_001.jpg");
		outerTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Holztextur_1.jpg");
		innerTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Dachtextur_2.jpg");
		outerRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Holztextur_3.jpg");
		innerRoofTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("texture/Granit17.jpg");
		floorTexture = new GLTexture2D(textureFile);
		textureFile = FileLoader.getFileFromFilesFolder("dome256.jpg");
		pictureTexture = new GLTexture2D(textureFile);
		
		start();
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		gl = drawable.getGL().getGL2();
		
		// Lichter setzen
		pointLight = new GLPointLight(GL2.GL_LIGHT7, new GLVertex(2f, 6f, -2f));
		pointLight.setUseColorMaterial(true);
		pointLight.setAmbient(new GLVertex(0.6f, 0.6f, 0.6f, 1.0f));
		pointLight.setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f, 1.0f));
		pointLight.setSpecular(new GLVertex(0.5f, 0.5f, 0.5f, 1.0f));
		pointLight.setAttenuation(0.9f, 0.01f, 0.008f);
		GLCuboid lightCube = new GLCuboid(new GLVertex(0f, 0f, 0f), 0.3f);
		lightCube.setColor(new GLVertex(1f, 1f, 1f));
		pointLight.setLightObject(lightCube);
		this.addLight(pointLight);
		
		house = new GLHouse(new GLVertex(0.0f, 0.0f, 0.0f), 12, 6, 12, 6);
		house.setColor(new GLVertex(1.0f, 1.0f, 1.0f));
		house.setMaterial(GLMaterial.DEFAULT_OWN);
		house.getRoofLeft().setColor(Color.WHITE);
		house.getRoofRight().setColor(Color.WHITE);
		house.getRoofRidge().setColor(Color.WHITE);
		house.setTextureOuterWall(outerTexture, 0);
		house.setTextureInnerWall(innerTexture, 0);
		house.setTextureOuterRoof(outerRoofTexture, 0);
		house.setTextureInnerRoof(innerRoofTexture, 0);
		house.setTextureFloor(floorTexture, 0);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		GLUT glut = new GLUT();
		if (helpMode) showHelp(gl);
		// Haus zeichnen
		house.draw(gl);
	}
	
	@Override
	public void endGL(GL2 gl) {
		super.endGL(gl);
	}
	
	private void renderTeaPot(GL2 gl, GLVertex pos, float scale, float yRotate) {
		GLUT glut = new GLUT();
		gl.glPushMatrix();
			gl.glTranslatef(pos.getX(), pos.getY(), pos.getZ());
			gl.glRotatef(-90.0f, 1, 0, 0);
			gl.glRotatef(yRotate, 0, 0, 1);
			glut.glutSolidTeapot(scale, false);
		gl.glPopMatrix();
	}
	
	@Override
	public void cameraMode1() {
		// Szene 1 (drinnen)
		szene = Szene.SZENE1;
		camera.setUseBounds(true);
		camera.setPosition(new GLVertex(10, 10, 10));
		camera.setDirection(new GLVertex(-1, -0.8f, -1));
	}

	@Override
	public void cameraMode2() {
		// Szene 2 (drau�en)
		szene = Szene.SZENE2;
		konfig.getCamera().setUseBounds(false);
		konfig.getCamera().setPosition(new GLVertex(10, 15, 10));
		konfig.getCamera().setDirection(new GLVertex(-1, 0, -1));
	}

	@Override
	public void cameraMode3() {
		// Szene 3 (drau�en)
		szene = Szene.SZENE3;
		konfig.getCamera().setUseBounds(false);
		konfig.getCamera().setPosition(new GLVertex(10, 15, 10));
		konfig.getCamera().setDirection(new GLVertex(-1, 0, -1));
	}

	@Override
	public void cameraMode4() {

	}
	
	@Override
	public void keyH() {
		// Hilfe an/ausschalten
		helpMode = !helpMode;
	}

	private void showHelp(GL2 gl) {
		// funktioniert irgendwie nicht
		Utility.drawText(gl, konfig.getTextFont(), konfig.getTextColor(), "Hilfe", 30, 100);
	}
}
