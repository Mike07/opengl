varying vec3 eyeSpaceNormal;
varying vec4 shadowCoord;

void main() {
eyeSpaceNormal =  gl_Normal.xyz;

shadowCoord =  gl_TextureMatrix[2] * gl_ModelViewMatrix * gl_Vertex;
shadowCoord = shadowCoord/(shadowCoord.w);

gl_Position =  gl_ProjectionMatrix * gl_ModelViewMatrix *  gl_Vertex;
}
