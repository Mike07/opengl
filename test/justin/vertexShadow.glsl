varying vec3 normal;
varying vec3 position;
varying vec4 texCoord;

void main() {
	normal =  gl_NormalMatrix * gl_Normal;

	vec4 position2 = gl_ModelViewMatrix * gl_Vertex;
	position = position2.xyz / position2.w;

	texCoord =  gl_TextureMatrix[5] * gl_ModelViewMatrix * gl_Vertex;
	texCoord = texCoord / (texCoord.w);

	gl_Position =  gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
}
