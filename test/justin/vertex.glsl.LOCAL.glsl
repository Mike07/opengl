varying vec3 eyeSpaceNormal;
varying vec4 texCoord2;
//uniform mat4 TexMatrix;

void main() {
eyeSpaceNormal =  gl_Normal;

texCoord2 =  gl_TextureMatrix[2] *  gl_ModelViewMatrix * gl_Vertex;
texCoord2 = texCoord2/(texCoord2.w);

gl_Position =  gl_ProjectionMatrix *  gl_ModelViewMatrix *  gl_Vertex;

}
