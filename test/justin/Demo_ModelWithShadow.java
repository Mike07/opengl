
package justin;

import java.awt.Color;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.animation.PositionAnimation;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.interpolation.LinearInterpolation;
import falk_guenther_meier.model.light.GLSpotLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.shader.ShaderProgram;
import falk_guenther_meier.model.world.JoglAppShadowMapping;
import falk_guenther_meier.util.Utility;

/**
 * Demo-Programm, um zu zeigen, wie mit der neuen "JoglApp" unser Modell mit Justins Schatten verbunden werden kann.
 * @author Johannes
 */
public class Demo_ModelWithShadow extends JoglAppShadowMapping {
	private GLUquadric quadric;
	private float sphereY = 2.0f;
	private GLOpenRoom room;
	private GLCuboid cubeBig;
	private GLCuboid cubeLittle;
	private GLCuboid plane;
	private ShaderProgram shadowShader;
	private ShaderProgram standardShader;
	private GLCustomComplexObject spheres;

	public static void main(String[] args) {
    	new Demo_ModelWithShadow();
	}

	public Demo_ModelWithShadow() {
		super("Justins Test f�r Shadow-Mapping", 800, 600, true);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GL2 gl = drawable.getGL().getGL2();

		quadric = glu.gluNewQuadric();
		glu.gluQuadricDrawStyle(quadric, GLU.GLU_FILL);
		glu.gluQuadricNormals(quadric, GLU.GLU_SMOOTH);
		glu.gluQuadricOrientation(quadric, GLU.GLU_OUTSIDE);

		GLVertex lightPos = new GLVertex(2.0f, 10.0f, 0.0f);
		GLSpotLight spotLight = new GLSpotLight(GL2.GL_LIGHT0, lightPos, GLVertex.invert(lightPos));
		spotLight.setAmbient(new GLVertex(0.2f, 0.4f, 0.6f));
		spotLight.setDiffuse(new GLVertex(0.2f, 0.4f, 0.6f));
		spotLight.setSpecular(new GLVertex(0.2f, 0.4f, 0.6f));

		this.setLightForShadowMapping(spotLight);
		this.addLight(spotLight);

		room = new GLOpenRoom(new GLVertex(0.0f, -10.0f, 0.0f), 30.0f, 20.0f, 2.0f, 0.5f);
		room.setColor(Color.GREEN);
		room.setMaterial(new GLMaterial());

		cubeBig = new GLCuboid(new GLVertex(10.0f, -0.0f, 0.0f), 2.0f);
		cubeBig.setColor(Color.BLUE);
		cubeBig.setMaterial(new GLMaterial());

		cubeLittle = new GLCuboid(new GLVertex(0.0f, -3.0f, 1.0f), 0.5f);
		cubeLittle.setColor(Color.YELLOW);
		cubeLittle.setMaterial(new GLMaterial());

		plane = new GLCuboid(new GLVertex(0.0f, -1.5f, 0.0f), 10.0f, 0.5f, 10.0f);
		plane.setColor(Color.BLACK);
		plane.setMaterial(new GLMaterial());

		PositionAnimation ani = new PositionAnimation(this.konfiguration.getTime(), this.cubeBig);
		ani.addInterpolation(new LinearInterpolation(cubeBig.getPosition().clone(), new GLVertex(0.0f, 5.0f, 0.0f), false), 8.0f);
		this.addAnimation(ani);

		ani = new PositionAnimation(this.konfiguration.getTime(), this.cubeLittle);
		ani.addInterpolation(new LinearInterpolation(cubeLittle.getPosition().clone(), new GLVertex(0.0f, 8.0f, 0.0f), false), 5.0f);
		this.addAnimation(ani);

		this.shadowShader = new ShaderProgram("vertexShadow.glsl", "fragmentShadow.glsl");
		this.shadowShader.compileShaders(this.getClass(), gl);

		this.standardShader = new ShaderProgram("vertexStandard.glsl", "fragmentStandard.glsl");
		this.standardShader.compileShaders(this.getClass(), gl);

		this.spheres = new GLCustomComplexObject();
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				GLObject o = new GLCuboid(new GLVertex(i, 0, j), 0.578f);
//				Utility.setRandomColor(o);
				o.setMaterial(GLMaterial.createColor(Utility.getRandomColor()));
				this.spheres.add(o);
			}
		}
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);

		if (this.isDrawShadow()) {
			this.shadowShader.useProgram(gl);
			this.shadowShader.setTextureUnit(gl, "shadowMap", 2); // bind texture unit 2 to shadowMap-Sampler in the Shader
		} else {
			this.standardShader.useProgram(gl);
		}

		gl.glPushMatrix();
			gl.glTranslatef(0f, sphereY, 1f);
			glu.gluSphere(quadric, 0.578f, 16, 16);
		gl.glPopMatrix();

		this.spheres.draw(gl);
		this.room.draw(gl);
		this.cubeBig.draw(gl);
		this.cubeLittle.draw(gl);
		this.plane.draw(gl);

		ShaderProgram.clearShaderProgram(gl);
	}

	@Override
	protected void drawShadowRendering(GL2 gl) {
		super.drawShadowRendering(gl);

		gl.glPushMatrix();
			gl.glTranslatef(0f, sphereY, 1f);
			glu.gluSphere(quadric, 0.578f, 16, 16);
		gl.glPopMatrix();

		this.spheres.draw(gl);
		this.room.draw(gl);
		this.cubeBig.draw(gl);
		this.cubeLittle.draw(gl);
		this.plane.draw(gl);
	}

	@Override
	protected void initFrame() {
		super.initFrame();
		sphereY += 0.03f;
		if (sphereY >= 5.0f) {
			sphereY = -3.0f;
		}
	}
}
