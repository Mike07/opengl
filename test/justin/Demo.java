package justin;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.InputStream;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.util.glsl.ShaderUtil;

/*
 * Diese Beispiel-Klasse erzeugt einen JOGL2-Kontext.
 * Dieser benoetigt nur OpenGL 2.1, was insbesondere
 * mit den verwendeten Shadern zusammenhaengt.
 */
public class Demo implements GLEventListener {

	static int shaderprogram = 0;
	static GLU glu = new GLU();
	static GLUquadric quadric;
	static float sphereY = 2.0f;
	static float radius = 0.578f;
	static int slices = 16;
	static int stacks = 16;

	float[] lightPos = { 2.0f, 15.0f, -2.0f, 0.0f };
	static float camRot = 0.0f;
	static int width = 640;
	static int height = 480;

	static int framebuffer = 0;
	static int depthbuffer = 0;
	static int depthtexture = 0;
	static int DEPTH_MAP_SIZE = 256;
	private static float[] cameraProjectionMatrix = new float[16];
	private static float[] cameraModelviewMatrix = new float[16];
	public static float[] lightProjectionMatrix = new float[16];
	public static float[] lightModelviewMatrix = new float[16];
	private static float[] cameraInverse = new float[16];

	public static void main(String[] args) {
		GLProfile.initSingleton();

		GLProfile glp = GLProfile.get(GLProfile.GL2);
		GLCapabilities caps = new GLCapabilities(glp);
		GLCanvas canvas = new GLCanvas(caps);
		canvas.addGLEventListener(new Demo());

		Frame frame = new Frame("Shadow Mapping");
		frame.add(canvas);
		frame.setSize(width, height);

		final Animator animator = new Animator(canvas);

		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				// Run this on another thread than the AWT event queue to
				// make sure the call to Animator.stop() completes before
				// exiting
				new Thread(new Runnable() {
					public void run() {
						animator.stop();
						System.exit(0);
					}
				}).start();
			}
		});

		frame.setVisible(true);
		animator.start();
	}

	// Alles initialisieren
	public void init(GLAutoDrawable drawable) {
		System.err.println("GLBasecode Init: " + drawable);
		// Use debug pipeline
		// drawable.setGL(new DebugGL(drawable.getGL()));

		GL2 gl = drawable.getGL().getGL2();

		System.err.println("Chosen GLCapabilities: " + drawable.getChosenGLCapabilities());
		System.err.println("INIT GL IS: " + gl.getClass().getName());
		System.err.println("GL_VENDOR: " + gl.glGetString(GL2.GL_VENDOR));
		System.err.println("GL_RENDERER: " + gl.glGetString(GL2.GL_RENDERER));
		System.err.println("GL_VERSION: " + gl.glGetString(GL2.GL_VERSION));

		gl.setSwapInterval(1);

		gl.glClearColor(0.25f, 0.25f, 0.3f, 1.0f);
		gl.glDisable(GL2.GL_CULL_FACE);
		gl.glShadeModel(GL2.GL_SMOOTH);
		gl.glClearDepth(1.0f);
		gl.glEnable(GL2.GL_DEPTH_TEST);
		gl.glDepthFunc(GL2.GL_LEQUAL);
		gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST);

		Demo.shaderprogram = Demo.compileShaders(gl);
		gl.glUseProgram(Demo.shaderprogram);
		// bind texture unit 2 to shadowMap-Sampler in the Shader.
		int loc = gl.glGetUniformLocation(Demo.shaderprogram, "shadowMap");
		if (loc != -1) {
			gl.glUniform1i(loc, 2); // Texture unit 2
		} else {
			System.out.println("Uniform shadowMap nicht gefunden ... " + loc); // TODO better Error-Handling
		}
		quadric = glu.gluNewQuadric();
		glu.gluQuadricDrawStyle(quadric, GLU.GLU_FILL);
		glu.gluQuadricNormals(quadric, GLU.GLU_SMOOTH);
		glu.gluQuadricOrientation(quadric, GLU.GLU_OUTSIDE);

		gl.glEnable(GL2.GL_LIGHTING);
		gl.glEnable(GL2.GL_LIGHT0);
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, lightPos, 0);

		// Initialize shadow map
		Demo.createDepthTexture(gl);

		// use normal framebuffer (screen)
		gl.glBindFramebuffer(GL2.GL_FRAMEBUFFER, 0);

	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		GL2 gl = drawable.getGL().getGL2();
		GLU glu = new GLU();

		if (height == 0) {
			height = 1;
		}

		float aspect = (float) width / (float) height;

		Demo.width = width;
		Demo.height = height;
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();

		glu.gluPerspective(45.0f, aspect, 0.1f, 100.0f);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	// Simple Camera function,
	// can be replaced by gluLookAt or own camera function
	public static void useCamera(GL2 gl) {
		gl.glRotated(20f, 1f, 0f, 0f);
		gl.glTranslatef(0.0f, -2.4f, -10.0f);
		gl.glRotated(camRot, 0f, 1f, 0f);
	}

	// Render the shadow map to the depth texture
	// "Aus der Sicht des Lichtes"
	public void makeShadowMap(GL2 gl) {
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		gl.glLoadIdentity();

		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glLoadMatrixf(Demo.lightProjectionMatrix, 0);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
		gl.glLoadMatrixf(Demo.lightModelviewMatrix, 0);

		Demo.startDepthMapRendering(gl);
		gl.glUseProgram(0); // No shader necessary
		displayObjects(gl, false);
		Demo.endDepthMapRendering(gl);
		// The calculated Matrix transforms coordinates from the cameras
		// clipspace into the
		// lights clipspace. In order to use the coordinates as texture
		// coordinates, it will
		// be mutliplied by 0.5 and 0.5 will be added: coordinates between
		// (0.0,0.0) and (1.0,1.0)
		Demo.calculateTextureMatrix(gl);
	}

	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		// calculate all Projection and View Matrices
		Demo.saveMatrices(gl, width, height, lightPos);
		// render the shadow map
		makeShadowMap(gl);

		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
		// use shadow map shader
		gl.glUseProgram(Demo.shaderprogram);

		// now the scene will be rendered to the screen
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glLoadMatrixf(Demo.cameraProjectionMatrix, 0);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadMatrixf(Demo.cameraModelviewMatrix, 0);

		displayObjects(gl);

		camRot += 0.133f;
		sphereY += 0.03f;
		if (sphereY >= 5.0f)
			sphereY = -3.0f;
	}

	public void displayObjects(GL2 gl) {
		// default with floor ;)
		displayObjects(gl, true);
	}

	// draw the objects
	public void displayObjects(GL2 gl, boolean floor) {
		gl.glPushMatrix();
		if (floor) {
			gl.glBegin(GL2.GL_QUADS);
			gl.glNormal3f(0f, 1f, 0f);
			gl.glVertex3f(-5.0f, -1.5f, -5.0f);
			gl.glVertex3f(-5.0f, -1.5f, 5.0f);
			gl.glVertex3f(5.0f, -1.5f, 5.0f);
			gl.glVertex3f(5.0f, -1.5f, -5.0f);
			gl.glEnd();
		}
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++) {
				gl.glPushMatrix();
				gl.glTranslatef(i, 0, j);
				glu.gluSphere(quadric, radius, slices, stacks);
				gl.glPopMatrix();
			}

		gl.glTranslatef(0f, sphereY, 1f);

		glu.gluSphere(quadric, radius, slices, stacks);

		gl.glPopMatrix();

	}

	public void dispose(GLAutoDrawable drawable) {
	}

	// Shader kompilieren und linken, Beispiel
	public static int compileShaders(GL2 gl) {
		int v = gl.glCreateShader(GL2.GL_VERTEX_SHADER);
		int f = gl.glCreateShader(GL2.GL_FRAGMENT_SHADER);
		int[] success = new int[1];
		String vsrc = Demo.readShader("vertex.glsl");
		gl.glShaderSource(v, 1, new String[] { vsrc }, (int[]) null, 0);
		gl.glCompileShader(v);

		gl.glGetShaderiv(v, GL2.GL_COMPILE_STATUS, success, 0);
		if (success[0] == GL2.GL_FALSE) {
			System.out.println("Fehler im VS");
		}

		String fsrc = Demo.readShader("fragment.glsl");
		gl.glShaderSource(f, 1, new String[] { fsrc }, (int[]) null, 0);
		gl.glCompileShader(f);
		gl.glGetShaderiv(v, GL2.GL_COMPILE_STATUS, success, 0);
		if (success[0] == GL2.GL_FALSE) {
			System.out.println("Fehler im FS");
		}
		int shaderprogram = gl.glCreateProgram();
		gl.glAttachShader(shaderprogram, v);
		gl.glAttachShader(shaderprogram, f);
		gl.glLinkProgram(shaderprogram);
		gl.glGetProgramiv(shaderprogram, GL2.GL_LINK_STATUS, success, 0);
		if (success[0] == GL2.GL_FALSE) {
			System.out.println("Linkage Error!");
		} else {
			System.out.println("shader: " + shaderprogram
					+ " - Linkage was successful.");
		}
		gl.glValidateProgram(shaderprogram);

		System.out.println(ShaderUtil.getShaderInfoLog(gl, shaderprogram));

		gl.glUseProgram(shaderprogram);
		return shaderprogram; // handle zurueckgeben
	}

	// Shader-Quellcode aus Datei lesen
	public static String readShader(String src) {
		InputStream is = Demo.class.getResourceAsStream(src);
		if (is == null) {
			System.err.println("Could not read " + src);
			System.exit(1);
		}
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

	public static void createDepthTexture(GL2 gl) {

		// FramebufferObjekt erstellen
		int[] tmp = new int[1];
		gl.glGenFramebuffers(1, tmp, 0);
		framebuffer = tmp[0];

		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, framebuffer);

		// Tiefentexture
		gl.glGenTextures(1, tmp, 0);
		depthtexture = tmp[0];

		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glActiveTexture(GL.GL_TEXTURE2);
		gl.glBindTexture(GL.GL_TEXTURE_2D, depthtexture);
		gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL2.GL_DEPTH_COMPONENT32,
				DEPTH_MAP_SIZE, DEPTH_MAP_SIZE, 0, GL2.GL_DEPTH_COMPONENT,
				GL.GL_UNSIGNED_BYTE, null);

		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER,
				GL.GL_NEAREST);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER,
				GL.GL_NEAREST);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S,
				GL.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T,
				GL.GL_CLAMP_TO_EDGE);

		checkFrameBuffer(gl);
		gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL.GL_DEPTH_ATTACHMENT,
				GL.GL_TEXTURE_2D, depthtexture, 0);
		gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
		checkFrameBuffer(gl);
		gl.glDrawBuffer(0);
		gl.glReadBuffer(0);

	}

	// prepare render to texture using fbo
	public static void startDepthMapRendering(GL2 gl) {

		gl.glShadeModel(GL2.GL_FLAT);
		gl.glColorMask(false, false, false, false);
		gl.glEnable(GL.GL_POLYGON_OFFSET_FILL);
		gl.glPolygonOffset(1.0F, 4.0F);

		gl.glEnable(GL.GL_CULL_FACE);
		gl.glCullFace(GL.GL_FRONT);
		gl.glFrontFace(GL.GL_CCW);

		gl.glClearDepth(1.0f);
		gl.glUseProgram(0);
		gl.glViewport(0, 0, DEPTH_MAP_SIZE, DEPTH_MAP_SIZE);
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, framebuffer);
		gl.glClear(GL.GL_DEPTH_BUFFER_BIT);
		gl.glDrawBuffer(0);
		gl.glReadBuffer(0);

	}

	// end render to fbo, prepare normal render
	public static void endDepthMapRendering(GL2 gl) {
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0);
		gl.glViewport(0, 0, width, height);
		gl.glEnable(GL.GL_TEXTURE_2D);
		gl.glActiveTexture(GL.GL_TEXTURE2);
		gl.glBindTexture(GL.GL_TEXTURE_2D, depthtexture);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S,
				GL.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T,
				GL.GL_CLAMP_TO_EDGE);

		gl.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER,
				GL.GL_NEAREST);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER,
				GL.GL_NEAREST);

		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_FUNC,
				GL2.GL_LESS);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_COMPARE_MODE,
				GL2.GL_NONE);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_DEPTH_TEXTURE_MODE,
				GL2.GL_INTENSITY);

		gl.glCullFace(GL.GL_BACK);

		gl.glDisable(GL.GL_CULL_FACE);
		gl.glShadeModel(GL2.GL_SMOOTH);
		gl.glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
		gl.glClearDepth(1.0f);
		gl.glColorMask(true, true, true, true);

	}

	// save all the matrices that will be used for both camera and lights view
	public static void saveMatrices(GL2 gl, int width, int height, float[] lightPos) {
		GLU glu = new GLU();

		// Setze die Einstellungen fuer die PROJECTION_MATRIX.
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluPerspective(45.0f, ((float) width / (float) height), 0.1f, 200.0f);

		// aktuelle Projection-Matrix speichern
		gl.glGetFloatv(GL2.GL_PROJECTION_MATRIX, Demo.cameraProjectionMatrix, 0);

		gl.glLoadIdentity();
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
		Demo.useCamera(gl);

		gl.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, Demo.cameraModelviewMatrix, 0);
		gl.glLoadIdentity();

		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		// TODO try Ortho here as well
		glu.gluPerspective(42.0f, 1.0f, 0.01f, 28.0f);

		gl.glGetFloatv(GL2.GL_PROJECTION_MATRIX, Demo.lightProjectionMatrix, 0);

		gl.glLoadIdentity();
		glu.gluPerspective(45.0f, ((float) width / (float) height), 0.1f, 200.0f);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
		glu.gluLookAt(lightPos[0], lightPos[1], lightPos[2], 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);
		gl.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, Demo.lightModelviewMatrix, 0);
		gl.glLoadIdentity();

	}

	public static void calculateTextureMatrix(GL2 gl) {
		// instead of using texture matrix you can also use a uniform!
		gl.glActiveTexture(GL.GL_TEXTURE2);
		gl.glMatrixMode(GL.GL_TEXTURE);
		gl.glActiveTexture(GL.GL_TEXTURE2);

		float texMat[] = { 0.5F, 0F, 0F, 0.0F, 0.0F, 0.5F, 0F, 0.0F, 0.0F, 0F,
				0.5F, 0.0F, 0.5F, 0.5F, 0.5F, 1.0F };
		gl.glLoadMatrixf(texMat, 0);
		gl.glMultMatrixf(lightProjectionMatrix, 0);
		gl.glMultMatrixf(lightModelviewMatrix, 0);

		// So you ask, why do i have to invert the Cameras View Matrix?
		// This is only neccesary if you use the Modelviewmatrix for the
		// translation and rotaiton of objects: if you do so,
		// you cannot distinguish between Model and View! (look at the vertex
		// shader)
		// So in order to compare the correct coordinates
		// you have to separate the Model and the view matrix.
		// You can also do this in a different manner,
		// but doing it this way is straight forward if
		// you use glRotate and glTranslate.
		Demo.fastInvert(cameraModelviewMatrix, cameraInverse);
		gl.glMultMatrixf(cameraInverse, 0);

		gl.glMatrixMode(GL2.GL_MODELVIEW_MATRIX);
		// Ende Texture-Matrix berechnen

	}

	public static void fastInvert(float mat[], float ret[]) {
		ret[0] = mat[0];
		ret[1] = mat[4];
		ret[2] = mat[8];
		ret[3] = 0.0f;
		ret[4] = mat[1];
		ret[5] = mat[5];
		ret[6] = mat[9];
		ret[7] = 0.0f;
		ret[8] = mat[2];
		ret[9] = mat[6];
		ret[10] = mat[10];
		ret[11] = 0.0f;
		ret[12] = -(mat[12] * ret[0] + mat[13] * ret[4] + mat[14] * ret[8]);
		ret[13] = -(mat[12] * ret[1] + mat[13] * ret[5] + mat[14] * ret[9]);
		ret[14] = -(mat[12] * ret[2] + mat[13] * ret[6] + mat[14] * ret[10]);
		ret[15] = 1.0f;

	}

	public static void checkFrameBuffer(GL gl) {
		int status = gl.glCheckFramebufferStatus(GL.GL_FRAMEBUFFER);
		System.out.println(status);
		switch (status) {
		case GL.GL_FRAMEBUFFER_COMPLETE:
			System.out.println("FRAMEBUFFER OK");
			break;
		case GL.GL_FRAMEBUFFER_UNSUPPORTED:
			System.out.println("<!> Unsupported framebuffer format\n");
			break;
		case GL.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			System.out.println("<!> Framebuffer incomplete, missing attachment\n");
			break;
		case GL.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			System.out.println("<!> Framebuffer incomplete attachement\n");
			break;
		case GL.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
			System.out.println("<!> Framebuffer incomplete, attached images must have same dimensions\n");
			break;
		case GL.GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
			System.out.println("<!> Framebuffer incomplete, attached images must have same format\n");
			break;
		default:
			System.out.println("<!> Framebuffer incomplete, other reason\n");
			return;
		}
	}
}
