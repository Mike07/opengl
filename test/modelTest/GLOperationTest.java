package modelTest;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.world.JoglApp;

/**
 * Testet relative Drehungen aus.
 * @author Hanno
 */
public class GLOperationTest extends JoglApp {
	GLCustomComplexObject scene; 
	GLCuboid cuboid;
	GLRectangle rect;

	public GLOperationTest(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}
	
	public static void main(String[] args) {
		new GLOperationTest("GLOperation Test with Push- and PopMatrix", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		//GL2 gl = drawable.getGL().getGL2();
		
		cuboid = new GLCuboid(new GLVertex(0, 0, 0), 0.5f);
		cuboid.setColor(new GLVertex(1,0,0));
		rect = new GLRectangle(new GLVertex(0, 0, 0), 0.3f, 0.3f);
		rect.setColor(new GLVertex(1,1,1));
		
		scene = new GLCustomComplexObject();
		scene.add(cuboid);
		scene.add(rect);

	}
	
	@Override
	public void beginGL(GL2 gl) {
		
		scene.glTranslateNow(2, 0, 0);
		scene.glRotateNow(0, 0, 45);
		
		rect.glScaleNow((float)Math.random(), (float)Math.random(), (float)Math.random());
		rect.glScaleNow(5, 5, 5);
		rect.glTranslateNow(1, 0, 0);
		scene.draw(gl);
		
	}
}
