package modelTest;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLCamera;
import falk_guenther_meier.model.animation.PositionDirectionAnimation;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.interpolation.ConstantInterpolation;
import falk_guenther_meier.model.interpolation.LinearInterpolation;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.world.JoglApp;

/**
 * Testet Animationen f�r die Kamera aus.
 * @author Johannes
 */
public class CameraDriveComplexTest extends JoglApp {
	private GLCuboid cuboid;
	private GLComplexObject openRoom;

	public CameraDriveComplexTest(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}
	
	public static void main(String[] args) {
		new CameraDriveComplexTest("CameraDriveTest: komplex, mit neuen Optionen!", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		cuboid = new GLCuboid(new GLVertex(), 0.5f);
		openRoom = new GLOpenRoom(new GLVertex(0.0f, -6.0f, 0.0f), 10f, 10f, 3f, 0.5f);

		GLCamera camera = this.getKonfiguration().getCamera();
		camera.setPosition(new GLVertex(0.0f, 2.0f, 0.0f));
		camera.setDirection(new GLVertex(1.1f, 0.0f, 0.0f));


		PositionDirectionAnimation ani = new PositionDirectionAnimation(this.getKonfiguration().getTime(), camera);

//		BezierInterpolation b = new BezierInterpolation(
//				pos.clone(), new GLVertex(14.0f, 0.0f, 0.0f),
//				new GLVertex(-1.0f, 10.0f, 0.0f), new GLVertex(1.0f, 3.0f, 0.0f), new GLVertex(4.0f, -12.0f, 0.0f));
		LinearInterpolation lin = new LinearInterpolation(new GLVertex(-4.0f, 2.0f, 0.0f), new GLVertex(8.0f, 0.0f, 0.0f), false);
		ConstantInterpolation cnst = new ConstantInterpolation(new GLVertex(0.0f, -0.1f, -1.0f));
		ani.addInterpolationBothVariable(lin, cnst, 10.0f);

		this.addAnimation(ani);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		openRoom.draw(gl);
		cuboid.draw(gl);
	}
}
