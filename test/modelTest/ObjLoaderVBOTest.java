package modelTest;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.ObjLoader;

public class ObjLoaderVBOTest extends JoglApp {
	
	GLCustomComplexObject c;
	ByteBuffer vertices;
	ByteBuffer normals;
	
	public ObjLoaderVBOTest(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);

		start();
	}
	
	public static void main(String[] args) {
		new ObjLoaderVBOTest("ObjLoaderVBOTest", 800, 600);
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		
		c = ObjLoader.objFile2ComplexObj(FileLoader.getFileFromFilesFolder("obj/bunny.obj"));
		
		int VERTICES_COUNT = 3*c.getObjects().size(); // 3 Vertices per Face * nFaces 
		vertices = ByteBuffer.allocateDirect(VERTICES_COUNT*3*4); // nVertices * floatsPerVertex * floatSize
		vertices.order(ByteOrder.nativeOrder());
		normals = ByteBuffer.allocateDirect(VERTICES_COUNT*3*4);
		normals.order(ByteOrder.nativeOrder());
		
		FloatBuffer verticesFB = vertices.asFloatBuffer();
		FloatBuffer normalsFB = normals.asFloatBuffer();
		
		for (GLObject object: c.getObjects()) {
			for(GLVertex vertex: object.getNodes()) {
				verticesFB.put(vertex.getValues(), 0, 3);
				GLVertex normal = vertex.getNormal();
				if (normal != null) {
					normalsFB.put(normal.getValues(), 0, 3);
				}
			}
		}
		
		//c.translate(0.5f, 0.5f, -0.5f);
	}
	
	
	@Override
	public void beginGL(GL2 gl) {

		gl.glPushMatrix();
		gl.glScalef(50f, 50f, 50f);
		gl.glColor3f(0.5f, 0.5f, 0.5f);
		gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL2.GL_NORMAL_ARRAY);
		
		gl.glVertexPointer(3,GL2.GL_FLOAT,0,vertices);
		gl.glNormalPointer(GL2.GL_FLOAT,0,normals);
		gl.glDrawArrays(GL2.GL_TRIANGLES,0,c.getObjects().size()*3);
		
		gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL2.GL_NORMAL_ARRAY);
		
		
		gl.glPopMatrix();
	}

}
