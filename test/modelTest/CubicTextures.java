package modelTest;

import java.awt.Color;
import java.io.File;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;

public class CubicTextures extends JoglApp {
	private GLCuboid cube;
	private GLRectangle rect;

	public CubicTextures(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);

		File textureFile = FileLoader.getFileFromFilesFolder("texture/mauerwerk_small_label.JPG", true);	
//		File textureFile = FileLoader.getFileFromFilesFolder("texture/gras_small.JPG", true);	
		GLTexture2D texture = new GLTexture2D(textureFile);

		rect = new GLRectangle(new GLVertex(-2, 0, 0), 2f);
//		rect.setColor(new GLVertex(0, 0, 1.0f));
		rect.setColor(Color.WHITE);
		rect.setTexture(texture);

		cube = new GLCuboid(new GLVertex(2,0,0), 2f);
		cube.setColor(Color.WHITE);
		cube.setTexture(texture);

		start();
	}

	public static void main(String[] args) {
		new CubicTextures("Aufgabe 18", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		cube.draw(gl);
		rect.draw(gl);
	}	
}
