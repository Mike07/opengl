package modelTest;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.world.JoglApp;

/**
 * Licht mit Objekt.
 * @author Michael
 */
public class LightObjectTest extends JoglApp {
	private GLCuboid cuboid;
	private GLCuboid cuboid2;

	public LightObjectTest(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}
	
	public static void main(String[] args) {
		new LightObjectTest("LightObjectTest", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		cuboid = new GLCuboid(new GLVertex(), 0.5f);
		cuboid2 = new GLCuboid(new GLVertex(), 1f);
		this.getKonfiguration().getLight().setLightObject(cuboid);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		this.getKonfiguration().getLight().translate(0.05f, -0.05f, 0.05f);
		cuboid2.draw(gl);
	}
}
