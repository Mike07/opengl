package modelTest;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLTriangle;
import falk_guenther_meier.model.objects.GLVertexBufferObject;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.ObjLoader;

public class ObjLoaderVBONeuTest extends JoglApp {
	
	GLCustomComplexObject vbos;
	GLVertexBufferObject propellerVBO;
	
	
	public ObjLoaderVBONeuTest(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);

		start();
	}
	
	public static void main(String[] args) {
		new ObjLoaderVBONeuTest("ObjLoaderVBOTestNeu", 800, 600);
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		GLPointLight light = this.getKonfiguration().getLight();
		light.setPosition(new GLVertex(1,10,0));
		light.setAmbient(new GLVertex(0.45f, 0.45f, 0.45f, 1.0f));
		light.setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f, 1.0f));
		light.setSpecular(new GLVertex(0.2f, 0.2f, 0.2f, 1.0f));
		
		GLCustomComplexObject complexObj = ObjLoader.objFile2ComplexObj(FileLoader.getFileFromFilesFolder("obj/airacobra/p39.obj", true));
		GLCustomComplexObject propeller = new GLCustomComplexObject();
		for(GLObject subObject: complexObj.getObjects()) {
			GLComplexObject subComplexObj = (GLComplexObject)subObject;
			for (GLObject object: subComplexObj.getObjects().toArray(new GLObject[3])) {
				GLVertex[] objNodes = object.getNodes();
				float centerZ = (objNodes[0].getZ() + objNodes[1].getZ() + objNodes[2].getZ()) / 3f;
				if (object instanceof GLTriangle && centerZ < -2.65f) {
					subComplexObj.getObjects().remove(object);
					propeller.add(object);
				}
			}
			//subComplexObj.getObjects().removeAll(propeller.getObjects());
			propeller.setTexture(subObject.getTextureBindings().get(0).getTexture());
		}
		
		vbos = GLVertexBufferObject.convert2VBOs(complexObj);
		propellerVBO = new GLVertexBufferObject(GL2.GL_TRIANGLES, propeller);
		//vbos.setColor(new GLVertex(0.5f, 0.5f, 0.5f));
		
	}
	
	
	@Override
	public void beginGL(GL2 gl) {

		float scaleFaktor = 1f;
		//vbos.glScaleNow(scaleFaktor, scaleFaktor, scaleFaktor);
		//vbos.draw(gl);
		
		propellerVBO.draw(gl);
		
		
		GLCuboid cube = new GLCuboid(new GLVertex(), 1f);
		cube.draw(gl);

	}

}
