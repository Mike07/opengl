package modelTest;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.platform.GLObject;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.ObjLoader;

public class ObjLoaderTest extends JoglApp {
	
	GLCustomComplexObject c;
	String path = "../../src//files/obj/knight.obj";
	
	public ObjLoaderTest(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}
	
	public static void main(String[] args) {
		new ObjLoaderTest("ObjectLoaderTest", 800, 600);
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		c = ObjLoader.objFile2ComplexObj(FileLoader.getFileFromFilesFolder("obj/bunny.obj"));
		c.translate(0.5f, 0.5f, -0.5f);
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		super.display(drawable);

		GL2 gl = drawable.getGL().getGL2();
		// Tiefenpuffer leeren
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
		
		// die OpenGL-Anweisungen machen das Gleiche wie oben das Modell
//		gl.glPushMatrix();
//		gl.glTranslatef(0.5f, 0.5f, -0.5f);
//		gl.glRotatef(90, 1, 0, 0);
		gl.glScalef(0.05f, 0.05f, 0.05f);
		c.draw(gl);
		GLObject cube = new GLCuboid(new GLVertex(0,0,0), 1);
		cube.translate(1, 1, 1);
//		cube.draw(gl);
//		gl.glPopMatrix();
	}

}
