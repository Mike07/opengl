package modelTest;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.animation.PositionAnimation;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.interpolation.BezierInterpolation;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.world.JoglApp;

/**
 * Testet Animationen anhand eines Pfades aus.
 * Parameter k�nnen Objekte oder eine Kamera sein.
 * @author Johannes
 */
public class ObjectDriveBezierTest extends JoglApp {
	private GLCuboid cuboid;
	private GLComplexObject openRoom;


	public ObjectDriveBezierTest(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}
	
	public static void main(String[] args) {
		new ObjectDriveBezierTest("ObjectDriveTest", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		cuboid = new GLCuboid(new GLVertex(), 0.5f);
		openRoom = new GLOpenRoom(new GLVertex(-6.0f, 0.0f, 0.0f), 2f, 2f, 1f, 0.2f);
		this.getKonfiguration().getCamera().setPosition(new GLVertex(0.0f, 4.0f, 10.0f));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(0.0f, -4.0f, -6.0f));

		// Objekt-Animation
		GLVertex pos = openRoom.getPosition().clone();

		PositionAnimation ani = new PositionAnimation(this.getKonfiguration().getTime(), openRoom);

		BezierInterpolation b = new BezierInterpolation(
				pos.clone(), new GLVertex(14.0f, 0.0f, 0.0f), false,
				new GLVertex(-1.0f, 10.0f, 0.0f), new GLVertex(1.0f, 3.0f, 0.0f), new GLVertex(4.0f, -12.0f, 0.0f));
		ani.addInterpolation(b, 5.0f);

		ani.setLoop(true);

		this.addAnimation(ani);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		openRoom.draw(gl);
		cuboid.draw(gl);
	}
}
