package modelTest;

import java.io.File;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.buildings.GLHouse;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.light.GLDirectionLight;
import falk_guenther_meier.model.light.GLSpotLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.textures.GLTexture2D;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.FileLoader;
import falk_guenther_meier.util.Utility;

/**
 * @author Michael
 */
public class GLHouseTest extends JoglApp {

	private GLComplexObject room;
	private GLHouse house;
	private File textureFile;

	public GLHouseTest(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);
		textureFile = FileLoader.getFileFromFilesFolder("texture/bretter.jpg");
		start();
	}
	
	public static void main(String[] args) {
		new GLHouseTest("GLHouseTest", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		this.getKonfiguration().getCamera().setPosition(new GLVertex(5.5f, 1.5f, 5.5f));
		this.getKonfiguration().getLight().setPosition(new GLVertex(0.0f, 2.0f, 2.0f));
		this.getKonfiguration().getLight().setUseColorMaterial(true);
		this.getKonfiguration().getLight().setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		this.getKonfiguration().getLight().setAttenuation(0.3f, 0.1f, 0.01f); // c, l, q
		
		GLSpotLight spot = new GLSpotLight(GL2.GL_LIGHT3, new GLVertex(2.0f, 2.0f, 2.0f),
		new GLVertex(-1.0f, -0.1f, 0.0f));
		spot.setSpotCutoff(20.0f);
		spot.setSpotExponent(0.9f);
		spot.setUseColorMaterial(true);
		spot.setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		spot.setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		spot.setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
//		spot.setLightOn(false);
		this.addLight(spot);
		
		GLDirectionLight direct = new GLDirectionLight(GL2.GL_LIGHT5, new GLVertex(1.0f, -1.0f, 0.0f));
		direct.setUseColorMaterial(true);
		direct.setAmbient(new GLVertex(0.5f, 0.5f, 0.5f));
		direct.setDiffuse(new GLVertex(0.5f, 0.5f, 0.5f));
		direct.setSpecular(new GLVertex(0.5f, 0.5f, 0.5f));
		this.addLight(direct);

		room = new GLOpenRoom(new GLVertex(), 20.0f, 15.0f, 2.5f, 0.04f);
		room.setColor(new GLVertex());
		room.setMaterial(new GLMaterial());
		room.rotate(0.0f, 45.0f, 0.0f);
		Utility.setRandomColor(room);
		
		house = new GLHouse(new GLVertex(), 2.0f, 1.0f, 2.0f, 1.0f);
		house.setMaterial(new GLMaterial());
		GLTexture2D texture = new GLTexture2D(textureFile);
		house.getFront().setTexture(texture);
		house.getLeft().setTexture(texture);
		house.getRight().setTexture(texture);
		house.getBack().setTexture(texture);
		// oben wird gar keine Textur angezeigt !?
		house.getAboveFront().setTexture(texture);
		house.getAboveBack().setTexture(texture);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		room.draw(gl);
		house.draw(gl);
	}
}
