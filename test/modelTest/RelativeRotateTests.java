package modelTest;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.world.JoglApp;

/**
 * Testet relative Drehungen aus.
 * @author Johannes
 */
public class RelativeRotateTests extends JoglApp {
	// GL2-Kontext
	private GL2 gl;
	GLCuboid cuboid;
	int count = 0;

	public RelativeRotateTests(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}
	
	public static void main(String[] args) {
		new RelativeRotateTests("RelativeTests", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		// Objekt initialisieren (mit initialem Zustand -> Position, Drehung)
		cuboid = new GLCuboid(new GLVertex(0, 0, 0), 0.5f);
		cuboid.translate(1.0f, 1.0f, 0.0f);
		cuboid.rotate(new GLVertex(0.0f, 1.0f, 0.0f), new GLVertex(0.0f, 1.0f, -1.0f), 1.0f); // funktioniert nicht !!
//		cuboid.rotate(new GLVertex(1.0f, 1.0f, 0.0f), 0.0f, 0.0f, 45.0f); // funktioniert
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		super.display(drawable);
		gl = drawable.getGL().getGL2();
		// Tiefenpuffer leeren
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);

		cuboid.draw(gl);
	}
}
