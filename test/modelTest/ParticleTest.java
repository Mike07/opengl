package modelTest;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.particle.WaterParticleSystem;
import falk_guenther_meier.model.world.JoglApp;

public class ParticleTest extends JoglApp {
	private WaterParticleSystem p;

	public ParticleTest(String name_value, int width, int height) {
		super(name_value, width, height);
	}

	public static void main(String[] args) {
		new ParticleTest("TimeIntervallTest", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		p = new WaterParticleSystem();
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		gl.glScalef(20.0f, 20.0f, 20.0f);
		p.beginGL(gl);
	}
}
