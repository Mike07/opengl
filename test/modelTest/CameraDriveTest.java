package modelTest;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.animation.PositionAnimation;
import falk_guenther_meier.model.interpolation.LinearInterpolation;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.world.JoglApp;

/**
 * Testet Kamerafahrten aus.
 * @author Johannes
 */
public class CameraDriveTest extends JoglApp {
	// GL2-Kontext
	private GL2 gl;
	private GLCuboid cuboid;

	public CameraDriveTest(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}
	
	public static void main(String[] args) {
		new CameraDriveTest("CameraDriveTest", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		cuboid = new GLCuboid(new GLVertex(0, 0, 0), 0.5f);
		cuboid.translate(1.0f, 1.0f, 0.0f);

		// neue Animation erstellen
		GLVertex pos = this.getKonfiguration().getCamera().getPosition().clone();
		PositionAnimation ani = new PositionAnimation(this.getKonfiguration().getTime(),
				this.getKonfiguration().getCamera());

		ani.addInterpolation(new LinearInterpolation(pos.clone(), new GLVertex(0.0f, 0.0f, -2.0f), false), 1.0f);
		ani.addInterpolation(new LinearInterpolation(ani.getActuallEndPoint(), new GLVertex(0.0f, 2.0f, 0.0f), false), 0.5f);
		ani.addInterpolation(new LinearInterpolation(ani.getActuallEndPoint(), new GLVertex(2.5f, 1.0f, 2.5f), false), 1.5f);

		System.out.println("Kamera-Animation: " + ani.toString());
		this.addAnimation(ani);
		System.out.println(ani);
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		super.display(drawable);
		gl = drawable.getGL().getGL2();
		// Tiefenpuffer leeren
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
		
		cuboid.draw(gl);
	}
}
