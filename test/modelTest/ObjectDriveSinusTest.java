package modelTest;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.animation.PositionAnimation;
import falk_guenther_meier.model.buildings.GLOpenRoom;
import falk_guenther_meier.model.interpolation.LinearInterpolation;
import falk_guenther_meier.model.interpolation.SinusInterpolation;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.platform.GLComplexObject;
import falk_guenther_meier.model.world.JoglApp;

/**
 * Testet Animationen anhand eines Pfades aus.
 * Parameter k�nnen Objekte oder eine Kamera sein.
 * @author Michael
 */
public class ObjectDriveSinusTest extends JoglApp {
	private GLCuboid cuboid;
	private GLComplexObject openRoom;

	public ObjectDriveSinusTest(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}
	
	public static void main(String[] args) {
		new ObjectDriveSinusTest("ObjectDriveTest", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		cuboid = new GLCuboid(new GLVertex(), 0.5f);
		openRoom = new GLOpenRoom(new GLVertex(), 2f, 2f, 1f, 0.2f);

		// Objekt-Animation
		GLVertex pos = openRoom.getPosition().clone();
		PositionAnimation ani = new PositionAnimation(this.getKonfiguration().getTime(), openRoom);

		ani.addInterpolation(new LinearInterpolation(pos.clone(), new GLVertex(0.0f, 0.0f, -2.0f), false), 1f);
		ani.addInterpolation(new LinearInterpolation(ani.getActuallEndPoint(), new GLVertex(0.0f, 2.0f, 0.0f), false), 0.5f);
		ani.addInterpolation(new LinearInterpolation(ani.getActuallEndPoint(), new GLVertex(2.5f, 1.0f, 2.5f), false), 1.5f);
		ani.addInterpolation(new SinusInterpolation(ani.getActuallEndPoint(), new GLVertex(-12.0f, 0.0f, -12.0f), false,
				new GLVertex(0.0f, 1.0f, 0.0f), 8), 8.0f);

		// Kamera-Animation
		GLVertex posCam = this.getKonfiguration().getCamera().getPosition().clone();
		PositionAnimation aniCam = new PositionAnimation(this.getKonfiguration().getTime(),
				this.getKonfiguration().getCamera());

		aniCam.addInterpolation(new LinearInterpolation(posCam.clone(), new GLVertex(0.0f, 2.0f, -1.0f), false), 1f);
		aniCam.addInterpolation(new LinearInterpolation(aniCam.getActuallEndPoint(), new GLVertex(1.0f, 2.0f, 2.0f), false), 1f);
		aniCam.addInterpolation(new LinearInterpolation(aniCam.getActuallEndPoint(), new GLVertex(3.7f, 2f, 3.7f), false), 1.5f);

		System.out.println("Objekt-Animation: " + ani.toString());
		System.out.println("Kamera-Animation: " + aniCam.toString());
		this.addAnimation(ani);
		this.addAnimation(aniCam);
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		openRoom.draw(gl);
		cuboid.draw(gl);
	}
}
