package modelTest;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.observers.TimeIntervallObservable;
import falk_guenther_meier.model.observers.TimeIntervallObserver;
import falk_guenther_meier.model.time.TimeIntervallSender;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.Utility;

public class TimeIntervallTest extends JoglApp implements TimeIntervallObserver {
	private float time;

	public TimeIntervallTest(String name_value, int width, int height) {
		super(name_value, width, height);
	}

	public static void main(String[] args) {
		new TimeIntervallTest("TimeIntervallTest", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		TimeIntervallSender sender = new TimeIntervallSender(this.getKonfiguration().getTime(), 0.5f);
		sender.addTimeIntervallObserver(this);
	}

	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		Utility.drawText(gl, this.getKonfiguration().getTextFont(), this.getKonfiguration().getTextColor(),
				Float.toString(time) + " Zeit",
				gl.getContext().getGLDrawable().getWidth() - 95, gl.getContext().getGLDrawable().getHeight() - 70);
	}

	@Override
	public void updateTimeIntervall(TimeIntervallObservable changed) {
		this.time = changed.getTime();
	}
}
