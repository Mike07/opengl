package modelTest;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.observers.TranslateObservable;
import falk_guenther_meier.model.world.JoglApp;

/**
 * Testet Observer ...
 * @author Johannes
 */
public class ObserverTest extends JoglApp {
	// GL2-Kontext
	private GL2 gl;
	GLCuboid cuboid;
	GLCuboid second;
	int count = 0;

	public ObserverTest(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}
	
	public static void main(String[] args) {
		new ObserverTest("ObserverTest", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		// Objekt initialisieren (mit initialem Zustand -> Position, Drehung)
		cuboid = new GLCuboid(new GLVertex(0, 0, 0), 0.5f);
		cuboid.rotate(45, 0, 0);
		cuboid.translate(0.1f, 0.1f, 0.1f);

		second = new GLCuboid(new GLVertex(0, 0, 0), 0.5f) {
			@Override
			public void updateTranslate(TranslateObservable changed, float x, float y, float z) {
				super.updateTranslate(changed, x, y, z);
				this.translate(x, 2*y, z);
			}
		};
		second.rotate(45, 0, 0);
		second.translate(0.1f, 1.1f, 0.1f);

		cuboid.addTranslateObserver(second);
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		super.display(drawable);
		gl = drawable.getGL().getGL2();
		// Tiefenpuffer leeren
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);

		// Positionsveränderung zur Laufzeit (-> einfache Animation)
		if (count > 0 && count < 51) {
			cuboid.translate(0.01f, 0.01f, 0.01f);
		} else {
			cuboid.translate(-0.01f, -0.01f, -0.01f);
		}
		cuboid.draw(gl);
		
		count = (count + 1) % 101;

		second.draw(gl);
	}
}
