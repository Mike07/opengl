package modelTest;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLPolygon;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.objects.GLTriangle;
import falk_guenther_meier.model.world.JoglApp;

public class CameraTest extends JoglApp {
	public CameraTest(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}
	
	public static void main(String[] args) {
		new CameraTest("ObjektTests", 800, 600);
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		super.display(drawable);

		GL2 gl = drawable.getGL().getGL2();
		// Tiefenpuffer leeren
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);

		gl.glPushMatrix();


		gl.glScalef(0.1f, 0.1f, 0.1f);

		GLU glu = new GLU();
		// eye, center, up
		GLVertex eye = new GLVertex(1.0f, 0.0f, -1.0f);
		GLVertex center = new GLVertex(0.0f, 0.0f, 0.0f);
		GLVertex up = new GLVertex(0.0f, 1.0f, 0.0f);
		glu.gluLookAt(eye.getX(), eye.getY(), eye.getZ(), center.getX(), center.getY(), center.getZ(), up.getX(), up.getY(), up.getZ());


		GLTriangle triangle = this.createTriangle(2.0f);
		triangle.rotate(90.0f, 0.0f, 0.0f);
		triangle.scale(2.0f, 2.0f, 2.0f);
		triangle.draw(gl);

		GLRectangle rectangle = this.createRectangle(2.0f);
		rectangle.translate(2.0f, 2.0f, 0.0f);
		rectangle.draw(gl);

		GLPolygon polygon = this.createPolygon(1.0f);
		polygon.translate(4.0f, 4.0f, 0.0f);
		polygon.draw(gl);

		GLCuboid cube = this.createCuboid(1.0f);
		cube.scale(2.0f, 2.0f, 2.0f);
		cube.rotate(45.0f, 0.0f, 0.0f);
		cube.translate(8.0f, 8.0f, 0.0f);
		cube.draw(gl);

		gl.glPopMatrix();
//		gl.glFlush();
	}	

	private GLTriangle createTriangle(float width) {
		GLVertex a = new GLVertex(0.0f, 0.0f, 0.0f);
		GLVertex b = new GLVertex(width, 0.0f, 0.0f);
		GLVertex c = new GLVertex(width, width, 0.0f);
		return new GLTriangle(a, b, c, true);
	}

	private GLRectangle createRectangle(float width) {
		float w = width / 2;
		GLVertex a = new GLVertex(-w, -w, 0.0f);
		GLVertex b = new GLVertex(w, -w, 0.0f);
		GLVertex c = new GLVertex(w, w, 0.0f);
		GLVertex d = new GLVertex(-w, w, 0.0f);
		return new GLRectangle(a, b, c, d, true);
	}

	private GLPolygon createPolygon(float unit) {
		float u = unit;
		GLVertex a = new GLVertex(-u, u, 0.0f);
		GLVertex b = new GLVertex(-2*u, -u, 0.0f);
		GLVertex c = new GLVertex(0.0f, 0.0f, 0.0f);
		GLVertex d = new GLVertex(2*u, -u, 0.0f);
		GLVertex e = new GLVertex(u, u, 0.0f);
		return new GLPolygon(false, a, b, c, d, e);
	}

	private GLCuboid createCuboid(float width) {
		GLVertex a = new GLVertex(0.0f, 0.0f, 0.0f);
		return new GLCuboid(a, width, 2*width, 3*width);
	}
}
