package modelTest;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.GLMaterial;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.world.JoglApp;

/**
 * Demonstriert eine Möglichkeit, wie man mit dem neuen Modell programmieren kann.
 * @author Michi
 */
public class ModelPreview extends JoglApp {
	// GL2-Kontext
	private GLCuboid cuboid;
	private int count = 0;

	public ModelPreview(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}
	
	public static void main(String[] args) {
		new ModelPreview("ModelPreview", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		// Objekt initialisieren (mit initialem Zustand -> Position, Drehung)
		cuboid = new GLCuboid(new GLVertex(0, 0, 0), 0.5f);
		cuboid.setMaterial(GLMaterial.PEARL);
		cuboid.rotate(45, 0, 0);
		cuboid.translate(0.1f, 0.1f, 0.1f);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		// Positionsveränderung zur Laufzeit (-> einfache Animation)
		if (count > 0 && count < 51) {
			cuboid.translate(0.01f, 0.01f, 0.01f);
		} else {
			cuboid.translate(-0.01f, -0.01f, -0.01f);
		}
		cuboid.draw(gl);
		
		count = (count + 1) % 101;
	}	

}
