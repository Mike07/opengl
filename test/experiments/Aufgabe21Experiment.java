
package experiments;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.observers.TranslateObservable;
import falk_guenther_meier.model.platform.GLCustomComplexObject;
import falk_guenther_meier.model.world.JoglApp;

/**
 * 
 * @author Hanno
 */
public class Aufgabe21Experiment extends JoglApp {
    // GL2-Kontext
    private GL2 gl;
    GLCuboid cuboid;
    GLCuboid second;
    GLCustomComplexObject cuboidCollection;

    int count = 0;

    public Aufgabe21Experiment(String Name_value, int breite, int hoehe) {
        super(Name_value, breite, hoehe, false);
    }

    public static void main(String[] args) {
        new Aufgabe21Experiment("Aufgabe 21", 800, 600);
    }

    @Override
    public synchronized void init(GLAutoDrawable drawable) {
        super.init(drawable);

        cuboid = new GLCuboid(new GLVertex(0, 0, 0), 0.5f);
        cuboid.rotate(45, 0, 0);
        cuboid.translate(0.1f, 0.1f, 0.1f);

        second = new GLCuboid(new GLVertex(0, 0, 0), 0.5f) {
            @Override
            public void updateTranslate(TranslateObservable changed, float x, float y, float z) {
                super.updateTranslate(changed, x, y, z);
                this.translate(x, 2*y, z);
            }
        };
        second.rotate(45, 0, 0);
        second.translate(0.1f, 1.1f, 0.1f);

        cuboid.addTranslateObserver(second);

        cuboidCollection = new GLCustomComplexObject();
        cuboidCollection.add(cuboid);
        cuboidCollection.add(second);
        cuboidCollection.setReferencePoint(new GLVertex());
        cuboidCollection.setColor(new GLVertex(0.8f, 0.6f, 0.7f));
        start();
    }

    @Override
    public synchronized void display(GLAutoDrawable drawable) {
        super.display(drawable);
        gl = drawable.getGL().getGL2();

        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glEnable(GL2.GL_CULL_FACE);
        gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);

        gl.glEnable(GL2.GL_LIGHTING);
        gl.glEnable(GL2.GL_LIGHT0);

        gl.glEnable(GL2.GL_COLOR_MATERIAL);
        gl.glEnable(GL2.GL_NORMALIZE);
        gl.glShadeModel(GL2.GL_SMOOTH);

        // Position
        float[] position = {100.0f, 100.0f, 0.0f, 0.0f}; // w=0 bedeutet, das Licht wird hier nur als Richtungsvektor angegeben
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, position, 0);

        // globales ambientes Licht
        float[] gAmbient = {0.0f, 0.0f, 0.0f, 1.0f}; // w=1 Standard
        gl.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, gAmbient, 0);

        // ambientes Licht
        float[] ambient = {1.0f, 1.0f, 1.0f, 1.0f};
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, ambient, 0);
        // diffuses Licht
        float[] diffus = {1.0f, 1.0f, 1.0f, 1.0f};
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, diffus, 1);
        // spekulares Licht
        float[] specular = {1.0f, 1.0f, 1.0f, 1.0f};
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, specular, 1);
        gl.glLightf(GL2.GL_LIGHT0 , GL2.GL_CONSTANT_ATTENUATION, 0.0f);

        if (count > 0 && count < 51) {
            cuboid.translate(0.01f, 0.01f, 0.01f);
        } else {
            cuboid.translate(-0.01f, -0.01f, -0.01f);
        }

        count = (count + 1) % 101;

        cuboidCollection.rotate(0, 0, 2f);
        cuboidCollection.draw(gl);

        gl.glDisable(GL2.GL_LIGHTING);
        gl.glDisable(GL2.GL_LIGHT0);

    }
}
