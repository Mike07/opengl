package experiments;

import falk_guenther_meier.model.math.GLVertex;

public class PassByValueReference {
	public static void main(String[] args) {
		GLVertex v = new GLVertex(1, 2, 3, 4);
		System.out.println(v);
		float[] help = v.getValues();
		help[0] = 20;
		System.out.println(v);
		/*
		 * Erkenntnis:
		 * Arrays werden hier auch Pass-By-Reference �bergeben!
		 */
//		float result = (float) Math.sin(Math.toRadians(180));
		float g = (float) (180 * Math.PI / 180);
		float cos = (float) (Math.cos(g));
//		float sin = (float) (Math.sin(g));
		System.out.println(Math.toDegrees(Math.acos(cos)));
		System.out.println(GLVertex.getAngleBetween(new GLVertex(1.0f, 0.0f, 0.0f), new GLVertex(0.0f, 1.0f, 0.0f)));
	}
}
