package experiments;

import static com.bulletphysics.demos.opengl.IGL.GL_COLOR_BUFFER_BIT;
import static com.bulletphysics.demos.opengl.IGL.GL_DEPTH_BUFFER_BIT;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import org.lwjgl.LWJGLException;

import com.bulletphysics.collision.broadphase.AxisSweep3_32;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.demos.opengl.DemoApplication;
import com.bulletphysics.demos.opengl.IGL;
import com.bulletphysics.demos.opengl.LWJGL;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;


public class JBulletTestLWJGL extends DemoApplication {
	
	private LinkedList<RigidBody> bodies = new LinkedList<RigidBody>();
	
	public JBulletTestLWJGL(IGL gl) {
		super(gl);
	}

	public static void main(final String[] args) throws LWJGLException {
		final JBulletTestLWJGL ccdDemo = new JBulletTestLWJGL(LWJGL.getGL());
		ccdDemo.initPhysics();
//		ccdDemo.getDynamicsWorld().setDebugDrawer(new GLDebugDrawer(LWJGL.getGL()));

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Robot robot = new Robot();
					robot.delay(300);
					robot.keyPress(KeyEvent.VK_H);
					robot.keyRelease(KeyEvent.VK_H);
				} catch (AWTException e) {
					e.printStackTrace();
				}
			}
		}).start();
		try {
			LWJGL.main(args, 800, 600, "Bullet Physics Demo. http://bullet.sf.net", ccdDemo);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void clientMoveAndDisplay() {
		gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// simple dynamics world doesn't handle fixed-time-stepping
		float ms = getDeltaTimeMicroseconds();

		// step the simulation
		if (dynamicsWorld != null) {
			dynamicsWorld.stepSimulation(ms / 1000000f);
			// optional but useful: debug drawing
			dynamicsWorld.debugDrawWorld();
		}

		renderme();

		//glFlush();
		//glutSwapBuffers();
	}

	@Override
	public void displayCallback() {
		gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		renderme();

		// optional but useful: debug drawing to detect problems
		if (dynamicsWorld != null) {
			dynamicsWorld.debugDrawWorld();
		}

		//glFlush();
		//glutSwapBuffers();
	}

	public void initPhysics() {
		// collision configuration contains default setup for memory, collision
		// setup. Advanced users can create their own configuration.
		CollisionConfiguration collisionConfiguration = new DefaultCollisionConfiguration();

		// use the default collision dispatcher. For parallel processing you
		// can use a diffent dispatcher (see Extras/BulletMultiThreaded)
		CollisionDispatcher dispatcher = new CollisionDispatcher(collisionConfiguration);

		// the maximum size of the collision world. Make sure objects stay
		// within these boundaries
		// Don't make the world AABB size too large, it will harm simulation
		// quality and performance
		Vector3f worldAabbMin = new Vector3f(-10000, -10000, -10000);
		Vector3f worldAabbMax = new Vector3f(10000, 10000, 10000);
		int maxProxies = 16024;
		AxisSweep3_32 overlappingPairCache =
				new AxisSweep3_32(worldAabbMin, worldAabbMax, maxProxies);
		//BroadphaseInterface overlappingPairCache = new SimpleBroadphase(
		//		maxProxies);

		// the default constraint solver. For parallel processing you can use a
		// different solver (see Extras/BulletMultiThreaded)
		SequentialImpulseConstraintSolver solver = new SequentialImpulseConstraintSolver();

		dynamicsWorld = new DiscreteDynamicsWorld(
				dispatcher, overlappingPairCache, solver,
				collisionConfiguration);
		dynamicsWorld.setGravity(new Vector3f(0, -10, 0));
		
		initObjects();
	}

	private void initObjects() {
		// create a ground body
		CollisionShape groundShape = new BoxShape(new Vector3f(50.f, 50.f, 50.f));
		Vector3f pos = new Vector3f(0.f, -57.f, 0.f);
		this.createAndAddRigidBody(pos, groundShape, 0f, 0f, 0f);
		
		CollisionShape wallShape = new BoxShape(new Vector3f(50.f, 10.f, 1.f));
		pos = new Vector3f(0.f, 5.f, 50.f);
		this.createAndAddRigidBody(pos, wallShape, 0f, 0f, 0f);
		
		// create dynamic rigidbodies
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				CollisionShape colShape = new BoxShape(new Vector3f(1f, 1f, 1f));
				Vector3f pos = new Vector3f(2, 20, 0);
				RigidBody body = createRigidBody(pos, colShape, 1f, 1.5f, 1.5f);
				bodies.addFirst(body);
				dynamicsWorld.addRigidBody(body);
			}
		}, new Date(1), 600);
	}
	
	private RigidBody createRigidBody(Vector3f pos, CollisionShape shape, float mass, float restitution, float friction) {
		Transform startTransform = new Transform();
		startTransform.setIdentity();
		startTransform.origin.set(pos);

		Vector3f localInertia = new Vector3f(0f, 0f, 0f);
		if (mass != 0.0f) {
			shape.calculateLocalInertia(mass, localInertia);
		}

		// using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects

		DefaultMotionState myMotionState = new DefaultMotionState(startTransform);		
		RigidBodyConstructionInfo cInfo = new RigidBodyConstructionInfo(mass, myMotionState,
				shape, localInertia);
		
		// Sto�zahl, Restitutionskoeffizient:
		// 0: vollkommen plastischer Sto� (Bewegungsenergie nimmt ab, z.B. wegen W�rme),
		// 1: vollkommen elastischer Sto� (Bewegungsenergie bleibt konstant)
		cInfo.restitution = restitution;
		// Reibung
		cInfo.friction = friction;

		RigidBody body = new RigidBody(cInfo);
		
		Transform worldTrans = body.getWorldTransform(new Transform());
		worldTrans.origin.set(pos);
		worldTrans.setRotation(new Quat4f(0f, 0f, 0f, 1f));
		body.setWorldTransform(worldTrans);
		return body;
	}
	
	private void createAndAddRigidBody(Vector3f pos, CollisionShape shape, float mass, float restitution, float friction) {
		RigidBody body = this.createRigidBody(pos, shape, mass, restitution, friction);
		dynamicsWorld.addRigidBody(body);
	}
	
}
