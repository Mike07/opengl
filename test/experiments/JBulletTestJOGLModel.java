package experiments;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import physic.GLCuboidPhysic;
import physic.PhysicAnimation;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.world.JoglApp;

public class JBulletTestJOGLModel extends JoglApp {	
	private GLCuboidPhysic ground;
	private GLCuboidPhysic box;
	private GLCuboidPhysic box2;
	private PhysicAnimation ani;
	
	public JBulletTestJOGLModel(String name_value, int width, int height) {
		super(name_value, width, height);
	}

	public static void main(String[] args) {
		new JBulletTestJOGLModel("JBullet", 800, 600);		
	}
	
	private void initPhysics() {
		this.ani = new PhysicAnimation(this.getKonfiguration().getTime());
		this.addAnimation(ani);

		this.ground = new GLCuboidPhysic(new GLVertex(0.0f, -10.0f, 0.0f), 20f, 11f, 20f, 0.0f);
		this.ani.addPhysicable(this.ground);

		this.box = new GLCuboidPhysic(new GLVertex(2.0f, 10.0f, 0.0f), 1f, 1f, 1f, 1.0f);
		this.ani.addPhysicable(this.box);

		this.box2 = new GLCuboidPhysic(new GLVertex(1.8f, 16.0f, 0.0f), 1f, 1f, 1f, 1.0f, 1.0f, 0.5f);
		this.ani.addPhysicable(this.box2);
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		this.getKonfiguration().getCamera().setPosition(new GLVertex(17, 0, 17));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(-15, -3, -15));
		this.initPhysics();
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);

		ground.draw(gl);
		box.draw(gl);
		box2.draw(gl);
	}
}
