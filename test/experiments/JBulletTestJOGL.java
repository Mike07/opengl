package experiments;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

import com.bulletphysics.collision.broadphase.AxisSweep3;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;
import com.bulletphysics.linearmath.Clock;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;

import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLSphere;
import falk_guenther_meier.model.world.JoglApp;

public class JBulletTestJOGL extends JoglApp {	
	private GLSphere sphere;
	private GLCuboid ground;
	private GLCuboid box;
	private GLCuboid box2;
	private static RigidBody body;
	private static RigidBody body2;
	private Clock clock = new Clock();
	private DiscreteDynamicsWorld dynamicsWorld;
	
	public JBulletTestJOGL(String name_value, int width, int height) {
		super(name_value, width, height);
		initPhysics();
	}

	public static void main(String[] args) {
		new JBulletTestJOGL("JBullet", 800, 600);		
	}
	
	private void initPhysics() {
		// collision configuration contains default setup for memory, collision
		// setup. Advanced users can create their own configuration.
		CollisionConfiguration collisionConfiguration = new DefaultCollisionConfiguration();

		// use the default collision dispatcher. For parallel processing you
		// can use a diffent dispatcher (see Extras/BulletMultiThreaded)
		CollisionDispatcher dispatcher = new CollisionDispatcher(
				collisionConfiguration);

		// the maximum size of the collision world. Make sure objects stay
		// within these boundaries
		// Don't make the world AABB size too large, it will harm simulation
		// quality and performance
		Vector3f worldAabbMin = new Vector3f(-10000, -10000, -10000);
		Vector3f worldAabbMax = new Vector3f(10000, 10000, 10000);
		int maxProxies = 1024;
		AxisSweep3 overlappingPairCache =
				new AxisSweep3(worldAabbMin, worldAabbMax, maxProxies);
		//BroadphaseInterface overlappingPairCache = new SimpleBroadphase(
		//		maxProxies);

		// the default constraint solver. For parallel processing you can use a
		// different solver (see Extras/BulletMultiThreaded)
		SequentialImpulseConstraintSolver solver = new SequentialImpulseConstraintSolver();

		dynamicsWorld = new DiscreteDynamicsWorld(
				dispatcher, overlappingPairCache, solver,
				collisionConfiguration);

		dynamicsWorld.setGravity(new Vector3f(0, -9.81f, 0));

// --------------------
		// create a few basic rigid bodies
		CollisionShape groundShape = new BoxShape(new Vector3f(10f, 5.5f, 10f));

		// keep track of the shapes, we release memory at exit.
		// make sure to re-use collision shapes among rigid bodies whenever
		// possible!
//		ObjectArrayList<CollisionShape> collisionShapes = new ObjectArrayList<CollisionShape>();
//		// JM: wof�r wird das ben�tigt ?!
//		collisionShapes.add(groundShape);

		Transform groundTransform = new Transform();
		groundTransform.setIdentity();
		groundTransform.origin.set(new Vector3f(0.f, -10f, 0.f));

		float mass = 0f;

		// rigidbody is dynamic if and only if mass is non zero,
		// otherwise static
		boolean isDynamic = (mass != 0f);

		Vector3f localInertia = new Vector3f(0, 0, 0);
		if (isDynamic) {
			groundShape.calculateLocalInertia(mass, localInertia);
		}

		// using motionstate is recommended, it provides interpolation
		// capabilities, and only synchronizes 'active' objects
		DefaultMotionState myMotionState = new DefaultMotionState(groundTransform);
		RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(
				mass, myMotionState, groundShape, localInertia);
		RigidBody ground = new RigidBody(rbInfo);

		// add the body to the dynamics world
		dynamicsWorld.addRigidBody(ground);

//-----------------------
		// create a dynamic rigidbody

		// btCollisionShape* colShape = new
		// btBoxShape(btVector3(1,1,1));
		CollisionShape colShape = new BoxShape(new Vector3f(0.5f, 0.5f, 0.5f));
//			collisionShapes.add(colShape);

		// Create Dynamic Objects
		Transform startTransform = new Transform();
		startTransform.setIdentity();

		mass = 1f;

		// rigidbody is dynamic if and only if mass is non zero,
		// otherwise static
		isDynamic = (mass != 0f);

		localInertia = new Vector3f(0, 0, 0);
		if (isDynamic) {
			colShape.calculateLocalInertia(mass, localInertia);
		}

		startTransform.origin.set(new Vector3f(2, 10, 0));

		// using motionstate is recommended, it provides
		// interpolation capabilities, and only synchronizes
		// 'active' objects
		myMotionState = new DefaultMotionState(startTransform);

		rbInfo = new RigidBodyConstructionInfo(
				mass, myMotionState, colShape, localInertia);
//		System.out.println("Rest.: " + rbInfo.restitution);
//		System.out.println("Fric.: " + rbInfo.friction);
		body = new RigidBody(rbInfo);

		dynamicsWorld.addRigidBody(body);
//-----------------------
		colShape = new BoxShape(new Vector3f(0.5f, 0.5f, 0.5f));
//			collisionShapes.add(colShape);

		// Create Dynamic Objects
		startTransform = new Transform();
		startTransform.setIdentity();

		mass = 1f;

		// rigidbody is dynamic if and only if mass is non zero,
		// otherwise static
		isDynamic = (mass != 0f);

		// Inertia = Tr�gheitsmoment
		// K�rperabh�ngig
		// Kugel: J = 2/5 * m * r^2
		localInertia = new Vector3f(0, 0, 0);
		if (isDynamic) {
			colShape.calculateLocalInertia(mass, localInertia);
		}

		startTransform.origin.set(new Vector3f(1.8f, 16, 0));

		// using motionstate is recommended, it provides
		// interpolation capabilities, and only synchronizes
		// 'active' objects
		myMotionState = new DefaultMotionState(startTransform);

		rbInfo = new RigidBodyConstructionInfo(
				mass, myMotionState, colShape, localInertia);
		rbInfo.restitution = 1.0f; // Sto�zahl, Restitutionskoeffizient:  0: vollkommen plastischer Sto� (Bewegungsenergie nimmt ab, z.B. wegen W�rme), 1: vollkommen elastischer Sto� (Bewegungsenergie bleibt konstant)
		rbInfo.friction = 0.5f; // Reibung
		body2 = new RigidBody(rbInfo);
		dynamicsWorld.addRigidBody(body2);
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		this.getKonfiguration().getCamera().setPosition(new GLVertex(17, 0, 17));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(-15, -3, -15));
//		sphere = new GLSphere(new GLVertex(2, 10, 0), 1f, 5);
		box = new GLCuboid(new GLVertex(0, 0, 0), 1f);
		box2 = new GLCuboid(new GLVertex(0, 0, 0), 1f);
		ground = new GLCuboid(new GLVertex(0, -10f, 0), 20f, 11f, 20f);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		this.simulate();
//		sphere.draw(gl);

		Transform trans = new Transform();
		body.getMotionState().getWorldTransform(trans);
		
		Matrix4f matrix = new Matrix4f();
		trans.getMatrix(matrix);

		GLMatrix mat = this.convertMatrix(trans.basis);
		box.glTransformNow(mat);
		box.glTranslateNow(trans.origin.x, trans.origin.y, trans.origin.z);
		box.draw(gl);
		
		body2.getMotionState().getWorldTransform(trans);
		trans.getMatrix(matrix);
		mat = this.convertMatrix(trans.basis);
		box2.glTransformNow(mat);
		box2.glTranslateNow(trans.origin.x, trans.origin.y, trans.origin.z);
		box2.draw(gl);

		ground.draw(gl);
	}
	
	private void simulate() {
		// simple dynamics world doesn't handle fixed-time-stepping
//		float ms = this.getKonfiguration().getTime().getDiffToLastTime(); // JM: wieso funktioniert unser Zeitgeber nicht ?!
		float ms = this.getDeltaTimeMicroseconds();
		ms = ms / 1000000f;

		// step the simulation
		if (dynamicsWorld != null) {
			dynamicsWorld.stepSimulation(ms); // Angabe in Sekunden
//			System.out.println(ms);
//			System.out.println(this.getKonfiguration().getTime().getDiffToLastTime());
//			System.out.println();
			// optional but useful: debug drawing
			dynamicsWorld.debugDrawWorld();
		}
	}

	public float getDeltaTimeMicroseconds() {
		float dt = clock.getTimeMicroseconds();
		clock.reset();
		return dt;
	}

	private GLMatrix convertMatrix(Matrix4f mat) {
		GLMatrix result = new GLMatrix();
		result.set(mat.m00, mat.m10, mat.m20, mat.m30, mat.m01, mat.m11, mat.m21, mat.m31, mat.m02, mat.m12, mat.m22, mat.m32, mat.m03, mat.m13, mat.m23, mat.m33);
//		result.set(mat.m00, mat.m01, mat.m02, mat.m03, mat.m10, mat.m11, mat.m12, mat.m13, mat.m20, mat.m21, mat.m22, mat.m23, mat.m30, mat.m31, mat.m32, mat.m33);
		return result;
	}

	private GLMatrix convertMatrix(Matrix3f mat) {
		GLMatrix result = new GLMatrix();
		result.set(mat.m00, mat.m10, mat.m20, 0f, mat.m01, mat.m11, mat.m21, 0f, mat.m02, mat.m12, mat.m22, 0f, 0f, 0f, 0f, 1f);
//		result.set(mat.m00, mat.m01, mat.m02, 0f, mat.m10, mat.m11, mat.m12, 0f, mat.m20, mat.m21, mat.m22, 0f, 0f, 0f, 0f, 1f);
		return result;
	}	
}
