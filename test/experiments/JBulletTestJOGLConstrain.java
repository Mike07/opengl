package experiments;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

import com.bulletphysics.collision.broadphase.AxisSweep3;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;
import com.bulletphysics.dynamics.constraintsolver.SliderConstraint;
import com.bulletphysics.linearmath.Clock;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;

import falk_guenther_meier.model.math.GLMatrix;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.Utility;

public class JBulletTestJOGLConstrain extends JoglApp {	
	private GLCuboid ground;
	private GLCuboid box;
	private GLCuboid boxFirst;
	private GLCuboid boxSecond;
	private GLCuboid boxConnect;
	private static RigidBody body;
	private static RigidBody bodyFirst;
	private static RigidBody bodySecond;
	private static RigidBody bodyConnect;
	private Clock clock = new Clock();
	private DiscreteDynamicsWorld dynamicsWorld;
	
	public JBulletTestJOGLConstrain(String name_value, int width, int height) {
		super(name_value, width, height);
		initPhysics();
	}

	public static void main(String[] args) {
		new JBulletTestJOGLConstrain("JBullet", 800, 600);		
	}
	
	private void initPhysics() {
		CollisionConfiguration collisionConfiguration = new DefaultCollisionConfiguration();
		CollisionDispatcher dispatcher = new CollisionDispatcher(collisionConfiguration);

		Vector3f worldAabbMin = new Vector3f(-10000, -10000, -10000);
		Vector3f worldAabbMax = new Vector3f(10000, 10000, 10000);
		int maxProxies = 1024;
		AxisSweep3 overlappingPairCache =
				new AxisSweep3(worldAabbMin, worldAabbMax, maxProxies);
		SequentialImpulseConstraintSolver solver = new SequentialImpulseConstraintSolver();

		dynamicsWorld = new DiscreteDynamicsWorld(
				dispatcher, overlappingPairCache, solver,
				collisionConfiguration);
		dynamicsWorld.setGravity(new Vector3f(0, -9.81f, 0));

// --------------------
		CollisionShape groundShape = new BoxShape(new Vector3f(10f, 5.5f, 10f));
		Transform groundTransform = new Transform();
		groundTransform.setIdentity();
		groundTransform.origin.set(new Vector3f(0.f, -10f, 0.f));

		float mass = 0f;
		boolean isDynamic = (mass != 0f);

		Vector3f localInertia = new Vector3f(0, 0, 0);
		if (isDynamic) {
			groundShape.calculateLocalInertia(mass, localInertia);
		}

		DefaultMotionState myMotionState = new DefaultMotionState(groundTransform);
		RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(
				mass, myMotionState, groundShape, localInertia);
		RigidBody ground = new RigidBody(rbInfo);

		dynamicsWorld.addRigidBody(ground);

//-----------------------
		CollisionShape colShape = new BoxShape(new Vector3f(0.5f, 0.5f, 0.5f));
		Transform startTransform = new Transform();
		startTransform.setIdentity();

		mass = 1f;
		isDynamic = (mass != 0f);

		localInertia = new Vector3f(0, 0, 0);
		if (isDynamic) {
			colShape.calculateLocalInertia(mass, localInertia);
		}

		startTransform.origin.set(new Vector3f(2, 10, 0));
		myMotionState = new DefaultMotionState(startTransform);

		rbInfo = new RigidBodyConstructionInfo(
				mass, myMotionState, colShape, localInertia);
		body = new RigidBody(rbInfo);

		dynamicsWorld.addRigidBody(body);
//-----------------------
		colShape = new BoxShape(new Vector3f(0.5f, 0.5f, 0.5f));
		startTransform = new Transform();
		startTransform.setIdentity();

		mass = 1f;
		isDynamic = (mass != 0f);
		localInertia = new Vector3f(0, 0, 0);
		if (isDynamic) {
			colShape.calculateLocalInertia(mass, localInertia);
		}

		startTransform.origin.set(new Vector3f(-1.0f, 16, 0));
		myMotionState = new DefaultMotionState(startTransform);

		rbInfo = new RigidBodyConstructionInfo(
				mass, myMotionState, colShape, localInertia);
		rbInfo.restitution = 1.0f; // Sto�zahl, Restitutionskoeffizient:  0: vollkommen plastischer Sto� (Bewegungsenergie nimmt ab, z.B. wegen W�rme), 1: vollkommen elastischer Sto� (Bewegungsenergie bleibt konstant)
		rbInfo.friction = 1f; // Reibung
		bodyFirst = new RigidBody(rbInfo);
		dynamicsWorld.addRigidBody(bodyFirst);
//-----------------------
		colShape = new BoxShape(new Vector3f(0.5f, 0.5f, 0.5f));
		startTransform = new Transform();
		startTransform.setIdentity();

		mass = 1f;
		isDynamic = (mass != 0f);
		localInertia = new Vector3f(0, 0, 0);
		if (isDynamic) {
			colShape.calculateLocalInertia(mass, localInertia);
		}

		startTransform.origin.set(new Vector3f(1.0f, 16, 0));
		myMotionState = new DefaultMotionState(startTransform);

		rbInfo = new RigidBodyConstructionInfo(
				mass, myMotionState, colShape, localInertia);
		rbInfo.restitution = 1.0f; // Sto�zahl, Restitutionskoeffizient:  0: vollkommen plastischer Sto� (Bewegungsenergie nimmt ab, z.B. wegen W�rme), 1: vollkommen elastischer Sto� (Bewegungsenergie bleibt konstant)
		rbInfo.friction = 1f; // Reibung
		bodySecond = new RigidBody(rbInfo);
  		dynamicsWorld.addRigidBody(bodySecond);
//-----------------------
		colShape = new BoxShape(new Vector3f(2.0f, 0.1f, 0.1f));
		startTransform = new Transform();
		startTransform.setIdentity();

		mass = 1f;
		isDynamic = (mass != 0f);
		localInertia = new Vector3f(0, 0, 0);
		if (isDynamic) {
			colShape.calculateLocalInertia(mass, localInertia);
		}

		startTransform.origin.set(new Vector3f(0.0f, 16, 0));
		myMotionState = new DefaultMotionState(startTransform);

		rbInfo = new RigidBodyConstructionInfo(
				mass, myMotionState, colShape, localInertia);
		rbInfo.restitution = 1.0f; // Sto�zahl, Restitutionskoeffizient:  0: vollkommen plastischer Sto� (Bewegungsenergie nimmt ab, z.B. wegen W�rme), 1: vollkommen elastischer Sto� (Bewegungsenergie bleibt konstant)
		rbInfo.friction = 1f; // Reibung
		bodyConnect = new RigidBody(rbInfo);
  		dynamicsWorld.addRigidBody(bodyConnect);
//-----------------------
//		Point2PointConstraint con = new Point2PointConstraint(bodyFirst, bodyConnect,
//  				new Vector3f(0.5f, 0.5f, 0.5f), new Vector3f(-2.0f, 0.0f, 0.0f));
//		Point2PointConstraint con2 = new Point2PointConstraint(bodyConnect, bodySecond,
//  				new Vector3f(2.0f, 0.0f, 0.0f), new Vector3f(-0.5f, 0.5f, 0.5f));
//  		HingeConstraint con = new HingeConstraint(bodyFirst, bodySecond, // Achse
//  				new Vector3f(1.0f, 0.0f, 0.0f), new Vector3f(-1.0f, 0.0f, 0.0f), // Drehpunkte
//  				new Vector3f(0.0f, 1.0f, 0.0f), new Vector3f(0.0f, 1.0f, 0.0f)); // Richtung der Drehachse
//  		con.setAngularOnly(false); // ? (false ist Standard)
//  		con.setLimit(low, high, _softness, _biasFactor, _relaxationFactor); // ??
  		Transform a = new Transform();
  		a.origin.set(0.5f, 0.0f, 0.0f);
  		a.basis.setIdentity();
  		Transform b = new Transform();
  		b.origin.set(-0.5f, 0.0f, 0.0f);
  		b.basis.setIdentity();
  		SliderConstraint con = new SliderConstraint(bodyFirst, bodySecond, a, b, true);
  		dynamicsWorld.addConstraint(con, true);
//  		dynamicsWorld.addConstraint(con2, true);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		this.getKonfiguration().getCamera().setPosition(new GLVertex(17, 0, 17));
		this.getKonfiguration().getCamera().setDirection(new GLVertex(-15, -3, -15));
		box = new GLCuboid(new GLVertex(0, 0, 0), 1f);
		Utility.setRandomColor(box);
		boxFirst = new GLCuboid(new GLVertex(0, 0, 0), 1f);
		Utility.setRandomColor(boxFirst);
		boxSecond = new GLCuboid(new GLVertex(0, 0, 0), 1f);
		Utility.setRandomColor(boxSecond);
		boxConnect = new GLCuboid(new GLVertex(0, 0, 0), 4f, 0.2f, 0.2f);
		Utility.setRandomColor(boxConnect);
		ground = new GLCuboid(new GLVertex(0, -10f, 0), 20f, 11f, 20f);
		Utility.setRandomColor(ground);
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		this.simulate();

		Transform trans = new Transform();
		body.getMotionState().getWorldTransform(trans);
		
		Matrix4f matrix = new Matrix4f();
		trans.getMatrix(matrix);

		GLMatrix mat = this.convertMatrix(trans.basis);
		box.glTransformNow(mat);
		box.glTranslateNow(trans.origin.x, trans.origin.y, trans.origin.z);
		box.draw(gl);
		
		bodyFirst.getMotionState().getWorldTransform(trans);
		trans.getMatrix(matrix);
		mat = this.convertMatrix(trans.basis);
		boxFirst.glTransformNow(mat);
		boxFirst.glTranslateNow(trans.origin.x, trans.origin.y, trans.origin.z);
		boxFirst.draw(gl);

		bodySecond.getMotionState().getWorldTransform(trans);
		trans.getMatrix(matrix);
		mat = this.convertMatrix(trans.basis);
		boxSecond.glTransformNow(mat);
		boxSecond.glTranslateNow(trans.origin.x, trans.origin.y, trans.origin.z);
		boxSecond.draw(gl);

		bodyConnect.getMotionState().getWorldTransform(trans);
		trans.getMatrix(matrix);
		mat = this.convertMatrix(trans.basis);
		boxConnect.glTransformNow(mat);
		boxConnect.glTranslateNow(trans.origin.x, trans.origin.y, trans.origin.z);
		boxConnect.draw(gl);

		ground.draw(gl);
	}
	
	private void simulate() {
		float ms = this.getDeltaTimeMicroseconds();
		ms = ms / 1000000f;

		if (dynamicsWorld != null) {
			dynamicsWorld.stepSimulation(ms); // Angabe in Sekunden
			dynamicsWorld.debugDrawWorld();
		}
	}

	public float getDeltaTimeMicroseconds() {
		float dt = clock.getTimeMicroseconds();
		clock.reset();
		return dt;
	}

	private GLMatrix convertMatrix(Matrix3f mat) {
		GLMatrix result = new GLMatrix();
		result.set(mat.m00, mat.m10, mat.m20, 0f, mat.m01, mat.m11, mat.m21, 0f, mat.m02, mat.m12, mat.m22, 0f, 0f, 0f, 0f, 1f);
		return result;
	}	
}
