package experiments;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLCuboid;
import falk_guenther_meier.model.objects.GLLine;
import falk_guenther_meier.model.objects.GLPoint;
import falk_guenther_meier.model.world.JoglApp;

public class LightMaterialTest extends JoglApp {
	// GL2-Kontext
	private GL2 gl;
	GLCuboid c;
	boolean t = true;

	public LightMaterialTest(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe);
	}
	
	public static void main(String[] args) {
		new LightMaterialTest("DrawTests", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		c = new GLCuboid(new GLVertex(0.5f,0.5f,0.5f), 0.5f);
		c.rotate(45, 0, 0);
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		super.display(drawable);
		gl = drawable.getGL().getGL2();
		// Tiefenpuffer leeren
		gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);

		if (t) c.translate(0.5f, 0.5f, 0.5f);
		else c.translate(-0.5f, -0.5f, -0.5f);
		t=!t;
		c.draw(gl);
		
		
		GLLine line = new GLLine(new GLVertex(0,0,0), new GLVertex(1f,1f,0f));
		line.draw(gl);
		
		GLPoint point = new GLPoint(new GLVertex(1.1f,1f,0f));
		point.setSize(4f);
		point.draw(gl);
	}	

}
