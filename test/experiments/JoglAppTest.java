package experiments;

import java.awt.Color;

import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLRectangle;
import falk_guenther_meier.model.world.JoglApp;
import falk_guenther_meier.util.SkyBoxLoader;

public class JoglAppTest extends JoglApp {
	
	GLRectangle rect;
	
	public JoglAppTest(String name_value, int width, int height) {
		super(name_value, width, height, false);
//		this.getKonfiguration().setShowSkybox(false);
		this.getKonfiguration().setSkybox(SkyBoxLoader.getSundownSkyBox(getKonfiguration().getCamera(), 100f));
		start();
	}
	
	public static void main(String[] args) {
		new JoglAppTest("sdaf", 800, 600);
	}

	@Override
	public synchronized void init(GLAutoDrawable drawable) {
		super.init(drawable);
		rect = new GLRectangle(new GLVertex(0, 0, 0), 50, 50);
		rect.setColor(new GLVertex(Color.CYAN));
		rect.rotate(90, 0, 0);
		rect.translate(0, -10, 0);
	}

	@Override
	public synchronized void display(GLAutoDrawable drawable) {
		super.display(drawable);
		rect.draw(drawable.getGL().getGL2());
	}

}
