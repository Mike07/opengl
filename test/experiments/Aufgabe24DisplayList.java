package experiments;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;

import falk_guenther_meier.model.light.GLPointLight;
import falk_guenther_meier.model.math.GLVertex;
import falk_guenther_meier.model.objects.GLHeightMapRectangle;
import falk_guenther_meier.model.world.JoglApp;

/**
 * Display-Listen austesten!
 * @author Johannes
 */
public class Aufgabe24DisplayList extends JoglApp {
	private GLHeightMapRectangle map;
	private GLPointLight light1;
	private int listIndex;
	
	public Aufgabe24DisplayList(String Name_value, int breite, int hoehe) {
		super(Name_value, breite, hoehe, false);

		map = new GLHeightMapRectangle("heightmap/height_little.png", 1.0f, 1.0f, 0.5f, "texture/grass-texture.png", 1);
		map.scale(4.0f, 2.0f, 4.0f);

		start();
	}

	public static void main(String[] args) {
		new Aufgabe24DisplayList("Aufgabe 24 mit Display-Test", 800, 600);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);
		// ----- Punktlicht -----
		// Position, Punktlicht, da w=1
		GLVertex position = new GLVertex(0.0f, 2.0f, 0.0f, 1.0f);
		GLVertex ambient = new GLVertex(0.6f, 0.6f, 0.8f, 1.0f);
		GLVertex diffuse = new GLVertex(0.6f, 0.6f, 1f, 1.0f);
		GLVertex specular = new GLVertex(0.8f, 0.8f, 0.8f, 1.0f);
		light1 = new GLPointLight(GL2.GL_LIGHT1, null, position, ambient, diffuse, specular);
		// Wichtig, da sonst die Landschaft starke Blaut�ne enth�lt !!
		this.getKonfiguration().getLight().setUseColorMaterial(false);
		drawable.getGL().getGL2().glEnable(GL2.GL_CULL_FACE);

		// DisplayList:
		GL2 gl = drawable.getGL().getGL2();
		listIndex = gl.glGenLists(1);
		if (listIndex != 0) {
			gl.glNewList(listIndex, GL2.GL_COMPILE);
			map.draw(gl);
			gl.glEndList();
		} else {
			throw new IllegalStateException();
		}
	}
	
	@Override
	public void beginGL(GL2 gl) {
		super.beginGL(gl);
		
		light1.beginGL(gl);

//		map.draw(gl);
		gl.glCallList(listIndex);
	}
	
	@Override
	public void endGL(GL2 gl) {
		light1.endGL(gl);
		super.endGL(gl);
	}
}
